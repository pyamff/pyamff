#!/usr/bin/env python3

from pyamff.config import ConfigClass
from ase.io import Trajectory
from pyamff.utilities.preprocessor import normalize, fetchProp, Scaler
from pyamff.neighborlist import NeighborLists
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.mlModels.lossFunctions import SE, RMLSE, RMSE_noForce
from pyamff.fingerprints.behlerParrinello import represent_BP
from pyamff.utilities.dataPartition import DataPartitioner, partitionData
from pyamff.utilities.dataPartition import batchGenerator, batchGenerator_ele
from pyamff.utilities.logTool import setLogger, writeSysInfo
from pyamff.utilities.preprocessor import normalizeParas
from torch.multiprocessing import Process
from pyamff.training import Trainer
from pyamff.utilities import fileIO as io
from pyamff.fingerprints.fingerprints import Fingerprints
import math

from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
#from pyamff.cross_validation_helper import k_fold_split,test_split, fetchProp_validation,test_model #test_model, k_fold_split
from pyamff.cross_validation_helper import test_model,train_test_split_new
from collections import OrderedDict
import torch.distributed as dist
import os, sys, time, glob
import torch
import torch.multiprocessing as mp
from numpy.random import randint
import numpy as np
import pickle
import random

os.system('rm batches/ fingerprints/ *.log -rf')

logger = setLogger()
charge_logger = setLogger(name='charge', logfile='charge.log')
charge_logger2 = setLogger(name='percent', logfile='percent_charge.log')

def init_processes(rank, size, thread, fn, 
                    partition, partition_Ele, testpartition, testpartition_Ele,
                    nBatchPerProc, maxEpoch, device, reportTestRMSE, 
                    masterAddr, masterPort, backend):

    # Initialize the distributed environment
    #print ('fn: ',fn) #bound method Trainer.parallelFit
    #os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_ADDR'] = masterAddr
    os.environ['GLOO_SOCKET_IFNAME'] = 'lo'
    #os.environ['MASTER_ADDR'] = 'env://'
    #os.environ['MASTER_PORT'] = str(randint(low=10000, high=99999))
    #os.environ['MASTER_PORT'] = '12355'
    os.environ['MASTER_PORT'] = masterPort
    #print('lauching', rank)
    dist.init_process_group(backend, rank=rank, world_size=size)
#    dist.init_process_group('nccl', rank=rank, world_size=size)
    #fn(rank, size)
    fn(rank, size, thread, 
        partition, partition_Ele, 
        testpartition, testpartition_Ele, 
        nBatchPerProc, maxEpoch, 
        device, reportTestRMSE=reportTestRMSE, 
        logger=logger, charge_logger=charge_logger,charge_logger2=charge_logger2)

def init_fpProcesses(rank, size, fn, nFPs, nBatchPerProc, batchIDs, trainingImages, properties, normalize, logger,
                     fpDir, useExisting, test, screen_rcut, screen_k, compute_forces, masterAddr, masterPort, backend):

    #Initialize the distributed environment.
    #print ("fn : ",fn) #bound method loop images
    #os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_ADDR'] = masterAddr
    os.environ['GLOO_SOCKET_IFNAME'] = 'lo'
    #os.environ['MASTER_ADDR'] = 'env://'
    #os.environ['MASTER_PORT'] = '12355'
    os.environ['MASTER_PORT'] = masterPort
    #dist.init_process_group(backend, rank=rank, world_size=size)
    dist.init_process_group('gloo', rank=rank, world_size=size)
    fn(rank, size, nFPs, nBatchPerProc, batchIDs, trainingImages, properties, 
        normalize, logger, fpDir, useExisting, test, screen_rcut, screen_k, compute_forces)

def main():
    
    # Read in parameters

    st = time.time()
    writeSysInfo(logger)
    cwd = os.getcwd()

    print('%8.2fs: Reading inputs' % (time.time()-st))
    config = ConfigClass()
    config.initialize()

    if config.config["use_deterministic"]:
        torch.manual_seed(0)
        random.seed(a=0)
        np.random.seed(seed=0)
        torch.use_deterministic_algorithms(mode=True, warn_only=True)
    
    fp_paras = config.config['fp_paras'].fp_paras
    nFPs = {}
    for key in fp_paras.keys():
        nFPs[key] = len(fp_paras[key])

    useCuda = False

    if config.config['device_type'] == 'GPU' and torch.cuda.is_available():
        useCuda = True

    device = torch.device("cuda:1" if useCuda else "cpu")

    print('PLEASE call this function under dlossdr folder!!!')
    from pyamff import fmodules
    def one_step(params, images):

        print('%8.2fs: Checking for scaler:'%(time.time()-st),config.config['scaler_type'])
        scaler = Scaler(scalerType=config.config['scaler_type'], forceTraining=config.config['force_training'],  activeLearning=False, loss_type=None, cohe=config.config['use_cohesive_energy'], device=device)
        scaler = scaler.set_scaler()
        test_scaler = Scaler(scalerType=config.config['scaler_type'], forceTraining=config.config['force_training'],  activeLearning=False, loss_type=None, cohe=config.config['use_cohesive_energy'], device=device)
        test_scaler = test_scaler.set_scaler()
        if config.config['scaler_type'] in ['NoScaler', 'LinearScaler', 'MinMaxScaler','STDScaler']:
            scaler.adjust = config.config['adjust']
            test_scaler.adjust = config.config['adjust']
        if scaler is None:
            print('Normalization function is not defined!', sys.stderr)
            sys.exit()

        trainingImages, properties, scaler = fetchProp(images, 
                                                        refEs=None, 
                                                        scaler=scaler,
                                                        forceTraining=config.config['force_training'])

        # Get fingerprints: we need to be careful of trainFF, testFF and train_testFF tags here
        srcData = list(trainingImages.keys()) # training Images

        useExisting = config.config['fp_use_existing'] # for training
        if not useExisting:
            fpDir = None
            print('%8.2fs: Calculating training fingerprints' % (time.time()-st))

        # now same check for testing images
        if config.config['fp_engine'] == 'Fortran':

            fpcalc = Fingerprints(uniq_elements=config.config['fp_paras'].uniq_elements, filename=config.config['fp_parameter_file'], nfps=nFPs)
            if fpDir is None:
                fpsDir = os.getcwd() + '/fingerprints'
            else:
                fpsDir = fpDir
            if not os.path.exists(fpsDir):
                os.mkdir(fpsDir)
            nProc = config.config['process_num']
            processes = []
            test = False # I am only adding for regular pyamff and not cross validation because I dont know that code yet
            batches = partitionData(srcData, nProc)
            fpRange={}
            print('Fingerprints class with read_fpparas finished')
            for rank in range(nProc):
                    p = Process(target=init_fpProcesses,
                                args=(rank, nProc, fpcalc.loop_images, nFPs, config.config['fp_batch_num'], 
                                    batches[rank], trainingImages, properties,
                                    True, logger, 
                                    fpsDir, useExisting, test,
                                    config.config['screen_rcut'],
                                    config.config['screen_k'],
                                    config.config['force_training'],
                                    config.config['master_addr'],
                                    config.config['master_port'],
                                    config.config['backend']
                                    ))
                    p.start()
                    processes.append(p)
            for p in processes:
                p.join()
            if test == False: # for trainFF and train_testFF; for testFF, we will do this after model is loaded
                fname = os.path.join(fpsDir, 'fprange.pckl')
                with open(fname, 'rb') as f:
                    fpRange = pickle.load(f)
                fpRange, magnitudeScale, interceptScale = normalizeParas(fpRange)
            # and now we calculate fingerprints for train_test_mode: testing images figerprints
            # right now, I am not redefing the fingerprints class because I am assuming ordering of elements is same as train file
            print('fpcalc.loop_images finished')

        nProc = config.config['process_num']
        nBatchPerProc = config.config['batch_num_per_proc']
        nBatches = nProc*nBatchPerProc

        processes = []
        print('%8.2fs: Partitioning data' % (time.time()-st))

        for p in params:
            p = p.requires_grad_(True)
        
        partitions = DataPartitioner(srcData=srcData,
                                fpRange=fpRange,
                                magnitudeScale=magnitudeScale,
                                interceptScale=interceptScale,
                                nProc=nProc,
                                fpDir=fpDir,
                                nBatches=nBatches,
                                device=device,
                                seed=1234,
                                st=st)

        partition = partitions.use(rank)
        partition_Ele = partitions.use_Ele(rank)
        batch_size = math.ceil(float(len(partition))/float(nBatches))
        kwargs = {'num_workers': 0, 'pin_memory': True}

        # Define the NN model
        model = NeuralNetwork(
                    hiddenlayers=config.config['hidden_layers'],
                    activation=config.config['activation_function'],
                    nFPs=nFPs,
                    forceTraining=config.config['force_training'],
                    cohE = config.config['use_cohesive_energy'],
                    # TODO: load pretrained params
                    params=params,
                    scaler=scaler,
                    debug=config.config['nn_values'],
                    initial_weights =config.config['initial_weights'],
                    ifElectronegativity=config.config['ifelectronegativity'],
                    if_short=config.config['if_short'],
                    if_long=config.config['if_long'],
                    process_number=nProc,
                    partitions=partitions
                    #slope=slope,
                    #energyRange = energyRange,
                    #forceRange = forceRange
                    )

            # Define loss function
        criterion = SE(cohe=config.config['use_cohesive_energy'],
                        energyCoefficient=1.0,  
                        forceCoefficient=1.0, 
                        device=device)

        criterion_e = SE(cohe=config.config['use_cohesive_energy'],
                        energyCoefficient=1.0,  
                        forceCoefficient=0.0 , 
                        device=device)

        criterion_f = SE(cohe=config.config['use_cohesive_energy'],
                        energyCoefficient=0.0,  
                        forceCoefficient=1.0, 
                        device=device)


        batches = torch.utils.data.DataLoader(partition,
                                    batch_size=batch_size,
                                    collate_fn=batchGenerator,
                                    shuffle=False,
                                    **kwargs)
        
        batches_Ele = torch.utils.data.DataLoader(partition_Ele,
                                            batch_size=batch_size,
                                            collate_fn=batchGenerator_ele,
                                            shuffle=False)
    
        for i, data in enumerate(zip(batches, batches_Ele)):
            # Alan: call remaked forward() function
            batch = data[0]
            batch_ele = data[1]

            predEnergies, predForces,\
                charges_dict, charge_percent = model(
                                        batch.allElement_fps, batch.dgdx, # NOTE: here the input fps is turbulated
                                        batch, batch_ele,
                                        device, logger=logger)
            
            # initialize two dicts for logging

            loss = criterion(predEnergies, predForces, batch.energies, batch.forces,
                                          natomsEnergy = batch.natomsPerImageEnergy,
                                          natomsForce = batch.natomsPerImageForce)
            loss_e = criterion_e(predEnergies, predForces, batch.energies, batch.forces,
                                          natomsEnergy = batch.natomsPerImageEnergy,
                                          natomsForce = batch.natomsPerImageForce)
            loss_f = criterion_f(predEnergies, predForces, batch.energies, batch.forces,
                                          natomsEnergy = batch.natomsPerImageEnergy,
                                          natomsForce = batch.natomsPerImageForce)
            
            # lossgrads, = torch.autograd.grad(loss, allElement_fps[elem],
            #                                 retain_graph=True, create_graph=False)
            # lossgrads_e, = torch.autograd.grad(loss_e, allElement_fps[elem],
            #                                 retain_graph=True, create_graph=False)
            # lossgrads_f, = torch.autograd.grad(loss_f, allElement_fps[elem],
            #                                 retain_graph=True, create_graph=False)
            # lossgrads_q, = torch.autograd.grad(torch.sum(charges_dict[elem]), allElement_fps[elem],
            #                                 retain_graph=True, create_graph=False)

        
        model.share_memory()

        return loss, loss_e, loss_f, charges_dict, predForces, predEnergies, batch.allElement_fps, trainingImages
    
    # ------------------------------------------------------------------------
    # NOTE: for calculate d(loss)/d(weight) by 2*2 NN
    # set ifelectronegativity = False, init_model_parameters = True
    # SEE /mnt/d/Alan/PhD/pyamff_training/train_TiO2_NN_L2/dlossdg FOLDER
    def initialize_param():
        params = list()

        model_path = '../TiO2/pyamff.pt'
        # model_path = '../Ge/pyamff_100.pt'

        loaded = torch.load(model_path)
        
        for i in loaded['state_dict']:
            params.append(loaded['state_dict'][i])

        return params, list(loaded['state_dict'].keys())
    # ------------------------------------------------------------------------

    
    from ase.io import read
    from Alan_package.Alan_vasp.bader_charge.outcar2traj import force_setter

    tot_num_l = []
    tot_ana_l = []
    tot_num_e = []
    tot_ana_e = []
    tot_num_f = []
    tot_ana_f = []
    tot_num_q = []
    tot_ana_q = []

    print('Reading training images from %s' % cwd+'/'+config.config['trajectory_file'])

    step_size = 1e-4
    images = Trajectory(config.config['trajectory_file'], 'r')
    # tot_len = len(images[0]) # how many atoms number
    tot_len = len(images[0]) # how many atoms number
    tot_xyz = 3 # xyz
    first_time = True

    tot_num_e = torch.zeros((tot_len, tot_xyz))
    tot_ana_e = torch.zeros((tot_len, tot_xyz))
    
    for which_atom in range(tot_len): 
        for which_xyz in range(tot_xyz):
            numerical_loss = list()
            numerical_loss_e = list()
            numerical_loss_f = list()
            numerical_loss_q = list()
            for iter in [1, -1, 0]: # left turbulence, right turbulence, original to get analytical
                if first_time:
                    pass
                    first_time = False
                else:
                    fmodules.fpcalc.cleanup()

                images = read(config.config['trajectory_file'], index=":")
                ml_e_val = images[0].get_potential_energy()
                ml_f_val_arr = images[0].get_forces()
                images[0].positions[which_atom, which_xyz] += iter*step_size

                calc = force_setter(energy=ml_e_val, forces=ml_f_val_arr)
                images[0].pbc = True
                images[0].set_calculator(calc)

                params, _ = initialize_param()
                loss, loss_e, loss_f, charge_dict, predForces, predEnergies, allElement_fps, images = one_step(params, images)
                
                numerical_loss.append(allElement_fps) # for debugging
                numerical_loss_q.append(images[0].positions[which_atom, which_xyz]) # for debugging
                numerical_loss_f.append(loss_f.item()) # for debugging

                numerical_loss_e.append(predEnergies.item())
                os.system('rm batches/ fingerprints/ *.log -rf')

            # num_l = (numerical_loss[0] - numerical_loss[1]) / (2*step_size)
            # num_q = (numerical_loss_q[0] - numerical_loss_q[1]) / (2*step_size)
            num_e = (numerical_loss_e[0] - numerical_loss_e[1]) / (2*step_size)
            # num_f = (numerical_loss_f[0] - numerical_loss_f[1]) / (2*step_size)   
        
            # tot_num_l.append(num_l)
            # tot_ana_l.append(predForces[which_atom, which_xyz].item())

            tot_num_e[which_atom, which_xyz] = num_e                                                        # dE/dr
            tot_ana_e[which_atom, which_xyz] = predForces[which_atom, which_xyz].item()                     # partial E/ partial r

            # tot_num_f.append(num_f)
            # tot_ana_f.append(predForces[which_atom, which_xyz].item())

            # tot_num_q.append(num_q)
            # tot_ana_q.append(predForces[which_atom, which_xyz].item())


    disp_dict = {'0': 'X', '1': 'Y', '2': 'Z'}
    # NOTE: unwrap the array 
    print('[dE/dr]:')
    print('                      numerical       analytical      difference')
    for which_atom in range(tot_len): 
        for which_xyz in range(tot_xyz):
            print(' atom_{} direction_{} : {:.8f}      {:.8f}      {:.8f}'.format( which_atom, disp_dict[str(which_xyz)], \
                                                        tot_num_e[which_atom, which_xyz], tot_ana_e[which_atom, which_xyz],  \
                                                    np.divide(tot_ana_e[which_atom, which_xyz], tot_num_e[which_atom, which_xyz])*100 ))
    print()

    # # Define the pyamff training 
    # logger.info('Setup PyAMFF trainer:')
    # #print('%8.2fs: Setup PyAMFF trainer:'%(time.time()-st))
    # # TODO: Alan: haven't include the config.config['force_training'] in this function!!
    # calc = Trainer(model=model,
    #                 criterion=criterion,
    #                 optimizer=config.config['optimizer_type'],
    #                 # TODO: check it can be reloaded
    #                 fpParas=config.config['fp_paras'],
    #                 energyCoefficient=config.config['energy_coefficient'],
    #                 forceCoefficient=config.config['force_coefficient'],
    #                 lossConvergence=losstol,
    #                 energyRMSEtol=config.config['energy_tol'],
    #                 forceRMSEtol=config.config['force_tol'],
    #                 lossgradtol=config.config['loss_grad_tol'],
    #                 #TODO
    #                 learningRate=config.config['learning_rate'],
    #                 model_logfile='pyamff.pt',
    #                 logmodel_interval=100,
    #                 test_loginterval=config.config['test_log_interval'],
    #                 test_scaler=test_scaler,
    #                 debug=None,
    #                 weight_decay=config.config['weight_decay'],
    #                 fpRange=fpRange,
    #                 nImages=nImages,
    #                 tnImages=tnImages,
    #                 write_final_grads =config.config['write_final_grads'])

    # # Train the NN
    # logger.info('=======================================================')
    # logger.info('Starting training')
    # head = "{:>12s} {:>14s} {:>12s} {:>12s} {:>12s} {:>12s} {:>12s} {:>12s}".format('Epoch', 'LossValue', 'EnergyLoss', 'ForceLoss','EnergyRMSE', 'ForceRMSE', 'TestEnergyRMSE', 'TestForceRMSE')
    # logger.info('%s', head)

    # #for node in range(2):
    # #  print(mastaddr[node], mastips[node])
    # #  for rank in range(int(nProc/2)):

    # print("%8.2fs: Training started" % (time.time()-st))
    # print(' '*12,'Epoch    LossValue   EnergyRMSE    ForceRMSE    TestEnergyRMSE    TestForceRMSE ')
    # train_st = time.time()
    # mp.set_start_method(config.config['mp_start_method'], force=True)
    # for rank in range(nProc):
    # #      local_rank = rank
    # #      rank = ranks_per_node * node + local_rank
    #         #print('rank', rank)

    # #      mp.spawn(calc.parallelFit, nprocs=process_number, args=(process_number, config.config['thread_num'],
    # #                                                                 partitions.use(rank),
    # #                                                                 batch_number, config.config['epochs_max'],
    # #                                                                 device, logger),join=True)
    #         p = Process(target=init_processes,
    #                     args=(rank, nProc, config.config['thread_num'],
    #                         #config.config['master_addr'], 
    #                         #mastips[node],
    #                         #config.config['master_port'],
    #                         calc.parallelFit, 
    #                         partitions.use(rank), 
    #                         partitions.use_Ele(rank),
    #                         testpartitions.use(rank),
    #                         testpartitions.use_Ele(rank),
    #                         #config.config['batch_number'],
    #                         nBatches,
    #                         config.config['epochs_max'],
    #                         device,
    #                         reportTestRMSE,
    #                         config.config['master_addr'],
    #                         config.config['master_port'],
    #                         config.config['backend']
    #                         #'nccl'
    #                         ))
    #         p.start()
    #         processes.append(p)
    # for p in processes:
    #     p.join()
    # et = time.time()
    # print("%8.2fs: Training done, time used: %.2fs" % (et-st,et-train_st))

if __name__ == "__main__":
    main()
