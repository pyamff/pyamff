"""
Example script using the tensorflow version of the NN model
"""

from amp import Amp
from amp.descriptor.gaussian import Gaussian, make_symmetry_functions
from amp.descriptor.cutoffs import Cosine,Polynomial
from amp.model.tflow import NeuralNetwork
from amp.convert import tflow_save_to_openkim

Gs = {"H": [{"type":"G2", "element":"H", "eta":0.05, "Rs":0.},
            {"type":"G2", "element":"Pd", "eta":160., "Rs":0.},
           ],
      "Pd": [{"type":"G2", "element":"H", "eta":0.05, "Rs":0.},
             {"type":"G2", "element":"Pd", "eta":400., "Rs":6.0},
             {"type":"G4", "elements":["Pd", "Pd"],"eta":0.01, "gamma":1.0, "zeta":241.0, "theta_s":0.07},
             {"type":"G4", "elements":["Pd", "Pd"],"eta":0.01, "gamma":1.0, "zeta":365.0, "theta_s":0.23},
             ]}

calc = Amp(descriptor=Gaussian(Gs=Gs,
                               cutoff=Cosine(6.0)
                               ),
           cores=1,
           model=NeuralNetwork(hiddenlayers=(3,2), activation='sigmoid',
                               maxTrainingEpochs=1,
                               energy_coefficient=1.0,
                               force_coefficient=0.1,
                               optimizationMethod='l-BFGS-b',
                               convergenceCriteria={'energy_rmse': 0.05,
                                                    'force_rmse': 0.05}))
try:
  calc.train(images='train.traj',overwrite=True)
except:
  tflow_save_to_openkim(calc)
