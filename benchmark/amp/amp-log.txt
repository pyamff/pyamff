
   oo      o       o   oooooo
  o  o     oo     oo   o     o
 o    o    o o   o o   o     o
o      o   o  o o  o   o     o
oooooooo   o   o   o   oooooo
o      o   o       o   o
o      o   o       o   o
o      o   o       o   o

Amp: Atomistic Machine-learning Package
Developed by Andrew Peterson, Alireza Khorshidi, and others,
Brown University.
PI Website: http://brown.edu/go/catalyst
Official repository: http://bitbucket.org/andrewpeterson/amp
Official documentation: http://amp.readthedocs.io/
Citation:
  Alireza Khorshidi & Andrew A. Peterson,
  Computer Physics Communications 207: 310-324 (2016).
  http://doi.org/10.1016/j.cpc.2016.05.010
======================================================================
User: mse-lil
Hostname: login03
Date: 2021-05-27T15:09:53 (2021-05-27T07:09:53 UTC)
Architecture: x86_64
PID: 132124
Amp version: 0.7.0-beta
Amp directory: /work/mse-lil/code/amp/amp
 Last commit: unknown hash
 Last commit date: unknown date
Python: v3.7.9: /work/mse-lil/.local/miniconda3/envs/python3.7/bin/python
ASE v3.20.1: /work/mse-lil/.local/miniconda3/envs/python3.7/lib/python3.7/site-packages/ase
NumPy v1.18.0: /work/mse-lil/.local/miniconda3/envs/python3.7/lib/python3.7/site-packages/numpy
SciPy v1.5.2: /work/mse-lil/.local/miniconda3/envs/python3.7/lib/python3.7/site-packages/scipy
ZMQ/PyZMQ v4.3.2/v19.0.2: /work/mse-lil/.local/miniconda3/envs/python3.7/lib/python3.7/site-packages/zmq
pxssh: Not available from pxssh.
pxssh (via pexpect v4.8.0): /work/mse-lil/.local/miniconda3/envs/python3.7/lib/python3.7/site-packages/pexpect/pxssh.py
======================================================================
No queuing system detected; single machine assumed.
Parallel configuration determined from environment for <single machine>:
  localhost: 72
Loaded file: amp.amp
Calculation requested.
Calculating potential energy...
Cutoff function: <Cosine cutoff with Rc=6.000 from amp.descriptor.cutoffs>
2 unique elements included: H, Pd
Number of symmetry functions for each element:
  H: 2
 Pd: 4
H feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 160.0, Rs = 0.0
Pd feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 400.0, Rs = 6.0
 2: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=241.0, theta_s=0.07
 3: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=365.0, theta_s=0.23
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 0 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 0 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprints calculated. 0.0 min.
...potential energy calculated. 0.0 min.
Calculation requested.
Calculating forces...
Cutoff function: <Cosine cutoff with Rc=6.000 from amp.descriptor.cutoffs>
2 unique elements included: H, Pd
Number of symmetry functions for each element:
  H: 2
 Pd: 4
H feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 160.0, Rs = 0.0
Pd feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 400.0, Rs = 6.0
 2: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=241.0, theta_s=0.07
 3: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=365.0, theta_s=0.23
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 1 total images, 1 of which are needed.
 0 new calculations needed.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 1 total images, 1 of which are needed.
 0 new calculations needed.
...fingerprints calculated. 0.0 min.
Calculating fingerprint derivatives...
 Data stored in file amp-fingerprint-primes.
 File exists with 0 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprint derivatives calculated. 0.0 min.
...forces calculated. 0.0 min.
Calculation requested.
Calculating potential energy...
Cutoff function: <Cosine cutoff with Rc=6.000 from amp.descriptor.cutoffs>
2 unique elements included: H, Pd
Number of symmetry functions for each element:
  H: 2
 Pd: 4
H feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 160.0, Rs = 0.0
Pd feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 400.0, Rs = 6.0
 2: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=241.0, theta_s=0.07
 3: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=365.0, theta_s=0.23
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 1 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 1 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprints calculated. 0.0 min.
...potential energy calculated. 0.0 min.
Calculation requested.
Calculating forces...
Cutoff function: <Cosine cutoff with Rc=6.000 from amp.descriptor.cutoffs>
2 unique elements included: H, Pd
Number of symmetry functions for each element:
  H: 2
 Pd: 4
H feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 160.0, Rs = 0.0
Pd feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 400.0, Rs = 6.0
 2: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=241.0, theta_s=0.07
 3: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=365.0, theta_s=0.23
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 2 total images, 1 of which are needed.
 0 new calculations needed.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 2 total images, 1 of which are needed.
 0 new calculations needed.
...fingerprints calculated. 0.0 min.
Calculating fingerprint derivatives...
 Data stored in file amp-fingerprint-primes.
 File exists with 1 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprint derivatives calculated. 0.0 min.
...forces calculated. 0.0 min.
Calculation requested.
Calculating potential energy...
Cutoff function: <Cosine cutoff with Rc=6.000 from amp.descriptor.cutoffs>
2 unique elements included: H, Pd
Number of symmetry functions for each element:
  H: 2
 Pd: 4
H feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 160.0, Rs = 0.0
Pd feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 400.0, Rs = 6.0
 2: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=241.0, theta_s=0.07
 3: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=365.0, theta_s=0.23
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 2 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 2 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprints calculated. 0.0 min.
...potential energy calculated. 0.0 min.
Calculation requested.
Calculating forces...
Cutoff function: <Cosine cutoff with Rc=6.000 from amp.descriptor.cutoffs>
2 unique elements included: H, Pd
Number of symmetry functions for each element:
  H: 2
 Pd: 4
H feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 160.0, Rs = 0.0
Pd feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 400.0, Rs = 6.0
 2: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=241.0, theta_s=0.07
 3: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=365.0, theta_s=0.23
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 3 total images, 1 of which are needed.
 0 new calculations needed.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 3 total images, 1 of which are needed.
 0 new calculations needed.
...fingerprints calculated. 0.0 min.
Calculating fingerprint derivatives...
 Data stored in file amp-fingerprint-primes.
 File exists with 2 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprint derivatives calculated. 0.0 min.
...forces calculated. 0.0 min.
Calculation requested.
Calculating potential energy...
Cutoff function: <Cosine cutoff with Rc=6.000 from amp.descriptor.cutoffs>
2 unique elements included: H, Pd
Number of symmetry functions for each element:
  H: 2
 Pd: 4
H feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 160.0, Rs = 0.0
Pd feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 400.0, Rs = 6.0
 2: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=241.0, theta_s=0.07
 3: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=365.0, theta_s=0.23
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 3 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 3 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprints calculated. 0.0 min.
...potential energy calculated. 0.0 min.
Calculation requested.
Calculating forces...
Cutoff function: <Cosine cutoff with Rc=6.000 from amp.descriptor.cutoffs>
2 unique elements included: H, Pd
Number of symmetry functions for each element:
  H: 2
 Pd: 4
H feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 160.0, Rs = 0.0
Pd feature vector functions:
 0: G2, H, eta = 0.05, Rs = 0.0
 1: G2, Pd, eta = 400.0, Rs = 6.0
 2: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=241.0, theta_s=0.07
 3: G4, (Pd, Pd), eta=0.01, gamma=1.0, zeta=365.0, theta_s=0.23
Calculating neighborlists...
 Data stored in file amp-neighborlists.
 File exists with 4 total images, 1 of which are needed.
 0 new calculations needed.
...neighborlists calculated. 0.0 min.
Fingerprinting images...
 Data stored in file amp-fingerprints.
 File exists with 4 total images, 1 of which are needed.
 0 new calculations needed.
...fingerprints calculated. 0.0 min.
Calculating fingerprint derivatives...
 Data stored in file amp-fingerprint-primes.
 File exists with 3 total images, 0 of which are needed.
 1 new calculations needed.
 Calculated 1 new images.
...fingerprint derivatives calculated. 0.0 min.
...forces calculated. 0.0 min.
