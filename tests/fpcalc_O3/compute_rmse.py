from pyamff.ase_calc import aseCalc
from pyamff.ase_calc_fortran import aseCalcF
from ase.io import Trajectory

### function to get an individual image's atomic U,F MSEs
def image_mse(trueU, pred_U ,trueF,pred_F,num_atoms): #for the one image:
    # atomic Error = (True-ML)/(num atoms)
    # Squared Error = {(True-ML)/(num atoms)}^2
    # Mean of Squared Error of 1 Image = Squared Error; mean of 1 number = that number
    U_mse_img =((pred_U-trueU)/num_atoms)**2 #
    # Force is N*3 matrix, where N = num atoms
    # Atomic Force has 3 numbers (x,y,z) components of force
    # =>
    F_sse_img = 0.0
    for vector in range(len(trueF)): #For each row of forces (i.e forces of 1 atom)
            F_err_compwise = 0 #we need to find the sum of squared errors in each component (we sum so that we can find the mean)
            for component in range(3): #x,y,z ;for each component we subtract true atoms's comp. - ml pred atom's comp. and square it
                F_err_compwise +=(trueF[vector][component]-pred_F[vector][component])**2   # per vector per component force error squared
            atomic_image_F_err = F_err_compwise #atomwise sum - this should a number ; atomic SSE
            #note, the first major for loop iterates #num atoms in image times
            F_sse_img +=atomic_image_F_err  # add the atomic SSE to image SSE, as sum of atomic SSE = Image SSE
    #^ this above calculates the Sum of Squared Errors; to get the Mean of Squared Errors (MSE), we need division by 3*N (i.e total number of force components)
    F_mse_img = F_sse_img/(3*num_atoms)
    return U_mse_img,F_mse_img

def test(traj_file,calc_file="./mlff.pyamff"):
    #test_img_list = Trajectory('./cde.traj')
    # calc = aseCalc('./pyamff.pt')
    test_img_list = Trajectory(traj_file)
    calc = aseCalcF(calc_file)

    U_mse = 0.0
    F_mse = 0.0
    for test_image in test_img_list:
            #image+=1 # keep track of which image we are working with
            true_U = test_image.get_potential_energy()
            trueF = test_image.get_forces()
            #print ("TEST: acf.allElement_fps: ",acf.allElement_fps)
            #Predict the Machine Learning Energy and Force
            test_image.calc = calc
            pred_U, pred_F = test_image.get_potential_energy(),test_image.get_forces()
            # print ('pred_U: ',pred_U,'                 pred_F_norm: ',np.linalg.norm(pred_F))
            # Forces are fine for Linear and NoScaler;  just dfps forward propagated through NN Model
            funcUMSE,funcFMSE = image_mse(true_U,pred_U,trueF,pred_F,len(test_image))
            U_mse +=funcUMSE
            F_mse +=funcFMSE
        ### Now get the loss (assuming force coefficient = 1.0
        #total_loss = (U_mse+F_mse)**0.5
    U_RMSE = (U_mse/len(test_img_list))**0.5
    F_RMSE = (F_mse/(len(test_img_list)))**0.5
    print ( U_RMSE,F_RMSE)

test('./img0234.traj',"./mlff.pyamff")
