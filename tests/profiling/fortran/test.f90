MODULE TEST_SETTINGS
    USE grvy
    IMPLICIT NONE
    
    ! Parameters
    INTEGER :: grvy_flag
    INTEGER, PARAMETER :: MAX_METHOD_LEN=20
    INTEGER, PARAMETER :: MAX_LINE_LEN = 100
    INTEGER, PARAMETER :: MAX_FILENAME_LEN=1000

    ! Settings from config.ini
    ! INTEGER,DIMENSION(2) :: hidden !TODO: Allocatable ! TODO: Get rid of because not training
    REAL :: energy_coeff = 1.00
    REAL :: force_coeff = 0.0
    LOGICAL :: train_energy
    LOGICAL :: train_forces
    CHARACTER :: timestring*50 = ''
    CHARACTER(len=MAX_METHOD_LEN) :: act_func
    CHARACTER(len=MAX_METHOD_LEN) :: run_type
    CHARACTER(len=MAX_METHOD_LEN) :: loss_type
    CHARACTER(len=MAX_METHOD_LEN) :: scaler_type
    CHARACTER(len=MAX_FILENAME_LEN) :: fp_file
    CHARACTER(len=MAX_FILENAME_LEN) :: config_file
    CHARACTER(len=MAX_FILENAME_LEN) :: poscar_file



    ! Input from POSCAR
    INTEGER :: num_ele = 0
    INTEGER :: num_atoms = 0
    REAL(8) :: lattice_constant = 1.0
    REAL(8), DIMENSION(3,3) :: cell = 0.0

    INTEGER, DIMENSION(:), ALLOCATABLE :: uniqueNrs
    INTEGER, DIMENSION(:), ALLOCATABLE :: atomicNumbers
    REAL(8), DIMENSION(:,:), ALLOCATABLE :: positions
    INTEGER, DIMENSION(:), ALLOCATABLE :: num_per_ele
    INTEGER, DIMENSION(:), ALLOCATABLE :: symbols ! size of positions, element type
    CHARACTER(len=2), DIMENSION(:), ALLOCATABLE :: type_ele
END MODULE TEST_SETTINGS

PROGRAM test
    USE test_settings
    USE pyamff
    !TODO: Use condititional statement for cases where grvy is not loaded
    IMPLICIT NONE

    REAL(8) :: U
    REAL(8), DIMENSION(:,:), ALLOCATABLE :: F

    ! Get the config file name
    CALL GETARG(1,config_file)
    IF (LEN(TRIM(config_file)) .EQ. 0) config_file = "config.ini"

    CALL GRVY_ASCI_TIME(timestring)
    WRITE(*,'(A,A26)') 'Test Program Began On:',TRIM(timestring)
    ! CALL GRVY_TIMER_INIT('PyAMFF-Fortran-Calculator')
    CALL GRVY_READ_CONFIG_FILE() ! TODO: GET absolute paths
    CALL GRVY_READ_POSCAR()
    ALLOCATE(F(num_atoms,3))
    read_file = .TRUE.
    CALL calc_ase(num_atoms, positions, cell, atomicNumbers, num_ele, uniqueNrs, fp_file, F, U)


    CALL GRVY_ASCI_TIME(timestring)
    WRITE(*,'(A,A26)') 'Test Program Ended On:',TRIM(timestring)
    ! Finalize the main program timer
    CALL GRVY_TIMER_FINALIZE()
    ! Print performance summary to stdout
    CALL GRVY_TIMER_SUMMARIZE()
END PROGRAM


SUBROUTINE GRVY_READ_CONFIG_FILE()
    USE TEST_SETTINGS    
    IMPLICIT NONE

    ! Initialize/read the file
    CALL GRVY_TIMER_BEGIN('test-settings-file')
    CALL GRVY_INPUT_FOPEN(config_file,grvy_flag);
    ! Dump the file to stdout
    ! TODO: ONly in debug mode
    ! WRITE (*,*) "------------- CONFIG FILE -------------"
    ! CALL GRVY_INPUT_FDUMP(grvy_flag) !TODO: Choice of deliminted _delim or not
    ! WRITE (*,*) "---------- END OF CONFIG FILE ---------"

    ! Setup defaults for all variables that aren't logical (default part of read)
    ! TODO: Separate functions
    ! TODO: If flag is not 0, then we have an error
    ! TODO: Just match pyamff standard then can read in any pyamff file
    CALL GRVY_TIMER_BEGIN('set-test-defaults')
    CALL GRVY_INPUT_REGISTER_CHAR("Main/run_type","fingerprints",grvy_flag)
    CALL GRVY_INPUT_REGISTER_CHAR("Main/poscar_file","POSCAR",grvy_flag)
    CALL GRVY_INPUT_REGISTER_CHAR("Main/run_type","fingerprints",grvy_flag)
    CALL GRVY_INPUT_REGISTER_CHAR("Main/scaler_type","NoScaler",grvy_flag)
    CALL GRVY_INPUT_REGISTER_CHAR("Main/parameter_file","mlff.pyamff",grvy_flag)
    !TODO: only calculating not training so don't need anything about model
    CALL GRVY_INPUT_REGISTER_CHAR("Model/activation_function","sigmoid",grvy_flag)
    ! CALL GRVY_INPUT_REGISTER_INT("Model/hidden_layers",(/20,10/),grvy_flag)
    CALL GRVY_INPUT_REGISTER_CHAR("Loss/loss_type","SE",grvy_flag)
    CALL GRVY_INPUT_REGISTER_DOUBLE("Loss/energy_coefficient",1.00d0,grvy_flag)
    CALL GRVY_INPUT_REGISTER_DOUBLE("Loss/force_coefficient",0.02d0,grvy_flag)
    CALL GRVY_TIMER_END('set-test-defaults')

    ! Read in the variables
    CALL GRVY_TIMER_BEGIN('read-test-params')
    CALL GRVY_INPUT_FREAD_CHAR("Main/run_type",run_type,grvy_flag)
    CALL GRVY_INPUT_FREAD_CHAR("Main/poscar_file",poscar_file,grvy_flag)
    CALL GRVY_INPUT_FREAD_LOGICAL("Main/energy_training",train_energy,.TRUE.,grvy_flag)
    CALL GRVY_INPUT_FREAD_LOGICAL("Main/force_training",train_forces,.TRUE.,grvy_flag)
    CALL GRVY_INPUT_FREAD_CHAR("Main/scaler_type",scaler_type,grvy_flag)
    CALL GRVY_INPUT_FREAD_CHAR("Main/parameter_file",fp_file,grvy_flag)
    CALL GRVY_INPUT_FREAD_CHAR("Model/activation_function",act_func,grvy_flag)
    ! CALL GRVY_INPUT_FREAD_CHAR("Model/hidden_layers",hidden,grvy_flag)
    CALL GRVY_INPUT_FREAD_CHAR("Loss/loss_type",loss_type,grvy_flag)
    CALL GRVY_INPUT_FREAD_REAL("Loss/energy_coefficient",energy_coeff,grvy_flag)
    CALL GRVY_INPUT_FREAD_REAL("Loss/force_coefficient",force_coeff,grvy_flag)
    CALL GRVY_TIMER_END('read-test-params')
    ! TODO: Error-handling of inputs
    ! CALL GRVY_TIMER_BEGIN('check-test-params')
    ! CALL GRVY_TIMER_END('check-test-params')

    ! Close file
    CALL GRVY_INPUT_FCLOSE()
    CALL GRVY_TIMER_END('test-settings-file')
END SUBROUTINE GRVY_READ_CONFIG_FILE

SUBROUTINE GRVY_READ_POSCAR()
    USE TEST_SETTINGS
    USE iso_c_binding
    IMPLICIT NONE

    INTEGER :: io, pos, i, tele
    INTEGER :: file_unit = 33
    LOGICAL :: exists, useSelect = .FALSE.
    CHARACTER(len=MAX_LINE_LEN) :: line,line_cpy
    

    CALL GRVY_TIMER_BEGIN('read-poscar')

    OPEN(newunit=file_unit,file=poscar_file,action="read",status="old")
    INQUIRE(unit=file_unit,exist=exists) !size not needed except for stream
    CLOSE(file_unit)
    IF(.NOT. exists) THEN
        print *, "'" // poscar_file // "' does not exist"
        error stop
    END IF
        
    ! NOTE: We are assuming no blank lines and no duplicates
    ! NOTE: Should technically check each read statment for error
    OPEN(newunit=file_unit,file=poscar_file,action="read",status="old",iostat=io)
    IF (io .NE. 0) THEN
        PRINT *, "error opening file"
        ERROR STOP
    END IF
    ! first line is either blank or a comment so skip it
    READ(file_unit,*) 
    ! second line is the lattice constant
    READ(file_unit,*) lattice_constant

    ! third-fifth lines are the lattice vectors
    READ(file_unit,*) cell(1,:)
    READ(file_unit,*) cell(2,:)
    READ(file_unit,*) cell(3,:)
    cell = cell*lattice_constant
    ! sixth line is the element symbols 
    READ(file_unit,'(A)') line
    line = ADJUSTL(line)
    line_cpy = line
    num_ele = 0
    pos = 0
    DO WHILE (LEN(TRIM(line)) .NE. 0)
        num_ele = num_ele + 1
        pos = INDEX(line," ")
        line = line(pos+1:)
    END DO
    ALLOCATE(type_ele(num_ele))
    ALLOCATE(num_per_ele(num_ele))
    ALLOCATE(uniqueNrs(num_ele))
    pos = INDEX(line_cpy," ")
    DO i=1,num_ele-1
        type_ele(i) = line_cpy(:pos-1)
        uniqueNrs(i) = getAtomicNumber(type_ele(i))
        line_cpy = line_cpy(pos+1:)
        pos = INDEX(line_cpy," ")
    END DO
    type_ele(num_ele) = TRIM(line_cpy)
    uniqueNrs(num_ele) = getAtomicNumber(type_ele(num_ele))


    ! seventh line is number of each element
    READ(file_unit,'(A)') line
    line = ADJUSTL(line) 
    DO i=1,num_ele
        READ(line,*) num_per_ele(i)
    END DO
    num_atoms = SUM(num_per_ele)
    ALLOCATE(positions(num_atoms,3))
    ALLOCATE(symbols(num_atoms))
    ALLOCATE(atomicNumbers(num_atoms))
    pos = 1
    DO i=1,num_ele
        tele = num_per_ele(i)
        symbols(pos:pos+tele-1) = i
        atomicNumbers(pos:pos+tele-1) = uniqueNrs(i)
        pos = pos+tele
    END DO
    ! eighth line is either "DIRECT","CARTESIAN" or "SELECTIVE DYNAMICS"
    ! if eighth line is "SELECTIVE DYNAMICS" then ninth line is "DIRECT" or "CARTESIAN"
    READ(file_unit,'(A)') line
    line = TRIM(ADJUSTL(line))
    ! TODO: Check for all cases by having a lower case function
    IF (TRIM(line) .EQ. "Selective Dynamics" .OR. TRIM(line) .EQ. "Selective dynamics") THEN
        useSelect = .TRUE.
        READ(file_unit,'(A)') line
        line = TRIM(ADJUSTL(line))
        IF (.NOT. (line .EQ. "Direct" .OR. line .EQ. "Cartesian")) THEN
            PRINT *, "error while reading line no. 9 in '" // poscar_file // "'"
            ERROR STOP
        END IF
    ELSE IF (.NOT. (line .EQ. "Direct" .OR. line .EQ. "Cartesian")) THEN
        PRINT *, "error while reading line no. 8 in '" // poscar_file // "'"
        ERROR STOP
    END IF
    ! remaining lines are : coord_x coord_y coord_z sd_x sd_y sd_z
    i=1
    DO
        READ(file_unit,'(A)',iostat=io) line
        line = ADJUSTL(line) !Lines may start with spaces depending on generator (e.g. ASE)
        IF (io .EQ. 0) THEN
            IF(useSelect) THEN
                ! get the position of third space
                pos = INDEX(ADJUSTL(line), " ")
                pos = pos + INDEX(ADJUSTL(line(pos+1:))," ")
                pos = pos + INDEX(ADJUSTL(line(pos+1:))," ")
                READ(line(:pos),*) positions(i,:)
            ELSE
                READ(line,*) positions(i,:)
            END IF
        ELSE IF (io .GT. 0) THEN ! error
            PRINT *, "error while reading file"
            ERROR STOP
        ELSE IF (io .LT. 0) THEN ! EOF
            EXIT
        END IF
        i=i+1
    END DO
    CLOSE(file_unit)
    CALL GRVY_TIMER_END('read-poscar')
    CONTAINS
    !TODO: Sort alphabetically, then do binarySearch
    function getAtomicNumber(symbol) result (num)
        CHARACTER(len=2), INTENT(IN) :: symbol
        INTEGER :: num
        CALL GRVY_TIMER_BEGIN('get-atomic-number')
        IF (symbol .EQ. "H") THEN
            num = 1
        ELSE IF (symbol .EQ. "He") THEN
            num=2
        ELSEIF (symbol .EQ. "Li") THEN
                num=3
        ELSEIF (symbol .EQ. "Be") THEN
                num=4
        ELSEIF (symbol .EQ. "B") THEN
                num=5
        ELSEIF (symbol .EQ. "C") THEN
                num=6
        ELSEIF (symbol .EQ. "N") THEN
                num=7
        ELSEIF (symbol .EQ. "O") THEN
                num=8
        ELSEIF (symbol .EQ. "F") THEN
                num=9
        ELSEIF (symbol .EQ. "Ne") THEN
                num=10
        ELSEIF (symbol .EQ. "Na") THEN
                num=11
        ELSEIF (symbol .EQ. "Mg") THEN
                num=12
        ELSEIF (symbol .EQ. "Al") THEN
                num=13
        ELSEIF (symbol .EQ. "Si") THEN
                num=14
        ELSEIF (symbol .EQ. "P") THEN
                num=15
        ELSEIF (symbol .EQ. "S") THEN
                num=16
        ELSEIF (symbol .EQ. "Cl") THEN
                num=17
        ELSEIF (symbol .EQ. "Ar") THEN
                num=18
        ELSEIF (symbol .EQ. "K") THEN
                num=19
        ELSEIF (symbol .EQ. "Ca") THEN
                num=20
        ELSEIF (symbol .EQ. "Sc") THEN
                num=21
        ELSEIF (symbol .EQ. "Ti") THEN
                num=22
        ELSEIF (symbol .EQ. "V") THEN
                num=23
        ELSEIF (symbol .EQ. "Cr") THEN
                num=24
        ELSEIF (symbol .EQ. "Mn") THEN
                num=25
        ELSEIF (symbol .EQ. "Fe") THEN
                num=26
        ELSEIF (symbol .EQ. "Co") THEN
                num=27
        ELSEIF (symbol .EQ. "Ni") THEN
                num=28
        ELSEIF (symbol .EQ. "Cu") THEN
                num=29
        ELSEIF (symbol .EQ. "Zn") THEN
                num=30
        ELSEIF (symbol .EQ. "Ga") THEN
                num=31
        ELSEIF (symbol .EQ. "Ge") THEN
                num=32
        ELSEIF (symbol .EQ. "As") THEN
                num=33
        ELSEIF (symbol .EQ. "Se") THEN
                num=34
        ELSEIF (symbol .EQ. "Br") THEN
                num=35
        ELSEIF (symbol .EQ. "Kr") THEN
                num=36
        ELSEIF (symbol .EQ. "Rb") THEN
                num=37
        ELSEIF (symbol .EQ. "Sr") THEN
                num=38
        ELSEIF (symbol .EQ. "Y") THEN
                num=39
        ELSEIF (symbol .EQ. "Zr") THEN
                num=40
        ELSEIF (symbol .EQ. "Nb") THEN
                num=41
        ELSEIF (symbol .EQ. "Mo") THEN
                num=42
        ELSEIF (symbol .EQ. "Tc") THEN
                num=43
        ELSEIF (symbol .EQ. "Ru") THEN
                num=44
        ELSEIF (symbol .EQ. "Rh") THEN
                num=45
        ELSEIF (symbol .EQ. "Pd") THEN
                num=46
        ELSEIF (symbol .EQ. "Ag") THEN
                num=47
        ELSEIF (symbol .EQ. "Cd") THEN
                num=48
        ELSEIF (symbol .EQ. "In") THEN
                num=49
        ELSEIF (symbol .EQ. "Sn") THEN
                num=50
        ELSEIF (symbol .EQ. "Sb") THEN
                num=51
        ELSEIF (symbol .EQ. "Te") THEN
                num=52
        ELSEIF (symbol .EQ. "I") THEN
                num=53
        ELSEIF (symbol .EQ. "Xe") THEN
                num=54
        ELSEIF (symbol .EQ. "Cs") THEN
                num=55
        ELSEIF (symbol .EQ. "Ba") THEN
                num=56
        ELSEIF (symbol .EQ. "La") THEN
                num=57
        ELSEIF (symbol .EQ. "Ce") THEN
                num=59
        ELSEIF (symbol .EQ. "Pr") THEN
                num=60
        ELSEIF (symbol .EQ. "Nd") THEN
                num=61
        ELSEIF (symbol .EQ. "Pm") THEN
                num=62
        ELSEIF (symbol .EQ. "Sm") THEN
                num=63
        ELSEIF (symbol .EQ. "Eu") THEN
                num=64
        ELSEIF (symbol .EQ. "Gd") THEN
                num=65
        ELSEIF (symbol .EQ. "Tb") THEN
                num=66
        ELSEIF (symbol .EQ. "Dy") THEN
                num=67
        ELSEIF (symbol .EQ. "Ho") THEN
                num=68
        ELSEIF (symbol .EQ. "Er") THEN
                num=69
        ELSEIF (symbol .EQ. "Tm") THEN
                num=70
        ELSEIF (symbol .EQ. "Yb") THEN
                num=71
        ELSEIF (symbol .EQ. "Lu") THEN
                num=72
        ELSEIF (symbol .EQ. "Hf") THEN
                num=73
        ELSEIF (symbol .EQ. "Ta") THEN
                num=74
        ELSEIF (symbol .EQ. "W") THEN
                num=75
        ELSEIF (symbol .EQ. "Os") THEN
                num=76
        ELSEIF (symbol .EQ. "Ir") THEN
                num=77
        ELSEIF (symbol .EQ. "Pt") THEN
                num=78
        ELSEIF (symbol .EQ. "Au") THEN
                num=79
        ELSEIF (symbol .EQ. "Hg") THEN
                num=80
        ELSEIF (symbol .EQ. "Tl") THEN
                num=81
        ELSEIF (symbol .EQ. "Pb") THEN
                num=82
        ELSEIF (symbol .EQ. "Bi") THEN
                num=83
        ELSEIF (symbol .EQ. "Po") THEN
                num=84
        ELSEIF (symbol .EQ. "At") THEN
                num=85
        ELSEIF (symbol .EQ. "Rn") THEN
                num=86
        ELSEIF (symbol .EQ. "Fr") THEN
                num=87
        ELSEIF (symbol .EQ. "Ra") THEN
                num=88
        ELSEIF (symbol .EQ. "Ac") THEN
                num=89
        ELSEIF (symbol .EQ. "Th") THEN
                num=90
        ELSEIF (symbol .EQ. "Pa") THEN
                num=91
        ELSEIF (symbol .EQ. "U") THEN
                num=92
        ELSEIF (symbol .EQ. "Np") THEN
                num=93
        ELSEIF (symbol .EQ. "Pu") THEN
                num=94
        ELSEIF (symbol .EQ. "Am") THEN
                num=95
        ELSEIF (symbol .EQ. "Cm") THEN
                num=96
        ELSEIF (symbol .EQ. "Bk") THEN
                num=97
        ELSEIF (symbol .EQ. "Cf") THEN
                num=98
        ELSEIF (symbol .EQ. "Es") THEN
                num=99
        ELSEIF (symbol .EQ. "Fm") THEN
                num=100
        ELSEIF (symbol .EQ. "Md") THEN
                num=101
        ELSEIF (symbol .EQ. "No") THEN
                num=102
        ELSEIF (symbol .EQ. "Lr") THEN
                num=103
        ELSEIF (symbol .EQ. "Rf") THEN
                num=104
        ELSEIF (symbol .EQ. "Db") THEN
                num=105
        ELSEIF (symbol .EQ. "Sg") THEN
                num=106
        ELSEIF (symbol .EQ. "Bh") THEN
                num=107
        ELSEIF (symbol .EQ. "Hs") THEN
                num=108
        ELSEIF (symbol .EQ. "Mt") THEN
                num=109
        ELSEIF (symbol .EQ. "Ds") THEN
                num=100
        ELSEIF (symbol .EQ. "Rg") THEN
                num=111
        ELSEIF (symbol .EQ. "Cn") THEN
                num=112
        ELSEIF (symbol .EQ. "Nh") THEN
                num=113
        ELSEIF (symbol .EQ. "Fl") THEN
                num=114
        ELSEIF (symbol .EQ. "Mc") THEN
                num=115
        ELSEIF (symbol .EQ. "Lv") THEN
                num=116
        ELSEIF (symbol .EQ. "Ts") THEN
                num=117
        ELSEIF (symbol .EQ. "Og") THEN
                num=118
        ELSE
            print *, "Element symbol '" //symbol// "' is unknown"
            error stop
        END IF
        CALL GRVY_TIMER_END('get-atomic-number')
    end function

END SUBROUTINE GRVY_READ_POSCAR

SUBROUTINE VERIFY_SETTINGS()
    USE TEST_SETTINGS
    IMPLICIT NONE
END SUBROUTINE VERIFY_SETTINGS


