.. _lossfunction:

============
LossFunction
============

These are the options that go in **[LossFunction]** section of the config.ini file.

**options**:

    ``loss_type``: The type of loss function used during fitting.

        kind: ``string``

        default: ``SSE``

        options:
            ``SSE``: Sum of squared error
            ``MSE``: Mean squared error
            ``RMSE``: Root-mean-squared error
            ``RMLSE``: Root-mean-log squared error


    ``energy_coefficient``: The weight for the energy term of the loss function.

         kind: ``float``

         default: ``1.0``

    ``force_coefficient``: The weight for the force term of the loss function.

         kind: ``float``

         default: ``0.02``


