.. _debug:

=====
Debug
=====

    options:

These are the options that go in **[Debug]** section of the config.ini file.

**init_model_parameters**: When this flag is set to True, then PyAMFF will initialize random model paramaters.
    kind: ``boolean``
    default: ``False``

**nn_values**:
   
    kind: ``boolean``
    
    default: ``False``

**use_deterministic**: When this flag is set to True, deterministic algorithms are used by Torch.

    kind: ``boolean``
    
    default: ``False``
