.. _optimizehyperparameters:

========================
Optimize Hyperparameters
========================

These are the options that go in **[Optimize Hyperparameters]** section of the config.ini file.

**optimize_activation_func:**

     kind: ``boolean``

     default: ``False``

**optimize_scaler**:

     kind: ``boolean``

     default: ``False``

**optimize_force_coefficient**:

     kind: ``boolean``

     default: ``False``

**optimize_model_architecture**:

     kind: ``boolean``

     default: ``False``

**differential_ratio**:

     kind: ``float``

     default: ``0.2``
