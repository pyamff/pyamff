.. _fingerprints:

============
Fingerprints
============

These are the options that go in **[Fingerprints]** section of the config.ini file.

**fp_type**: The type of fingerprint to be calculated.

    kind: ``string``

    default: ``BP``

    values:

        ``-BP``: Behler-Parinello symmetry functions (G1 and G2)

**fp_engine**: This option allows you to choose between the old and new Fortran implementations for the fingerprints engine.
 
    kind: ``string``

    default: ``Fortran``

    values: 

        ``-Fortran``
        
        ``-OldFortran``


**fp_parameter_file**: Name of fingerprint parameter file.

    kind: ``string``

    default: ``fpParas.dat``


**fp_batch_mode**: 

    kind: ``boolean``

    default: ``True``

**fp_batch_num**:

    kind: ``integer``

    default: ``10``  

**fp_use_existing**: This option, when set to True, has PyAMFF read existing fingerprints in the working directory instead
of calculating a new set of fingerprints from the provided trajectory file.

    kind: ``boolean``

    default: ``False``

**fp_test_use_existing**:

    kind: ``boolean``

    default: ``True``

**fp_file**:

    kind: ``string``

    default: ``fps.pckl``

**fp_dir**: This option allows you to configure where PyAMFF will look for pre-calculated fingerprints.

    kind: ``string``

    default: ``fingerprints``

**test_fp_dir**:

    kind: ``string``
    
    default: ``test_fingerprints``

**gr_calc**:

    kind: ``boolean``

    default: ``False``

**gr_cutoff**:

    kind: ``boolean``

    default: ``5``
