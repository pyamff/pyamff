.. _main:

====
Main
====

These are the options that go in **[Main]** section of the config.ini file.

    options:

        **run_type**: The type of run to perform
            **kind**: ``string``

            **default**: ``trainFF``

            **values**:
                - trainFF
                - testFF
                - train_testFF
                - fingerprints

        **epochs_max**: Takes an integer number for the number of training epochs

            **kind**: integer
            
            **default**: ``10000``

        **test_log_interval**:
            
            **kind**: integer
            
            **default**: 10        

        **trajectory_file**: The ``ase`` trajectory file containing the training images.

            **kind**: ``string``

            **default**: ``train.traj``
        
        **test_trajectory_file**:
            
            **kind**: ``string``
           
            **default**: train_test.traj

        **energy_training**: Trains force field using energy of the training images. 

            **kind**: ``boolean``
            
            **default**: ``True``

        **force_training**: Trains force field using forces of the training images.

            **kind**: ``boolean``

            **default**: ``True``

        **use_cohesive_energy**:

            **kind**: ``boolean``

            **default**: ``False``

        **scaler_type**:

            **kind**: ``string``
           
            **default**: LinearScaler

            **values**:
                - LinearScaler
                - NoScaler
                - MinMaxScaler
                - LogScaler
                - STDScaler
     
        **adjust**:
            
            **kind**: ``boolean``

            **default**: ``False``

        **restart**:

            **kind**: ``boolean``

            **default**: ``False``

        **model_path**:
            **Description**: The location of the saved parameters file

            **kind**: ``string``
            
            **default**: ``pyamff.pt``


