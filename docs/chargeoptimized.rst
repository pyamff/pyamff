.. _ChargeOptimized:

===============
ChargeOptimized
===============

These are the options that go in **[ChargeOptimized]** section of the config.ini file.

**options**:
``if_chg``:
    ``type``: ``Boolean``
    ``default``: ``False``