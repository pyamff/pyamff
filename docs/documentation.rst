.. _documentation:

=============
Documentation
=============


Flags
-----
This is the comprehensive list of configuration flags in PyAMFF.


:ref:`Main <main>`: General settings.

:ref:`ChargeOptimized <chargeoptimized>`: Settings for charge optimization.

:ref:`Parallelization <parallelization>`: Settings for parallelization of calculations.

:ref:`Fingerprints <fingerprints>`: Settings for fingerprints.

:ref:`MachineLearningModel <machinelearningmodel>`: Choose which machine learning model to train.

:ref:`LossFunction <lossfunction>`: the loss functions available.

:ref:`Optimizer <optimizer>`: Settings for optimization.

:ref:`CrossValidation <crossvalidation>`: Settings for cross validation

:ref:`OptimizeHyperparameters <optimizehyperparameters>`: Settings for hyperparameter optimization.

:ref:`Debug <debug>`: Debug Settings.


Modules
-------
This links to the documentation for code in PyAMFF. This is probably only useful to you if you're contributing to our codebase.

:ref:`PyAMFF <pyamff>`: 

.. toctree::
   :maxdepth: 3
   :titlesonly:
   
   pyamff




