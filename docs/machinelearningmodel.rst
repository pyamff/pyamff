.. _machinelearningmodel:

====================
MachineLearningModel
====================

These are the options that go in **[MachineLearningModel]** section of the config.ini file.

**model_type**: The type of machine learning protocol to be used.

type: ``str``

default: ``neural_network``

options::
    neural_network: Neural Network

**activation_function**:

Description: The activation function for nodes in the hidden layers.

Type: ``str``
    
default: ``sigmoid``

options::
    1. sigmoid
    2. relu
    3. tanh
    4. softplus
    5. reluTanh

**hidden_layers**:

..  *note: This option has two formats it can accept, depending on if randomsearch is used*

Description: Describes the connectivity of the hidden layers.
    
Type: ``integer list``
    
default::
    20 10

..  *If randomsearch is being used, the config entry would look like*::
        hidden_layers = [[10,20],
                         [10,20]]

    *If randomsearch is not being used, the config entry would look like*::
        hidden_layers = 10 10

    | Note, in the second example, layer entries are delimited by *spaces,* but by commas and brackets in the first.

**layer_types**

Description: A list describing each hidden layer. Each layer type should be separated by spaces.

Type: ``str``

Default::
    linear [...] linear

**initial weights**:

    kind: ``string``

    default: ``random_truncated_normal``

    values:
      
      - ``random_truncated_normal``

      - ``sqrt_prev_layer_num_nuerons``

      - ``random_uniform``
