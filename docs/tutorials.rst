.. _tutorials:

=========
Tutorials
=========

:ref:`Configuration Guide <configguide>`
:ref:`Train NN with Behler-Parrinello fingerprints <trainNN_tutorial>`


