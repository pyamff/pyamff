.. _configguide:

===================
Configuration Guide
===================

PyAMFF must be run with a config.ini file with the options specified in the Flags section below.
Each section header is denoted by square brackets, and is followed by the key/value pairs.
For example, to set the *run_type* key of the *[Main]* section to the value *trainFF*,
your config.ini file would include the lines::

    [Main]
    run_type = trainFF

PyAMFF also requires a fpParas.dat file to specify the values for fingerprint calculations.
If you do not have a fpParas.dat file, one can be generated from a POSCAR.
The fpParas.dat file should be in the following format::

    #type  centralElement  neighborElement  eta   Rs
     G1    H               H                0.05  0.0
     G1    H               Pd               0.05  0.0
     G1    Pd              Pd               0.05  0.0
    #type  centralElement  neighborElement1  neighborElement2  eta  zeta  gamma  theta
     G2    Pd              Pd                Pd                0.02 100.0 1.0      0.5

Lastly, your images must be in an ASE.io.trajectory file named train.traj. For information on how to get here,
please see the `ASE documentation. <https://wiki.fysik.dtu.dk/ase/ase/io/trajectory.html#>`


There are specific options for each method, and a set of general options which are shared between methods.


Flags
-----

:ref:`Main <main>`: General settings.

:ref:`ChargeOptimized <chargeoptimized>`: Settings for charge optimization.

:ref:`Parallelization <parallelization>`: Settings for parallelization of calculations.

:ref:`Fingerprints <fingerprints>`: Settings for fingerprints.

:ref:`MachineLearningModel <machinelearningmodel>`: Choose which machine learning model to train.

:ref:`LossFunction <lossfunction>`: the loss functions available.

:ref:`Optimizer <optimizer>`: Settings for optimization.

:ref:`CrossValidation <crossvalidation>`: Settings for cross validation

:ref:`OptimizeHyperparameters <optimizehyperparameters>`: Settings for hyperparameter optimization.

:ref:`LearningScheduler <schedulers>`: Settings for Learning Rate Schedulers.

:ref:`Debug <debug>`: Debug Settings.
