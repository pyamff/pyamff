.. _parallelization:

===============
Parallelization
===============

These are the options that go in **[Parallelization]** section of the config.ini file.

**options**:

   **master_addr:**
   
     kind: ``string`` 

     default: ``127.0.0.1``

   **master_port:**

     | *Note*: This needs to be different between instances of PyAMFF running simultaneously.
     kind: ``string``

     default: ``12355``

   **dynamic:** 

    kind: ``boolean``

    default: ``False``

    **mp_start_method:** The method of starting child processes when using parallelizing.
    | *Note:* Fork is generally faster than spawn, but spawn **MUST** be used when running on GPUs.
     kind: ``string``

     default: ``spawn``

     values:

        ``-fork``

        ``-spawn``

   **process_num:** Takes an integer as the number of parallel jobs to perform.
 
     kind: ``integer``

     default: ``1``

   **batch_num_per_proc:** Takes an integer as the number of batches per process.

     kind: ``integer``

     default: ``100``

Total number of batches are equal to 'process_number * batch_number'.

    **thread_num:**

        kind:  ``integer``

        default: ``1``

    **gr_process_num**:

        kind: ``integer``

        default: ``1``

    **device_type**:

        kind: ``string``

        default: ``CPU``
        values:
            - ``CPU``
            - ``GPU``

    **backend**:
     
        kind: ``string``

        default: ``gloo``

        values:

            - ``gloo``

            - ``nccl``
