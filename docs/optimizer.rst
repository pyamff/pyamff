.. _optimizer:

=========
Optimizer
=========

These are the options that go in **[Optimizer]** section of the config.ini file.

**optimizer_type**: The optimizer to be used for the machine learning.

    kind: ``string``

    default: ``LBFGSScipy``

    options:

        - ``LBFGSScipy``: L-BFGS in Scipy

        - ``ADAM``: ADAM

        - ``LBFGS``
       
        - ``Rprop``

        - ``SGD``

**step_size**:

    kind: ``float``

    default: ``0.1``

**force_tol**:
   
    kind: ``float``

    default: ``0.01``

**energy_tol**:
    
    kind: ``float``

    default: ``0.0001``

**loss_grad_tol**:
     
    kind: ``float``

    default: ``1e-04``

**loss_convergence**:

    kind: ``float``

    default: ``1e-09``

**learning_rate**:
    
    kind: ``float``

    default: ``0.01``

**weight_decay**:

    kind: ``float``

    default: ``0.00``

**write_final_grads**:

    kind: ``boolean``

    default: ``False``
