.. _schedulers:

==========
Schedulers
==========

There are a variety of options for learning rate schedulers made available through the config.
Note, that these options are only available when using the SGD, ADAM, and Rprop optimizers.

To use a learning rate scheduler, create a section in your config.ini called ``[LearningScheduler]``.
Then, add a line for ``scheduler_type`` with the desired scheduling method.

**options**:

    **scheduler_type**:
        kind: ``string``
        default: ``None``
        values:
            ``- None``
            ``- ReduceLROnPlateau``
            ``- StepLR``
            ``- MultiStepLR``
            ``- ExponentialLR``
            ``- CosineAnnealingLR``


Finally, each scheduler needs parameters:

**ReduceLROnPlateau:**

    **options:**

        patience:
            kind: integer
            default: 10

        factor:
            kind: float
            default: 0.1

        min_lr:
            kind: float
            default: 0.00001


**StepLR:**

    **options:**

        step_size:
            kind: integer
            default: 100

        gamma:
            kind: float
            default: 0.1


**MultiStepLR:**

    options:

        milestones:
            kind: integerlist
            default: 500 1000 1500

        gamma:
            kind: float
            default: 0.1


**ExponentialLR:**

    options:

        gamma:
            kind: float
            default: 0.1


**CosineAnnealingLR:**

    options:

        t_max:
            kind: integer
            default: 1000

        eta_min:
            kind: float
            default: 0.00001