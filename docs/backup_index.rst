.. PyAMFF documentation master file, created by
   sphinx-quickstart on Sat Jun  6 15:26:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyAMFF: Python Atom-Centered Machine Learning Force Field
=========================================================

The PyAMFF package contains ...

Machine Learning
================

.. image:: fig/NN.png
   :align: center


The PyAMFF team
===============

Henkelman Group (UT Austin)

Flags
=====

.. toctree::
   :maxdepth: 3

   main
   parallelization
   fingerprints
   machinelearningmodel
   lossfunction
   optimizer
   debug
   modules


#.. automodule:: config
#    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


