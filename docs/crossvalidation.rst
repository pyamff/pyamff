.. _crossvalidation:

===============
Cross Validaton
===============

These are the options that go in **[Cross Validation]** section of the config.ini file.

**cross_validation**:

     kind: ``boolean``

     default: ``False``

**test_set_size**:

    kind: ``float``

    default: ``0.2``

**write_train_test**:

    kind: ``boolean``

    default: ``False``

**analyze_test**:

    kind: ``boolean``

    default: ``False``
