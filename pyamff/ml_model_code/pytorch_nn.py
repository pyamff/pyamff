# from . import generateTensorFlowArrays
# from .. import FileDatabase
from collections import OrderedDict
import numpy as np

import torch
from torch.utils import data
from scipy.stats import truncnorm
import torch.nn as nn
from torch.nn import Linear
from pyamff.ml_model_code.loss_functions import LossFunction
from pyamff.utilities.truncated_normal_dist import TruncatedNormal
from pyamff.utilities.fileIO import saveData

from pyamff.fingerprints.fingerprints_wrapper import atomCenteredFPs
# from mlModels.linear import Linear

import time, sys, copy, pickle


class NeuralNetwork(nn.Module):
    """
    hiddenlayers: define the structure of the neural network.
                 i.e. (2,3) define a neural network with two hidden layers,
                            containing 2 and 3 neurons, respectively
    nFPs: a dictionary that defines number of fingerprints for each element.
          i.e. {'H': 3, 'Pd':2}: 3 and 2 fingerprints for 'H' and 'Pd', respectively
    maxEpochs: maximum epochs the training process will take
    energyCoefficient: the weight of energy in the loss function
    forceCoefficient: the weight of force in the loss function
    params: a list or array of parameters will be used to initilize the NN model
          i.e., [ #params for 'H': fp1: fingerprint 1, n1: neuron 1 in the corresponding layer
                  [[fp1_n1, fp2_n1, fp3_n1], [fp1_n2, fp2_n2, fp3_n2]],   #weights connecting inputlayer to hiddenlayer 1
                  [ bias_n1,                  bias_n2],                   #bias on hiddenlayer 1
                  [[n1_n1, n2_n1], [n1_n2, n2_n2], [n1_n3, n2_n3]],       #weights connecting hiddenlayer 1 to hiddenlayer 2
    e             [ bias_n1,       bias_n2,         bias_n3],             #bias on hiddenlayer 2
                  [[n1_n1, n2_n1, n3_n1]],                                #weights connecting hiddenlayer 1 to outputlayer
                  [ bias_n1],                                             #bias on outputlayer

                  #params for 'Pd': fp1: fingerprint 1, n1: neuron 1 in the corresponding layer
                  [[fp1_n1, fp2_n1], [fp1_n2, fp2_n2]],                   #weights connecting inputlayer to hiddenlayer 1
                  [ bias_n1,          bias_n2],                           #bias on hiddenlayer 1
                  [[n1_n1, n2_n1], [n1_n2, n2_n2], [n1_n3, n2_n3]],       #weights connecting hiddenlayer 1 to hiddenlayer 2
                  [ bias_n1,       bias_n2,         bias_n3],             #bias on hiddenlayer 2
                  [[n1_n1, n2_n1, n3_n1]],                                #weights connecting hiddenlayer 1 to outputlayer
                  [ bias_n1],                                             #bias on outputlayer
                ]
    """

    def __init__(
        self,
        hiddenlayers=(5, 5),
        nFPs={"Au": 10, "H": 5},
        activation="sigmoid",
        forceTraining=True,
        cohE=False,
        params=None,
        scaler=None,
        debug=False,
        initial_weights="random_truncated_normal",
        slope=None,  # Modified by jycho
        layer_types=["linear", "linear"],  # this line
        dropout_p=[-1],  # this line
        # energyRange = None,
        # forceRange = None
        if_chg=False,
        partitions=None,
    ):
        super().__init__()
        self.hiddenlayers = hiddenlayers
        self.activation = activation
        self.n_layers = len(hiddenlayers) + 2
        # self.n_layers = len(layer_types) + 2
        self.layer_types = layer_types  # this line
        self.dropout_p = dropout_p  # this line

        if activation == "sigmoid":
            self.actF = nn.Sigmoid()
        if activation == "relu":
            self.actF = nn.ReLU()
        if activation == "tanh":
            self.actF = nn.Tanh()
        if activation == "softplus":
            self.actF = nn.Softplus()
        if activation == "reluTanh":
            self.actF_1 = nn.ReLU()
            self.actF_2 = nn.Tanh()
        self.nFPs = nFPs
        self.elements = np.array([element for element in nFPs.keys()])
        self.nn_models = {}
        self.hd_names = {}
        self.model_params = []
        self.model_namedparams = []
        self.debug = debug
        self.initial_weights = initial_weights
        self.slope = None  # Added by jycho

        # print('  paras',list(self.nn_models.parameters()))
        self.forceTraining = forceTraining
        # Parameters used to scale energy and forces
        self.cohE = cohE
        self.scaler = scaler
        self.scalerType = scaler.scalerType
        self.adjust = False
        self.imageIndices = None
        self.nimages = None
        self.fp_d = None
        self.dEdg_AtomIndices = None
        self.force_AtomIndices = None
        self.natomsPerElement = None
        self.ntotalAtoms = None

        if not if_chg:
            for element, n_Gs in self.nFPs.items():
                self.hd_names[element] = []
                if "dropout" in self.layer_types:
                    self.atomModel_withdropout(n_Gs, element)
                else:
                    self.atomModel(n_Gs, element)
                # self.model_params += list(self.nn_models[element].parameters())
                # self.model_namedparams += list(self.nn_models[element].named_parameters())

            self.nn_models = nn.ModuleDict(self.nn_models)
            self.setParams(params)
            for element in self.elements:
                self.nn_models[element].double()
        else:
            # print('Refer Electro-negativity to repeat initialization of NN')
            self.scalerType = scaler.scalerType
            self.params = params

            self.init_param(partitions)

    """
    atomModel: define the NN model for each atom.
    It will generate a model dictionary with 'element' as the key and a inner dictionary with the name
    of each layer as the key
    """

    def init_param(self, partitions):
        '''
        Alan-chg: repeat the initialzation of NN parameters until the pred_charges 
            can correspond the Electronegativity of the element
            partition=None,
            ifElectronegativity=True,
            process_number=40
        '''
        en_dict = {'H': 2.20,'He': 4.16,'Li': 0.98,'Be': 1.57,'B': 2.04,'C': 2.55,
                   'N': 3.04,'O': 3.44,'F': 3.98,'Ne': 4.79,'Na': 0.93,'Mg': 1.31,
                   'Al': 1.61,'Si': 1.98,'P': 2.19,'S': 2.58,'Cl': 3.16,'Ar': 3.24,
                   'K': 0.82,'Ca': 1.01,'Sc': 1.36,'Ti': 1.54,'V': 1.63,'Cr': 1.66,
                   'Mn': 1.55,'Fe': 1.83,'Co': 1.88,'Ni': 1.92,'Cu': 1.90,'Zn': 1.65,
                   'Ga': 1.81,'Ge': 2.01,'As': 2.18,'Se': 2.55,'Br': 2.96,'Kr': 3.00,
                   'Rb': 0.82,'Sr': 0.95,'Y': 1.22,'Zr': 1.33,'Nb': 1.59,'Mo': 2.16,
                   'Tc': 1.91,'Ru': 2.20,'Rh': 2.28,'Pd': 2.20,'Ag': 1.93,'Cd': 1.69,
                   'In': 1.78,'Sn': 1.96,'Sb': 2.05,'Te': 2.12,'I': 2.66,'Xe': 2.60,
                   'Cs': 0.79,'Ba': 0.89,'Hf': 1.32,'Ta': 1.51,'W': 2.36,'Re': 1.93,
                   'Os': 2.18,'Ir': 2.20,'Pt': 2.28,'Au': 2.54,'Hg': 2.00,'Ti': 1.62,
                   'Pb': 2.33,'Bi': 2.02,'Po': 1.99,'At': 2.22,'Rn': 2.43
                }

        def batchGenerator(acfs):
            if len(acfs) == 1:
                batch = atomCenteredFPs()
                batch.stackFPs(acfs)
                return batch
            batch = atomCenteredFPs()
            batch.stackFPs(acfs)

            return batch

        def batchGenerator_ele(efps):
            '''
            efps:OrderDict{0:efp, 1:efp ...}
            '''

            return efps

        all_satisfied = False
        find_iter = 0

        while not all_satisfied:

            # NN re-initialization
            for element, n_Gs in self.nFPs.items():
                self.hd_names[element] = []
                self.atomModel_Ele(n_Gs, element)
            self.nn_models = nn.ModuleDict(self.nn_models)
            self.setParams(self.params)
            for element in self.elements:
                self.nn_models[element].double()
            
            if partitions is None: # for ase_calc
                return 

            for rank in range(len(partitions.partitions)):

                partition = partitions.use(rank)
                partition_Ele = partitions.use_Ele(rank)

                # generate data loader (batch size = 1)
                batches = torch.utils.data.DataLoader(
                    partition,
                    batch_size=1,
                    collate_fn=batchGenerator,
                    shuffle=False)

                batches_Ele = torch.utils.data.DataLoader(
                    partition_Ele,
                    batch_size=1,
                    collate_fn=batchGenerator_ele,
                    shuffle=False)

                # data loader (batch size = 1)
                for i, data in enumerate(zip(batches, batches_Ele)):
                    # Zichen: call remaked forward() function
                    batch = data[0]
                    batch_ele = data[1]

                # call forward of NN
                # charges, Es = self.forward_init(fps=batch.allElement_fps)
                energies, forces, charges, charge_percent = self.forward(
                    batch.allElement_fps,
                    batch.dgdx,
                    batch,
                    'cpu',
                    logger=None,
                    batch_ele=batch_ele,
                    )

                Es = charge_percent['Es']
                Ewald_tot = charge_percent['Ewald_tot']
                Epercent = charge_percent['Epercent']

                for element in charges.keys():
                    charges[element] = float(torch.mean(charges[element]))
                '''
                Zichen: satisfied requirement #1: 
                Qi < 0, Qj > 0 if electronegative i > j
                '''
                charge_flag = True
                charge_sort = sorted(zip(charges.values(), charges.keys()))
                en_sort = {}
                for element in self.nFPs.keys():
                    en_sort[element] = en_dict[element]
                # larger the electronegative value, smaller the charge of the element
                en_sort = sorted(zip(en_sort.values(), en_sort.keys()),reverse=True)

                charge_list = []
                # Judge the value relationship of charge_sort == en_sort
                for i in range(len(en_sort)):
                    if en_sort[i][1] != charge_sort[i][1]:
                        charge_flag = False
                    charge_list.append(charge_sort[i][0])

                charge_list = np.array(charge_list)
                # Judge the charge_sort have at least one postitive value and at least one negative value
                if any(map(lambda x: x < 0, charge_list)) and any(
                        map(lambda x: x >= 0, charge_list)):
                    pass
                else:
                    charge_flag = False

                # if the batch in this loop do not satisfied EN, break to re-initialization NN
                if charge_flag == False:
                    print('[{}]  Charges Stop at index {}: [info] {}'.format(
                        find_iter + 1, rank, charges))
                    break

            # stop iteration when all_satisfied = True
            # all_satisfied = charge_flag and Es_flag and Ewald_tot_flag
            all_satisfied = charge_flag

            find_iter += 1
        
        print('Iter complete, total {} times'.format(find_iter))
        print('Satisfied init charges: {} '.format(charge_sort))

        return find_iter, charge_sort, Es, Epercent

    def set_scaler(self, scaler):
        # print(scaler.scalerType)

        if self.scalerType != scaler.scalerType:
            print("Sclar type is not consistent", sys.stderr)
            sys.exit(2)
        if self.scalerType == "NoScaler":
            self.slope = torch.tensor(scaler.slope)
            
            self.intercept = torch.tensor(scaler.intercept)
        if self.scalerType in ["LinearScaler", "MinMaxScaler"]:
            self.adjust = scaler.adjust
            if self.adjust:
                self.slope = nn.parameter.Parameter(torch.tensor(scaler.slope))
                self.intercept = nn.parameter.Parameter(torch.tensor(scaler.intercept))
            else:
                self.slope = torch.tensor(scaler.slope)
                self.intercept = torch.tensor(scaler.intercept)
            self.fc_slope = scaler.fc_slope
            self.fc_intercept = scaler.fc_intercept
        if self.scalerType in ["LogScaler"]:
            self.eMinMax = scaler.eMinMax
            self.eRange = self.eMinMax[0] - self.eMinMax[1]
            # self.fMinMax = scaler.fMinMax
        if self.scalerType in ["STDScaler"]:
            self.intercept = torch.tensor(scaler.intercept)
            self.slope = torch.tensor(scaler.slope)
            self.e_std = scaler.slope
            self.f_std = scaler.f_std
            self.ef_coef = self.e_std / self.f_std

    def setParams(self, params):
        i = 0
        # mean, stddev, lowerbound, upbound
        tn = TruncatedNormal(
            torch.Tensor([0.0]),
            torch.Tensor([0.1]),
            torch.Tensor([-0.2]),
            torch.Tensor([0.2]),
        )
        # params_to_dump = []
        if params is not None:  # if initital weights and biases given
            for param in self.parameters():
                param.data = torch.tensor(params[i]).double()
                # print('shape', param.data.shape)
                i += 1
        else:  # we are picking initial weights
            # lower, upper = 3.5, 6
            # mu, sigma = 5, 0.7
            # X = stats.truncnorm(
            #    (lower - mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma)
            # for param in self.parameters():
            # param.data = torch.tensor(truncnorm.rvs(-10, 10, loc=0.0, scale=0.1, size=list(param.data.shape)))
            #    param.data = tn.rsample(param.data.shape)
            #    params_to_dump.append(param.data)
            # with open("saved_model_params.pyamff", "wb") as f:
            #    pickle.dump(params_to_dump, f)

            # self.initial_weights ='sqrt_prev_layer_num_neurons'
            if (
                self.initial_weights == "sqrt_prev_layer_num_neurons"
            ):  # scale initial weights by root(num_neurons)
                # print ('scaling initial weights with nn layer size')
                # print ('self.parameters: ',self.parameters)
                # print ('self.parameters: ',)
                num_neurons_in_previous_layer = 1  # for input layer, we randomly sample, no previous layer exists to scale w.r.t
                # would it be better to scale w.r.t number of fingerprints for the input layer or is this better?
                for param in self.parameters():  # for each parameter set
                    # print ('param.data.shape: ',param.data.shape)
                    # print ('param: ',param)
                    if (
                        len(param.data.shape) != 1
                    ):  # if weights (i.e do not want to scale inital biases, only initial weights)
                        # print ('nnipl: ',num_neurons_in_previous_layer)
                        param.data = tn.rsample(param.data.shape) / (
                            np.sqrt(num_neurons_in_previous_layer)
                        )
                        # num_neurons_in_previous_layer = param.data.shape[1]
                    else:  # else it is a bias initialization
                        num_neurons_in_previous_layer = param.data.shape[
                            0
                        ]  # get the number of neurons
                        param.data = tn.rsample(param.data.shape)
            elif self.initial_weights == "random_uniform":  # pick uniform weights
                # print ("uniform random")
                for param in self.parameters():
                    # by default pytorch produces numbers randomly uniform on [0,1)
                    # so shifting it to produce uniform numbers on [-1, 1)
                    param.data = 2 * torch.rand(param.data.shape) - 1
                    # print (param.data.shape)
            else:  # do not scale initial weights
                # print ('default random weights else')
                for param in self.parameters():
                    # param.data = 0*tn.rsample(param.data.shape)
                    param.data = tn.rsample(param.data.shape)

    def atomModel(self, n_Gs, element):
        self.nn_models[element] = nn.ModuleDict(
            {"inputLayer": Linear(n_Gs, self.hiddenlayers[0]).double()}
        )
        for i in range(len(self.hiddenlayers) - 1):
            self.hd_names[element].append("hiddenLayer_" + str(i + 1))
            self.nn_models[element][self.hd_names[element][i]] = Linear(
                self.hiddenlayers[i], self.hiddenlayers[i + 1]
            ).double()
        self.nn_models[element]["outputLayer"] = Linear(
            self.hiddenlayers[-1], 1
        ).double()

    def atomModel_Ele(self, n_Gs, element):
        self.nn_models[element] = nn.ModuleDict(
            {"inputLayer": Linear(n_Gs, self.hiddenlayers[0]).double()}
        )
        for i in range(len(self.hiddenlayers) - 1):
            self.hd_names[element].append("hiddenLayer_" + str(i + 1))
            self.nn_models[element][self.hd_names[element][i]] = Linear(
                self.hiddenlayers[i], self.hiddenlayers[i + 1]
            ).double()
        self.nn_models[element]["outputLayer"] = Linear(
            self.hiddenlayers[-1], 2
        ).double() # Alan-chg: output nodes changed into 2 (Ei, qi)

    def atomModel_withdropout(self, n_Gs, element):
        self.nn_models[element] = nn.ModuleDict(
            {"inputLayer": Linear(n_Gs, self.hiddenlayers[0]).double()}
        )
        last_output = self.hiddenlayers[0]
        last_hl_index = 0
        dropout_index = 1

        # input to first hidden layer already added, so start index at 1
        for i in range(1, len(self.layer_types)):
            layer_type = self.layer_types[i]

            if layer_type == "dropout":
                self.hd_names[element].append("dropoutLayer_" + str(dropout_index))
                self.nn_models[element][self.hd_names[element][i - 1]] = nn.Dropout(
                    p=self.dropout_p[dropout_index - 1]
                )
                dropout_index += 1

            # linear layers
            else:
                self.hd_names[element].append("hiddenLayer_" + str(last_hl_index + 1))
                self.nn_models[element][self.hd_names[element][i - 1]] = Linear(
                    self.hiddenlayers[last_hl_index],
                    self.hiddenlayers[last_hl_index + 1],
                ).double()
                last_hl_index += 1

        self.nn_models[element]["outputLayer"] = Linear(
            self.hiddenlayers[last_hl_index], 1
        ).double()

    def set(
        self,
        imageIndices,
        nimages,
        dEdg_AtomIndices,
        force_AtomIndices,
        natomsPerElement,
        ntotalAtoms,
        slope=None,
    ):

        self.imageIndices = imageIndices
        self.nimages = nimages
        self.dEdg_AtomIndices = dEdg_AtomIndices
        self.force_AtomIndices = force_AtomIndices
        self.natomsPerElement = natomsPerElement
        self.ntotalAtoms = ntotalAtoms
        if self.slope is None:
            self.slope = slope

    """
    forward function: define the operation that acts on each layer of neural network
    fps: list of tensor of fingerprints for one image with requires_grad=True
         for each tensor, must have requires_grad=True to get gradient
         {'H':tensor([ [G1,G2,...,Gg],  Atom 1  in Image 1
                       [G1,G2,...,Gg],  Atom 2  in Image 1
                              ...
                       [G1,G2,...,Gg],  Atom N1 in Image 1
                       [G1,G2,...,Gg],  Atom 1  in Image 2
                              ...
                       [G1,G2,...,Gg],  Atom N2 in Image 2
                              ...
                       [G1,G2,...,Gg],  Atom NM in Image M
                      ])
          'Pd': ...}
    imageIndices: used to sum up energy for each image over atoms
    nimages: number of images in the training batch
    fp_d: derivative of fingeprints
          format: refer to dgdx in function 'generateInputs()'
    dEdg_AtomIndices: used to gather dEdg for forces calculations
    force_AtomIndices: used to sum up force for each atom over neighbor atoms 
    natomsPerElement: total number of atoms of each type of element.
            {'H':4,'Pd':26}
    ntotalAtoms: total number of atoms in the training batch
    """

    def forward(self, fps, fp_d, batch, device, logger=None, batch_ele=None):
        # def forward(self, batch):
        # fps = batch.allElement_fps
        # fp_d = batch.dgdx
        # for params in self.nn_models.parameters():
        #    print(params)
        #    break
        if batch_ele is not None:
            if_chg = True
            self.slope = 1.0
            self.intercept = 0.0
        else:
            if_chg = False

        self.imageIndices = batch.fp_imageIndices
        self.nimages = batch.nimages

        # self.fp_d = fp_d
        self.dEdg_AtomIndices = batch.dEdg_AtomIndices

        self.force_AtomIndices = batch.force_AtomIndices
        self.force_AtomIndices = self.force_AtomIndices.to(device)

        self.natomsPerElement = batch.natomsPerElement

        self.ntotalAtoms = batch.ntotalAtoms
        energies = torch.tensor([[0.0]] * self.nimages, device=device).double()  # Initialize energies
        forces = torch.tensor([], dtype=torch.double, device=device)

        if if_chg:
            energies_ele = torch.zeros(self.nimages,device=device).double()
            Ele_forces1 = torch.tensor([], dtype=torch.double, device=device)
            Ele_forces2 = torch.tensor([], dtype=torch.double, device=device)

            NN_char_dict = {} # record charges of elements(as key of dict)
            chem_num_dict = {} # record the accumulated number of certain element types

            # Alan-chg: record the info of mean pred_energies in ONE BATCH
            char_percent = {
                'Ewald_tot': 0,
                'Ereal': 0,
                'Epoint': 0,
                'Erecip': 0,
                'Echarged': 0,
                'Escreen': 0,
                'Es': 0,
                'Epercent': 0
            }

        for element in self.elements:
            # print('test', element,fps[element])
            fp = fps[element].requires_grad_()
            x = self.actF(self.nn_models[element]["inputLayer"](fp))
            # x = self.actF_1(self.nn_models[element]['inputLayer'](fp))
            x = x.to(device)
            if self.debug == True:
                hl_count = 0
                dead_hid = {}
                dead_hid["element"] = element
                dead_in = 0

                f_input = open("input.dat", "a")
                for i in x.detach().numpy():
                    f_input.write("{}\n".format(" ".join(map(str, i))))
                f_input.flush()
                f_input.close()
                f_input_mean = open("input_mean.dat", "a")
                f_input_mean.write(
                    "{}\n".format(
                        " ".join(
                            map(
                                str,
                                np.sum(x.detach().numpy(), axis=0) / self.ntotalAtoms,
                            )
                        )
                    )
                )
                f_input_mean.flush()
                f_input_mean.close()

                if self.activation == "sigmoid":
                    dead_in += np.count_nonzero((x.detach().numpy() < 0.001))
                    dead_in += np.count_nonzero((0.999 < x.detach().numpy()))
                    dead_hid[hl_count] = (dead_in, x.numel())
                    hl_count += 1
                if self.activation == "tanh":
                    dead_in += np.count_nonzero((x.detach().numpy() < -0.999))
                    dead_in += np.count_nonzero((0.999 < x.detach().numpy()))
                    dead_hid[hl_count] = (dead_in, x.numel())
                    hl_count += 1
                if self.activation == "relu" or self.activation == "softplus":
                    dead_in += np.count_nonzero((x.detach().numpy() < 0.0))
                    dead_hid[hl_count] = (dead_in, x.numel())
                    hl_count += 1

            for hd_name in self.hd_names[element]:
                dead_tmp = 0
                x = self.actF(self.nn_models[element][hd_name](x))
                # x = self.actF_2(self.nn_models[element][hd_name](x))
                x = x.to(device)
                if self.debug == True:

                    f_hid = open("{}.dat".format(hd_name), "a")
                    for i in x.detach().numpy():
                        f_hid.write("{}\n".format(" ".join(map(str, i))))
                    f_hid.flush()
                    f_hid.close()
                    f_hid_mean = open("{}_mean.dat".format(hd_name), "a")
                    f_hid_mean.write(
                        "{}\n".format(
                            " ".join(
                                map(
                                    str,
                                    np.sum(x.detach().numpy(), axis=0)
                                    / self.ntotalAtoms,
                                )
                            )
                        )
                    )
                    f_hid_mean.flush()
                    f_hid_mean.close()

                    if self.activation == "sigmoid":
                        dead_tmp += np.count_nonzero((x.detach().numpy() < 0.001))
                        dead_tmp += np.count_nonzero((0.999 < x.detach().numpy()))
                    if self.activation == "tanh":
                        dead_tmp += np.count_nonzero((x.detach().numpy() < -0.999))
                        dead_tmp += np.count_nonzero((0.999 < x.detach().numpy()))
                    if self.activation == "relu" or self.activation == "softplus":
                        dead_in += np.count_nonzero((x.detach().numpy() < 0.0))
                        dead_hid[hl_count] = (dead_in, x.numel())

                    dead_hid[hl_count] = (dead_tmp, x.numel())
                    hl_count += 1

            if self.scalerType == "MinMaxScaler":
                # x = self.actF(self.nn_models[element]['outputLayer'](x))
                x = self.nn_models[element]["outputLayer"](x)
            elif self.scalerType == "LinearScaler":
                if self.adjust:
                    # x = self.actF(self.nn_models[element]['outputLayer'](x))
                    x = self.nn_models[element]["outputLayer"](x)
                else:
                    x = self.nn_models[element]["outputLayer"](x)
            elif self.scalerType == "LogScaler":
                x = self.nn_models[element]["outputLayer"](x)
            elif self.scalerType == "STDScaler":
                x = self.nn_models[element]["outputLayer"](x)
            elif self.scalerType == "NoScaler":
                x = self.nn_models[element]["outputLayer"](x)
            # print('x', x)
            x = x.to(device)

            if self.debug == True:
                f_out = open("output.dat", "a")
                for i in x.detach().numpy():
                    f_out.write("{}\n".format(" ".join(map(str, i))))
                f_out.flush()
                f_out.close()
                f_out_mean = open("output_mean.dat", "a")
                f_out_mean.write(
                    "{}\n".format(
                        " ".join(
                            map(
                                str,
                                np.sum(x.detach().numpy(), axis=0) / self.ntotalAtoms,
                            )
                        )
                    )
                )
                f_out_mean.flush()
                f_out_mean.close()
                logger.info(
                    "%s", "  ".join("{}".format(v) for k, v in dead_hid.items())
                )

            self.imageIndices[element] = self.imageIndices[element].to(device)

            if if_chg:
                x_out = x[:, 0].view(-1, 1) # Es
            else:
                x_out = x

            energies = torch.add(
                energies,
                torch.zeros(self.nimages, 1, device=device)
                .double()
                .scatter_add_(0, self.imageIndices[element], x_out),
            )

            # TODO: training in batchs, it maybe better to define torch.tensor() ahead of time
            # x.backward(torch.tensor([[1.0]]*self.natomsPerElement[element]), retain_graph=True)
            if self.forceTraining:
                (dedg,) = torch.autograd.grad(
                    energies,
                    fp,
                    grad_outputs=energies.data.new(energies.shape).fill_(1.0),
                    retain_graph=True,
                    create_graph=True,
                )
                fp_d[element] = fp_d[element].to(device)
                currforces = torch.sum(
                    torch.mul(
                        fp_d[element], torch.flatten(dedg)[self.dEdg_AtomIndices[element]]
                    ),
                    1,
                )
                forces = torch.cat([forces, currforces])

            if if_chg:
                '''# NOTE: 
                # x[:, 0] = all atomic energy of this element type in this batch; 
                # x[:, 1] = all charges of this element type in this batch'''
                NN_char_dict[element] = x[:, 1].view(-1, 1) 
                chem_num_dict[element] = 0 # use for define which image in 'charges' variable

        if if_chg:
            # for record charge.log
            char_log_dict = dict()
            for element in self.elements:
                char_log_dict[element] = torch.tensor([], dtype=torch.double, device=device)
                
            image_num = len(batch_ele[0])

            # the sequence is the same as the acfs
            for image_index in range(image_num):
                ewald_FPs = batch_ele[0][image_index]

                # NOTE: update _oxi_states
                ewald_FPs.set_oxi_states(NN_char_dict, chem_num_dict)

                # Update the chem_num_dict in each image ( atom numbers in elements)
                for element in self.elements:
                    chem_num_dict[element] += ewald_FPs.chem_num_dict[element]

                # NOTE: Compute the correction for a charged cell
                tuned_oxi_states = ewald_FPs.set_net_charge_energy()
                
                # for record charge.log
                for element in self.elements:
                    char_log_dict[element] = tuned_oxi_states[np.array(ewald_FPs.chem_symbol) == element]
                    
                '''
                **Eele**: Add the energies of same type of 
                element in the same image together
                '''

                ewald_tot = ewald_FPs.total_energy
                energies_ele[image_index] = ewald_tot

                '''
                # NOTE: dE/dr|q = -qi*qj/r2
                '''
                # curr_Eleforces1 = ewald_FPs.f_recip
                # curr_Eleforces1 = ewald_FPs.f_real
                curr_Eleforces1 = ewald_FPs.f_recip + ewald_FPs.f_real

                # TODO: assure the Ele_force index correspond force index!
                # seems like they are all in the sequence of Atoms.get_ChemSymbols()
                Ele_forces1 = torch.cat([Ele_forces1, curr_Eleforces1])

                char_percent['Erecip'] += ewald_FPs.reciprocal_space_energy
                char_percent['Ereal'] += ewald_FPs.real_space_energy
                char_percent['Epoint'] += ewald_FPs.point_energy
                char_percent['Escreen'] += 0
                char_percent['Echarged'] += ewald_FPs._charged_cell_energy
                
                # print('Ewald_tot = {:.7f}'.format(ewald_tot))
                # print('Ereal = {:.7f}'.format(ereal))
                # print('Epoint = {:.7f}'.format(epoint))
                # print('Erecip = {:.7f}'.format(erecip))
                # print('Echarged = {:.7f}'.format(echarge))

            '''
            # NOTE: dE/dq|r * dq/dg * dg/dr
            '''
            for element in self.elements:
                fp = fps[element]
                deledg, = torch.autograd.grad(energies_ele, fp, 
                                            grad_outputs=energies_ele.data.new(energies_ele.shape).fill_(1.0),
                                            retain_graph=True,
                                            create_graph=True)
                fp_d[element] = fp_d[element].to(device) 
                curr_Eleforces2 = torch.sum(torch.mul(fp_d[element],
                                            torch.flatten(deledg)[self.dEdg_AtomIndices[element]]), 1)

                Ele_forces2 = torch.cat([Ele_forces2, curr_Eleforces2])

            # Record 'Es', 'Ewald_tot', 'Epercent' key (no require_grad)
            Es_temp = energies.clone().detach().flatten()
            Ewald_tot_temp = energies_ele.clone().detach().flatten()
            char_percent['Epercent'] = torch.sum((Ewald_tot_temp/(Ewald_tot_temp + Es_temp))) * 100
            char_percent['Es'] = torch.sum(Es_temp) 
            char_percent['Ewald_tot'] = torch.sum(Ewald_tot_temp) 

            # take average value over this batch (with image_num images)
            for key in char_percent.keys():
                char_percent[key] = torch.tensor([char_percent[key] / image_num])

        if self.scalerType == "MinMaxScaler":
            energies = torch.flatten(energies)
            if self.forceTraining:
                forces = torch.mul(
                    torch.zeros(
                        [self.ntotalAtoms, 3], dtype=torch.double, device=device
                    ).scatter_add_(0, self.force_AtomIndices, forces),
                    -self.slope,
                )
            # forces = (forces - self.fc_intercept)/self.slope
            if self.adjust:
                energies = torch.mul(energies, self.slope) + self.intercept
            elif self.forceTraining:
                forces = (forces - self.fc_intercept) / self.slope
        elif self.scalerType == "LinearScaler":
            energies = torch.flatten(torch.mul(energies, self.slope))
            if self.forceTraining:
                forces = torch.mul(
                    torch.zeros(
                        [self.ntotalAtoms, 3], dtype=torch.double, device=device
                    ).scatter_add_(0, self.force_AtomIndices, forces),
                    -self.slope,
                )
            if self.adjust:
                energies = energies + self.intercept
        elif self.scalerType == "NoScaler":
            energies = torch.flatten(torch.mul(energies, self.slope))
            if self.forceTraining:
                forces = torch.mul(
                    torch.zeros(
                        [self.ntotalAtoms, 3], dtype=torch.double, device=device
                    ).scatter_add_(0, self.force_AtomIndices, forces),
                    -self.slope,
                )

            # Alan-chg: total E and F!
            if if_chg:
                energies = torch.add(energies, energies_ele)

                Ele_forces1 = torch.mul(Ele_forces1, self.slope)
                Ele_forces2 = torch.mul(torch.zeros([self.ntotalAtoms, 3], dtype=torch.double, device=device).\
                        scatter_add_(0, self.force_AtomIndices, Ele_forces2), -self.slope)

                forces = forces + Ele_forces1 + Ele_forces2

        elif self.scalerType == "LogScaler":
            energies = torch.flatten(energies)
            expE = torch.exp(batch.eToForces)
            if self.forceTraining:
                forces = torch.mul(
                    torch.zeros(
                        [self.ntotalAtoms, 3], dtype=torch.double, device=device
                    ).scatter_add_(0, self.force_AtomIndices, forces),
                    -self.eRange * expE,
                )
        elif self.scalerType == "STDScaler":
            energies = torch.flatten(energies)
            if self.forceTraining:
                forces = torch.mul(
                    torch.zeros(
                        [self.ntotalAtoms, 3], dtype=torch.double, device=device
                    ).scatter_add_(0, self.force_AtomIndices, forces),
                    -self.ef_coef,
                )

        if if_chg:
            return energies, forces, char_log_dict, char_percent 

        return energies, forces  # , dead_hid

    def parametersDict(self):
        Modelparameters = {}
        Modelparameters["hiddenlayers"] = self.hiddenlayers
        Modelparameters["nFPs"] = self.nFPs
        if self.scalerType in ["LinearScaler", "STDScaler"]:
            Modelparameters["slope"] = self.slope.data.item()
            Modelparameters["intercept"] = self.intercept.data.item()
        Modelparameters["forceTraining"] = self.forceTraining
        Modelparameters["activation"] = self.activation
        Modelparameters["scaler"] = self.scaler
        return Modelparameters

    def toggleForceTraining(self):
        self.forceTraining = (not self.forceTraining)
