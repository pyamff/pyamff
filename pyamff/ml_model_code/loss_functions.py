import torch.nn as nn
import torch


class LossFunction(nn.Module):
    def __init__(
        self,
        loss_type="SE",
        cohe=False,
        energy_coefficient=1.00,
        force_coefficient=0.05,
        device=torch.device("cpu"),
    ):
        super().__init__()
        self.loss_type = loss_type
        self.cohe = cohe
        self.energy_coefficient = energy_coefficient
        self.force_coefficient = force_coefficient
        self.energy_loss = None
        self.force_loss = None
        self.energy_RMSE = None
        self.force_RMSE = None
        self.device = device

    """
    inputs: [energies, forces]
          energies: tensor([E_1, E_2, ..., E_N])
          forces: tensor([f_1, f_2, ..., f_3N])
          N is the total number of atoms in the tranining set
    target: [energy_reference, forces_reference]
    """
    # def set_inputs(self, input1, input2, target1, target2, natomsEnergy, natomsForce):


# This method is global and calculates the mean square error for energy and force.
def calc_mse(input1, input2, target1, target2, n_atoms_energy, n_atoms_force,checkF=True):
    energy_mse = torch.sum(
        torch.pow(torch.div(torch.sub(input1, target1), n_atoms_energy), 2.0)
    )
    if checkF:
        force_mse = (
            torch.sum(
                torch.div(
                    torch.sum(torch.pow(torch.sub(input2, target2), 2.0), dim=1),
                    n_atoms_force,
                )
            )
            / 3.0
        )
    else: force_mse = None
    return energy_mse, force_mse


class MSE(LossFunction):
    """
    MSE is an implementation of the mean squared error loss function.
    """

    def __init__(
        self,
        loss_type="MSE",
        cohe=False,
        energy_coefficient=1.00,
        force_coefficient=0.05,
        device=torch.device("cpu"),
    ):
        super(MSE, self).__init__(
            loss_type, cohe, energy_coefficient, force_coefficient, device
        )

    # This is a forward pass method for calculating the loss.
    def forward(self, input1, input2, target1, target2, n_atoms_energy, n_atoms_force,checkF=True):
        # Send tensors to device (should be no op if it's already there)
        input1 = input1.to(self.device)
        input2 = input2.to(self.device)
        target1 = target1.to(self.device)
        target2 = target2.to(self.device)
        n_atoms_energy = n_atoms_energy.to(self.device)
        n_atoms_force = n_atoms_force.to(self.device)
        
        # Check if calculation is for cohesive energy.
        if self.cohe:
            self.energy_loss = torch.sum(torch.pow(torch.sub(input1, target1), 2.0))
        else:
            self.energy_loss = torch.sum(
                torch.pow(torch.div(torch.sub(input1, target1), n_atoms_energy), 2.0)
            )

        # if not training on forces, then energy_coeff = 1
        if checkF:
            self.force_loss = (
                torch.sum(
                    torch.div(torch.sum(torch.pow(torch.sub(input2, target2), 2.0), dim=1),
                            n_atoms_force)
                    )/3.0
                )
            loss = torch.mul(self.energy_loss, self.energy_coefficient) + torch.mul(self.force_loss, self.force_coefficient)
        else:
            loss = self.energy_loss
        return loss


class SSE(LossFunction):
    """
    SE is an implementation of the sum of squared errors loss function.
    """

    def __init__(
        self,
        loss_type="SE",
        cohe=False,
        energy_coefficient=1.00,
        force_coefficient=0.05,
        device=torch.device("cpu"),
    ):
        super(SSE, self).__init__(
            loss_type, cohe, energy_coefficient, force_coefficient, device
        )

    def forward(self, input1, input2, target1, target2, n_atoms_energy, n_atoms_force,checkF=True):
        # Send tensors to device (should be no op if it's already there)
        input1 = input1.to(self.device)
        input2 = input2.to(self.device)
        target1 = target1.to(self.device)
        target2 = target2.to(self.device)
        n_atoms_energy = n_atoms_energy.to(self.device)
        n_atoms_force = n_atoms_force.to(self.device)

        # Check if calculation is for cohesive energy.
        if self.cohe:
            # loss = sum (predicted - actual)^2
            self.energy_loss = torch.sum(torch.pow(torch.sub(input1, target1), 2.0))
            if checkF:
                self.force_loss = (torch.sum(
                    torch.sum(torch.pow(torch.sub(input2, target2), 2.0), dim=1)
                )) / 3.0
        else:
            # If the calculation is not for cohesive energy, calculate the loss as follows:
            # loss = sum (predicted - actual)^2 / number of atoms
            self.energy_loss = torch.sum(
                torch.pow(torch.div(torch.sub(input1, target1), n_atoms_energy), 2.0)
            )
            if checkF:
                self.force_loss = (
                    torch.sum(
                        torch.div(
                            torch.sum(torch.pow(torch.sub(input2, target2), 2.0), dim=1),
                            n_atoms_force,
                        )
                    )
                    / 3.0
                )
        # if not training on forces, then energy_coeff = 1
        if checkF: 
            loss = torch.mul(self.energy_loss, self.energy_coefficient) + torch.mul(self.force_loss, self.force_coefficient)
        else:
            loss = self.energy_loss
        return loss


class RMLSE(LossFunction):
    def __init__(
        self,
        loss_type="RMLSE",
        energy_coefficient=1.00,
        force_coefficient=0.05,
        device=torch.device("cpu"),
        cohe=None,
    ):
        super(RMLSE, self).__init__(
            loss_type, cohe, energy_coefficient, force_coefficient, device
        )

    def forward(self, input1, input2, target1, target2, n_atoms_energy, n_atoms_force,checkF=True):
        input1 = input1.to(self.device)
        input2 = input2.to(self.device)
        target1 = target1.to(self.device)
        target2 = target2.to(self.device)
        n_atoms_energy = n_atoms_energy.to(self.device)
        n_atoms_force = n_atoms_force.to(self.device)
        # Calculate the loss value for energy and force
        if self.cohe:
            self.energy_loss = torch.sqrt(
                torch.sum(torch.log(torch.pow(torch.sub(input1, target1), 2))),
                )
        else:
            # needs verification. Don't we divide by N before summing? we want units of eV/atom
            self.energy_loss = torch.sqrt(
                torch.div(
                    torch.sum(torch.log(torch.pow(torch.sub(input1, target1), 2))),
                    n_atoms_energy,
                )
            )
        loss = self.energy_loss
        if checkF:
            self.force_loss = torch.sqrt(
                    torch.div(
                        torch.sum(torch.log(torch.pow(torch.sub(input2, target2), 2))),
                        n_atoms_force,
                    )
                )
            loss += torch.mul(self.force_loss, self.force_coefficient)
        # return total loss from this pass by summing energy loss and adjusted force loss
        return loss


class RMSE(LossFunction):
    """
    RMSE is an implementation of the root mean squared error loss function.
    """
    def __init__(
        self,
        loss_type="RMSE",
        cohe=False,
        energy_coefficient=1.00,
        force_coefficient=0.05,
        device=torch.device("cpu")
    ):
        super(RMSE, self).__init__(
            loss_type, cohe, energy_coefficient, force_coefficient, device
        )

        # This is a forward pass method for calculating the loss.
    def forward(self, input1, input2, target1, target2, n_atoms_energy, n_atoms_force,checkF=True):
        # Send tensors to device (should be no op if it's already there)
        input1 = input1.to(self.device)
        input2 = input2.to(self.device)
        target1 = target1.to(self.device)
        target2 = target2.to(self.device)
        n_atoms_energy = n_atoms_energy.to(self.device)
        n_atoms_force = n_atoms_force.to(self.device)

        # Check if calculation is for cohesive energy.
        if self.cohe:
            self.energy_loss = torch.sqrt(torch.sum(torch.pow(torch.sub(input1, target1), 2.0)))
        else:
            self.energy_loss = torch.sqrt(torch.sum(
                torch.pow(torch.div(torch.sub(input1, target1), n_atoms_energy), 2.0)
            ))
        # if not training on forces, then energy_coeff = 1
        if checkF:
            self.force_loss = torch.sqrt((
                torch.sum(
                    torch.div(torch.sum(torch.pow(torch.sub(input2, target2), 2.0), dim=1),
                            n_atoms_force)
                    )/3.0
                ))
            loss = torch.mul(self.energy_loss, self.energy_coefficient) + torch.mul(self.force_loss, self.force_coefficient)
        else:
            loss = self.energy_loss
        return loss


class MAE(LossFunction):
    """
    MAE is an implementation of the mean absolute error loss function.
    """

    def __init__(
        self,
        loss_type="MAE",
        cohe=False,
        energy_coefficient=1.00,
        force_coefficient=0.05,
        device=torch.device("cpu"),
    ):
        super(MSE, self).__init__(
            loss_type, cohe, energy_coefficient, force_coefficient, device
        )

    # This is a forward pass method for calculating the loss.
    def forward(self, input1, input2, target1, target2, n_atoms_energy, n_atoms_force,checkF=True):
        # Send tensors to device (should be no op if it's already there)
        input1 = input1.to(self.device)
        input2 = input2.to(self.device)
        target1 = target1.to(self.device)
        target2 = target2.to(self.device)
        n_atoms_energy = n_atoms_energy.to(self.device)
        n_atoms_force = n_atoms_force.to(self.device)

        # Check if calculation is for cohesive energy.
        if self.cohe:
            self.energy_loss = torch.sum(torch.abs(torch.sub(input1, target1)))
        else:
            self.energy_loss = torch.sum(
                torch.abs(torch.div(torch.sub(input1, target1), n_atoms_energy))
            )
        # if not training on forces, then energy_coeff = 1
        if checkF:
            self.force_loss = (
                torch.sum(
                    torch.div(torch.sum(torch.abs(torch.sub(input2, target2)), dim=1),
                            n_atoms_force)
                    )/3.0
                )

            loss = torch.mul(loss,self.energy_coefficient) + torch.mul(self.force_loss, self.force_coefficient)
        else:
            loss = self.energy_loss
        return loss

