"""
input: text file structured like config.ini but values in a list like
[Main]
epochs_max = [10, 100, 1000]

[MachineLearningModel]
num_hidden_layers = [3, 2]

# can also write this in one line
hidden_layers = [[16,8], # neurons in 1st hidden layer
                 [8,4,2], # neurons in 2nd hidden layer
                 [4] # neurons in 3rd hidden layer
                 ]

new parameters:
    num_hidden_layers
        used for models with only linear layers
        [3, 2]
    layer_types
        used for models with linear and dropout layers - for each configuration num_hidden_layers (linear count) is constant
        make sure to provide enough values in hidden_layers (at least as many values as the sequence with the most linear layers)
        [['linear', 'linear'], ['linear', 'dropout', 'linear']]
    dropout_p
        used for models with linear and dropout layers
        [[0.5, 0.3], # rate for 1st dropout layer (if applicable)
         [0.5, 0.3], # rate for 2nd dropout layer (if applicable)
         [0.4] # rate for 3rd dropout layer (if applicable)
        ]


"""

import ast
import numpy as np
import os
import pathlib
import ase
from ase.io import Trajectory, read, write
import random
import itertools
import sys
import multiprocessing
import shutil
import subprocess
import copy
from pyamff.ase_calc import aseCalc


def read_input(file):
    sections = [
        "RandomSearch",
        "Main",
        "Parallelization",
        "Fingerprints",
        "MachineLearningModel",
        "LossFunction",
        "Optimizer",
        "CrossValidation",
        "OptimizeHyperparameters",
        "Debug",
    ]
    parameters = {sec: {} for sec in sections}
    with open(file) as f:
        lines = f.readlines()
        cont_line = False
        lines = [line.strip() for line in lines]

        for line in lines:

            if line.strip()[1:-1] in sections:
                curr_sec = line.strip()[1:-1]

            # use = to denote new option
            if "=" in line and line[0] != "#" and line[-1] != "," and line[-1] != "[":
                split_line = line.split("=")
                split_line = [l.strip() for l in split_line]
                try:
                    split_line[1] = ast.literal_eval(split_line[1])
                except Exception as e:
                    print(e)
                    print("Error line:", line)
                    sys.exit()
                if isinstance(split_line[1], str):
                    split_line[1] = split_line[1].strip()
                if isinstance(split_line[1], list):
                    if isinstance(split_line[1][0], str):
                        size = len(split_line[1])
                        for i in range(size):
                            split_line[1][i] = split_line[1][i].strip()
                parameters[curr_sec][split_line[0]] = split_line[1]

            elif "=" in line and line[0] != "#" and line[-1] in [",", "["]:
                split_line = line.split("=")
                split_line = [l.strip() for l in split_line]
                parameters[curr_sec][split_line[0]] = split_line[1]
                cont_line = True

            elif cont_line == True and line[0] != "#" and line[-1] == ",":
                parameters[curr_sec][split_line[0]] += line.strip()

            elif cont_line == True and line[0] != "#" and line[-1] != ",":
                try:
                    parameters[curr_sec][split_line[0]] += line.strip()
                    parameters[curr_sec][split_line[0]] = ast.literal_eval(
                        parameters[curr_sec][split_line[0]]
                    )
                    cont_line = False
                except Exception as error:
                    # print(error)
                    parameters[curr_sec][split_line[0]] += "]"
                    parameters[curr_sec][split_line[0]] = ast.literal_eval(
                        parameters[curr_sec][split_line[0]]
                    )
                    cont_line = False

    randomsearch_parameters = parameters["RandomSearch"]
    del parameters["RandomSearch"]

    return randomsearch_parameters, parameters


def test(parameters):
    for sec in parameters:
        print(sec)
        for option in parameters[sec]:
            print(f"\t{option}: {parameters[sec][option]}")


def random_convert(param):
    if isinstance(param, int) or isinstance(param, float):
        return param

    if "random.randint" in param:
        endpoints = param[len("random.randint") :].strip()[1:-1].split(",")
        endpoints = [int(point) for point in endpoints]
        return np.random.randint(endpoints[0], endpoints[1])

    if "random.uniform" in param:
        endpoints = param[len("random.uniform") :].strip()[1:-1].split(",")
        endpoints = [float(point) for point in endpoints]
        return np.random.uniform(endpoints[0], endpoints[1])

    if "random.normal" in param:
        meanstd = param[len("random.normal") :].strip()[1:-1].split(",")
        meanstd = [float(val) for val in meanstd]
        return np.random.normal(meanstd[0], meanstd[1])

    return param


def parslice(param_name, config, limit_name, limit):
    if param_name in config["MachineLearningModel"]:
        param = config["MachineLearningModel"][param_name]
        if isinstance(param, list):
            if isinstance(param[0], str):  # ex ['1 2 3', '4 5 6', '7 8']
                newparam = []
                for s in param:
                    lst = s.split()
                    if len(lst) == limit:
                        newparam.append(" ".join(lst))
                config["MachineLearningModel"][param_name] = newparam
                if newparam == []:
                    print(f"no suitable {param_name} for {limit_name} of {limit}")
                    sys.exit()
            elif isinstance(param[0], list):  # ex [[1, 2], [3, 4], [5]]
                if len(param) < limit:
                    print(f"not enough given {param_name} for {limit_name} of {limit}")
                    sys.exit()
                config["MachineLearningModel"][param_name] = param[:limit]
        elif isinstance(param, str):  # ex '1 2 3'
            lst = param.split()
            if len(lst) != limit:
                print(f"no suitable {param_name} for {limit_name} of {limit}")
                sys.exit()
            config["MachineLearningModel"][param_name] = " ".join(lst)
    return config


def random_config(parameters):
    config = copy.deepcopy(parameters)

    # first check for num_hidden_layers
    if "num_hidden_layers" in config["MachineLearningModel"]:
        param = config["MachineLearningModel"]["num_hidden_layers"]
        if isinstance(param, list):
            num_hidden_layers = random.choice(param)
        else:
            num_hidden_layers = param

        # slice hidden_layers
        config = parslice(
            "hidden_layers", config, "num_hidden_layers", num_hidden_layers
        )
        del config["MachineLearningModel"]["num_hidden_layers"]

    # layer types check
    if "layer_types" in config["MachineLearningModel"]:
        param = config["MachineLearningModel"]["layer_types"]
        if isinstance(param, list):
            layer_types = random.choice(param)
        else:
            layer_types = param

        config["MachineLearningModel"]["layer_types"] = layer_types
        linear_count = layer_types.split().count("linear")
        dropout_count = layer_types.split().count("dropout")

        # slice hidden_layers
        config = parslice("hidden_layers", config, "linear_count", linear_count)

        # slice dropout_p
        config = parslice("dropout_p", config, "dropout_count", dropout_count)
        if "dropout_p" in config["MachineLearningModel"]:
            if config["MachineLearningModel"]["dropout_p"] == []:
                del config["MachineLearningModel"]["dropout_p"]

    for sec in config:
        for option in config[sec]:
            param = config[sec][option]

            # only 1 option
            if isinstance(param, str):
                config[sec][option] = random_convert(param)

            elif isinstance(param, list):
                size = len(param)

                # if list of obj, choose random
                if not isinstance(param[0], list):
                    config[sec][option] = random_convert(
                        random.choice(config[sec][option])
                    )

                if isinstance(param[0], list):
                    # if list of list of obj, choose random from each list
                    for i in range(size):
                        size2 = len(param[i])
                        config[sec][option][i] = random_convert(
                            random.choice(config[sec][option][i])
                        )

    return config


def write_config(config, file):
    with open(file, "w") as f:
        for sec in config:
            # only write nonempty sec
            if config[sec] != {}:
                f.write(f"[{sec}]\n")
                for option in config[sec]:
                    param = config[sec][option]
                    # need to write list in correct format
                    if isinstance(param, list):
                        str_lst = list(map(str, param))
                        str_lst = " ".join(str_lst)
                        f.write(f"{option} = {str_lst}\n")
                    else:
                        f.write(f"{option} = {param}\n")
                f.write("\n")


def create_folds(traj, cv=5):
    traj = Trajectory(traj)
    images = list(traj)
    random.shuffle(images)
    num_images = len(images)
    fold_images = int(num_images / cv)
    folds = []
    for i in range(cv - 1):
        folds.append(images[:fold_images])
        images = images[fold_images:]
    folds.append(images)  # last fold
    for i, fold in enumerate(folds):
        write(f"randomsearch/folds/fold{i}.traj", fold)
    return folds


def create_trainval(folds):
    val = []
    train = []
    for i, fold in enumerate(folds):
        pathlib.Path(f"randomsearch/trainval/fold{i}").mkdir(exist_ok=True)
        v = folds[i]
        t = folds[:i] + folds[i + 1 :]
        t = list(itertools.chain(*t))
        val.append(v)
        train.append(t)
        write(f"randomsearch/trainval/fold{i}/train.traj", t)
        write(f"randomsearch/trainval/fold{i}/val.traj", v)
    return val, train


def setup_iters(parameters, n_iter, cv):
    train_dirs = []
    val_dirs = []
    for i in range(n_iter):
        pathlib.Path(f"randomsearch/iters/iter{i}").mkdir(exist_ok=True)
        config = random_config(parameters)

        for j in range(cv):
            pathlib.Path(f"randomsearch/iters/iter{i}/fold{j}/train").mkdir(
                parents=True, exist_ok=True
            )
            pathlib.Path(f"randomsearch/iters/iter{i}/fold{j}/val").mkdir(
                parents=True, exist_ok=True
            )
            train_path = f"randomsearch/trainval/fold{j}/train.traj"
            val_path = f"randomsearch/trainval/fold{j}/val.traj"
            shutil.copy(
                train_path, f"randomsearch/iters/iter{i}/fold{j}/train/train.traj"
            )
            shutil.copy(val_path, f"randomsearch/iters/iter{i}/fold{j}/val/val.traj")

            write_config(config, f"randomsearch/iters/iter{i}/fold{j}/train/config.ini")
            shutil.copy(
                "fpParas.dat", f"randomsearch/iters/iter{i}/fold{j}/train/fpParas.dat"
            )
            shutil.copy(
                "fpParas.dat", f"randomsearch/iters/iter{i}/fold{j}/val/fpParas.dat"
            )

            train_dirs.append(f"randomsearch/iters/iter{i}/fold{j}/train")
            val_dirs.append(f"randomsearch/iters/iter{i}/fold{j}/val")

    train_dirs = [f"{os.getcwd()}/{dir}" for dir in train_dirs]
    val_dirs = [f"{os.getcwd()}/{dir}" for dir in val_dirs]
    return train_dirs, val_dirs


def run_pyamff(train_dir):
    os.chdir(train_dir)
    result = subprocess.run(["pyamff"], capture_output=True, text=True)
    if result.stderr:
        print(f"Errors from {train_dir}:")
        print(result.stderr)
        return False
    else:
        print(f"No errors from {train_dir}")
        return True


def calc_valid_loss(val_dir, train_dir):
    os.chdir(val_dir)
    pt_path = f"{train_dir}/pyamff.pt"
    shutil.copy(pt_path, f"{val_dir}/pyamff.pt")

    images = read("val.traj", index=":")
    calc = aseCalc("./pyamff.pt")
    calc.model.eval()

    predE = []
    predF = []
    for img in images:
        img.calc = calc
        energy = img.get_potential_energy()
        predE.append(energy)
        force = img.get_forces()
        predF.append(force)

    targetE = []
    targetF = []
    natomsE = []
    natomsF = []
    trajectory = Trajectory("val.traj")
    for image in trajectory:
        energy = image.get_potential_energy()
        targetE.append(energy)
        force = image.get_forces()
        targetF.append(force)
        natomsE.append(len(image))
        # natosmF is n_image x 3?
        natomsF.append(len(image))

    natomsF = np.array(natomsF).reshape(-1, 1)
    energyloss = np.sum(np.power(np.divide(np.subtract(predE, targetE), natomsE), 2.0))
    forceloss = (
        np.sum(
            np.divide(
                np.sum(np.power(np.subtract(predF, targetF), 2.0), axis=1), natomsF
            )
        )
        / 3.0
    )

    print("energy", energyloss)
    print("force", forceloss)

    # use default energy and force coeff from yaml
    energy_coeff = 1.0
    force_coeff = 0.02
    loss = energy_coeff * energyloss + force_coeff * forceloss

    return energyloss, forceloss, loss


def write_bestmodel(sorted_iters, sorted_loss, full_dataset, home):
    os.chdir(home)
    best_iter = sorted_iters[0]
    shutil.copy(
        f"{best_iter}/fold0/train/config.ini", f"randomsearch/bestmodel/config.ini"
    )
    shutil.copy(
        f"{best_iter}/fold0/train/fpParas.dat", f"randomsearch/bestmodel/fpParas.dat"
    )
    write(f"randomsearch/bestmodel/train.traj", full_dataset)

    with open("randomsearch/bestmodel/sorted_model_losses", "w") as f:
        for i in range(len(sorted_iters)):
            f.write(f"{sorted_iters[i]}: {sorted_loss[i]}\n")


def main(parameters_file):
    home = os.getcwd()
    randomsearch_parameters, parameters = read_input(parameters_file)
    cv = randomsearch_parameters["cv"]
    n_iter = randomsearch_parameters["n_iter"]
    traj = randomsearch_parameters["trajectory_file"]
    pathlib.Path("randomsearch").mkdir(exist_ok=True)
    pathlib.Path("randomsearch/folds").mkdir(exist_ok=True)
    pathlib.Path("randomsearch/trainval").mkdir(exist_ok=True)
    pathlib.Path("randomsearch/iters").mkdir(exist_ok=True)
    pathlib.Path("randomsearch/bestmodel").mkdir(exist_ok=True)
    folds = create_folds(traj, cv)
    full_dataset = Trajectory(traj)
    val, train = create_trainval(folds)
    train_dirs, val_dirs = setup_iters(parameters, n_iter, cv)

    for train_dir in train_dirs:
        success = False
        while success != True:
            success = run_pyamff(train_dir)

    loss_lst = []
    for i in range(len(val_dirs)):
        val_dir = val_dirs[i]
        train_dir = train_dirs[i]
        energyloss, forceloss, loss = calc_valid_loss(val_dir, train_dir)
        loss_lst.append(loss)

    average_losses = [np.mean(loss_lst[cv * i : cv * (i + 1)]) for i in range(n_iter)]
    iter_dirs = [f"randomsearch/iters/iter{i}" for i in range(n_iter)]
    sorted_iters = [x for _, x in sorted(zip(average_losses, iter_dirs))]
    sorted_loss = sorted(average_losses)

    write_bestmodel(sorted_iters, sorted_loss, full_dataset, home)


# parameters = read_input('randomsearchinput')
# config = random_config(parameters)
# test(config)
# write_config(config, 'testfile.ini')
if __name__ == "__main__":
    main(parameters_file="randomsearchinput")
