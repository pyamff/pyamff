#!/usr/bin/env python3
import argparse # Eboni added
from pyamff.pyamff_runner import PyAMFFRunner

# Eboni added this section so that we can specify a config.ini file during testing
parser = argparse.ArgumentParser(description="Python Atom-Centered Machine Learning Force Field",
                                prog="pyamff")
parser.add_argument('config_file',type=str,help='INI file with configuration settings (DEFAULT: config.ini)',
                                    default='config.ini',nargs='?')
args = parser.parse_args()

def main():
    # pyamff = PyAMFFRunner()
    pyamff = PyAMFFRunner(config_file=args.config_file)
    pyamff.run()


if __name__ == "__main__":
    main()
