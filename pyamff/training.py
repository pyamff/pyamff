#!/usr/bin/env python
import time, os
import sys, math
import torch
import numpy as np
from collections import OrderedDict
from pyamff.utilities.data_partition import batchGenerator, batchGenerator_ele
from pyamff.utilities.preprocessor import normalize, fetch_prop
from pyamff.utilities import fileIO as io
from pyamff.ml_model_code.loss_functions import LossFunction, calc_mse
from pyamff.ml_model_code.pytorch_nn import NeuralNetwork
from pyamff.utilities.analyze import plotGradFlow
from pyamff.optimizer.lbfgs import LBFGSScipy
from pyamff.optimizer.lbfgs_new import LBFGSNew
from pyamff.optimizer.sd import SD
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.optim.lr_scheduler as lr_scheduler
import random


def avg_gradients(model):
    size = float(dist.get_world_size())
    for param in model.parameters():
        dist.all_reduce(param.grad.data, op=dist.ReduceOp.SUM)
        # param.grad.data /= size


class Trainer:
    """
    The Trainer class is used to train the model.
    """
    def __init__(
        self,
        model,
        criterion: LossFunction = None,
        fpParas=None,
        energyCoefficient: float =1.00,
        forceCoefficient: float =0.1,
        optimizer: str = "LBFGS",
        learningRate: float = 0.1,
        lossConvergence: float = 1e-4,
        energyRMSEtol: float = 0.01,
        forceRMSEtol: float = 0.1,
        lossgradtol: float = 1e-09,
        model_logfile: str = "pyamff.pt",
        logmodel_interval: int = 100,
        test_loginterval: int = 10,
        debug: str = "force",
        weight_decay: float = 0.,
        fpRange=None,
        nImages=None,
        tnImages=None,
        write_final_grads: bool = False,
        scheduler_type: str = "",
        scheduler_params: dict = {},
    ) -> None:
        self.model = model
        self.criterion = criterion
        self.fpParas = fpParas
        self.energyCoefficient = energyCoefficient
        self.forceCoefficient = forceCoefficient
        self.optimizer = optimizer
        self.learningRate = learningRate
        self.lossConvergence = lossConvergence
        self.lossgradtol = lossgradtol
        self.model_logfile = model_logfile
        self.logmodel_interval = logmodel_interval
        self.test_loginterval = test_loginterval
        self.weight_decay = weight_decay
        self.debug = debug
        self.fpRange = fpRange
        self.nImages = nImages
        self.tnImages = tnImages
        self.scaler = model.scaler
        self.scalerType = model.scalerType
        # if self.scalerType in ['MinMaxScaler','LogScaler']:
        #   self.eRange = self.model.eMinMax[1] - self.model.eMinMax[0]
        #   self.minE = self.model.eMinMax[0]
        #   self.fRange = self.model.fMinMax[1] - self.model.fMinMax[0]
        #   self.minF = self.model.fMinMax[0]
        # Variable used to store training info
        self.energyRMSE = None
        self.forceRMSE = None
        self.energy_loss = 0.0
        self.force_loss = 0.0
        self.energyRMSEtol = energyRMSEtol
        self.forceRMSEtol = forceRMSEtol
        self.nIter = 0
        self.running = True
        self.write_final_grads = write_final_grads
        self.scheduler_type = scheduler_type
        self.scheduler_params = scheduler_params
        self.scheduler = None
        self.new_logger_times = 0 # Alan-chg: record the change of partial charges & energies info

    """
    trainingimages: {'hashedkey':Atoms(), ...}
    descriptor: amp.descriptor object. 
               Use descriptor.fingerprints to fetch fingerprint data with
               following structure:
               {'hashedkey': fps for image 1, ...}
               atomSymbols, fpdata = zip(*fps1):
               atomSymbols: a tuple ('Au', 'Au',...,)
               fpdata: a tuple ([G1, G2, ...,] for atom 1, [G1, G2, ...,] for atom 2, ...)
               Use descriptor.fingerprintprimes to fetch corresponding derivatives
    """

    def set_scaler(self, scaler):
        self.scaler = scaler

    # Alan-chg: record the change of partial charges & energies info
    # TODO: output the charge and percent_charge of the test set as the function of Epoch
    def update_logger(self, log, log_dict):
        keys = log_dict.keys()
        # call two times this function means two charge_logger initialized
        if self.new_logger_times != 2: 
            log.info('Starting training')
            lines = '       Epoch  '
            for ele in keys:
                lines += '  {}  '.format(ele)
            log.info(lines)
            self.new_logger_times += 1

        lines = '{:12d}  '.format(self.nIter)
        for key in keys:
            lines += '{:.6f}    '.format(log_dict[key])
        log.info(lines)

    def save_model(self):
        state_dict = self.model.state_dict()
        modelParameters = self.model.parametersDict()
        self.preprocess_paras = {}
        if self.scalerType in ["LinearScaler", "MinMaxScaler", "STDScaler"]:
            # Jiyoung edited: currently, only two scalers work with adjust and scaler.intercept
            # scaler.slope doesn't change
            if self.model.adjust:  # if training slopes and intercepts
                self.preprocess_paras["intercept"] = self.model.intercept
                self.preprocess_paras["slope"] = self.model.slope
            else:  # not training slopes and intercepts
                self.preprocess_paras["intercept"] = self.scaler.intercept
                self.preprocess_paras["slope"] = self.scaler.slope
        if self.scalerType == "NoScaler":
            self.preprocess_paras["intercept"] = self.scaler.intercept
            self.preprocess_paras["slope"] = self.scaler.slope
        if self.scalerType in ["LogScaler"]:
            self.preprocess_paras["energyRange"] = self.model.eMinMax
            # self.preprocess_paras['forceRange'] = self.model.forceRange
        self.preprocess_paras["fpRange"] = self.fpRange
        self.preprocess_paras["fingerprints"] = self.fpParas
        if "dropout" not in self.model.layer_types:
            torch.save(
                {
                    "state_dict": state_dict,
                    "preprocess_paras": self.preprocess_paras,
                    "Modelparameters": modelParameters,
                    "dropoutparameters": {
                        "layer_types": ["linear"] * len(self.model.hiddenlayers),
                        "dropout_p": [-1],
                    },
                },
                self.model_logfile,
            )

        if "dropout" in self.model.layer_types:
            torch.save(
                {
                    "state_dict": state_dict,
                    "preprocess_paras": self.preprocess_paras,
                    "Modelparameters": modelParameters,
                    "dropoutparameters": {
                        "layer_types": self.model.layer_types,
                        "dropout_p": self.model.dropout_p,
                    },
                },
                "pyamff.pt",
            )

    # function to report testRMSE during train_testFF mode
    def testRMSE(self, batches, device, logger, rank, parallel=True, batches_Ele=None,images=None):  

        tenergyloss = 0.0
        tforceloss = 0.0
        tenergyMSE = 0.0
        tforceMSE = 0.0
        tenergylossRMSE = 0.0
        tforcelossRMSE = 0.0
        tloss = 0.0
        tforceRMSE = None

        if batches_Ele is None:
            for batch in batches:
                predEnergies, predForces = self.ddp_model(
                    batch.allElement_fps, batch.dgdx, batch, device, logger=logger
                )
                tloss += self.criterion(
                    predEnergies,
                    predForces,
                    batch.energies,
                    batch.forces,
                    n_atoms_energy=batch.natomsPerImageEnergy,
                    n_atoms_force=batch.natomsPerImageForce,
                    checkF=self.model.forceTraining
                )
                tenergyloss += self.criterion.energy_loss
                if self.model.forceTraining: tforceloss += self.criterion.force_loss

                tenergyMSE_new, tforceMSE_new = self.scaler.calculate_mse(
                    predEnergies,
                    predForces,
                    batch.energies,
                    batch.forces,
                    batch.natomsPerImageEnergy,
                    batch.natomsPerImageForce
                )
                tenergyMSE += tenergyMSE_new
                if self.model.forceTraining: tforceMSE += tforceMSE_new
        # Alan-chg
        else:
            for i, data in enumerate(zip(batches, batches_Ele)):
                # Alan: call remaked forward() function
                batch = data[0]
                batch_ele = data[1]

                predEnergies,  predForces, \
                pred_charges, charge_percent = self.ddp_model(
                    batch.allElement_fps, batch.dgdx,
                    batch,
                    device, logger=logger, batch_ele=batch_ele)

                tloss += self.criterion(predEnergies, predForces, batch.energies, batch.forces,
                                    n_atoms_energy = batch.natomsPerImageEnergy,
                                    n_atoms_force = batch.natomsPerImageForce)

                tenergyloss += self.criterion.energy_loss
                tforceloss += self.criterion.force_loss

                tenergyMSE_new, tforceMSE_new = self.test_scaler.calculate_mse(predEnergies, predForces, batch.energies, batch.forces,
                                                                        batch.natomsPerImageEnergy,
                                                                        batch.natomsPerImageForce)
                tenergyMSE += tenergyMSE_new
                tforceMSE  += tforceMSE_new

        if math.isnan(tenergyloss.item()):
            raise ValueError("energy RMSE is nan")
        if math.isnan(tforceloss.item()):
            raise ValueError("force RMSE is nan")

        if parallel:
            dist.all_reduce(tloss, dist.ReduceOp.SUM)
            dist.all_reduce(tenergyloss, dist.ReduceOp.SUM)
            if self.model.forceTraining: dist.all_reduce(tforceloss, dist.ReduceOp.SUM)
            if self.criterion.loss_type == "SE":
                if images is None: tloss = torch.pow(torch.div(tloss, self.tnImages), 0.5)
                else: tloss = torch.pow(torch.div(tloss, images), 0.5)

            if self.criterion.loss_type != "SE" or self.scalerType not in [
                "LinearScaler",
                "NoScaler",
            ]:
                dist.all_reduce(tenergyMSE, dist.ReduceOp.SUM)
                if self.model.forceTraining: dist.all_reduce(tforceMSE, dist.ReduceOp.SUM)
        if images is None:
            tenergylossRMSE = np.sqrt(tenergyloss.item() / self.tnImages)
            if self.model.forceTraining: 
                tforcelossRMSE = np.sqrt(tforceloss.item() / self.tnImages)

            if self.criterion.loss_type != "SE" or self.scalerType not in [
                "LinearScaler",
                "NoScaler",
            ]:
                tenergyRMSE = np.sqrt(tenergyMSE.item() / self.tnImages)
                if self.model.forceTraining: 
                    tforceRMSE = np.sqrt(tforceMSE.item() / self.tnImages)
            else:
                tenergyRMSE = tenergylossRMSE
                if self.model.forceTraining: 
                    tforceRMSE = tforcelossRMSE
        else:
            tenergylossRMSE = np.sqrt(tenergyloss.item() / images)
            if self.model.forceTraining: 
                tforcelossRMSE = np.sqrt(tforceloss.item() / images)

            if self.criterion.loss_type != "SE" or self.scalerType not in [
                "LinearScaler",
                "NoScaler",
            ]:
                tenergyRMSE = np.sqrt(tenergyMSE.item() / images)
                if self.model.forceTraining: 
                    tforceRMSE = np.sqrt(tforceMSE.item() / images)
            else:
                tenergyRMSE = tenergylossRMSE
                if self.model.forceTraining: 
                    tforceRMSE = tforcelossRMSE

        self.energyRMSE = tenergyRMSE
        if self.model.forceTraining: 
            self.forceRMSE = tforceRMSE

        return tenergyRMSE, tforceRMSE

    def get_loss(self, batches, device, logger, parallel=True, batches_Ele=None,):

        curr_loss = 0.0
        energyloss = 0.0
        forceloss = 0.0
        energyMSE = 0.0
        forceMSE = 0.0
        energylossRMSE = 0.0
        forcelossRMSE = 0.0
        loss = 0.0
        num = 0
        cum_loss = 0.0  # this will collect loss from each batch but will not be used for lossgrads to save time
        time.sleep(0.1)

        #if_chg = (batches_Ele is not None)
        if batches_Ele is not None:
            if_chg = True
        else:
            if_chg = False

        # Alan-chg: update charge.log & percent_charge.log
        if not if_chg:
            for batch in batches:
                # print ('criterion: ',self.criterion) #SE()
                predEnergies, predForces = self.ddp_model(
                    batch.allElement_fps, batch.dgdx, batch, device, logger=logger
                )
                loss = self.criterion(
                    predEnergies,
                    predForces,
                    batch.energies,
                    batch.forces,
                    n_atoms_energy=batch.natomsPerImageEnergy,
                    n_atoms_force=batch.natomsPerImageForce,
                    checkF=self.model.forceTraining
                )
                # print ('+140 loss before loss grad: ',loss)
                lossgrads = torch.autograd.grad(
                    loss, self.model.parameters(), retain_graph=True, create_graph=False
                )
                cum_loss += loss  # accumulate loss for all batches
                for p, g in zip(self.model.parameters(), lossgrads):
                    if num == 0:
                        p.grad = g
                    else:
                        p.grad += g

                num += 1  # this counter checks if its the first batch, then assign g otherwise accumulate g

                energyloss += self.criterion.energy_loss
                if self.model.forceTraining: forceloss += self.criterion.force_loss

                energyMSE_new, forceMSE_new = self.scaler.calculate_mse(
                    predEnergies,
                    predForces,
                    batch.energies,
                    batch.forces,
                    batch.natomsPerImageEnergy,
                    batch.natomsPerImageForce
                )
                energyMSE += energyMSE_new
                if self.model.forceTraining: forceMSE += forceMSE_new
        else: # Alan-chg
            charges_epoch = dict()
            charge_percent_epoch = dict()

            for i, data in enumerate(zip(batches, batches_Ele)):
                batch = data[0]
                batch_ele = data[1]

                predEnergies, predForces, \
                charges, charge_percent = self.ddp_model(
                    batch.allElement_fps, batch.dgdx,
                    batch, 
                    device, 
                    logger=logger,
                    batch_ele=batch_ele)

                if i == 0:
                    for ele in charges.keys():
                        charges_epoch[ele] = charges[ele]
                    for type in charge_percent.keys():
                        charge_percent_epoch[type] = charge_percent[type]
                else:
                    for ele in charges.keys():
                        charges_epoch[ele] = torch.cat((charges_epoch[ele], charges[ele]))
                    for type in charge_percent.keys():
                        charge_percent_epoch[type] = torch.cat((charge_percent_epoch[type], charge_percent[type]))

                loss = self.criterion(predEnergies, predForces, batch.energies, batch.forces,
                                n_atoms_energy = batch.natomsPerImageEnergy,
                                n_atoms_force = batch.natomsPerImageForce)

                #print ('+140 loss before loss grad: ',loss)
                lossgrads = torch.autograd.grad(loss, self.model.parameters(),
                                                retain_graph=True, create_graph=False)

                # print('lossgrads: {}'.format(lossgrads))
                # print('loss: {}'.format(loss))

                cum_loss += loss #accumulate loss for all batches
                for p, g in zip(self.model.parameters(), lossgrads):
                    if num == 0:
                        p.grad = g
                    else:
                        p.grad += g

                num += 1  # this counter checks if its the first batch, then assign g otherwise accumulate g

                energyloss += self.criterion.energy_loss
                forceloss += self.criterion.force_loss

                energyMSE_new, forceMSE_new = self.scaler.calculate_mse(
                    predEnergies,
                    predForces,
                    batch.energies,
                    batch.forces,
                    batch.natomsPerImageEnergy,
                    batch.natomsPerImageForce,
                )

                energyMSE += energyMSE_new
                forceMSE += forceMSE_new

        if parallel:
            avg_gradients(self.ddp_model)
            dist.all_reduce(
                cum_loss, dist.ReduceOp.SUM
            )  # all reduce should be on cum_loss and not loss
            dist.all_reduce(energyloss, dist.ReduceOp.SUM)
            if self.model.forceTraining: dist.all_reduce(forceloss, dist.ReduceOp.SUM)
            if self.criterion.loss_type == "SE":
                cum_loss = torch.pow(torch.div(cum_loss, self.nImages), 0.5)

            if self.criterion.loss_type != "SE" or self.scalerType not in [
                "LinearScaler",
                "NoScaler",
            ]:
                dist.all_reduce(energyMSE, dist.ReduceOp.SUM)
                if self.model.forceTraining: dist.all_reduce(forceMSE, dist.ReduceOp.SUM)
        energylossRMSE = np.sqrt(energyloss.item() / self.nImages)
        if self.model.forceTraining: forcelossRMSE = np.sqrt(forceloss.item() / self.nImages)

        if self.criterion.loss_type != "SE" or self.scalerType not in [
            "LinearScaler",
            "NoScaler",
        ]:
            energyRMSE = np.sqrt(energyMSE.item() / self.nImages)
            if self.model.forceTraining: forceRMSE = np.sqrt(forceMSE.item() / self.nImages)
        else:
            energyRMSE = energylossRMSE
            if self.model.forceTraining: forceRMSE = forcelossRMSE

        self.energyRMSE = energyRMSE
        if self.model.forceTraining: self.forceRMSE = forceRMSE
        # print ('getLoss loss+183 training: ',float(cum_loss.item()))

        if if_chg == False:
            if self.model.forceTraining: 
                return (
                    float(cum_loss.item()),
                    energylossRMSE,
                    forcelossRMSE,
                    energyRMSE,
                    forceRMSE,
                    lossgrads,
                )
            else:
                return (
                    float(cum_loss.item()),
                    energylossRMSE,
                    None,
                    energyRMSE,
                    None,
                    lossgrads,
                )
        else:
            return (
                float(cum_loss.item()),
                energylossRMSE,
                forcelossRMSE,
                energyRMSE,
                forceRMSE,
                lossgrads,
                charges_epoch, 
                charge_percent_epoch
            )

    def parallel_fit(
        self,
        rank,
        size,
        thread,
        partition,
        test_partition,
        batch_number,
        maxEpochs,
        device,
        reportTestRMSE=False,
        logger=None,
        parallel=True,
        charge_logger=None, 
        charge_logger2=None,
        partition_Ele=None,
        testpartition_Ele=None, 
        if_chg=False,
    ):  
        # torch.manual_seed(0)
        # random.seed(a=0)
        # np.random.seed(seed=0)
        # torch.use_deterministic_algorithms(mode=True, warn_only=True)

        # os.environ['MASTER_ADDR'] = '127.0.0.1'
        # os.environ['MASTER_PORT'] = '1234'

        # dist.init_process_group('gloo', init_method='env://', rank=rank, world_size=size)
        self.running = True
        if self.optimizer == "LBFGSScipy":
            pass
        else:
            self.model = self.model.to(device)

        if device == torch.device("cuda:0"):
            self.ddp_model = DDP(self.model, device_ids=[rank])
            n_gpus = torch.cuda.device_count()
        else:
            self.ddp_model = DDP(self.model, device_ids=[])

        torch.manual_seed(1234)
        torch.set_num_threads(thread)
        if parallel:
            batch_size = math.ceil(float(len(partition)) / float(batch_number))
            kwargs = {"num_workers": 0, "pin_memory": True}
            batches = torch.utils.data.DataLoader(
                partition,
                batch_size=batch_size,
                collate_fn=batchGenerator,
                shuffle=False,
                **kwargs
            )
            if if_chg:
                batches_Ele = torch.utils.data.DataLoader(partition_Ele,
                                                batch_size=batch_size,
                                                collate_fn=batchGenerator_ele,
                                                shuffle=False)

            if reportTestRMSE:
                batch_size = math.ceil(float(len(test_partition)) / float(batch_number))
                kwargs = {"num_workers": 0, "pin_memory": True}
                testBatches = torch.utils.data.DataLoader(
                    test_partition,
                    batch_size=batch_size,
                    collate_fn=batchGenerator,
                    shuffle=False,
                    **kwargs
                )
                if if_chg:
                    testBatches_Ele = torch.utils.data.DataLoader(testpartition_Ele,
                                                            batch_size=batch_size,
                                                            collate_fn=batchGenerator_ele,
                                                            shuffle=False,
                                                            **kwargs)
        else:  # I dont know why do we have this else check
            batches = [partition]
            testBatches = [test_partition]
            if if_chg:
                batches_Ele = [partition_Ele]
                testBatches_Ele = [testpartition_Ele]

        # calculate loss and RMSE for initialized model
        # loss, energylossRMSE, forcelossRMSE, energyRMSE, forceRMSE = self.getLoss(batches, device, logger)
        # if rank == 0:
        #    logger.info('%s', "Initial Loss:  {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(loss, energylossRMSE, forcelossRMSE, energyRMSE, forceRMSE))

        # Define Optimizers

        if "LBFGS" in self.optimizer:

            if self.optimizer == "LBFGSScipy":

                optimizer = LBFGSScipy(
                    self.model.parameters(),
                    max_iter=maxEpochs,
                    logger=logger,
                    rank=rank,
                )
                curr_loss = 0.0
                energyloss = 0.0
                forceloss = 0.0
                energylossRMSE = 0.0
                forcelossRMSE = 0.0
                start_time = time.time()
                activation = {}

                def closure():
                    loss = 0
                    energyloss = 0.0
                    forceloss = 0.0
                    energyMSE = 0.0
                    forceMSE = 0.0
                    energylossRMSE = 0.0
                    forcelossRMSE = 0.0
                    batchid = 0
                    if not if_chg:
                        for batch in batches:
                            predEnergies, predForces = self.model(
                                batch.allElement_fps,
                                batch.dgdx,
                                batch,
                                device,
                                logger=logger,
                            )

                            loss += self.criterion(
                                predEnergies,
                                predForces,
                                batch.energies,
                                batch.forces,
                                n_atoms_energy=batch.natomsPerImageEnergy,
                                n_atoms_force=batch.natomsPerImageForce,
                                checkF=self.model.forceTraining
                            )
                            lossgrads = torch.autograd.grad(
                                loss,
                                self.model.parameters(),
                                retain_graph=True,
                                create_graph=False,
                            )

                            for p, g in zip(self.model.parameters(), lossgrads):
                                p.grad = g
                            batchid += 1
                            energyloss += self.criterion.energy_loss
                            if self.model.forceTraining: forceloss += self.criterion.force_loss
                            # Calc RMSE
                            energyMSE_new, forceMSE_new = self.scaler.calculate_mse(
                                predEnergies,
                                predForces,
                                batch.energies,
                                batch.forces,
                                batch.natomsPerImageEnergy,
                                batch.natomsPerImageForce
                            )
                            energyMSE += energyMSE_new
                            if self.model.forceTraining: forceMSE += forceMSE_new
                    else:
                        # Alan-chg: update charge.log & percent_charge.log
                        charges_epoch = dict()
                        charge_percent_epoch = dict()

                        for i, data in enumerate(zip(batches, batches_Ele)):
                            # Alan-chg: call remaked forward() function
                            batch = data[0]
                            batch_ele = data[1]

                            predEnergies, predForces,\
                            charges, charge_percent = self.model(
                                                        batch.allElement_fps, batch.dgdx,
                                                        batch,
                                                        device, 
                                                        logger=logger,
                                                        batch_ele=batch_ele)

                            # initialize two dicts for logging
                            if i == 0:
                                for ele in charges.keys():
                                    charges_epoch[ele] = charges[ele]
                                for type in charge_percent.keys():
                                    charge_percent_epoch[type] = charge_percent[type]
                            else:
                                for ele in charges.keys():
                                    charges_epoch[ele] = torch.cat((charges_epoch[ele], charges[ele]))
                                for type in charge_percent.keys():
                                    charge_percent_epoch[type] = torch.cat((charge_percent_epoch[type], charge_percent[type]))

                            loss += self.criterion(predEnergies, predForces, batch.energies, batch.forces,
                                                n_atoms_energy = batch.natomsPerImageEnergy,
                                                n_atoms_force = batch.natomsPerImageForce)

                            lossgrads = torch.autograd.grad(loss, self.model.parameters(),
                                                            retain_graph=True, create_graph=False)

                            for p, g in zip(self.model.parameters(), lossgrads):
                                p.grad = g
                            batchid += 1
                            energyloss += self.criterion.energy_loss
                            forceloss += self.criterion.force_loss
                            # Calc RMSE
                            energyMSE_new, forceMSE_new = self.scaler.calculate_mse(predEnergies, predForces, batch.energies, batch.forces,
                                                                                    batch.natomsPerImageEnergy,
                                                                                    batch.natomsPerImageForce)
                            energyMSE += energyMSE_new
                            forceMSE += forceMSE_new

                    if parallel:
                        avg_gradients(self.model)
                        dist.all_reduce(loss, dist.ReduceOp.SUM)
                        dist.all_reduce(energyloss, dist.ReduceOp.SUM)
                        if self.model.forceTraining: dist.all_reduce(forceloss, dist.ReduceOp.SUM)
                        if self.criterion.loss_type != "SE" or self.scalerType not in [
                            "LinearScaler",
                            "NoScaler",
                        ]:
                            dist.all_reduce(energyMSE, dist.ReduceOp.SUM)
                            if self.model.forceTraining: dist.all_reduce(forceMSE, dist.ReduceOp.SUM)

                        if self.criterion.loss_type == 'SE': #this cum_loss is only for logging.
                            cum_loss = torch.pow(torch.div(loss.clone().detach(), self.nImages), 0.5) # sqrt(loss/img) so RMSE
                        # this needs double-checking 
                        # cannot only declare cum_loss for a single loss type, or need to rewrite output
                        elif self.criterion.loss_type == 'MSE':
                            cum_loss = torch.pow(torch.div(loss.clone().detach(), self.nImages), 0.5)
                        elif self.criterion.loss_type == 'MAE':
                            cum_loss = torch.div(loss.clone().detach(),self.nImages)
                        else: #RMSE, RMLSE # probably faster to just do torch.div(loss.clone().detach(),self.n)
                            # cum_loss = torch.pow(torch.div(torch.pow(loss.clone().detach(),2.0),self.nImages),0.5)
                            cum_loss = torch.div(loss.clone().detach(),self.nImages**0.5)

                    energylossRMSE = np.sqrt(energyloss.item() / self.nImages)
                    if self.model.forceTraining: forcelossRMSE = np.sqrt(forceloss.item() / self.nImages)

                    if self.criterion.loss_type != "SE" or self.scalerType not in [
                        "LinearScaler",
                        "NoScaler",
                    ]:
                        energyRMSE = np.sqrt(energyMSE.item() / self.nImages)
                        if self.model.forceTraining: forceRMSE = np.sqrt(forceMSE.item() / self.nImages)
                    else:
                        energyRMSE = energylossRMSE
                        if self.model.forceTraining: forceRMSE = forcelossRMSE

                    # Raise error if energy RMSE or/and force RMSE is nan
                    if math.isnan(energyRMSE):
                        raise ValueError("Energy RMSE is nan")
                    if math.isnan(forceRMSE):
                        raise ValueError("Force RMSE is nan")

                    self.energyRMSE = energyRMSE
                    if self.model.forceTraining: self.forceRMSE = forceRMSE

                    # Alan-chg check if testRMSE has to be calculated
                    if reportTestRMSE and self.nIter % self.test_loginterval == 0:
                        teRMSE, tfRMSE = self.testRMSE(testBatches, device, logger, rank, batches_Ele=testBatches_Ele)
                    else: 
                        teRMSE, tfRMSE = False, False

                    # Alan-chg update charge.log & percent_charge.log
                    if rank == 0:
                        if if_chg:
                            # averaged over batches
                            for ele in charges_epoch.keys():
                                charges_epoch[ele] = torch.mean(charges_epoch[ele])
                            for type in charge_percent_epoch.keys():
                                charge_percent_epoch[type] = torch.mean(charge_percent_epoch[type])

                            self.update_logger(charge_logger, charges_epoch)
                            self.update_logger(charge_logger2, charge_percent_epoch)
                        if teRMSE:
                            if self.model.forceTraining: 
                                logger.info('%s', "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(self.nIter, cum_loss, energylossRMSE, forcelossRMSE, energyRMSE, forceRMSE, teRMSE, tfRMSE))
                                print('%12d %12.6f %12.6f %12.6f %12.6f %12.6f' % (self.nIter, cum_loss, energyRMSE, forceRMSE, teRMSE, tfRMSE))
                            else:
                                logger.info('%s', "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(self.nIter, cum_loss, energylossRMSE, energyRMSE, teRMSE))
                                print('%12d %12.6f %12.6f %12.6f' % (self.nIter, cum_loss, energyRMSE, teRMSE))
                        elif self.model.forceTraining: 
                            logger.info('%s', "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(self.nIter, cum_loss, energylossRMSE, forcelossRMSE, energyRMSE, forceRMSE))
                            print('%12d %12.6f %12.6f %12.6f' % (self.nIter, cum_loss, energyRMSE, forceRMSE))
                        else: 
                            logger.info('%s', "{:12d} {:12.8f} {:12.8f} {:12.8f}".format(self.nIter, cum_loss, energylossRMSE, energyRMSE))
                            print('%12d %12.6f %12.6f' % (self.nIter, cum_loss, energyRMSE))
                    self.nIter += 1

                    if (energyRMSE < self.energyRMSEtol and ((self.model.forceTraining and forceRMSE < self.forceRMSEtol) or (not self.model.forceTraining))):
                        logger.info("Minimization converged")
                        if not self.model.forceTraining:
                           self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                        self.save_model()
                        io.saveFF(self.model, self.preprocess_paras, filename="mlff.pyamff")

                    if self.nIter >= maxEpochs: #we don't want the log file to have both Minimizatino Converged and Max Epoch Reached
                        if rank == 0:
                            logger.info('Max Epoch Reached') 
                            if not self.model.forceTraining:
                               self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                            self.save_model()
                            io.saveFF(self.model, self.preprocess_paras, filename="mlff.pyamff")

                    return cum_loss, energylossRMSE, forcelossRMSE, energyRMSE, forceRMSE

                optimizer.step(closure)
                if not self.model.forceTraining:
                   self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                self.save_model()
                io.saveFF(self.model, self.preprocess_paras, filename="mlff.pyamff")
                sys.exit()

            elif self.optimizer == "LBFGS":
                """
                Uses torch LBFGS optimizer. Loss is not averaged over batches.
                """
                optimizer = torch.optim.LBFGS(
                    self.model.parameters(),
                    lr=self.learningRate,  # learning rate
                    tolerance_grad=1e-10,
                    tolerance_change=1e-10,
                    # max_iter=maxEpochs,
                    max_iter=1,
                    # max_eval=None,
                    max_eval=None,
                    history_size=10,
                    line_search_fn=None,  # either 'strong_wolfe' or None
                )

                def closure():
                    curr_loss = 0.0
                    energyloss = 0.0
                    forceloss = 0.0
                    energyMSE = 0.0
                    forceMSE = 0.0
                    energylossRMSE = 0.0
                    forcelossRMSE = 0.0
                    loss = 0.0
                    num = 0
                    cum_loss = 0.0  # this will accumulate the loss while loss will be used for lossgrads
                    optimizer.zero_grad()
                    time.sleep(0.1)  # Naman: oh my god i hate this patch
                    if not if_chg:
                        for batch in batches:
                            # optimizer.zero_grad() # why this is invoked for every batch? we dont make step after every batch, so calling this basically negates lossgrads from previous batch!!

                            predEnergies, predForces = self.ddp_model(
                                batch.allElement_fps,
                                batch.dgdx,
                                batch,
                                device,
                                logger=logger,
                            )
                            loss = self.criterion(
                                predEnergies,
                                predForces,
                                batch.energies,
                                batch.forces,
                                n_atoms_energy=batch.natomsPerImageEnergy,
                                n_atoms_force=batch.natomsPerImageForce,
                                checkF=self.model.forceTraining
                            )
                            cum_loss += loss
                            lossgrads = torch.autograd.grad(
                                loss,
                                self.model.parameters(),
                                retain_graph=True,
                                create_graph=False,
                            )
                            for p, g in zip(self.model.parameters(), lossgrads):
                                if num == 0:
                                    p.grad = g
                                else:
                                    p.grad += g
                            num += 1  # this is not a pretty solution, and we should think of a better way.
                            energyloss += self.criterion.energy_loss
                            if self.model.forceTraining: forceloss += self.criterion.force_loss

                            energyMSE_new, forceMSE_new = self.scaler.calculate_mse(
                                predEnergies,
                                predForces,
                                batch.energies,
                                batch.forces,
                                batch.natomsPerImageEnergy,
                                batch.natomsPerImageForce
                            )

                            energyMSE += energyMSE_new
                            if self.model.forceTraining: forceMSE += forceMSE_new
                            if math.isnan(energyloss.item()):
                                raise ValueError("energy SE is nan")
                            if self.model.forceTraining and math.isnan(forceloss.item()):
                                raise ValueError("force SE is nan")
                            
                    else:
                        # Alan-chg: update charge.log & percent_charge.log
                        charges_epoch = dict()
                        charge_percent_epoch = dict()

                        for i, data in enumerate(zip(batches, batches_Ele)):
                            # Zichen: call remaked forward() function
                            batch = data[0]
                            batch_ele = data[1]

                            predEnergies, predForces,\
                            charges, charge_percent = self.ddp_model(
                                                        batch.allElement_fps, batch.dgdx,
                                                        batch, device, 
                                                        logger=logger, 
                                                        batch_ele=batch_ele)

                            # initialize two dicts for logging
                            if i == 0:
                                for ele in charges.keys():
                                    charges_epoch[ele] = charges[ele]
                                for type in charge_percent.keys():
                                    charge_percent_epoch[type] = charge_percent[type]
                            else:
                                for ele in charges.keys():
                                    charges_epoch[ele] = torch.cat((charges_epoch[ele], charges[ele]))
                                for type in charge_percent.keys():
                                    charge_percent_epoch[type] = torch.cat((charge_percent_epoch[type], charge_percent[type]))

                            loss = self.criterion(
                                predEnergies,
                                predForces,
                                batch.energies,
                                batch.forces,
                                n_atoms_energy=batch.natomsPerImageEnergy,
                                n_atoms_force=batch.natomsPerImageForce,
                            )

                            cum_loss += loss
                            lossgrads = torch.autograd.grad(
                                loss,
                                self.model.parameters(),
                                retain_graph=True,
                                create_graph=False,
                            )

                            for p, g in zip(self.model.parameters(), lossgrads):
                                if num == 0:
                                    p.grad = g
                                else:
                                    p.grad += g
                            num += 1 #this is not a pretty solution, and we should think of a better way.
                            energyloss += self.criterion.energy_loss
                            forceloss += self.criterion.force_loss

                            # Calc RMSE
                            energyMSE_new, forceMSE_new = self.scaler.calculate_mse(
                                predEnergies,
                                predForces,
                                batch.energies,
                                batch.forces,
                                batch.natomsPerImageEnergy,
                                batch.natomsPerImageForce,
                            )

                            energyMSE += energyMSE_new
                            forceMSE  += forceMSE_new
                            if math.isnan(energyloss.item()):
                                raise ValueError('energy SE is nan')
                            if math.isnan(forceloss.item()):
                                raise ValueError('force SE is nan')

                    if parallel:
                        avg_gradients(self.ddp_model)
                        dist.all_reduce(cum_loss, dist.ReduceOp.SUM)
                        dist.all_reduce(energyloss, dist.ReduceOp.SUM)
                        if self.model.forceTraining: dist.all_reduce(forceloss, dist.ReduceOp.SUM)
                        if (
                            self.criterion.loss_type == "SE"
                        ):  # this cum_loss is only for logging.
                            cum_loss = torch.pow(torch.div(cum_loss, self.nImages), 0.5)

                        if self.criterion.loss_type != "SE" or self.scalerType not in [
                            "LinearScaler",
                            "NoScaler",
                        ]:
                            dist.all_reduce(energyMSE, dist.ReduceOp.SUM)
                            if self.model.forceTraining: dist.all_reduce(forceMSE, dist.ReduceOp.SUM)

                    energylossRMSE = np.sqrt(energyloss.item() / self.nImages)
                    if self.model.forceTraining: forcelossRMSE = np.sqrt(forceloss.item() / self.nImages)

                    if self.criterion.loss_type != "SE" or self.scalerType not in [
                        "LinearScaler",
                        "NoScaler",
                    ]:
                        energyRMSE = np.sqrt(energyMSE.item() / self.nImages)
                        if self.model.forceTraining: forceRMSE = np.sqrt(forceMSE.item() / self.nImages)
                    else:
                        energyRMSE = energylossRMSE
                        if self.model.forceTraining: forceRMSE = forcelossRMSE

                    self.energyRMSE = energyRMSE
                    if self.model.forceTraining: self.forceRMSE = forceRMSE

                    # check if testRMSE has to be calculated
                    if reportTestRMSE and self.nIter % self.test_loginterval == 0:
                        teRMSE, tfRMSE = self.testRMSE(
                            testBatches, device, logger, rank
                        )
                    else:
                        teRMSE, tfRMSE = False, False
                    if rank == 0:
                        if if_chg:
                            # averaged over batches
                            for ele in charges_epoch.keys():
                                charges_epoch[ele] = torch.mean(charges_epoch[ele])
                            for type in charge_percent_epoch.keys():
                                charge_percent_epoch[type] = torch.mean(charge_percent_epoch[type])

                            self.update_logger(charge_logger, charges_epoch)
                            self.update_logger(charge_logger2, charge_percent_epoch)
                        if teRMSE:
                            if self.model.forceTraining: 
                                logger.info(
                                    "%s",
                                    "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(
                                        self.nIter,
                                        cum_loss,
                                        energylossRMSE,
                                        forcelossRMSE,
                                        energyRMSE,
                                        forceRMSE,
                                        teRMSE,
                                        tfRMSE,
                                    ),
                                )
                                print(
                                    "%12d %12.6f %12.6f %12.6f %12.6f %12.6f"
                                    % (
                                        self.nIter,
                                        cum_loss,
                                        energyRMSE,
                                        forceRMSE,
                                        teRMSE,
                                        tfRMSE,
                                    )
                                )
                            else:
                                logger.info(
                                    "%s",
                                    "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f} ".format(
                                        self.nIter,
                                        cum_loss,
                                        energylossRMSE,
                                        energyRMSE,
                                        teRMSE,
                                    ),
                                )
                                print(
                                    "%12d %12.6f %12.6f %12.6f "
                                    % (
                                        self.nIter,
                                        cum_loss,
                                        energyRMSE,
                                        teRMSE,
                                    )
                                )
                        elif self.model.forceTraining:
                            logger.info(
                                "%s",
                                "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(
                                    self.nIter,
                                    cum_loss,
                                    energylossRMSE,
                                    forcelossRMSE,
                                    energyRMSE,
                                    forceRMSE,
                                ),
                            )
                            print(
                                "%12d %12.6f %12.6f %12.6f"
                                % (self.nIter, cum_loss, energyRMSE, forceRMSE)
                            )
                        else:
                            logger.info(
                                "%s",
                                "{:12d} {:12.8f} {:12.8f} {:12.8f}".format(
                                    self.nIter,
                                    cum_loss,
                                    energylossRMSE,
                                    energyRMSE,
                                ),
                            )
                            print("%12d %12.6f %12.6f"% (self.nIter, cum_loss, energyRMSE))
                    count = 0
                    for i in range(0, len(lossgrads)):
                        if torch.max(torch.abs(lossgrads[i])).item() < self.lossgradtol:
                            count += 1
                    if count == len(lossgrads):
                        if rank == 0:
                            print(" " * 12, "lossgrads are small, stopping training")
                            if not self.model.forceTraining:
                               self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                            self.save_model()
                            io.saveFF(
                                self.model, self.preprocess_paras, filename="mlff.pyamff"
                            )
                        self.running = False
                        logger.info("Minimization converged")
                        if self.write_final_grads == True:
                            final_lossgrads = open("lossgrads_final.txt", "w")
                            final_lossgrads.write("Minimization converved: \n")
                            count = 0
                            for i in range(0, len(lossgrads)):
                                if (
                                    torch.max(torch.abs(lossgrads[i])).item()
                                    < self.lossgradtol
                                ):
                                    count += 1
                                final_lossgrads.write(
                                    "Final Max loss grads for parameter set "
                                    + str(i)
                                    + ": "
                                    + str(torch.max(torch.abs(lossgrads[i])).item())
                                    + "\n"
                                )
                            final_lossgrads.write(
                                "Total Parameter Sets: " + str(len(lossgrads)) + "\n"
                            )
                            final_lossgrads.write("Entire Loss Grad: \n")
                            final_lossgrads.write(str(lossgrads) + "\n")
                            # final_lossgrads.write('\n'+str(loss)+'\n')

                    self.nIter += 1

                    if self.nIter % self.logmodel_interval == 0:
                        # while saving the model, only make rank 0 do it
                        if rank == 0:
                            self.save_model()
                            io.saveFF(
                                self.model, self.preprocess_paras, filename="mlff.pyamff"
                            )
                    if (
                        energyRMSE < self.energyRMSEtol
                        and ((self.model.forceTraining and forceRMSE < self.forceRMSEtol) or (not self.model.forceTraining))
                    ):
                        self.running = False
                        if rank == 0:
                            logger.info("Minimization converged")
                            if not self.model.forceTraining:
                               self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                            self.save_model()
                            io.saveFF(
                                self.model, self.preprocess_paras, filename="mlff.pyamff"
                            )

                    if (
                        self.nIter >= maxEpochs and self.running == True
                    ):  # we don't want the log file to have both Minimizatino Converged and Max Epoch Reached
                        self.running = False
                        if rank == 0:
                            logger.info("Max Epoch Reached")
                            print(" " * 12, "Max Epoch Reached")
                            if not self.model.forceTraining:
                               self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                            self.save_model()
                            io.saveFF(
                                self.model, self.preprocess_paras, filename="mlff.pyamff"
                            )
                        if self.write_final_grads == True:
                            final_lossgrads = open("lossgrads_final.txt", "w")
                            final_lossgrads.write("Max Epochs Reached: \n")
                            count = 0
                            for i in range(0, len(lossgrads)):
                                if (
                                    torch.max(torch.abs(lossgrads[i])).item()
                                    < self.lossgradtol
                                ):
                                    count += 1
                                final_lossgrads.write(
                                    "Final Max loss grads for parameter set "
                                    + str(i)
                                    + ": "
                                    + str(torch.max(torch.abs(lossgrads[i])).item())
                                    + "\n"
                                )
                            final_lossgrads.write(
                                "Total Parameter Sets: " + str(len(lossgrads)) + "\n"
                            )
                            final_lossgrads.write("Entire Loss Grad: \n")
                            final_lossgrads.write(str(lossgrads))
                            final_lossgrads.write("\n" + str(loss) + "\n")
                    # make only rank 0 report the update to optimizer
                    if rank != 0:
                        optimizer.zero_grad()
                        # for p in self.model.parameters():
                        #    p.grad = None

                    return float(cum_loss.item())
                
                # Naman made this less than zero, if someone wants to restart the model and check if it restarted from the same spot
                if (maxEpochs < 0):  
                    self.running = False

                while self.running:
                    optimizer.step(closure)

        else:
            if self.optimizer == "SGD":
                """
                Uses torch SGD optimizer. Loss is not averaged over batches in order to save memory.
                """

                optimizer = torch.optim.SGD(
                    self.model.parameters(),
                    lr=self.learningRate,
                    weight_decay=self.weight_decay,
                )

            elif self.optimizer == "ADAM":
                """
                Uses torch ADAM optimizer. Loss is not averaged over batches in order to save memory.
                """
                optimizer = torch.optim.Adam(
                    self.model.parameters(),
                    lr=self.learningRate,
                    weight_decay=self.weight_decay,
                )

            elif self.optimizer == "Rprop":
                """
                Uses torch Rprop optimizer. Loss is not averaged over batches in order to save memory.
                """
                optimizer = torch.optim.Rprop(
                    self.model.parameters(), lr=self.learningRate
                )
            # set up the scheduler if there is one
            if self.scheduler_type:
                self.set_scheduler(optimizer)
                scheduler = self.scheduler
            else:
                scheduler = None
            # torch.cuda.set_device(device)
            if maxEpochs <= 0:
                self.running = False

            while self.running:

                if rank == 0:
                    optimizer.step()

                self.nIter += 1
                optimizer.zero_grad()
                if not if_chg:
                    (
                        loss,
                        energylossRMSE,
                        forcelossRMSE,
                        energyRMSE,
                        forceRMSE,
                        lossgrads,
                    ) = self.get_loss(batches, device, logger)
                else:
                    (
                        loss,
                        energylossRMSE,
                        forcelossRMSE,
                        energyRMSE,
                        forceRMSE,
                        lossgrads,
                        charges_epoch,
                        charge_percent_epoch,
                    ) = self.get_loss(batches, device, logger, batches_Ele=batches_Ele)
                if scheduler is not None:
                    # Make a step on the scheduler, if it is in use.
                    if isinstance(scheduler, lr_scheduler.ReduceLROnPlateau):
                        # ReduceLROnPlateau needs to have the loss value to determine if a plateau is occurring
                        scheduler.step(loss)
                    else:
                        # Everything else just goes by the epoch
                        scheduler.step()

                if reportTestRMSE and (self.nIter - 1) % self.test_loginterval == 0:
                    teRMSE, tfRMSE = self.testRMSE(testBatches, device, logger, rank)
                else:
                    teRMSE, tfRMSE = False, False

                if rank == 0:
                    if if_chg:
                            # averaged over batches
                            for ele in charges_epoch.keys():
                                charges_epoch[ele] = torch.mean(charges_epoch[ele])
                            for type in charge_percent_epoch.keys():
                                charge_percent_epoch[type] = torch.mean(charge_percent_epoch[type])
                            
                            self.update_logger(charge_logger, charges_epoch)
                            self.update_logger(charge_logger2, charge_percent_epoch)
                    if teRMSE:
                        if self.model.forceTraining:
                            logger.info(
                                "%s",
                                "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(
                                    self.nIter,
                                    loss,
                                    energylossRMSE,
                                    forcelossRMSE,
                                    energyRMSE,
                                    forceRMSE,
                                    teRMSE,
                                    tfRMSE,
                                ),
                            )
                            print(
                                "%12d %12.6f %12.6f %12.6f %12.6f %12.6f"
                                % (self.nIter, loss, energyRMSE, forceRMSE, teRMSE, tfRMSE)
                            )
                        else:
                            logger.info(
                                "%s",
                                "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(
                                    self.nIter,
                                    loss,
                                    energylossRMSE,
                                    energyRMSE,
                                    teRMSE,
                                ),
                            )
                            print(
                                "%12d %12.6f %12.6f %12.6f %12.6f %12.6f"
                                % (self.nIter, loss, energyRMSE, forceRMSE, teRMSE, tfRMSE)
                            )
                    elif self.model.forceTraining:
                        logger.info(
                            "%s",
                            "{:12d} {:12.8f} {:12.8f} {:12.8f} {:12.8f} {:12.8f}".format(
                                self.nIter,
                                loss,
                                energylossRMSE,
                                forcelossRMSE,
                                energyRMSE,
                                forceRMSE,
                            ),
                        )
                        print("%12d %12.6f %12.6f %12.6f"% (self.nIter, loss, energyRMSE, forceRMSE))
                    else:
                        logger.info("%s","{:12d} {:12.8f} {:12.8f} {:12.8f}".format(self.nIter,loss,energylossRMSE,energyRMSE))
                        print("%12d %12.6f %12.6f"% (self.nIter, loss, energyRMSE, forceRMSE))

                # Raise error if energy RMSE or/and force RMSE is nan
                if math.isnan(energyRMSE):
                    raise ValueError("energy RMSE is nan")

                if self.model.forceTraining and math.isnan(forceRMSE):
                    raise ValueError("force RMSE is nan")
                count = 0
                for i in range(0, len(lossgrads)):
                    if torch.max(torch.abs(lossgrads[i])).item() < self.lossgradtol:
                        count += 1
                    # try: #if we want to ever try using the norm of the entire lossgrads as convergence
                    #   if torch.linalg.matrix_norm(lossgrads[i]).item() < self.lossgradtol:
                    #    count += 1
                    # except:
                    #   if torch.linalg.vector_norm(lossgrads[i]).item() < self.lossgradtol:
                    #    count += 1
                    # print ("Max loss grads: ",torch.max(torch.abs(lossgrads[i])).item())
                if count == len(lossgrads):
                    if rank == 0:  # only rank 0 will do the writing
                        print(" " * 12, "lossgrads are small enough")
                        logger.info("Minimizaton converged")
                        if not self.model.forceTraining:
                           self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                        self.save_model()
                        io.saveFF(
                            self.model, self.preprocess_paras, filename="mlff.pyamff"
                        )
                    if self.write_final_grads == True:
                        final_lossgrads = open("lossgrads_final.txt", "w")
                        final_lossgrads.write("Minimization converged: \n")
                        for i in range(0, len(lossgrads)):
                            if (
                                torch.max(torch.abs(lossgrads[i])).item()
                                < self.lossgradtol
                            ):
                                count += 1
                            final_lossgrads.write(
                                "Final Max loss grads for parameter set "
                                + str(i)
                                + ": "
                                + str(torch.max(torch.abs(lossgrads[i])).item())
                                + "\n"
                            )
                        final_lossgrads.write(
                            "Total Parameter Sets: " + str(len(lossgrads)) + "\n"
                        )
                        final_lossgrads.write("Entire Loss Grad: \n")
                        final_lossgrads.write(str(lossgrads))
                    self.running = False

                if self.nIter % self.logmodel_interval == 0:
                    self.save_model()
                    io.saveFF(self.model, self.preprocess_paras, filename="mlff.pyamff")

                if energyRMSE < self.energyRMSEtol and ((self.model.forceTraining and forceRMSE < self.forceRMSEtol) or (not self.model.forceTraining)):
                    if rank == 0:  # only rank 0 will do the writing
                        logger.info("Minimizaton converged")
                        if not self.model.forceTraining:
                           self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                        self.save_model()
                        io.saveFF(self.model, self.preprocess_paras, filename="mlff.pyamff")
                    self.running = False

                if (self.nIter >= maxEpochs and self.running == True):  # rare case if the optimization converged at last epoch
                    self.running = False
                    if rank == 0:
                        logger.info("Max Epoch Reached")
                        print(" " * 12, "Max Epoch Reached")
                        if not self.model.forceTraining:
                           self.check_fit(batches,device,rank,logger,parallel=parallel,testBatches=(testBatches if reportTestRMSE else None))
                        self.save_model()
                        io.saveFF(self.model, self.preprocess_paras, filename="mlff.pyamff")
                    if self.write_final_grads == True:
                        final_lossgrads = open("lossgrads_final.txt", "w")
                        final_lossgrads.write("Max Epochs Reached: \n")
                        count = 0
                        for i in range(0, len(lossgrads)):
                            if (
                                torch.max(torch.abs(lossgrads[i])).item()
                                < self.lossgradtol
                            ):
                                count += 1
                            final_lossgrads.write(
                                "Final Max loss grads for parameter set "
                                + str(i)
                                + ": "
                                + str(torch.max(torch.abs(lossgrads[i])).item())
                                + "\n"
                            )
                        final_lossgrads.write(
                            "Total Parameter Sets: " + str(len(lossgrads)) + "\n"
                        )
                        final_lossgrads.write("Entire Loss Grad: \n")
                        final_lossgrads.write(str(lossgrads) + "\n")
                        # final_lossgrads.write('\n'+str(loss)+'\n')

        dist.destroy_process_group()

    def set_scheduler(self, optimizer):
        """
        This is a setter for the learning rate scheduler.
        This gets the options for each scheduler and sets them accordingly.
        """
        if self.scheduler_type == 'ReduceLROnPlateau':
            self.scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, **self.scheduler_params)
        elif self.scheduler_type == 'StepLR':
            self.scheduler = lr_scheduler.StepLR(optimizer, **self.scheduler_params)
        elif self.scheduler_type == 'CosineAnnealingLR':
            self.scheduler = lr_scheduler.CosineAnnealingLR(optimizer, **self.scheduler_params)
        elif self.scheduler_type == 'ExponentialLR':
            self.scheduler = lr_scheduler.ExponentialLR(optimizer, **self.scheduler_params)
        elif self.scheduler_type == 'MultiStepLR':
            self.scheduler = lr_scheduler.MultiStepLR(optimizer, **self.scheduler_params)
        else:
            self.scheduler = None
    
    # This is used exclusively in the case where forceTraining was initially false
    # it just provides a final test so that we can get the final force RMSE for rank 1
    # NOTE: If using charges, then forceTraining is always true so this function should not be called
    def check_fit(self,batches,device,rank=0,logger=None,parallel=True,testBatches=None):
        #the if is just a sanity check, could just call directly
        if not self.model.forceTraining:
            self.model.toggleForceTraining() # turn on
        # energy values shouldn't change so just get force
        _, tfRMSE = self.testRMSE(batches, device, logger, rank,parallel,images=self.nImages)
        logger.info("%s","Training Data Force RMSE: {:12.8f}".format(tfRMSE))
        print("Training Data Force RMSE = %12.6f"%(tfRMSE))
        if testBatches is not None:
            _, tfRMSE = self.testRMSE(testBatches, device, logger, rank)
            logger.info("%s","Testing Data Force RMSE: {:12.8f}".format(tfRMSE))
            print("Testing Data Force RMSE = %12.6f"%(tfRMSE))
        # undo change we made (if we made one)
        if not self.model.forceTraining:
            self.model.toggleForceTraining() # turn off
