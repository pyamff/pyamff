#!/usr/bin/env python3

from pyamff.config import ConfigClass
from ase.io import Trajectory
from pyamff.utilities.preprocessor import normalize, fetch_prop, Scaler
from pyamff.neighbor_list import NeighborLists
from pyamff.ml_model_code.pytorch_nn import NeuralNetwork
from pyamff.ml_model_code.loss_functions import MSE, SSE, RMSE, RMLSE, MAE
from pyamff.fingerprints.behler_parrinello import represent_BP
from pyamff.utilities.data_partition import (
    Dataset,
    batchGenerator,
    DataPartitioner,
    partitionData,
)
from pyamff.utilities.log_tool import setLogger, writeSysInfo
from pyamff.utilities.preprocessor import normalize_paras
from torch.multiprocessing import Process
from pyamff.training import Trainer
from pyamff.utilities import fileIO as io
from pyamff.fingerprints.fingerprints import Fingerprints
from pyamff.fingerprints.fingerprints_wrapper import *
from pyamff.fingerprints.fingerprints_wrapper import atomCenteredFPs

# from pyamff.cross_validation_helper import k_fold_split,test_split, fetchProp_validation,test_model #test_model, k_fold_split
from pyamff.cross_validation_helper import test_model, train_test_split_new
from collections import OrderedDict
import torch.distributed as dist
import os, sys, time, glob
import torch
import torch.multiprocessing as mp
from numpy.random import randint
import numpy as np
import pickle
import random
import datetime
import warnings

try:
    from pyamff import fmodules

    FMODULES = True
except ModuleNotFoundError:
    print(
        "A compiled fmodules file was not found. Proceeding with Python fingerprints.",
        file=sys.stderr,
    )
    FMODULES = False



def init_processes(
    rank,
    size,
    thread,
    fn,
    partition,
    test_partition,
    batches_per_proc,
    max_epochs,
    device,
    reportTestRMSE,
    master_addr,
    master_port,
    backend,
    partition_Ele,
    testpartition_Ele,
    if_chg,
    logger=None,
    charge_logger=None,
    charge_logger2=None
):
    # Initialize the distributed environment
    # print ('fn: ',fn) #bound method Trainer.parallelFit
    #os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ["MASTER_ADDR"] = master_addr
    # os.environ["GLOO_SOCKET_IFNAME"] = "lo0" # if I remove, can it find the correct one
    # os.environ['MASTER_ADDR'] = 'env://'
    # os.environ['MASTER_PORT'] = str(randint(low=10000, high=99999))
    # os.environ['MASTER_PORT'] = '12355'
    os.environ["MASTER_PORT"] = master_port
    # print('lauching', rank)
    dist.init_process_group(backend, rank=rank, world_size=size)
    #    dist.init_process_group('nccl', rank=rank, world_size=size)
    # fn(rank, size)
    fn(
        rank,
        size,
        thread,
        partition,
        test_partition,
        batches_per_proc,
        max_epochs,
        device,
        reportTestRMSE=reportTestRMSE,
        logger=logger,
        if_chg=if_chg,
        partition_Ele=partition_Ele,
        testpartition_Ele=testpartition_Ele,
        charge_logger=charge_logger,
        charge_logger2=charge_logger2
    )


def init_fp_processes(
    rank,
    size,
    fn,
    num_fingerprints,
    batches_per_proc,
    batch_ids,
    training_images,
    properties,
    existing,
    normalize,
    logger,
    fpDir,
    use_existing,
    test,
    master_addr,
    master_port,
    backend,
    if_chg,
):
    # Initialize the distributed environment.
    # print ("fn : ",fn) #bound method loop images
    # os.environ['MASTER_ADDR'] = '127.0.0.1'
    # print('masterAddr', masterAddr)
    os.environ["MASTER_ADDR"] = master_addr
    #os.environ["GLOO_SOCKET_IFNAME"] = "lo"  # Cancel comment out (jycho)
    # os.environ['MASTER_ADDR'] = 'env://'
    # os.environ['MASTER_PORT'] = '12355'
    os.environ["MASTER_PORT"] = master_port
    # dist.init_process_group(backend, rank=rank, world_size=size)
    timeout = datetime.timedelta(seconds=18000)
    dist.init_process_group("gloo", rank=rank, world_size=size, timeout=timeout)
    fn(
        rank,
        size,
        num_fingerprints,
        batches_per_proc,
        batch_ids,
        training_images,
        properties,
        existing,
        normalize,
        logger,
        fpDir,
        use_existing,
        if_chg=if_chg
    )


class PyAMFFRunner(object):
    def __init__(
        self, active_learning=False, scaler=None, criterion=None,config_file:str=None
    ):  # accept self-defined scaler and loss function for developer
        # Moved this: 
        # Only if we are actually able to read the config files do we create the log files
        # I really don't like empty files
        # self.logger = setLogger()
        # self.charge_logger = setLogger(name='charge', logfile='charge.log')
        # self.charge_logger2 = setLogger(name='percent', logfile='percent_charge.log')
        # writeSysInfo(self.logger)
        # self.logger.info("=======================================================")
        # self.logger.info("Starting a PyAMFF job at %s", time.strftime("%Y-%m-%d %X %Z")) # Eboni changed, orig: %X %x %Z
        # self.logger.info("=======================================================")
        # # Initialize all instance attributes
        self.calc = None
        self.active_learning = active_learning
        self.st = time.time()
        self.cwd = os.getcwd()
        self.criterion = None  # Added by jycho
        # Variables
        self.scaler = None
        # self.trainingImages = None
        self.training_images = None  # Added by jycho
        self.testImages = None  # Added by jycho
        self.properties = None
        self.nImages = None
        self.srcData = None
        self.testData = None  # Added by jycho
        self.nFPs = {}
        self.fpRange = {}  # Added by jycho
        self.fpsDir = None
        self.tfpsDir = None  # Added by jycho
        self.train_test_mode = None  # Added by jycho
        if config_file is None: # Added by Eboni so can specify config file during tests
            self.set_parameters(config_file=(self.cwd + "/config.ini"))
        else:
            self.set_parameters(config_file=os.path.abspath(config_file))

        if self.use_deterministic:
            torch.manual_seed(0)
            random.seed(a=0)
            np.random.seed(seed=0)
            torch.use_deterministic_algorithms(mode=True)

        # if scaler is None:
        #   self.set_scaler()
        # else:
        #   self.scaler = scaler
        # if criterion is None:
        #   self.set_loss()
        # else:
        #   self.criterion = criterion
        # self.model = self.set_MLModel()
        # self.calc = self.set_trainer()
    def setup_logger(self):
        self.logger = setLogger()
        writeSysInfo(self.logger)
        self.logger.info("=======================================================")
        self.logger.info("Starting a PyAMFF job at %s", time.strftime("%Y-%m-%d %X %Z")) # Eboni changed, orig: %X %x %Z
        self.logger.info("=======================================================")
        self.charge_logger = None
        self.charge_logger2 = None

    def run(
        self, epochs_max=None, trajectory=None
    ):  # assign 'epochs_max' 'trajectory' for active learning

        if self.run_type == "train_testFF":  # Added by jycho
            self.train_test_mode = True  # Added by jycho
        else:
            self.train_test_mode = False

        if trajectory is None:
            trajectory = self.trajectory_file

        self.preprocess_data(trajectory)

        self.fpRange, magnitudeScale, interceptScale = (
            self.calc_fingerprints()
        )  # Modified by jycho
        # (self.trainingImages)
        # self.fpParas,
        # self.useExisting,
        # self.fp_dir,
        # self.fp_engine)

        if self.run_type == "fingerprints":  # Added by jycho
            self.logger.info("Fingerprint calculation done")
            print(" " * 12, "Fingerprint calculation done")
            sys.exit()

        if self.criterion is None:  # Added by jycho
                self.set_loss()
        else:
            self.criterion = self.criterion

        # self.losstol = self.forceCoefficient * (self.force_tol * self.nImages) ** 2 \
        #         + self.energyCoefficient* (self.energy_tol * self.nImages) ** 2

        # partitions = self.set_partitions(fpRange, magnitudeScale, interceptScale)
        self.partitions, self.test_partitions = self.set_partitions(
            self.fpRange, magnitudeScale, interceptScale
        )
        
        self.model = self.set_MLModel()  # Added by jycho

        # if self.run_type=='fingerprints':
        #    self.logger.info('Fingerprint calculation done')
        #    sys.exit()

        # setattr(self.model, 'scaler', self.scaler)
        
        self.model.set_scaler(self.scaler)

        self.calc = self.set_trainer()  # Added by jycho

        # setattr(self.calc, 'intercept', intercept)
        setattr(self.calc, "fpRange", self.fpRange)

        setattr(self.calc, "nImages", len(self.training_images))
        # setattr(self.calc, 'tnImages', len(self.trainingImages))
        if self.train_test_mode or self.run_type == "testFF":
            setattr(self.calc, "tnImages", len(self.testImages))

        # Train the NN
        self.logger.info("=======================================================")
        self.logger.info("Starting training")
        if self.train_test_mode == True or self.run_type == "testFF":
            if self.forceTraining:
                head = "{:>12s} {:>14s} {:>12s} {:>12s} {:>12s} {:>12s} {:>12s} {:>12s}".format(
                    "Epoch",
                    "LossValue",
                    "EnergyLoss",
                    "ForceLoss",
                    "EnergyRMSE",
                    "ForceRMSE",
                    "Test EnergyRMSE",
                    "Test ForceRMSE",
                )
            else:
                head = "{:>12s} {:>14s} {:>12s} {:>12s} {:>12s}".format(
                    "Epoch",
                    "LossValue",
                    "EnergyLoss",
                    "EnergyRMSE",
                    "Test EnergyRMSE",
                )
        else:
            if self.forceTraining:
                    head = "{:>12s} {:>14s} {:>12s} {:>12s} {:>12s} {:>12s}".format(
                    "Epoch",
                    "LossValue",
                    "EnergyLoss",
                    "ForceLoss",
                    "EnergyRMSE",
                    "ForceRMSE"
                )
            else:
                    head = "{:>12s} {:>14s} {:>12s} {:>12s}".format(
                    "Epoch",
                    "LossValue",
                    "EnergyLoss",
                    "EnergyRMSE"
                )
        self.logger.info("%s", head)
        if self.train_test_mode == True or self.run_type == "testFF":
            if self.forceTraining:
                print("{:>12s} {:>12s} {:>14s} {:>11s} {:>11s} {:>11s}".format(
                    "Epoch",
                    "CumLoss",
                    "EnergyRMSE",
                    "ForceRMSE",
                    "TestEnergyRMSE",
                    "TestForceRMSE"
                ))
            else:
                print("{:>12s} {:>12s} {:>14s} {:>11s}".format("Epoch","CumLoss","EnergyRMSE","TestEnergyRMSE"))
        else:
            if self.forceTraining:
                print("{:>12s} {:>12s} {:>14s} {:>11s}".format("Epoch","CumLoss","EnergyRMSE","ForceRMSE"))
            else:
                print("{:>12s} {:>12s} {:>14s}".format("Epoch","CumLoss","EnergyRMSE"))

        train_st = time.time()

        mp.set_start_method(self.mp_start_method, force=True)

        reportTestRMSE = False  # Added by jycho

        # test_partitions=partitions #TODO
        if self.train_test_mode == True or self.run_type == "testFF":
            reportTestRMSE = True  # Added and modified by jycho

        if epochs_max is None:
            epochs_max = self.epochs_max

        processes = []

        for rank in range(self.nProc):
            p = Process(
                target=init_processes,
                args=(
                    rank,
                    self.nProc,
                    self.thread_num,
                    # config.config['master_addr'],
                    # mastips[node],
                    # config.config['master_port'],
                    self.calc.parallel_fit,
                    self.partitions.use(rank),
                    self.test_partitions.use(rank),
                    # config.config['batch_number'],
                    self.nBatches,
                    epochs_max,
                    self.device,
                    reportTestRMSE,
                    self.master_addr,
                    self.master_port,
                    self.backend,
                    self.partitions.use_Ele(rank),
                    self.test_partitions.use_Ele(rank),
                    self.if_chg,
                    self.logger,
                    self.charge_logger,
                    self.charge_logger2
                    # 'nccl'
                ),
            )
            p.start()
            processes.append(p)

        for p in processes:
            p.join()

        et = time.time()
        # fmodules.fpcalc.cleanup_ase()

    ###################################################################################################################
    ###################################################################################################################

    # def set_parameters(self):
    def set_parameters(self, config_file="config.ini"):
        #moved check for if file exists here
        # If config file doesn't exist, nothing else should be done
        if not os.path.isfile(config_file):
            print("Specified configuration file %s does not exist" % ''.join(os.path.abspath(config_file)), sys.stderr)
            sys.exit(2)
        # now we can actually create log files
        self.setup_logger()
        
        # self.logger.info("Reading inputs from %s", self.cwd + "/config.ini")
        self.logger.info("Reading inputs from %s", config_file)

        config = ConfigClass()
        config.initialize(config_file=config_file)

        # Main
        self.run_type = config.config["run_type"]
        self.forceTraining = config.config["force_training"]
        self.trajectory_file = config.config["trajectory_file"]
        self.test_trajectory_file = config.config[
            "test_trajectory_file"
        ]  # Added by jycho
        self.useExisting = config.config["fp_use_existing"]
        self.testuseExisting = config.config["fp_test_use_existing"]  # Added by jycho
        self.epochs_max = config.config["epochs_max"]

        # Alan-chg
        self.if_chg = config.config["if_chg"]

        # Fingerprints Paras
        # self.fpParas = config.config['fp_paras'].fp_paras
        self.fp_paras = config.config["fp_paras"]
        fpParas = config.config["fp_paras"].fp_paras
        self.fp_dir = config.config["fp_dir"]
        self.test_fp_dir = config.config["test_fp_dir"]  # Added by jycho
        self.uniq_elements = config.config["fp_paras"].uniq_elements
        self.fp_parameter_file = config.config["fp_parameter_file"]
        self.fp_engine = config.config["fp_engine"]

        for key in fpParas.keys():
            self.nFPs[key] = len(fpParas[key])

        # Scaler type
        self.scalerType = config.config["scaler_type"]
        self.adjust = config.config["adjust"]
        self.cohe = config.config["use_cohesive_energy"]

        if self.cohe:
            cohe = config.config["fp_paras"].refEs
            self.refEs = OrderedDict(zip(self.uniq_elements, cohe))
        else:
            self.refEs = None

        # Machine-learning Model parameters:
        self.loss_type = config.config["loss_type"]
        self.model_type = config.config["model_type"]
        self.hiddenlayers = config.config["hidden_layers"]
        self.activation = config.config["activation_function"]
        self.forceTraining = config.config["force_training"] # second instance
        self.debug = config.config["nn_values"]
        self.init_model_parameters = config.config["init_model_parameters"]
        self.initial_weights = config.config["initial_weights"]
        self.model_path = config.config["model_path"]
        self.restart = config.config["restart"]

        self.layer_types = config.config["layer_types"].split()
        self.dropout_p = config.config["dropout_p"]
        # self.dropout_p=list(map(float, self.dropout_p.split()))
        if "dropout" not in self.layer_types:
            self.layer_types = ["linear"] * len(self.hiddenlayers)
            self.dropout_p = [-1]
        else:
            linear_count = self.layer_types.count("linear")
            if linear_count != len(self.hiddenlayers):
                print(
                    "number of linear layers does not match number of values provided in hiddenlayers"
                )
                if len(self.hiddenlayers) > linear_count:
                    print("taking the first values of hidden layers")
                    self.hiddenlayers = self.hiddenlayers[:linear_count]
                else:
                    sys.exit()
            if self.dropout_p == [-1]:
                self.dropout_p = [0.5] * self.layer_types.count("dropout")
            elif len(self.dropout_p) < self.layer_types.count("dropout"):
                needed = self.layer_types.count("dropout") - len(self.dropout_p)
                self.dropout_p.extend([0.5] * needed)
            elif len(self.dropout_p) > self.layer_types.count("dropout"):
                self.dropout_p = self.dropout_p[: self.layer_types.count("dropout")]

        # Loss function
        self.energyCoefficient = config.config["energy_coefficient"]
        self.forceCoefficient = config.config["force_coefficient"]

        # Optimizer Setup
        self.optimizer = config.config["optimizer_type"]
        self.learningRate = config.config["learning_rate"]
        self.model_logfile = "pyamff.pt"
        self.logmodel_interval = 100
        self.test_loginterval = config.config["test_log_interval"]
        self.weight_decay = config.config["weight_decay"]
        self.write_final_grads = config.config["write_final_grads"]

        # Convergence criterion
        self.energy_tol = config.config["energy_tol"]
        self.force_tol = config.config["force_tol"]
        self.lossgradtol = config.config["loss_grad_tol"]

        # Parallization
        self.nProc = config.config["process_num"]
        self.nBatchPerProc = config.config["batch_num_per_proc"]
        self.nBatches = self.nProc * self.nBatchPerProc
        self.master_addr = config.config["master_addr"]
        self.master_port = config.config["master_port"]
        self.backend = config.config["backend"]
        self.thread_num = config.config["thread_num"]
        self.mp_start_method = config.config["mp_start_method"]
        self.fp_batch_num = config.config["fp_batch_num"]

        useCuda = False
        if config.config["device_type"] == "GPU" and torch.cuda.is_available():
            useCuda = True
        self.device = torch.device("cuda:1" if useCuda else "cpu")

        # Scheduler
        if "lbfgs" in self.optimizer.lower():
            # lbfgs type optimizers do not use schedulers as there is no learning rate
            self.scheduler_type = None
            self.scheduler_params = {}
        else:
            # schedulers are optional, but the config is read as a string, so we should change it to None
            # if we don't want to use a scheduler for when it is created in the training.py code.
            if config.config["scheduler_type"] == "None":
                self.scheduler_type = None
                self.scheduler_params = {}
            else:
                self.scheduler_type = config.config["scheduler_type"]

            # Set scheduler parameters for each type of learning rate scheduler that is implemented.
            if self.scheduler_type == "ReduceLROnPlateau":
                self.scheduler_params = {"patience": config.config["patience"],
                                         "factor": config.config["factor"],
                                         "min_lr": config.config["min_lr"]}
            elif self.scheduler_type == "StepLR":
                self.scheduler_params = {"step_size": config.config["step_size"],
                                         "gamma": config.config["gamma"]}
            elif self.scheduler_type == "MultiStepLR":
                self.scheduler_params = {"milestones": config.config["milestones"],
                                         "gamma": config.config["gamma"]}
            elif self.scheduler_type == "CosineAnnealingLR":
                self.scheduler_params = {"T_max": config.config["t_max"],
                                         "eta_min": config.config["eta_min"]}
            elif self.scheduler_type == "ExponentialLR":
                self.scheduler_params = {"gamma": config.config["gamma"]}
        # Debug:
        self.use_deterministic = config.config["use_deterministic"]

        # Cross validation
        self.cross_validation = config.config["cross_validation"]  # Added by jycho
        self.test_set_size = config.config["test_set_size"]  # Added by jycho
        self.write_train_test = config.config["write_train_test"]  # Added by jycho

        if self.if_chg:
            self.charge_logger = setLogger(name='charge', logfile='charge.log')
            self.charge_logger2 = setLogger(name='percent', logfile='percent_charge.log')
            if not self.forceTraining:
                warnings.warn("Training with charges requires forceTraining to be True",stacklevel=2)
                self.forceTraining = True
        # if not training on forces, then force coefficient must be 0.
        if not self.forceTraining:
            warnings.warn("Training without forces. Loss coefficients for energy and forces will be set to 1.0 and 0.0, respectively.",stacklevel=2)
    ###################################################################################################################

    def set_scaler(self):
        # Define scaler for normalization
        scaler = Scaler(
            scalerType=self.scalerType,
            forceTraining=self.forceTraining,
            activeLearning=False,
            loss_type=self.loss_type,
            cohe=self.cohe,
            device=self.device,
        )

        self.scaler = scaler.set_scaler()

        if self.scalerType in ["NoScaler", "LinearScaler", "MinMaxScaler", "STDScaler"]:
            self.scaler.adjust = self.adjust

        if scaler is None:
            print("Normalization function is not defined!", sys.stderr)
            sys.exit()

    ###################################################################################################################

    def preprocess_data(self, trajectory):

        if isinstance(trajectory, str):

            # self.logger.info('Reading training images from %s' % self.cwd+'/'+trajectory)
            if os.path.isfile(trajectory):
                trajectory = self.trajectory_file  # Added by jycho
                self.logger.info(
                    "Reading training images from %s" % self.cwd + "/" + trajectory
                )  # Added by jycho
                # images = Trajectory(trajectory, 'r')
                self.training_images = Trajectory(trajectory, "r")  # Added by jycho
            else:
                print("Trajectory file %s does not exist" % trajectory, sys.stderr)
                sys.exit(2)

            # Added and modified by jycho
            if (
                os.path.isfile(trajectory) and self.train_test_mode == True
            ) or self.run_type == "testFF":
                trajectory = self.test_trajectory_file
                self.logger.info("Reading test images from %s" % self.cwd + "/" + trajectory)
                self.testImages = Trajectory(trajectory, "r")
            else:
                if (
                    self.train_test_mode
                ):  # User wanted train_test_mode but could not find test trajectory
                    print(
                        "Test trajectory file %s does not exist"
                        % self.test_trajectory_file,
                        sys.stderr,
                    )
                    sys.exit(2)
                else:
                    self.train_test_mode = False
            # Added and modified by jycho

        else:
            self.logger.info("Training images are accepted.")
            # images = trajectory
            self.training_images = trajectory  # Added by jycho

        # Added by jycho
        if self.scaler is None:
            self.set_scaler()
        else:
            self.scaler = scaler

        self.logger.info("Checking and preprocessing training images")

        # Process training data, scaling energy and force: Added by jycho
        # when preprocesssing, need forces regardless of trainig status
        if self.train_test_mode:
            self.testImages, self.testProperties, self.testScaler = fetch_prop(
                self.testImages,
                refEs=self.refEs,
                scaler=self.scaler,
                # forceTraining=self.forceTraining,
            )

        print("%8.2fs: Processing training data" % (time.time() - self.st))
        self.training_images, self.properties, self.trainScaler = fetch_prop(
            self.training_images,
            refEs=self.refEs,
            scaler=self.scaler,
            # forceTraining=self.forceTraining,
        )

        self.nImages = len(self.training_images)

        self.logger.info("  Number of training images: %d", self.nImages)
        print(" " * 12, "Number of training images: %d" % self.nImages)

        if (
            self.train_test_mode or self.run_type == "testFF"
        ):  # Added and modified by jycho
            self.ntestImages = len(self.testImages)
            self.logger.info("  Number of testing images: %d", self.ntestImages)
            print(" " * 12, "Number of testing images: %d" % self.ntestImages)

        # self.trainingImages, self.properties, self.scaler = fetchProp(images,
        #                                                              refEs=self.refEs,
        #                                                              scaler=self.scaler,
        #                                                              forceTraining=self.forceTraining)

        # self.nImages = len(self.trainingImages)
        # self.logger.info('  Number of training images: %d', self.nImages)

    ###################################################################################################################

    # def :_fingerprints(self, trainingImages):
    def calc_fingerprints(self):

        test = None
        self.srcData = list(
            self.training_images.keys()
        )  # training Images. Modified by jycho

        # Added by jycho
        if self.train_test_mode:
            self.testData = list(self.testImages.keys())
        # Added by jycho

        if self.useExisting:
            self.logger.info(
                "Using pre-calculated fingerprints from %s",
                self.cwd + "/" + self.fp_dir,
            )
            print(
                "%8.2fs: Trying to load training fingerprints" % (time.time() - self.st)
            )  # Added by jycho
            fpDir = self.fp_dir
            nfpFiles = len(glob.glob(os.path.join(fpDir, "*"))) - 1

            if nfpFiles != self.nImages:
                print(" " * 12, "Fingerprints were not found")
                self.useExisting = False
            else:
                self.logger.info(
                    "Using pre-calculated fingerprints from %s",
                    self.cwd + "/" + self.fp_dir,
                )
                print(
                    "%8.2fs: Loading training fingerprints" % (time.time() - self.st)
                )  # Added by jycho
                existing = [1] * nfpFiles

        # else:
        if not self.useExisting:  # Modified by jycho
            fpDir = None
            print("%8.2fs: Calculating training fingerprints" % (time.time() - self.st))
            existing = [0] * self.nImages

        if self.active_learning:
            fpDir = self.fp_dir
            if fpDir is None:
                fpsDir = self.cwd + "/fingerprints"
            else:
                fpsDir = fpDir
            nfpFiles = len(glob.glob(os.path.join(fpsDir, "*"))) - 1
            existing = [1] * nfpFiles
            existing.extend([0] * (len(self.srcData) - nfpFiles))
        else:
            existing = [0] * self.nImages

        # Now same check for testing images : Added and modified by jycho
        if self.train_test_mode or self.run_type == "testFF":
            if self.testuseExisting:
                self.logger.info(
                    "Using pre-calculated testing fingerprints from %s",
                    self.cwd + "/" + self.test_fp_dir,
                )
                testfpDir = self.test_fp_dir
                nfpFiles = len(glob.glob(os.path.join(testfpDir, "*"))) - 1

                if nfpFiles != self.ntestImages:
                    print(" " * 12, "Testing fingerprints were not found")
                    self.testuseExisting = False
                else:
                    self.logger.info(
                        "Loading precalculated testing fingerprints from %s",
                        self.cwd + "/" + self.test_fp_dir,
                    )
                    print(
                        "%8.2fs: Loading testing fingerprints" % (time.time() - self.st)
                    )

        if not self.testuseExisting:
            testfpDir = None

            # continue here for regular training fingerprints. We will do testing fingerprints after this
        if self.fp_engine == "Fortran":

            fpcalc = Fingerprints(
                uniq_elements=self.uniq_elements,
                filename=self.fp_parameter_file,
                nfps=self.nFPs,
            )

            if fpDir is None:
                self.fpsDir = self.cwd + "/fingerprints"
            else:
                self.fpsDir = fpDir

            if not os.path.exists(self.fpsDir):
                os.mkdir(self.fpsDir)

            processes = []
            # batches = partitionData(self.srcData, self.nProc)
            # fpRange={}

            test = False

            # Added by jycho
            if (self.run_type == "testFF"):  # This is for testing the model with testing dataset
                test = True  # True because letting loop images know don't write fprange.pckl file
            # Added by jycho

            batches = partitionData(self.srcData, self.nProc)
            fpRange = {}

            print("%8.2fs: Training" % (time.time() - self.st))
            for rank in range(self.nProc):
                p = Process(
                    target=init_fp_processes,
                    args=(
                        rank,
                        self.nProc,
                        fpcalc.loop_images,
                        self.nFPs,
                        self.fp_batch_num,
                        batches[rank],
                        self.training_images,
                        self.properties,
                        existing,  # modified by jycho
                        True,
                        self.logger,
                        self.fpsDir,
                        self.useExisting,
                        test,
                        self.master_addr,
                        self.master_port,
                        self.backend,
                        self.if_chg
                    ),
                )
                p.start()
                processes.append(p)

            for p in processes:
                p.join()

            # Added by jycho
            if (
                test == False
            ):  # For trainFF and train_testFF; for testFF, we will do this after model is loaded
                fname = os.path.join(self.fpsDir, "fprange.pckl")
                with open(fname, "rb") as f:
                    self.fpRange = pickle.load(f)
                self.fpRange, magnitudeScale, interceptScale = normalize_paras(
                    self.fpRange
                )

            if test == True:  # Added by jycho
                self.fpRange, magnitudeScale, interceptScale = normalize_paras(
                    self.fpRange
                )  # Added by jycho (for testFF)

            if self.train_test_mode:
                if testfpDir is None:
                    self.tfpsDir = self.cwd + "/test_fingerprints"
                else:
                    self.tfpsDir = testfpDir
                if not os.path.exists(self.tfpsDir):
                    os.mkdir(self.tfpsDir)

                processes = []
                test = True  # Make sure it does not dump to fpRange.pckl
                batches = partitionData(self.testData, self.nProc)

                for rank in range(self.nProc):
                    p = Process(
                        target=init_fp_processes,
                        args=(
                            rank,
                            self.nProc,
                            fpcalc.loop_images,
                            self.nFPs,
                            self.fp_batch_num,
                            batches[rank],
                            self.testImages,
                            self.testProperties,
                            existing,
                            True,
                            self.logger,
                            self.tfpsDir,
                            self.testuseExisting,
                            test,
                            self.master_addr,
                            self.master_port,
                            self.backend,
                        ),
                    )
                    p.start()
                    processes.append(p)

                for p in processes:
                    p.join()
            # Added by jycho

            # fname = os.path.join(self.fpsDir, 'fprange.pckl')
            # with open(fname, 'rb') as f:
            #    fpRange = pickle.load(f)
            # fpRange, magnitudeScale, interceptScale = normalizeParas(fpRange)

        else:  # this does not contain testFF and train_testFF mode. only use trainFF run_type with python fingerprints
            self.logger.info("Calculating neighborlists")
            fp_st = time.time()
            nl = NeighborLists(cutoff=6.0)

            # nl.calculate(trainingimages, fortran=True) ###ORIGINAL ZB
            nl.calculate(self.training_images, fortran=False)  # ZB
            et = time.time()
            self.logger.info("  Time used: %.2fs", (et - fp_st))

            # Calculate fingerprints

            self.logger.info("Calculating fingerprints")  ###ORIGINAL #ZB
            acfs, self.fpRange, magnitudeScale, interceptScale = represent_BP(
                nl,
                self.nFPs,
                self.training_images,
                self.properties,  # Modified by jycho
                G_paras=self.fp_paras.fp_paras,
                fpfilename=self.srcData,  # ZB
                fortran=False,
                logger=self.logger,
            )  # ZB
            self.logger.info("  Time used: %.2fs", (time.time() - et))

        return self.fpRange, magnitudeScale, interceptScale  # Modified by jycho

    ###################################################################################################################

    def set_loss(self):
        # Define loss function
        # while match cases are great, they do require version 3.10 or greater and some systems still run with 
        # 3.9 as default (e.g. fri-cluster until Dec 2024), so for now, let's keep it as if-case (EMDW)
        # In the future, we can switch back once all the documentation has been updated
        # Also, if-else/match-case blocks are only as good as their default/error case
        forceCoeff = self.forceCoefficient if self.forceTraining else 0.0
        energyCoeff = self.energyCoefficient if self.forceTraining else 1.0
        if self.loss_type == "MSE":
                self.criterion = MSE(
                    cohe=self.cohe,
                    energy_coefficient=energyCoeff,
                    force_coefficient=forceCoeff,
                    device=self.device,
                )

        elif self.loss_type == "SSE":
                self.criterion = SSE(
                    cohe=self.cohe,
                    energy_coefficient=energyCoeff,
                    force_coefficient=forceCoeff,
                    device=self.device,
                )

        elif self.loss_type == "RMLSE":
                self.criterion = RMLSE(
                    cohe=self.cohe,
                    energy_coefficient=energyCoeff,
                    force_coefficient=forceCoeff,
                    device=self.device,
                )
                
        elif self.loss_type == "RMSE":
                self.criterion = RMSE(
                    cohe=self.cohe,
                    energy_coefficient=energyCoeff,
                    force_coefficient=forceCoeff,
                    device=self.device
                )
        else:
            raise TypeError(f"Unknown loss type '{self.loss_type}' given")

        # match self.loss_type:
        #     case "MSE":
        #         self.criterion = MSE(
        #             cohe=self.cohe,
        #             energy_coefficient=self.energyCoefficient,
        #             force_coefficient=self.forceCoefficient,
        #             device=self.device,
        #         )

        #     case "SSE":
        #         self.criterion = SSE(
        #             cohe=self.cohe,
        #             energy_coefficient=self.energyCoefficient,
        #             force_coefficient=self.forceCoefficient,
        #             device=self.device,
        #         )

        #     case "RMLSE":
        #         self.criterion = RMLSE(
        #             cohe=self.cohe,
        #             energy_coefficient=self.energyCoefficient,
        #             force_coefficient=self.forceCoefficient,
        #             device=self.device,
        #         )
        #     case "RMSE":
        #         self.criterion = RMSE(
        #             cohe=self.cohe,
        #             energy_coefficient=self.energyCoefficient,
        #             force_coefficient=self.forceCoefficient,
        #             device=self.device,
        #         )
        #     case _:
        #         raise TypeError(f"Unknown loss type '{self.loss_type}' given")


        if self.forceTraining:
            self.losstol = (
                self.forceCoefficient * (self.force_tol * self.nImages) ** 2
                + self.energyCoefficient * (self.energy_tol * self.nImages) ** 2
            )
        else:
            self.losstol = (self.energy_tol * self.nImages) ** 2

        self.logger.info("  Set loss function  ")  # Added by jycho
        if self.forceTraining: self.logger.info("  Energy coefficient: %f", self.energyCoefficient)
        if self.forceTraining: self.logger.info("  Force coefficent:   %f", self.forceCoefficient)
        self.logger.info("  Energy tolerance:   %f", self.energy_tol)
        if self.forceTraining: self.logger.info("  Force tolerance:    %f", self.force_tol)
        self.logger.info("  Loss tolerance:     %f", self.losstol)  # Modified by jycho


    ###################################################################################################################

    def set_MLModel(self):

        if not self.init_model_parameters:
            with open("saved_model_params.pyamff", "rb") as f:
                params = pickle.load(f)
        else:
            params = None

        # Define the NN model
        self.logger.info("Defining machine-learning model")

        if self.model_type == "neural_network":
            if self.restart:
                self.logger.info("  Model type: neural_network")
                self.logger.info(
                    "  Model structure: %s",
                    " ".join([str(x) for x in self.hiddenlayers]),
                )
                self.logger.info("  Creating model from saved potential")

                # load a model, model_path is pyamff.pt default
                loaded = torch.load(self.model_path)
                modelParameters = loaded["Modelparameters"]
                dropoutparameters = loaded["layer_types"]
                model = NeuralNetwork(
                    hiddenlayers=modelParameters["hiddenlayers"],
                    nFPs=modelParameters["nFPs"],
                    forceTraining=modelParameters["forceTraining"],
                    # slope=modelParameters['slope'],
                    activation=modelParameters["activation"],
                    scaler=modelParameters["scaler"],
                    layer_types=dropoutparameters["layer_types"],
                    dropout_p=dropoutparameters["dropout_p"],
                    debug=self.debug,
                )
                model.load_state_dict(loaded["state_dict"])

            else:
                self.logger.info("  Model type: neural_network")
                self.logger.info(
                    "  Model structure: %s",
                    " ".join([str(x) for x in self.hiddenlayers]),
                )
                model = NeuralNetwork(
                    hiddenlayers=self.hiddenlayers,
                    activation=self.activation,
                    nFPs=self.nFPs,  # TODO
                    forceTraining=self.forceTraining,
                    cohE=self.cohe,
                    # TODO: load pretrained params
                    params=params,
                    scaler=self.scaler,
                    debug=self.debug,
                    initial_weights=self.initial_weights,
                    layer_types=self.layer_types,
                    dropout_p=self.dropout_p,
                    if_chg=self.if_chg,
                    partitions = self.partitions
                )

        else:
            print("Please assign machine-learning model type", sys.stderr)
            sys.exit(2)
        model.share_memory()
        return model

    ###################################################################################################################

    def set_partitions(self, fpRange, magnitudeScale, interceptScale):

        self.logger.info("Parallelization setup:")
        self.logger.info("  Total number of batches:       %d", self.nBatches)
        self.logger.info("  Number of processes:           %d", self.nProc)
        self.logger.info("  Number of batches per process: %d", self.nBatchPerProc)
        processes = []
        # partition_sizes = [1.0 / size for _ in range(size)]
        # partitions = DataPartitioner(datafilename="fps.pckl", sizes =partition_sizes, seed=1234)
        partitions = DataPartitioner(
            srcData=self.srcData,
            fpRange=self.fpRange,  # Modified by jycho
            magnitudeScale=magnitudeScale,
            interceptScale=interceptScale,
            nProc=self.nProc,
            fpDir=self.fpsDir,
            nBatches=self.nBatches,
            device=self.device,
            seed=1234,
            st=self.st,
            useExisting=self.useExisting,
        )
        fpRange = partitions.fpRange  # Modified by jycho

        # Ok we need to make batches for testing dataset: Added and modified by jycho
        if self.train_test_mode or self.run_type == "testFF":
            testpartitions = DataPartitioner(
                srcData=self.testData,
                fpRange=self.fpRange,
                magnitudeScale=magnitudeScale,
                interceptScale=interceptScale,
                nProc=self.nProc,
                fpDir=self.tfpsDir,
                nBatches=self.nBatches,
                device=self.device,
                seed=1234,
                test=self.train_test_mode,
                st=self.st,
                useExisting=self.testuseExisting,
            )
            tnImages = self.ntestImages
            reportTestRMSE = True
        else:
            testpartitions = partitions  # have to send something because parallelfit needs .use(rank) function
            tnImages = self.nImages  # again nothing to do trainFF  and testFF modes
            # tnImages = self.ntestImages
            reportTestRMSE = False

        return partitions, testpartitions
        # return partitions

    ###################################################################################################################

    def set_trainer(self):
        # Define the pyamff training
        self.logger.info("Setup PyAMFF trainer:")
        self.logger.info(f"Training with Forces: {self.forceTraining}")
        # print('%8.2fs: Setup PyAMFF trainer:'%(time.time()-st))
        if (self.train_test_mode == True or self.run_type == "testFF"):  # Added and modified by jycho
            calc = Trainer(
                model=self.model,
                criterion=self.criterion,  # TODO
                optimizer=self.optimizer,
                # TODO: check it can be reloaded
                fpParas=self.fp_paras,
                energyCoefficient=self.energyCoefficient,
                forceCoefficient=self.forceCoefficient,
                lossConvergence=self.losstol,  # Modified by jycho
                energyRMSEtol=self.energy_tol,
                forceRMSEtol=self.force_tol,
                lossgradtol=self.lossgradtol,
                learningRate=self.learningRate,
                model_logfile="pyamff.pt",
                logmodel_interval=100,
                test_loginterval=self.test_loginterval,
                debug=None,
                weight_decay=self.weight_decay,
                fpRange=self.fpRange,  # Modified by jycho
                nImages=self.nImages,  # Modified by jycho
                tnImages=self.ntestImages,  # TODO Modified by jycho
                write_final_grads=self.write_final_grads,
                scheduler_type = self.scheduler_type,
                scheduler_params = self.scheduler_params,
            )
        else:
            calc = Trainer(
                model=self.model,
                criterion=self.criterion,  # TODO
                optimizer=self.optimizer,
                # TODO: check it can be reloaded
                fpParas=self.fp_paras,
                energyCoefficient=self.energyCoefficient,
                forceCoefficient=self.forceCoefficient,
                lossConvergence=self.losstol,  # Modified by jycho
                energyRMSEtol=self.energy_tol,
                forceRMSEtol=self.force_tol,
                lossgradtol=self.lossgradtol,
                learningRate=self.learningRate,
                model_logfile="pyamff.pt",
                logmodel_interval=100,
                test_loginterval=self.test_loginterval,
                debug=None,
                weight_decay=self.weight_decay,
                fpRange=self.fpRange,  # Modified by jycho
                nImages=self.nImages,  # Modified by jycho
                # tnImages=self.ntestImages, #TODO Modified by jycho
                write_final_grads=self.write_final_grads,
                scheduler_type = self.scheduler_type,
                scheduler_params=self.scheduler_params,
            )
        return calc