import torch
import numpy as np

def calcCellNum(nAtoms, pos_car, cell, rcut):

    # Initialize variables
    orth_latt = torch.zeros(3, dtype=torch.float64)
    cross_ab = torch.zeros(3, dtype=torch.float64)
    cross_bc = torch.zeros(3, dtype=torch.float64)
    latt_const = torch.norm(cell, dim=1)
    sort_ids = torch.tensor([0, 1, 2], dtype=torch.long)
    sin_3 = torch.zeros(1, dtype=torch.float64)

    # output
    ncells = torch.tensor(3,dtype = torch.int)
    tncells = torch.tensor(1,dtype = torch.int)

    # Rank lattice constant
    _, sort_ids = torch.sort(latt_const)
    '''    for i in range(3):
        for j in range(3 - i - 1):
            if latt_const[sort_ids[j]] > latt_const[sort_ids[j + 1]]:
                sort_ids[j], sort_ids[j + 1] = sort_ids[j + 1], sort_ids[j]
    '''

    # Calculate cross products and norms
    #print(latt_const,sort_ids, _)
    cross_ab = torch.cross(cell[sort_ids[0].long()], cell[sort_ids[1].long()])
    cross_ab_norm = torch.norm(cross_ab)

    if torch.isclose(cross_ab_norm, torch.tensor(0.0, dtype=torch.float64), atol=1e-8):
        print('Cell is not 3-D')
        exit(1)

    cross_bc = torch.cross(cell[sort_ids[2]], cross_ab)
    cross_bc_norm = torch.norm(cross_bc)

    # calculate orth_latt
    orth_latt[sort_ids[0]] = latt_const[sort_ids[0]]
    orth_latt[sort_ids[1]] = cross_ab_norm / latt_const[sort_ids[0]]

    sin_3 = cross_bc_norm / (cross_ab_norm * latt_const[sort_ids[2]])

    if sin_3 <= 0.05:  # sin(4_degree) = 0.07
        orth_latt[sort_ids[2]] = latt_const[sort_ids[2]]
    else:
        orth_latt[sort_ids[2]] = cross_bc_norm / cross_ab_norm

    # Calculate ncells
    ncells = torch.tensor(torch.floor(2 * rcut / orth_latt) + 1,dtype=int)
    # ncells = torch.tensor(ncells,dtype = torch.int)
    tncells = ncells[0] * ncells[1] * ncells[2]
    tncells = torch.tensor(tncells,dtype = torch.int)

    return ncells, tncells
'''
def toIndex(uniq_elements, nelement, symbols, nAtoms):
    elementDict = dict(zip(uniq_elements,range(1, nelement+1)))

    nsymbols = torch.zeros(nAtoms,dtype=torch.int)
    for i in range(nAtoms):
        try:
            nsymbols[i] = elementDict[symbols[i]]
        except:
            sys.stderr.write('Element %s has no fingerprints defined' % (symbols[i]))
            sys.exit(2)
    #print(elementDict)
    return nsymbols
'''
def genSupercell(nAtoms , pos_car , symbols , cell , ncells , tncells, rcut ):

    # variables
    i_offset = torch.tensor(0, dtype=torch.int)
    id = torch.tensor(0, dtype=torch.int)
    offset_list = torch.zeros(tncells,3, dtype=torch.int)
    offset_vector = torch.zeros(tncells,3, dtype=torch.float64)

    # output
    supercell = torch.zeros(3, 3, dtype=torch.float64)
    supersymbols = torch.zeros(tncells * nAtoms, dtype=torch.int)
    ##print(symbols)
    pool_pos_car = torch.zeros(tncells * nAtoms, 3, dtype=torch.float64).reshape(tncells * nAtoms, 3)
    pool_ids = torch.zeros(tncells * nAtoms, dtype=torch.int)

    #i_offset = 0

    for i in range( ncells[0].item()):  # 对应 Fortran 中的 DO i = 1, ncells(1)
        for j in range(ncells[1].item()):  # 对应 Fortran 中的 DO j = 1, ncells(2)
            for k in range(ncells[2].item()):  # 对应 Fortran 中的 DO k = 1, ncells(3)
                offset_list[i_offset] = torch.tensor([i, j, k ], dtype=torch.int)
                i_offset += 1

    # Calculate offset_vector    
    offset_list = offset_list.double()
    offset_vector = torch.matmul(offset_list, cell)

    # Calculate supercell
    supercell = torch.einsum('ij,i->ij', cell, ncells)

    # Calculate pos_car and symbols for pool
    for i in range(tncells):
        for j in range(nAtoms):
            id_scalar = (i * nAtoms + j).item()  # Convert id tensor to scalar
            #print('sym',symbols[j])
            pool_pos_car[id_scalar] = offset_vector[i] + pos_car[j]
            pool_ids[id_scalar] = j
            supersymbols[id_scalar] = symbols[j]

    return supercell, supersymbols, pool_pos_car, pool_ids


def calcNlist(nAtoms, MAX_NEIGHS, pos_car, cell, symbols, pool_ids, max_npairs, tnAtoms,
              rcut, nelement, forceEngine , tncells, skipNeighList=None):

    # variables
    ghost_indices = torch.zeros(nAtoms, dtype=torch.int)
    pos_dir = torch.zeros(nAtoms, 3, dtype=torch.double)
    car2dir = torch.zeros(3, 3, dtype=torch.double)
    dir2car = torch.zeros(3, 3, dtype=torch.double)
    v_dir = torch.zeros(3, dtype=torch.double)
    v_car = torch.zeros(3, dtype=torch.double)
    v_unit = torch.zeros(3, dtype=torch.double)
    v_car_o = torch.zeros(3, dtype=torch.double)
    v_dir_o = torch.zeros(3, dtype=torch.double)
    dist = torch.tensor(0.0, dtype=torch.double)
    dist2 = torch.tensor(0.0, dtype=torch.double)
    rcut2 = torch.tensor(0.0, dtype=torch.double)
    dist2_o = torch.tensor(0.0, dtype=torch.double)
    i = torch.tensor(0, dtype=torch.int)
    j = torch.tensor(0, dtype=torch.int)
    check = torch.tensor(0, dtype=torch.int)
    natoms_percell = torch.tensor(0, dtype=torch.int)
    n_ghost = torch.tensor(0, dtype=torch.int)

    # output
    pairs = torch.zeros(nAtoms * MAX_NEIGHS, dtype=torch.double)
    num_pairs = torch.tensor(0, dtype=torch.int)
    npairs_incell = torch.tensor(0, dtype=torch.int)
    num_neigh = torch.zeros(nAtoms, dtype=torch.int)
    neighs = torch.zeros(nAtoms, MAX_NEIGHS, dtype=torch.int)
    neighs_incell = torch.zeros(nAtoms, MAX_NEIGHS, dtype=torch.int)
    rmins = torch.full((nelement, nelement), 100.0, dtype=torch.double)
    num_eachpair = torch.zeros(nAtoms, dtype=torch.int)
    pair_indices = torch.zeros(2, max_npairs, dtype=torch.int)
    pair_global_indices = torch.zeros(2, max_npairs, dtype=torch.int)
    unitvects_pair = torch.zeros(2, max_npairs, 3, dtype=torch.double)
    pair_info = torch.zeros(tnAtoms, MAX_NEIGHS, dtype=torch.int)
    gvects = torch.zeros(max_npairs, dtype=torch.int)

    try:
        rcut2 = rcut ** 2
    except ValueError:
        print('Error : Check if cutoff radius is defined')

    natoms_percell = nAtoms // tncells

    # Transpose
    dir2car = cell.t()
    car2dir = torch.inverse(dir2car)

    # Cart ---> direct coordinates
    for i in range(nAtoms):
        pos_dir[i] = torch.matmul(car2dir, pos_car[i])

    # pair items
    num_pairs = 0
    npairs_incell = 0
    num_neigh = torch.zeros(nAtoms, dtype=torch.int32)
    if skipNeighList is None or not skipNeighList:
        neighs = torch.zeros(nAtoms, MAX_NEIGHS, dtype=torch.int32)

    # Initialize rmins
    rmins = torch.full((nelement, nelement), 100.0, dtype=torch.double)

    for i in range(nAtoms):
        for j in range(i + 1, nAtoms):
            v_dir = pos_dir[j] - pos_dir[i]
            v_dir_o = v_dir.clone()

            v_dir = torch.fmod(v_dir + 0.5, 1.0) - 0.5
            v_car = torch.matmul(dir2car, v_dir)
            dist2 = torch.sum(v_car * v_car)

            if dist2 < rcut2:

                dist = torch.sqrt(dist2)
                v_unit = v_car / dist

                # Record minimum distance of two atoms

                i_symbol = symbols[pool_ids[i]]- 1
                j_symbol = symbols[pool_ids[j]] - 1
                if dist < rmins[i_symbol , j_symbol]:
                    if forceEngine == 0:
                        rmins[i_symbol, j_symbol] = dist
                        rmins[j_symbol, i_symbol] = dist
                    elif forceEngine == 1:
                        dist = rmins[i_symbol, j_symbol]

                if i < natoms_percell:
                    npairs_incell += 1

                num_eachpair[i] += 1
                num_eachpair[j] += 1
                if skipNeighList is None or not skipNeighList:

                    neighs[i, num_neigh[i]] = j
                    neighs[j, num_neigh[j]] = i

                neighs_incell[i, num_neigh[i]] = pool_ids[j] 
                neighs_incell[j, num_neigh[j]] = pool_ids[i] 
                pairs[num_pairs] = dist

                num_pairs += 1 # num_pairs = num_pairs + 1 in .F90
                #print('neighs_incell[i, num_neigh[i]]',pool_ids[j])
                #print('neighs_incell[j, num_neigh[j]]',pool_ids[i])

                num_neigh[i] += 1
                num_neigh[j] += 1

                pair_indices[0, num_pairs - 1] = pool_ids[i] 
                pair_indices[1, num_pairs - 1] = pool_ids[j] 
                unitvects_pair[0, num_pairs - 1, :] = v_unit
                unitvects_pair[1, num_pairs - 1 , :] = -v_unit

                pair_info[i, num_eachpair[i] - 1] = num_pairs
                pair_global_indices[0, num_pairs - 1] = i
                pair_global_indices[1, num_pairs - 1] = j
                if torch.any(v_dir * v_dir_o < 0.0):
                    gvects[num_pairs - 1] = -1
                else:
                    gvects[num_pairs - 1] = 1


    return npairs_incell, num_pairs, num_neigh, neighs, neighs_incell, rmins, num_eachpair, pairs, pair_info, pair_global_indices, pair_indices, gvects, unitvects_pair

