import torch
import numpy as np
from fbp import fg1, fdg1, commonparts, calcTriplet, fg2, fdg2, fg2_dg, find_loc_int, find_loc_char
from neighborlist import calcCellNum, genSupercell, calcNlist 


'''
def read_fpParas(filename, nelement, max_rcut):
    # inputs
    filename = str(filename)
    nelement = int(nelement)

    # variables
    cidx, nidx1, nidx2 = 0, 0, 0
    currIndex = 0
    nFPs = 0
    eta, gamma, Rs, zeta, lambda_, thetas, rcut = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    fp_type = ""
    uniq_elements = [''] * nelement
    coeh = torch.zeros(nelement, dtype=torch.double)
    rmins = 100.0 * torch.ones((nelement, nelement), dtype=torch.double)
    fpParas = [{
        'g1s': [{
            'fp_type': "",
            'species1': "",
            'species1_code': 0,
            'species2': "",
            'currIndex': 0,
            'etas':[],
            'gammas': [],
            'zetas': [],
            'rss': [],
            'r_cuts': [],
            'rmins': [],
            'theta_ss': [],
            'fp_maxs': [],
            'fp_mins': [],
            'startpoint': 0,
            'endpoint': 0,
            'nFPs': 0
        } for _ in range(nelement)],
        'g2s': [[{
            'fp_type': "",
            'species1': "",
            'species1_code': 0,
            'species2': "",
            'currIndex': 0,
            'etas': [],
            'gammas': [],
            'lambdas': [],
            'zetas': [],
            'r_cuts': [],
            'rmins':[],
            'theta_ss':[],
            'fp_maxs': [],
            'fp_mins': [],
            'startpoint': 0,
            'endpoint': 0,
            'nFPs': 0
        } for _ in range(nelement)] for _ in range(nelement)],
        'tnFPs': 0,
        'g1_startpoint': 0,
        'g1_endpoint': 0,
        'g2_startpoint': 0,
        'g2_endpoint': 0
    } for _ in range(nelement)]

    #max_rcut = 0.0

    # Read fp Parameters from fpParas.dat
    with open(filename, 'r') as file:
        file.readline()  # Skip a line
        fp_type = file.readline().strip()
        file.readline()  # Skip a line
        uniq_elements = file.readline().split()

        coeh_line = file.readline().split()
        for i in range(nelement):
            coeh[i] = float(coeh_line[i])

        file.readline()  # Skip a line
        nG1, nG2 = map(int, file.readline().split())

        # Rewind the fpParas.dat file
        file.seek(0)

        # Read fp Paras dat again and put data into the right place
        file.readline()  # skip first line
        file.readline()  # skip second line
        file.readline()  # skip third line
        file.readline()  # skip fourth line
        file.readline()  # skip fifth line
        file.readline()  # skip sixth line
        file.readline()  # skip seventh line

        currIndex = 0  # initialize currIndex

        if nG1 > 0:
            currIndex = 0 
            file.readline()  # skip #type
            for i in range(nG1):
                line = file.readline().split()
                G_type, center, neigh1, eta, Rs, rcut = line[0], line[1], line[2], float(line[3]), float(line[4]), float(line[5])

                # Ideally have dynamic rcut for different interactions
                if rcut > max_rcut:
                    if max_rcut > 0.0:
                        print('Error: multiple rcuts are not supported yet. Use a single rcut value')
                        exit()
                    else:
                        max_rcut = rcut

                cidx = find_loc_char(uniq_elements, center, len(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, len(uniq_elements))

                fpParas[cidx]['g1s'][nidx1]['fp_type'] = G_type
                fpParas[cidx]['g1s'][nidx1]['species1'] = center
                fpParas[cidx]['g1s'][nidx1]['species1_code'] = cidx
                fpParas[cidx]['g1s'][nidx1]['species2'] = neigh1               
                
                if (eta and Rs and rcut) is not None:
                    fpParas[cidx]['g1s'][nidx1]['etas'].append(eta)
                    fpParas[cidx]['g1s'][nidx1]['rss'].append(Rs)
                    fpParas[cidx]['g1s'][nidx1]['r_cuts'].append(rcut)
                    fpParas[cidx]['g1s'][nidx1]['nFPs'] += 1
                    fpParas[cidx]['tnFPs'] += 1

                fpParas[cidx]['g1s'][nidx1]['currIndex'] = currIndex + 1
                #fpParas[cidx]['g1s'][nidx1]['currIndex'] = currIndex

                if nidx1 == 0:
                    fpParas[cidx]['g1s'][nidx1]['startpoint'] = 0
                    fpParas[cidx]['g1s'][nidx1]['endpoint'] = fpParas[cidx]['g1s'][nidx1]['endpoint'] + 1
                else:
                    fpParas[cidx]['g1s'][nidx1]['startpoint'] = fpParas[cidx]['g1s'][nidx1 - 1]['endpoint'] 
                    fpParas[cidx]['g1s'][nidx1]['endpoint'] = fpParas[cidx]['g1s'][nidx1]['startpoint'] + fpParas[cidx]['g1s'][nidx1]['nFPs'] # -1 makes sp ep reasonable

        if nG2 > 0:
            file.readline()  # skip #type
            for k in range(nelement):
                fpParas[k]['g1_endpoint'] = fpParas[k]['tnFPs']
                fpParas[k]['g2_startpoint'] = fpParas[k]['g1_endpoint'] 

            for i in range(nG2):
                line = file.readline().split()
                G_type, center, neigh1, neigh2, eta, zeta,  lambda_, thetas, rcut = (
                    line[0], line[1], line[2], line[3], float(line[4]), float(line[5]), float(line[6]), float(line[7]), float(line[8])
                )

                # Ideally have dynamic rcut for different interactions
                if rcut > max_rcut:
                    if max_rcut > 0.0:
                        print('Error: multiple rcuts are not supported yet. Use a single rcut value')
                        exit()
                    else:
                        max_rcut = rcut

                cidx = find_loc_char(uniq_elements, center, len(uniq_elements))
                nidx1 = find_loc_char(uniq_elements, neigh1, len(uniq_elements))
                nidx2 = find_loc_char(uniq_elements, neigh2, len(uniq_elements))

                fpParas[cidx]['g2s'][nidx1][nidx2]['nFPs'] += 1
                if nidx1 != nidx2:
                    fpParas[cidx]['g2s'][nidx2][nidx1]['nFPs'] += 1

                if nidx1 > nidx2:
                    nidx1, nidx2 = nidx2, nidx1

                fpParas[cidx]['g2s'][nidx1][nidx2]['fp_type'] = G_type
                fpParas[cidx]['g2s'][nidx1][nidx2]['species1'] = center
                fpParas[cidx]['g2s'][nidx1][nidx2]['species1_code'] = cidx
                fpParas[cidx]['g2s'][nidx1][nidx2]['species2'] = neigh1
                #currIndex = i
                if eta is not None:
                    fpParas[cidx]['g2s'][nidx1][nidx2]['etas'].append(eta)
                    fpParas[cidx]['g2s'][nidx1][nidx2]['gammas'].append(gamma)
                    fpParas[cidx]['g2s'][nidx1][nidx2]['zetas'].append(zeta)
                    fpParas[cidx]['g2s'][nidx1][nidx2]['lambdas'].append(lambda_)
                    fpParas[cidx]['g2s'][nidx1][nidx2]['theta_ss'].append(thetas)
                    fpParas[cidx]['g2s'][nidx1][nidx2]['r_cuts'].append(rcut)

                #fpParas[cidx]['g1s'][nidx1]['nFPs'] += 1
                fpParas[cidx]['tnFPs'] += 1

            i, j, k = 0, 0, 0
            for i in range(nelement):
                for j in range(nelement):
                    for k in range(j, nelement):  
                        if fpParas[i]['g2s'][j][k]['nFPs'] == 0:
                                accN = 0
                        else:
                                accN = 1

                        if j == 0 and k == 0:
                            fpParas[i]['g2s'][j][k]['startpoint'] = fpParas[i]['g1_endpoint'] + accN
                        else:
                            if j == k:
                                if j > 0:
                                    fpParas[i]['g2s'][j][k]['startpoint'] = fpParas[i]['g2s'][j-1][nelement - 1]['endpoint'] + accN - 1
                                else:
                                    
                                    fpParas[i]['g2s'][j][k]['startpoint'] = fpParas[i]['g1_endpoint'] + accN
                            elif j < k:
                                fpParas[i]['g2s'][j][k]['startpoint'] = fpParas[i]['g2s'][j][k-1]['endpoint'] + accN - 1

                        if accN == 0:
                            fpParas[i]['g2s'][j][k]['endpoint'] = fpParas[i]['g2s'][j][k]['startpoint']
                        else:
                            fpParas[i]['g2s'][j][k]['endpoint'] = fpParas[i]['g2s'][j][k]['startpoint'] + fpParas[i]['g2s'][j][k]['nFPs']
                for j in range(nelement):
                    for k in range(j, nelement):
                        fpParas[i]['g2s'][j][k]['startpoint'] -= 1
                        fpParas[i]['g2s'][j][k]['endpoint'] -= 1

            for i in range(nelement):
                for j in range(nelement):
                    fpParas[i]['g1s'][j]['etas'] = torch.tensor(fpParas[i]['g1s'][j]['etas'], dtype=torch.double)
                    fpParas[i]['g1s'][j]['r_cuts'] = torch.tensor(fpParas[i]['g1s'][j]['r_cuts'], dtype=torch.double)
                    fpParas[i]['g1s'][j]['rss'] = torch.tensor(fpParas[i]['g1s'][j]['rss'], dtype=torch.double)
                    for k in range(nelement):
                        fpParas[i]['g2s'][j][k]['etas'] = torch.tensor(fpParas[i]['g2s'][j][k]['etas'], dtype=torch.double)
                        fpParas[i]['g2s'][j][k]['gammas'] = torch.tensor(fpParas[i]['g2s'][j][k]['gammas'], dtype=torch.double)
                        fpParas[i]['g2s'][j][k]['zetas'] = torch.tensor(fpParas[i]['g2s'][j][k]['zetas'], dtype=torch.double)
                        fpParas[i]['g2s'][j][k]['lambdas'] = torch.tensor(fpParas[i]['g2s'][j][k]['lambdas'], dtype=torch.double)
                        fpParas[i]['g2s'][j][k]['theta_ss'] = torch.tensor(fpParas[i]['g2s'][j][k]['theta_ss'], dtype=torch.double)
                        fpParas[i]['g2s'][j][k]['r_cuts'] = torch.tensor(fpParas[i]['g2s'][j][k]['r_cuts'], dtype=torch.double)

                    for t in range( j - 1 ):

                        fpParas[i]['g2s'][j][t]['etas'] = fpParas[i]['g2s'][t][j]['etas']
                        fpParas[i]['g2s'][j][t]['zetas'] = fpParas[i]['g2s'][t][j]['zetas']
                        fpParas[i]['g2s'][j][t]['lambdas'] = fpParas[i]['g2s'][t][j]['lambdas']
                        fpParas[i]['g2s'][j][t]['gammas'] = fpParas[i]['g2s'][t][j]['gammas']
                        fpParas[i]['g2s'][j][t]['theta_ss'] = fpParas[i]['g2s'][t][j]['theta_ss']
                        fpParas[i]['g2s'][j][t]['r_cuts'] = fpParas[i]['g2s'][t][j]['r_cuts']
                        fpParas[i]['g2s'][j][t]['startpoint'] = fpParas[i]['g2s'][t][j]['startpoint']
                        fpParas[i]['g2s'][j][t]['endpoint'] = fpParas[i]['g2s'][t][j]['endpoint']

    return coeh, fpParas

'''

def read_fpParas(filename, nelement):
    # inputs
    filename = str(filename)
    nelement = int(nelement)

    # variables
    currIndex = 0
    max_rcut = 0.0
    fpParas = {
        'g1s': {
            'fp_type': "",
            'currIndex': 0,
            'etas': [],
            'rss': [],
            'r_cuts': [],
            'startpoint': 0,
            'endpoint': 0,
            'nFPs': 0
        },
        'g2s': {
            'fp_type': "",
            'currIndex': 0,
            'etas': [],
            'zetas': [],
            'lambdas': [],
            'theta_ss': [],
            'r_cuts': [],
            'startpoint': 0,
            'endpoint': 0,
            'nFPs': 0
        },
        'tnFPs': 0,
        'g1_startpoint': 0,
        'g1_endpoint': 0,
        'g2_startpoint': 0,
        'g2_endpoint': 0
    }

    # Read fp Parameters from fpParas.dat
    with open(filename, 'r') as file:
        file.readline()  # Skip a line
        fp_type = file.readline().strip()
        file.readline()  # Skip a line
        uniq_elements = file.readline().split()  # This line is now ignored, as we don't care about the elements

        coeh_line = file.readline().split()
        coeh = torch.tensor([float(v) for v in coeh_line], dtype=torch.double)

        file.readline()  # Skip a line
        nG1, nG2 = map(int, file.readline().split())

        # Initialize currIndex
        currIndex = 0

        # Reading G1 parameters
        if nG1 > 0:
            currIndex = 0
            file.readline()  # skip #type
            for i in range(nG1):
                line = file.readline().split()
                G_type, center, neigh1, eta, Rs, rcut = line[0], line[1], line[2], float(line[3]), float(line[4]), float(line[5])

                # Check for max_rcut
                if rcut > max_rcut:
                    if max_rcut > 0.0:
                        print('Error: multiple rcuts are not supported yet. Use a single rcut value')
                        exit()
                    else:
                        max_rcut = rcut

                # Store the G1 parameters in a unified dictionary
                fpParas['g1s']['fp_type'] = G_type
                fpParas['g1s']['etas'].append(eta)
                fpParas['g1s']['rss'].append(Rs)
                fpParas['g1s']['r_cuts'].append(rcut)
                fpParas['g1s']['nFPs'] += 1
                fpParas['tnFPs'] += 1

                fpParas['g1s']['currIndex'] = currIndex + 1

            # Set startpoint and endpoint for G1
            fpParas['g1s']['startpoint'] = fpParas['g1_startpoint']
            fpParas['g1s']['endpoint'] = fpParas['g1s']['startpoint'] + fpParas['g1s']['nFPs']

        # Update global G1 endpoint after processing all G1s
        fpParas['g1_endpoint'] = fpParas['g1s']['endpoint']

        # Reading G2 parameters
        if nG2 > 0:
            file.readline()  # skip #type

            # Set the G2 startpoint
            fpParas['g2_startpoint'] = fpParas['g1_endpoint']

            for i in range(nG2):
                line = file.readline().split()
                G_type, center, neigh1, neigh2, eta, zeta, lambda_, thetas, rcut = (
                    line[0], line[1], line[2], line[3], float(line[4]), float(line[5]), float(line[6]), float(line[7]), float(line[8])
                )

                # Check for max_rcut
                if rcut > max_rcut:
                    if max_rcut > 0.0:
                        print('Error: multiple rcuts are not supported yet. Use a single rcut value')
                        exit()
                    else:
                        max_rcut = rcut

                # Store the G2 parameters in a unified dictionary
                fpParas['g2s']['fp_type'] = G_type
                fpParas['g2s']['etas'].append(eta)
                fpParas['g2s']['zetas'].append(zeta)
                fpParas['g2s']['lambdas'].append(lambda_)
                fpParas['g2s']['theta_ss'].append(thetas)
                fpParas['g2s']['r_cuts'].append(rcut)
                fpParas['g2s']['nFPs'] += 1
                fpParas['tnFPs'] += 1

            # Set startpoint and endpoint for G2
            fpParas['g2s']['startpoint'] = fpParas['g2_startpoint']
            fpParas['g2s']['endpoint'] = fpParas['g2s']['startpoint'] + fpParas['g2s']['nFPs']

        # Update global G2 endpoint after processing all G2s
        fpParas['g2_endpoint'] = fpParas['g2s']['endpoint']

        # Convert lists to tensors
        fpParas['g1s']['etas'] = torch.tensor(fpParas['g1s']['etas'], dtype=torch.double)
        fpParas['g1s']['r_cuts'] = torch.tensor(fpParas['g1s']['r_cuts'], dtype=torch.double)
        fpParas['g1s']['rss'] = torch.tensor(fpParas['g1s']['rss'], dtype=torch.double)

        fpParas['g2s']['etas'] = torch.tensor(fpParas['g2s']['etas'], dtype=torch.double)
        fpParas['g2s']['zetas'] = torch.tensor(fpParas['g2s']['zetas'], dtype=torch.double)
        fpParas['g2s']['lambdas'] = torch.tensor(fpParas['g2s']['lambdas'], dtype=torch.double)
        fpParas['g2s']['theta_ss'] = torch.tensor(fpParas['g2s']['theta_ss'], dtype=torch.double)
        fpParas['g2s']['r_cuts'] = torch.tensor(fpParas['g2s']['r_cuts'], dtype=torch.double)

    return coeh, fpParas, max_rcut


def calcg1s(nAtoms, npairs, symbols, maxneighs, MAX_NEIGHS, max_fps, neighs, num_neigh, max_npairs,
            fpParas,
            pair_global_indices, pair_indices, pairs, gvects, unitvects_pair, elementsmodes, fps, dfps):

    for i in range(npairs):
        index_1 = pair_global_indices[0, i]
        index_2 = pair_global_indices[1, i]
        #print(pair_global_indices[0, i])
        if index_1 >= nAtoms or index_2 >= nAtoms:
            print("Warning: ghost atoms found in head of pairs")
            continue

        o_index_1 = pair_indices[0, i]
        o_index_2 = pair_indices[1, i]
        center_1 = symbols[o_index_1] - 1
        center_2 = symbols[o_index_2] - 1

        dim_mode = 2

        # 访问全局的 g1s 参数
        sp_1 = fpParas['g1s']['startpoint']
        nFPs = fpParas['g1s']['nFPs']
        if nFPs == 0:
            continue

        ep_1 = sp_1 + nFPs * dim_mode  # 调整终点索引

        # 准备element_modes
        element_modes = torch.tensor([
            0.5 * (elementsmodes[center_1] + elementsmodes[center_2]),
            elementsmodes[center_1] - elementsmodes[center_2]
        ], dtype=torch.float64)

        #element_modes /= torch.norm(element_modes, p =2 )
        #print('element_modes',element_modes)
        # 计算g1和dg1
        #print(pairs[i])
        #print(nFPs)
        g1_1 = fg1(pairs[i], nFPs,
                    fpParas['g1s']['etas'],
                    fpParas['g1s']['rss'],
                    fpParas['g1s']['r_cuts'],
                    element_modes,2)

        dg1_1 = fdg1(pairs[i], nFPs,
                    fpParas['g1s']['etas'],
                    fpParas['g1s']['rss'],
                    fpParas['g1s']['r_cuts'],
                    element_modes,2)

        # 对于center_2
        if center_1 == center_2:
            g1_2 = g1_1.clone()
            dg1_2 = dg1_1.clone()
            sp_2, ep_2 = sp_1, ep_1
        else:
            sp_2 = fpParas['g1s']['startpoint']
            nFPs_2 = fpParas['g1s']['nFPs']

            if nFPs_2 == 0:
                continue
            ep_2 = sp_2 + nFPs_2 * dim_mode

            element_modes_2 = torch.tensor([
                0.5 * (elementsmodes[center_2] + elementsmodes[center_1]),
                elementsmodes[center_2] - elementsmodes[center_1]
            ], dtype=torch.float64)

            #element_modes_2  /= torch.norm(element_modes_2 , p =2 )

            g1_2 = fg1(pairs[i], nFPs_2,
                        fpParas['g1s']['etas'],
                        fpParas['g1s']['rss'],
                        fpParas['g1s']['r_cuts'],
                        element_modes_2, dim_mode)
            dg1_2 = fdg1(pairs[i], nFPs_2,
                        fpParas['g1s']['etas'],
                        fpParas['g1s']['rss'],
                        fpParas['g1s']['r_cuts'],
                        element_modes_2, dim_mode)

        # 更新fps
        fps[index_1, sp_1:ep_1] += g1_1
        fps[index_2, sp_2:ep_2] += g1_2

        # 更新dfps
        neighloc_1 = find_loc_int(neighs[index_1], index_2) + 1
        neighloc_2 = find_loc_int(neighs[index_2], index_1) + 1

        for k in range(3):
            dfps[index_1, 0, k, sp_1:ep_1] -= dg1_1 * unitvects_pair[0, i, k]
            dfps[index_2, 0, k, sp_2:ep_2] -= dg1_2 * unitvects_pair[1, i, k] 
            dfps[index_1, neighloc_1, k, sp_2:ep_2] += dg1_2 * unitvects_pair[1, i, k]
            dfps[index_2, neighloc_2, k, sp_1:ep_1] += dg1_1 * unitvects_pair[0, i, k]

        # 处理 ghost atoms 的情况
        if index_1 < nAtoms and index_2 >= nAtoms:
            # Similar to the first case, but now index_2 is a ghost atom
            sp_1 = fpParas['g1s']['startpoint']
            nFPs = fpParas['g1s']['nFPs']
            if nFPs == 0:
                continue

            ep_1 = sp_1 + nFPs * dim_mode
            fpdata_temp = [{'gs': torch.zeros(fpParas['tnFPs']),
                            'pre_dgdxs': torch.zeros(fpParas['tnFPs'])}]

            fpdata_temp[0]['gs'][sp_1:ep_1] = fg1(pairs[i], nFPs,
                                                 fpParas['g1s']['etas'],
                                                 fpParas['g1s']['rss'],
                                                 fpParas['g1s']['r_cuts'], element_modes,dim_mode)

            fpdata_temp[0]['pre_dgdxs'][sp_1:ep_1] = fdg1(pairs[i], nFPs,
                                                          fpParas['g1s']['etas'],
                                                          fpParas['g1s']['rss'],
                                                          fpParas['g1s']['r_cuts'], element_modes, dim_mode)

            sp_2 = fpParas['g1s']['startpoint']
            nFPs_2 = fpParas['g1s']['nFPs']
            ep_2 = sp_2 + nFPs_2 * dim_mode

            fpdata_temp.append({'gs': torch.zeros(fpParas['tnFPs']),
                                'pre_dgdxs': torch.zeros(fpParas['tnFPs'])})

            fpdata_temp[1]['gs'][sp_2:ep_2] = fg1(pairs[i], nFPs_2,
                                                  fpParas['g1s']['etas'],
                                                  fpParas['g1s']['rss'],
                                                  fpParas['g1s']['r_cuts'], element_modes, dim_mode)
            
            fpdata_temp[1]['pre_dgdxs'][sp_2:ep_2] = fdg1(pairs[i], nFPs_2,
                                                          fpParas['g1s']['etas'],
                                                          fpParas['g1s']['rss'],
                                                          fpParas['g1s']['r_cuts'], element_modes, dim_mode)

            fps[index_1, sp_1:ep_1] += fpdata_temp[0]['gs'][sp_1:ep_1]

            neighloc_2 = find_loc_int(neighs[o_index_2, :], index_1) + 1
            neighloc_1 = find_loc_int(neighs[index_1, :], index_2) + 1

            for k in range(3):
                dfps[index_1, 0, k, sp_1:ep_1] -= fpdata_temp[0]['pre_dgdxs'][sp_1:ep_1] * unitvects_pair[0, i, k]
                dfps[index_1, neighloc_1, k, sp_2:ep_2] += fpdata_temp[1]['pre_dgdxs'][sp_2:ep_2] * unitvects_pair[1, i, k]

        elif index_1 >= nAtoms and index_2 < nAtoms:
            # Similar to the first case, but now index_1 is a ghost atom
            sp_2 = fpParas['g1s']['startpoint']
            nFPs = fpParas['g1s']['nFPs']
            if nFPs == 0:
                continue

            ep_2 = sp_2 + nFPs * dim_mode
            fpdata_temp = [None, {'gs': torch.zeros(fpParas['tnFPs']),
                                  'pre_dgdxs': torch.zeros(fpParas['tnFPs'])}]

            fpdata_temp[1]['gs'][sp_2:ep_2] = fg1(pairs[i], nFPs,
                                                  fpParas['g1s']['etas'],
                                                  fpParas['g1s']['rss'],
                                                  fpParas['g1s']['r_cuts'], element_modes, dim_mode)
            
            fpdata_temp[1]['pre_dgdxs'][sp_2:ep_2] = fdg1(pairs[i], nFPs,
                                                          fpParas['g1s']['etas'],
                                                          fpParas['g1s']['rss'],
                                                          fpParas['g1s']['r_cuts'], element_modes, dim_mode)

            fps[index_2, sp_2:ep_2] += fpdata_temp[1]['gs'][sp_2:ep_2]

            neighloc_2 = find_loc_int(neighs[index_2, :], index_1) + 1
            neighloc_1 = find_loc_int(neighs[o_index_1, :], index_2) + 1

            for k in range(3):
                dfps[index_2, 0, k, sp_2:ep_2] -= fpdata_temp[1]['pre_dgdxs'][sp_2:ep_2] * unitvects_pair[1, i, k]
                if gvects[i] > 0:
                    dfps[o_index_1, neighloc_1, k, sp_2:ep_2] += fpdata_temp[1]['pre_dgdxs'][sp_2:ep_2] * unitvects_pair[1, i, k]

    return fps, dfps

def calcg2s_n(natoms, npairs, MAX_NPAIRS, ntriplets, MAX_NEIGHS, max_fps, symbols, r_cut, neighs, num_neigh, pairs, pair_global_indices, pair_indices,
              fpParas, unitvects, vectsigns, angles, angles_indices, element_modes, g1_fps, g1_dfps):

    theta_ss = torch.zeros(3, max_fps)
    reduced_etas = torch.zeros(3, max_fps)
    zetas = torch.zeros(3, max_fps)
    lambdas = torch.zeros(3, max_fps)
    pairIndex = torch.zeros(3, dtype=torch.int)
    center = torch.zeros(3, dtype=torch.int)
    neighbor = torch.zeros((3, 2), dtype=torch.int)
    gcenter = torch.zeros((3, 3), dtype=torch.int)

    #symbols = symbols.to(torch.int)
    neighs = neighs.to(torch.int)
    unitvects = unitvects.to(torch.float64)
    vectsigns = vectsigns.to(torch.float64)
    angles = angles.to(torch.float64)
    num_neigh = num_neigh.to(torch.int)

    # 使用从 g1_fps 和 g1_dfps 传递过来的初始 fps 和 dfps
    fps, dfps = g1_fps, g1_dfps

    # 计算三体项的共同部分
    pair_sins, fcuts, fexp = commonparts(npairs, pairs, r_cut)
    #print('fpParas',fpParas)

    # 遍历所有三体项
    for i in range(ntriplets):
        # 获取当前三体交互中的三个成对的索引
        pairIndex[0] = angles_indices[0, i]
        pairIndex[1] = angles_indices[1, i]
        pairIndex[2] = angles_indices[2, i]

        # 获取中心原子的索引
        center[0] = pair_indices[0, pairIndex[0]]
        center[1] = pair_indices[1, pairIndex[0]]
        center[2] = pair_indices[1, pairIndex[1]]

        # 获取中心原子的符号
        csymbol = symbols[center]
        #print('ccymb',csymbol)
        # 邻近原子的符号
        neighbor[0, :] = torch.tensor([csymbol[1], csymbol[2]])
        neighbor[1, :] = torch.tensor([csymbol[0], csymbol[2]])
        neighbor[2, :] = torch.tensor([csymbol[0], csymbol[1]])
        
        # 获取全局索引
        gcenter[0, :] = torch.tensor([
            pair_global_indices[0, pairIndex[0]],
            pair_global_indices[1, pairIndex[0]],
            pair_global_indices[1, pairIndex[1]]
        ])
        #print('gcenter',gcenter)
        # 如果全局索引超过原子数，跳过该三体项
        if (gcenter[0, :] > natoms).any():
            continue

        dim_mode = 2

        # 遍历三体交互中的三个原子，计算 g2 和 dg2
        for j in range(3):

            # 提取当前中心原子的三体特征参数 X共享全体参数
            nFPs = fpParas["g2s"]["nFPs"]
            #print(nFPs)
            theta_ss[j, :nFPs] = fpParas["g2s"]["theta_ss"]
            #print('theta_ss', theta_ss[j, :nFPs])
            reduced_etas[j, :nFPs] = fpParas["g2s"]["etas"]
            zetas[j, :nFPs] = fpParas["g2s"]["zetas"]
            lambdas[j, :nFPs] = fpParas["g2s"]["lambdas"]
        #print('angles',angles)
        # 计算 g2 和 dg2

        mode_1 = element_modes[center[0]]
        mode_2 = element_modes[center[1]]
        mode_3 = element_modes[center[2]]

        elementmodes = [mode_1,mode_2,mode_3]
        #print(element_modes)

        g2, dg2 = fg2_dg(nFPs, angles[:, i], r_cut, pairs[pairIndex[0]], pairs[pairIndex[1]], pairs[pairIndex[2]],
                         unitvects[pairIndex[0], :], unitvects[pairIndex[1], :], unitvects[pairIndex[2], :],
                         fexp[pairIndex[:]], fcuts[pairIndex[:]], pair_sins[pairIndex[:]],
                         theta_ss, reduced_etas, zetas, lambdas, elementmodes)
        # if torch.isnan(dg2).any():
        #     print(f"NaN detected in tensor: {i}")
        #     break
        #print('input',theta_ss, reduced_etas, zetas, lambdas)
        #print('g2',g2)
        #print('dg2',dg2[:3,:3,:,:])
        # 更新 fps 和 dfps
        for j in range(3):
            #currneighs = neighbor[j] - 1  # 调整为 0 索引
            nFPs = fpParas["g2s"]["nFPs"]

            if nFPs == 0:
                continue

            sp = fpParas["g2s"]["startpoint"] * dim_mode #for dim_mode =2 
            #ep = fpParas["g2s"]["endpoint"]
            ep = sp + nFPs * dim_mode  # 调整终点索引
            #ep4der = sp + nFPs * dim_mode * 3
            #print('ep',ep)
            # 更新中心原子的 fps 和 dfps
            if gcenter[0, j] <= natoms:

                #print('g2[j, :nFPs ]',g2[j, :nFPs * dim_mode])
                fps[gcenter[0, j], sp:ep] += g2[j, :nFPs * dim_mode ]
                dfps[gcenter[0, j], 0, :, sp:ep] += dg2[j, j, :, :nFPs* dim_mode]

                '''
                other_idx4neigh = (0, 1, 2)[:j] + (0, 1, 2)[j+1:]      #去除j对应中心原子的序号表
                #print('other_idx4neigh for j',j,other_idx4neigh)
                #print('trpl',gcenter[0,j],gcenter[0, other_idx4neigh[0]],gcenter[0, other_idx4neigh[1]])
                #mode_1,mode_2, mode = 0,0,0

                #here
                mode_1 = element_modes[gcenter[0, other_idx4neigh[0]]]
                mode_2 = element_modes[gcenter[0, other_idx4neigh[1]]]
                center_mode = element_modes[gcenter[0, j]]
                arm_1 = torch.tensor([0.5 * (center_mode + mode_1), center_mode - mode_1], dtype=torch.float64)
                arm_2 = torch.tensor([0.5 * (center_mode + mode_2), center_mode - mode_2], dtype=torch.float64)
                mode = arm_1 + arm_2
                mode /= torch.norm(mode, p = 2)

                #mode = torch.tensor([1,1])
                #print('--jcenter---')
                #print('gcenter[0, j]',gcenter[0, j])
                #print('gcenter[0, other_idx4neigh[0]]',gcenter[0, other_idx4neigh[0]])
                #print('gcenter[0, other_idx4neigh[1]]',gcenter[0, other_idx4neigh[1]])
                #print('arm1',arm_1)
                #print('arm2',arm_2)
                #print('mode j',mode)
                #print('***k center***')
                #print('torch.outer(g2[j, :nFPs])',g2[j, :nFPs])                
                fps[gcenter[0, j], sp:ep] += torch.outer(g2[j, :nFPs], mode).reshape(-1)

                #result = torch.outer(dg2[j, j, :, :nFPs], mode).reshape(-1)
                #print('fps[gcenter[0, j], sp:ep]',fps[gcenter[0, j], sp:ep])
                #print(sp,ep)
                #mode_expanded = mode.unsqueeze(0)  # 将 mode 变为 (1, 2) 以便广播
                #print(dg2[j, j, :, :nFPs],mode)
                result = torch.cat([torch.outer(dg2[j, j, l, :nFPs], mode).reshape(1, -1) for l in range(dg2.size(2))])
                #print('result',result)
                # 打印调试信息
                #print('mode:', mode)
                #print('dg2[j, j, :, :nFPs]:', dg2[j, j, :, :nFPs])
                #print('result:', result)

                # 更新 dfps
                #print('dfps before update:', dfps[gcenter[0, j], 0, :, sp:ep])

                #print('dg2 result',result)
                dfps[gcenter[0, j], 0, :, sp:ep] += result
                '''
                #print('after summ',dfps[gcenter[0, j], 0, :, sp:ep])
                #dfps[gcenter[0, j], 0, :, sp:ep] += torch.outer(dg2[j, j, :, :nFPs], mode2).reshape(-1)

                # 更新与中心原子相邻的原子的 dfps
                for k in range(3):
                    if j == k:
                        continue
                    if gcenter[0, k] <= natoms:

                        #print('j,k',j,k)

                        #print('other_idx4neigh for k',other_idx4neigh)
                        #print('k neigh',neighs[center[k], :])

                        neigh1_loc = find_loc_int(neighs[center[k], :], gcenter[0, j]) + 1 #for dfps loc ,+1 means to skip itself

                        #print('neighs[center[k], :]',neighs[center[k], :],gcenter[0, j],neigh1_loc)

                        #other_idx4kneigh = list({0, 1, 2} - {j, k})[0]
                        '''
                        neigh1_mode =  gcenter[0, j]
                        neigh2_mode = gcenter[0, other_idx4kneigh]
                        mode_1,mode_2, mode = 0,0,0
                        mode_1 = element_modes[neigh1_mode]
                        mode_2 = element_modes[neigh2_mode]

                        center_mode = element_modes[gcenter[0, j]]

                        arm_1 = torch.tensor([0.5 * (center_mode + mode_1), center_mode - mode_1], dtype=torch.float64)
                        arm_2 = torch.tensor([0.5 * (center_mode + mode_2), center_mode - mode_2], dtype=torch.float64)
                        mode1 = arm_1 + arm_2
                        mode1 /= torch.norm(mode1, p =2)

                        #print('center gcenter[0, k]',gcenter[0, k])
                        #print('neighs',neighs[center[k], :],gcenter[0, j])
                        #print(' neigh1_loc', neigh1_loc)
                        #print(' neigh2_loc', neigh2_loc)
                        #print('arm1 k',arm_1)
                        #print('arm2 k',arm_2)
                        #print('mode k',mode)
                        '''
                        '''
                        print('gcenter[0, j]--->gcenter[0, k]',gcenter[0, j],gcenter[0, k])
                        kcenter_mode = element_modes[gcenter[0, k]]
                        print('j -- k',center_mode,kcenter_mode)
                        modek= torch.tensor([0.5 * (center_mode + kcenter_mode),  center_mode - kcenter_mode], dtype=torch.float64)
                        #print('modek',modek)
                        modek /= torch.norm(modek, p = 2)
                        modek = mode
                        '''
                        #print('before dfps[center[k], neigh1_loc, :, sp:ep]',dfps[center[k], neigh1_loc, :, sp:ep])
                        #print('modek',modek)
                        #result = torch.cat([torch.outer(dg2[j, k, i, :nFPs], mode).reshape(-1) for i in range(dg2.size(2))])
                       #print('dg2[j, k, l, :nFPs]',dg2[j, k, :, :nFPs])

                        #result1 = torch.cat([torch.outer(dg2[j, k, l, :nFPs], mode).reshape(1, -1) for l in range(dg2.size(2))])
                        #print(f'result{k}',result1)
                        #print(f'brfotr result{k}',dfps[center[k], neigh1_loc, :, sp:ep])

                        dfps[center[k], neigh1_loc, :, sp:ep] += dg2[j, k, :, :nFPs* dim_mode]

                        #print(f'after result{k}',dfps[center[k], neigh1_loc, :, sp:ep])
                        #print('after dfps[center[k], neigh1_loc, :, sp:ep]',dfps[center[k], neigh1_loc, :, sp:ep])
                #print('\n')
               

            # 如果全局索引大于原子数，处理相邻原子的 dfps
            else:
                print('else case')
                for k in range(3):
                    if j == k:
                        continue
                    if gcenter[0, k] <= natoms:
                        neigh1_loc = find_loc_int(neighs[center[k]], gcenter[0, j])

                        mode_1 = element_modes[neigh1_loc]
                        mode_2 = element_modes[gcenter[0, j]]
                        center_mode = element_modes[gcenter[0, k]]
                        arm_1 = torch.tensor([0.5 * (center_mode + mode_1), center_mode - mode_1], dtype=torch.float64)
                        arm_2 = torch.tensor([0.5 * (center_mode + mode_2), center_mode - mode_2], dtype=torch.float64)
                        mode = arm_1 * arm_2

                        result = torch.cat([torch.outer(dg2[j, k, l, :nFPs], mode).reshape(1, -1) for l in range(dg2.size(2))])
                        dfps[center[k], neigh1_loc, :, sp:ep] += result

        #print('fps',fps)
        #print('dfps',dfps)
    return fps, dfps

def calcfps(nAtoms, pos_car, cell, symbols, max_fps, nelement,element_modes, forceEngine, fpParas, MAX_NEIGHS , max_rcut , actviated_G2 = True, neighsDefined = False):

    #! calcLammps: neighsDefined global with default of false

    nAtoms = torch.tensor(nAtoms, dtype= torch.int)
    pos_car = torch.tensor(pos_car, dtype= torch.float64)
    cell = torch.tensor(cell, dtype= torch.float64)
    max_rcut = torch.tensor( max_rcut, dtype= torch.float64)

    fps = torch.zeros(nAtoms, max_fps, dtype=torch.float64) #max fps!!!
    dfps = torch.zeros(nAtoms, MAX_NEIGHS, 3, max_fps, dtype=torch.float64) #max fps!!!

    vectsigns = torch.tensor([[1, 1, -1], [1, -1, -1], [1, -1, 1]], dtype=torch.int)

    ncells, tncells = calcCellNum(nAtoms, pos_car, cell, max_rcut)

    if neighsDefined:

        supercell[0, :] = ncells[0] * cell[0]
        supercell[1, :] = ncells[1] * cell[1]
        supercell[2, :] = ncells[2] * cell[2]

    else:
        tnAtoms = nAtoms * tncells
        #pool_pos_car = torch.zeros((tnAtoms, 3), dtype=torch.float)
        #pool_ids = torch.zeros(tnAtoms, dtype=torch.int)
        #supersymbols = torch.zeros(tnAtoms, dtype=torch.int)
        #tneighs = torch.zeros((tnAtoms, MAX_NEIGHS), dtype=torch.int)
        supercell, supersymbols, pool_pos_car, pool_ids = genSupercell(nAtoms, pos_car, symbols, cell, ncells, tncells, max_rcut)

    max_npairs = tnAtoms * MAX_NEIGHS

    npairs_incell, num_pairs, tnum_neigh, tneighs, tneighs_incell, rmins, num_eachpair, pairs, pair_info, pair_global_indices, pair_indices,gvects, unitvects_pair = calcNlist(tnAtoms, MAX_NEIGHS,  pool_pos_car, supercell, symbols, pool_ids,
            max_npairs,tnAtoms,max_rcut, nelement, forceEngine, tncells, skipNeighList=None)

    #print('num_eachpair',num_eachpair)
    num_neigh = tnum_neigh[:nAtoms]
    maxneighs = num_neigh.max().item()
    neighs = tneighs[:nAtoms]
    neighs_incell = tneighs_incell[:nAtoms]

    fps_g1, dfps_g1 = calcg1s(nAtoms, npairs_incell, symbols, maxneighs, MAX_NEIGHS,
                        max_fps, neighs, num_neigh, max_npairs, fpParas, pair_global_indices, pair_indices, pairs, gvects,unitvects_pair,element_modes,fps, dfps)
    #print('all_fps',dfps_g1[:,:3,:,:4])
    if 0 > 1:#actviated_G2: # nG2 > 0
        ntriplets, angles, angle_indices = calcTriplet(nAtoms, tnAtoms, MAX_NEIGHS, max_rcut, num_pairs,num_neigh, neighs, num_eachpair, pairs,
                                                        pair_info, pair_global_indices, gvects, unitvects_pair)
        #print('ntriplets:',ntriplets)

        fps, dfps =calcg2s_n(
            nAtoms,num_pairs, max_npairs, ntriplets, MAX_NEIGHS, max_fps,
            symbols, max_rcut, neighs, num_neigh, pairs, pair_global_indices, pair_indices,fpParas, unitvects_pair[0, :, :], vectsigns,
            angles[:, :ntriplets], angle_indices[:, : ntriplets],element_modes, fps_g1, dfps_g1)

        #print('dpfs of G2',dfps[:,:15,:,:])
    else :
        fps = fps_g1
        dfps = dfps_g1    

    #print('all_fps',all_fps)

    return  fps, dfps , neighs , num_neigh

