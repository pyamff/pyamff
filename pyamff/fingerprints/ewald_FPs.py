from math import log, pi, sqrt
import sys
from tkinter.tix import Tree
from scipy import constants
from scipy.special import erfc
from collections import Counter

import numpy as np
import torch

from pyamff.fingerprints.ewald_lattice import Lattice


# from pymatgen
class Ewald_FPs():
    """
    Calculates the electrostatic energy of a periodic array of charges using
    the Ewald technique.

    Ref:
    Ewald summation techniques in perspective: a survey
    Abdulnour Y. Toukmaji and John A. Board Jr.
    DOI: 10.1016/0010-4655(96)00016-1
    URL: http://www.ee.duke.edu/~ayt/ewaldpaper/ewaldpaper.html

    This matrix can be used to do fast calculations of Ewald sums after species
    removal.

    E = E_recip + E_real + E_point

    Atomic units used in the code, then converted to eV.
    """

    def __init__(self,
                 structure,
                 real_space_cut=None,
                 recip_space_cut=None,
                 eta=None,
                 acc_factor=12,
                 w=1 / 2**0.5,
                 compute_forces=True,
                 screen_rcut=0.0,
                 screen_k=1.0):
        """
        Initializes and calculates the Ewald sum. Default convergence
        parameters have been specified, but you can override them if you wish.

        Args:
            structure (ase.Atom): Originally the type is pymatgen.core.structure
                but here we use ase.Atom().
            real_space_cut (float): Real space cutoff radius dictating how
                many terms are used in the real space sum. Defaults to None,
                which means determine automagically using the formula given
                in gulp 3.1 documentation.
            recip_space_cut (float): Reciprocal space cutoff radius.
                Defaults to None, which means determine automagically using
                the formula given in gulp 3.1 documentation.
            eta (float): The screening parameter. Defaults to None, which means
                determine automatically.
            acc_factor (float): No. of significant figures each sum is
                converged to.
            w (float): Weight parameter, w, has been included that represents
                the relative computational expense of calculating a term in
                real and reciprocal space. Default of 0.7 reproduces result
                similar to GULP 4.2. This has little effect on the total
                energy, but may influence speed of computation in large
                systems. Note that this parameter is used only when the
                cutoffs are set to None.
            compute_forces (bool): Whether to compute forces. False by
                default since it is usually not needed.
        """

        # Converts unit of q*q/r into eV
        self.CONV_FACT = 1e10 * constants.e / (4 * pi * constants.epsilon_0)

        self._lattice = Lattice(structure.get_cell()[:])

        # self._charged = abs(structure.charge) > 1e-8  # bool
        self._vol = structure.get_volume()
        self._compute_forces = compute_forces

        self._acc_factor = acc_factor

        # set screening length
        self._eta = eta or (len(structure) * w / (self._vol**2))**(1 / 3) * pi
        self._sqrt_eta = sqrt(self._eta)

        # acc factor used to automatically determine the optimal real and
        # reciprocal space cutoff radii
        self._accf = sqrt(log(10**self._acc_factor))

        '''
        NOTE: real_space_cut value:

        1. > 0: consider real term, default
        2. = 0: consider real term, automatically determine the optimal rcut and gcut
        '''
        if real_space_cut == 0.0:
            real_space_cut = None

        '''
        NOTE: screenRcut value:
        1. = 0.0: traditional implementation, default (if screen = False)
        2. > 0: consider screen function within [0, screenRcut] (all space)
        '''
        self._screenRcut = screen_rcut
        self._screenK = screen_k
        if screen_rcut <= 0.0:
            self.if_screen = False
        else:
            self.if_screen = True

        # rcut (influenced by atom num and volume!)
        # self._rmax * self._gmax = Constance(55.2620422318571)
        self._rmax = real_space_cut or self._accf / self._sqrt_eta
        self._gmax = recip_space_cut or 2 * self._sqrt_eta * self._accf

        '''
        # The next few lines pre-compute certain quantities and store them.
        # Ewald summation is rather expensive, and these shortcuts are
        # necessary to obtain several factors of improvement in speedup.
        '''

        # Alan: get pred_qi based on chemical symbols to determine which NN output
        self.chem_symbol = structure.get_chemical_symbols()
        self.chem_num_dict = dict(Counter(self.chem_symbol))

        self._coords = structure.get_positions()
        self._frac_coords = self._lattice.get_fractional_coords(self._coords)
        self.numsites = len(structure)

        # Define the private attributes to lazy compute reciprocal and real
        # space terms.
        self._initialized = False
        self._recip = None
        self._real, self._point = None, None
        self._forces = None

        # qi(s)
        self._oxi_states = torch.zeros(self.numsites, dtype=torch.double)

        # system real net charge: Q
        try:
            self.total_charge = structure.get_tags()[0]
        except Exception:
            self.total_charge = 0
        # self._charged_cell_energy = torch.zeros(self.numsites, self.numsites)

        # initial variables for energy calculation
        self.init_for_real()
        self.init_for_recip()

    def init_for_recip(self):
        """
        Alan: precalculate the reciprocal variables instead in the forward function
        (self.erecip):
        """

        self.e_recip = torch.zeros((self.numsites, self.numsites), dtype=torch.double)
        self.f_recip = torch.zeros((self.numsites, 3), dtype=torch.double)

        rcp_latt = self._lattice.reciprocal_lattice
        # recip_nn = rcp_latt.get_points_in_sphere_wrapper([[0, 0, 0]], [0, 0, 0], self._gmax)
        recip_nn = rcp_latt.get_points_in_sphere_py([[0, 0, 0]], [0, 0, 0], self._gmax)

        frac_coords = [frac_coords for (frac_coords, dist, _idx, _img) in recip_nn if dist != 0]

        self.gs = torch.from_numpy(rcp_latt.get_cartesian_coords(frac_coords))

        g2s = torch.sum(self.gs**2, 1)

        exp_vals = torch.exp(-g2s / (4 * self._eta))

        self.exp_vals_g2s = exp_vals / g2s

        self.grs = torch.sum(self.gs[:, None] * torch.from_numpy(self._coords[None, :]), 2)

        for exp_vals_g2, gr in zip(self.exp_vals_g2s, self.grs):
            # Uses the identity sin(x)+cos(x) = 2**0.5 sin(x + pi/4)
            m = (gr[None, :] + pi / 4) - gr[:, None]
            m = torch.sin(m)
            m *= exp_vals_g2

            self.e_recip += m


    def _calc_recip(self):
        """
        Perform the reciprocal space summation. Calculates the quantity
        E_recip = 1/(2PiV) sum_{G < Gmax} exp(-(G.G/4/eta))/(G.G) S(G)S(-G)
        where
        S(G) = sum_{k=1,N} q_k exp(-i G.r_k)
        S(G)S(-G) = |S(G)|**2.

        This method is heavily vectorized to utilize numpy's C backend for speed.
        """
        prefactor = 2 * pi / self._vol

        # rcp_latt = self._lattice.reciprocal_lattice
        # recip_nn = rcp_latt.get_points_in_sphere_wrapper([[0, 0, 0]], [0, 0, 0], self._gmax)

        # frac_coords = [frac_coords for (frac_coords, dist, _idx, _img) in recip_nn if dist != 0]

        # gs = rcp_latt.get_cartesian_coords(frac_coords)

        # g2s = torch.sum(self.gs**2, 1)

        # exp_vals = torch.exp(-g2s / (4 * self._eta))
        # grs = torch.sum(self.gs[:, None] * self._coords[None, :], 2)

        # create array where q_2[i,j] is qi * qj
        qi_qj = self._oxi_states[None, :] * self._oxi_states[:, None]

        # calculate the structure factor
        con_grs = torch.cos(self.grs)
        sin_grs = torch.sin(self.grs)
        s_reals = torch.sum(self._oxi_states[None, :] * con_grs, 1)
        s_imags = torch.sum(self._oxi_states[None, :] * sin_grs, 1)

        for g, exp_vals_g2, con_gr, sin_gr, s_real, s_imag in zip(self.gs, self.exp_vals_g2s, con_grs, sin_grs, s_reals, s_imags):
            # Uses the identity sin(x)+cos(x) = 2**0.5 sin(x + pi/4)
            # m = (gr[None, :] + pi / 4) - gr[:, None]
            # m = torch.sin(m)
            # m *= exp_val / g2

            # e_recip += m

            if self._compute_forces:
                pref = 2 * exp_vals_g2 * self._oxi_states
                factor = prefactor * pref * (s_real * sin_gr - s_imag * con_gr)

                self.f_recip += factor[:, None] * g[None, :]

        self.f_recip *= self.CONV_FACT
        self.e_recip *= prefactor * self.CONV_FACT * qi_qj * 2**0.5
        return self.e_recip, self.f_recip


    def init_for_real(self):
        """
        Alan: precalculate the real and point variables instead in the forward function
        (nfcoords, rij, js):
        """
        self.all_erfc_val = list()
        self.all_nc_coords = list()
        self.all_rij = list()
        self.all_js = list()

        self.e_real = 0
        self.f_real = torch.zeros((self.numsites, 3), dtype=torch.double)

        for idx in range(self.numsites):
            # nf_coords, rij, js, _ = self._lattice.get_points_in_sphere_wrapper(self._frac_coords, self._coords[idx], self._rmax, zip_results=False)
            nf_coords, rij, js, _ = self._lattice.get_points_in_sphere_py(self._frac_coords, self._coords[idx], self._rmax, zip_results=False)

            # remove the rii term
            inds = rij > 1e-8
            js = js[inds]
            rij = rij[inds]
            nf_coords = nf_coords[inds]

            erfc_val = erfc(self._sqrt_eta * rij)

            # for force caclulation
            nc_coords = self._coords[idx] - self._lattice.get_cartesian_coords(nf_coords) # actually Rij not Rj

            self.all_erfc_val.append(torch.from_numpy(erfc_val))
            self.all_nc_coords.append(torch.from_numpy(nc_coords))
            self.all_rij.append(torch.from_numpy(rij))
            self.all_js.append(torch.from_numpy(js))


    def _calc_real_and_point(self):
        """Determines the self energy -(eta/pi)**(1/2) * sum_{i=1}^{N} q_i**2."""

        force_pf = 2 * self._sqrt_eta / sqrt(pi)

        # qs = self._oxi_states
        e_point = -(self._oxi_states**2) * sqrt(self._eta / pi)

        for erfc_val, nc_coords, rij, js, idx in zip(self.all_erfc_val, self.all_nc_coords, self.all_rij, self.all_js, range(self.numsites)):

            # nf_coords, rij, js, _ = self._lattice.get_points_in_sphere_wrapper(self._frac_coords, self._coords[idx], self._rmax, zip_results=False)

            # remove the rii term
            # inds = rij > 1e-8
            # js = js[inds]
            # rij = rij[inds]
            # nf_coords = nf_coords[inds]

            qi = self._oxi_states[idx]
            qj = self._oxi_states[js]

            # erfc_val = erfc(self._sqrt_eta * rij)

            new_ereals = erfc_val * qi * qj / rij

            # insert new_ereals
            # for key in range(self.numsites):
            #     self.e_real[key, idx] = torch.sum(new_ereals[js == key])
            self.e_real += torch.sum(new_ereals) # not need to make e_real a matrix

            if self._compute_forces:
                # nc_coords = self._lattice.get_cartesian_coords(nf_coords)

                fijpf = qj / rij**3 * (erfc_val + force_pf * rij * np.exp(-self._eta * rij**2))
                self.f_real[idx] += torch.sum(
                    torch.unsqueeze(fijpf, 1) * nc_coords * qi * self.CONV_FACT,
                    axis=0,
                )

        self.e_real *= 0.5 * self.CONV_FACT
        e_point *= self.CONV_FACT

        return self.e_real, e_point, self.f_real


    def _calc_ewald_terms(self):
        """Calculates and sets all Ewald terms (point, real and reciprocal)."""
        _recip, recip_forces = self._calc_recip()
        _real, self.e_point, real_point_forces = self._calc_real_and_point()
        # if self._compute_forces:
        #     self.recip_forces = recip_forces 
        #     self.real_point_forces = real_point_forces

    def set_net_charge_energy(self, ifequil_charge=True):
        """
        Alan: Compute the correction for a charged cell
        net predicted charge of the system: /sum{qj} from j to N

        NOTE: Call this function in NN forward function 
        NOTE: Do not loose the require_grad attribute
        """
        if ifequil_charge:
            
            # NOTE: self.total_charge  NO GRAD!
            # corrected partial charges
            self._oxi_states -= (torch.sum(self._oxi_states) - self.total_charge) / self.numsites

        else:
            self.total_charge = torch.sum(self._oxi_states) # NOTE: YES GRAD!

        # get additional charges if the system is not charge neutral
        self._charged_cell_energy = torch.tensor(-self.CONV_FACT / 2 * pi /
                                    self._vol / self._eta *
                                    self.total_charge**2)
        
        return self._oxi_states


    def set_oxi_states(self, charge_dict, start_index):
        """
        Alan: get pred_qi based on chemical symbols to determine which NN output
       
        NOTE: Call this function in NN forward function 
        NOTE: Do not loose the require_grad attribute
        """
        index_dict = {}

        # set charges in THIS IMAGE
        for element in start_index.keys():
            index_dict[element] = 0

        for element, i in zip(self.chem_symbol, range(len(self.chem_symbol))):
            self._oxi_states[i] = charge_dict[element][start_index[element] + index_dict[element]]
            index_dict[element] += 1

        return


    @property
    def reciprocal_space_energy(self):
        """The reciprocal space energy."""
        if not self._initialized:
            self._calc_ewald_terms()
            self._initialized = True
        return torch.sum(self.e_recip)

    @property
    def reciprocal_space_energy_matrix(self):
        """
        The reciprocal space energy matrix. Each matrix element (i, j)
        corresponds to the interaction energy between site i and site j in
        reciprocal space.
        """
        if not self._initialized:
            self._calc_ewald_terms()
            self._initialized = True
        return self.e_recip

    @property
    def real_space_energy(self):
        """The real space energy."""
        if not self._initialized:
            self._calc_ewald_terms()
            self._initialized = True
        return torch.sum(self.e_real)

    @property
    def real_space_energy_matrix(self):
        """
        The real space energy matrix. Each matrix element (i, j) corresponds to
        the interaction energy between site i and site j in real space.
        """
        if not self._initialized:
            self._calc_ewald_terms()
            self._initialized = True
        return self.e_real

    @property
    def point_energy(self):
        """The point energy."""
        if not self._initialized:
            self._calc_ewald_terms()
            self._initialized = True
        return torch.sum(self.e_point)

    @property
    def point_energy_matrix(self):
        """
        The point space matrix. A diagonal matrix with the point terms for each
        site in the diagonal elements.
        """
        if not self._initialized:
            self._calc_ewald_terms()
            self._initialized = True
        return self.e_point

    @property
    def total_energy(self):
        """The total energy."""
        if not self._initialized:
            self._calc_ewald_terms()
            self._initialized = True
        return torch.sum(self.e_recip) + self.e_real + torch.sum(self.e_point) + self._charged_cell_energy

