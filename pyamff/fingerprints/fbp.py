import torch
import numpy as np
 


Pi = torch.tensor(torch.pi,dtype = torch.float64)

def fg1(Rijs, nFPs, etas, r_ss , r_cuts, element_modes, dim_mode): 

    # outputs
    outputs = torch.zeros((nFPs,dim_mode), dtype=torch.float64)

    G1_outputs = torch.exp(-((Rijs - r_ss) ** 2 / (r_cuts ** 2)) * etas) * (0.5 * (torch.cos(Pi * Rijs / r_cuts) + 1))

    outer_product = torch.outer(G1_outputs, element_modes)
    outputs = outer_product.reshape(-1) 

    return outputs

def fdg1(Rij, nFPs, etas, r_ss, r_cuts, element_modes, dim_mode):

    #nFPs = torch.tensor(nFPs, dtype= torch.int)    
    outputs = torch.zeros((nFPs,dim_mode), dtype=torch.float64)

    dG1_outputs =  (((-etas * (Rij - r_ss) * (torch.cos(Pi * Rij / r_cuts) + 1)) / (r_cuts ** 2)) -
                (0.5 * (Pi / r_cuts) * torch.sin(Pi * Rij / r_cuts))) *  \
                torch.exp(-etas * ((Rij - r_ss) ** 2) / (r_cuts ** 2))

    outer_product = torch.outer(dG1_outputs, element_modes)

    outputs = outer_product.reshape(-1)

    return outputs

def commonparts(npairs, pairs, rcut):

    inv_rcut = 1 / rcut 
    #inv_rcut = torch.tensor(inv_rcut, dtype=torch.double)

    # output
    pair_sins = torch.zeros(npairs, dtype=torch.double)
    fcuts = torch.zeros(npairs, dtype=torch.double)
    fexp = torch.zeros(npairs, dtype=torch.double)  

    pair_sins = torch.sin(Pi * pairs * inv_rcut)
    fcuts = 0.5 * (torch.cos(Pi * pairs * inv_rcut) + 1)
    # only for g2
    fexp = torch.exp(-pairs ** 2)

    return pair_sins, fcuts, fexp


def calcTriplet(nAtoms, tnAtoms, MAX_NEIGHS, rcut, npairs,
                num_neigh, neighs, num_eachpair, pairs, pair_info, pair_global_indices, gvects, unitvects_pair):

    rcut2 = rcut ** 2
    #print('pairs',pairs)
    v1_unit = torch.zeros(3, dtype=torch.double)
    v2_unit = torch.zeros(3, dtype=torch.double)
    v3_unit = torch.zeros(3, dtype=torch.double)

    angles = torch.zeros(3, nAtoms * MAX_NEIGHS * MAX_NEIGHS, dtype=torch.double)
    angle_indices = torch.zeros(3, nAtoms * MAX_NEIGHS * MAX_NEIGHS, dtype=torch.int64)

    ntriplets = 0

    for i in range(nAtoms):
        for j_neigh in range(num_eachpair[i]):  
            ij_pair_loc = pair_info[i, j_neigh] - 1
            
            if ij_pair_loc < 0:
                    continue

            #print('---j_neigh',j_neigh)
            #print('ij_pair_loc ',ij_pair_loc )

            v1_unit = unitvects_pair[0, ij_pair_loc,:]
            #print("vi_unit", v1_unit)

            for k_neigh in range(j_neigh + 1 , num_eachpair[i]):  

                #print('####k_neigh',k_neigh)
                ik_pair_loc = pair_info[i, k_neigh] - 1 # num_pairs - 1 in Py

                if ik_pair_loc < 0:
                    continue

                s_neigh = pair_global_indices[1, ij_pair_loc] 
                l_neigh = pair_global_indices[1, ik_pair_loc] 

                if l_neigh < s_neigh:
                    continue

                if s_neigh >= tnAtoms:
                    continue

                temp_info = [i for i in pair_info[s_neigh, :num_eachpair[s_neigh]]-1 if i >=0 ]

                p_size = len(pair_global_indices[1, pair_info[s_neigh, :num_eachpair[s_neigh]]-1])
                #print('---PSZIE',p_size)

                if p_size == 0:
                    continue

                #print('pair_global_indices[1,pair_info[s_neigh, :num_eachpair[s_neigh]]]',pair_global_indices[1,pair_info[s_neigh, :num_eachpair[s_neigh]]])
                #print('pair_info[s_neigh, :num_eachpair[s_neigh]]',pair_info[s_neigh, :num_eachpair[s_neigh]])
                #temp = find_loc_int(pair_global_indices[1,pair_info[s_neigh, :num_eachpair[s_neigh]]], l_neigh, p_size)

                #temp_info = [i for i in pair_info[s_neigh, :num_eachpair[s_neigh]]-1 if i >=0 ]

                # print(temp_info)
                #temp = torch.where(torch.eq(pair_global_indices[1,temp_info], l_neigh))[0]
                jk_index = find_loc_int(pair_global_indices[1,temp_info], l_neigh)
                #jk_index = find_loc_int(pair_global_indices[1,pair_info[s_neigh, :num_eachpair[s_neigh]]], l_neigh, p_size)
                '''
                if jk_index == None:
                #     print("num_eachpair[sneigh]",num_eachpair[s_neigh])
                     print("pair_info:",pair_info[s_neigh, :num_eachpair[s_neigh]] - 1)
                #     print("pair_info-1:",temp_info)
                     print("global_indices:",pair_global_indices[1,pair_info[s_neigh, :num_eachpair[s_neigh]] - 1])
                #     continue
                   
                #print('jkindex',temp)
                # jk_index = temp[0] 
                print('jk_idx',jk_index)
                '''

                if jk_index != None:
                    jk_pair_loc = pair_info[s_neigh, jk_index] - 1
                    #print(' jk_pair_loc when jk =None', jk_pair_loc)
                    v2_unit = unitvects_pair[0, ik_pair_loc,:]

                    if (gvects[ik_pair_loc] < 0) or (gvects[ij_pair_loc] < 0):

                        a1 = torch.sum(v1_unit * v2_unit)
                        dist_3 = pairs[ij_pair_loc] ** 2 + pairs[ik_pair_loc] ** 2 - 2 * pairs[ij_pair_loc] * pairs[ik_pair_loc] * a1
                        v3_unit = (pairs[ik_pair_loc] * v2_unit - pairs[ij_pair_loc] * v1_unit) / torch.sqrt(dist_3)
                        #print('dist3 if',dist_3)
                    else:
                        #print('2nd case ijloc',ij_pair_loc)
                        a1 = torch.sum(v1_unit * v2_unit)
                        v3_unit = unitvects_pair[0, jk_pair_loc,:]
                        dist_3 = pairs[ij_pair_loc] ** 2 + pairs[ik_pair_loc] ** 2 - 2 * pairs[ij_pair_loc ] * pairs[ik_pair_loc] * a1
                        #dist_3 = pairs[jk_index]
                        #print('!!!pairs[ij_pair_loc]',pairs[ij_pair_loc - 1])
                        #print('!!!pairs[ik_pair_loc]',pairs[ik_pair_loc - 1])
                       # print('!!!a1 ',a1 )
                        #print('dist3 else',i, dist_3)

                    if dist_3 < rcut2:
                        #print("i,j,k ->",'dist_3',i,j_neigh,k_neigh,'  ',dist_3 )
                        ntriplets += 1

                        if torch.abs(a1) > 0.999999999999:
                            angles[0, ntriplets - 1] = 0.0
                        else:
                            angles[0, ntriplets - 1] = torch.acos(a1)

                        b1 = torch.sum(-v1_unit * v3_unit)
                        if torch.abs(b1) > 0.999999999999:
                            angles[1, ntriplets - 1] = 0.0
                        else:
                            angles[1, ntriplets - 1] = torch.acos(b1)

                        c1 = torch.sum(v2_unit * v3_unit)
                        if torch.abs(c1) > 0.999999999999:
                            angles[2, ntriplets - 1] = 0.0
                        else:
                            angles[2, ntriplets - 1] = torch.acos(c1)

                        #print('angle_indices[2, ntriplets - 1]',angle_indices[2, ntriplets - 1])
                        #print('jk_pair_loc',jk_pair_loc)
                        #print(len(pair_info[s_neigh,:]))
                        #print("angle_indices.shape=",angle_indices.shape)
                        angle_indices[0, ntriplets - 1] = ij_pair_loc
                        angle_indices[1, ntriplets - 1] = ik_pair_loc
                        angle_indices[2, ntriplets - 1] = jk_pair_loc

    return ntriplets, angles, angle_indices


def fg2(nFPs, theta_ss, etas, zetas, lambdas, theta, fexpt, fct, element_modes):
    
    # outputs
    outputs = torch.zeros(nFPs, dtype=torch.double)
    f_thetas = torch.zeros(nFPs, dtype=torch.double)
    f_rs = torch.zeros(nFPs, dtype=torch.double)
    f_thetas_1 = torch.zeros(nFPs, dtype=torch.double)

    f_thetas_1 = 1 + lambdas * torch.cos(theta - theta_ss)
    f_thetas = f_thetas_1 ** (zetas - 1)
    f_rs = fexpt ** etas
    G2_outputs = f_thetas * f_thetas_1 * f_rs * fct * 2 ** (1 - zetas)

    outer_product = torch.outer(G2_outputs, element_modes)

    # 将外积结果展平成一维向量
    outputs = outer_product.reshape(-1)  # 或者使用 .view(-1)

    return outputs,f_thetas, f_rs, f_thetas_1


def fg2_dg(nFPs, thetas, r_cut, Rij, Rik, Rjk, uvect_ij, uvect_ik, uvect_jk,
           fexp, fcuts, sins, theta_ss, reduced_etas, zetas, lambdas, element_modes):

    #thetas = torch.tensor(thetas, dtype=torch.float64)
    #reduced_etas = torch.tensor(reduced_etas, dtype=torch.float64)
    #zetas = torch.tensor(zetas, dtype=torch.float64)
    #lambdas = torch.tensor(lambdas, dtype=torch.float64)

    #uvect_ij = torch.zeros(3, dtype=torch.float64)
    #uvect_ik = torch.zeros(3, dtype=torch.float64)
    #uvect_jk = torch.zeros(3, dtype=torch.float64)
    dth_ijk = torch.zeros((3,3,3), dtype=torch.float64)

    # Initialize output tensors 
    # dim_mode = 2
    outputs = torch.zeros((3, nFPs*2), dtype=torch.float64)
    dg2 = torch.zeros((3, 3, 3, nFPs*2), dtype=torch.float64)

    # Constants and temp vars
    #print('thetas.size',thetas.size())

    radians = Pi / 180.0
    thetas_shft = torch.zeros((3,nFPs), dtype=torch.double)
    f_thetas_base = torch.zeros((3, nFPs), dtype=torch.double)
    f_thetas_m1 = torch.zeros((3, nFPs), dtype=torch.double)
    f_thetas = torch.zeros((3, nFPs), dtype=torch.double)
    f_rs = torch.zeros((3, nFPs), dtype=torch.double)
    dfrs_i_1 = torch.zeros((3, nFPs), dtype=torch.double)
    dfrs_i_2 = torch.zeros((3, nFPs), dtype=torch.double)
    dfrs_i_3 = torch.zeros((3, nFPs), dtype=torch.double)
    dth_coeff = torch.zeros((3, nFPs), dtype=torch.double)
    dth_i_1 = torch.zeros((3, nFPs), dtype=torch.double)
    dth_i_2 = torch.zeros((3, nFPs), dtype=torch.double)
    dth_i_3 = torch.zeros((3, nFPs), dtype=torch.double)

    # Calculations
    inv_rcut = 1 / (r_cut * r_cut)
    reduced_etas = reduced_etas * inv_rcut
    cos_theta = torch.cos(thetas)
    calc_dth = torch.abs(cos_theta[0]) < 0.999999 #Boolen

    fexpt = torch.prod(fexp)
    fct = torch.prod(fcuts)

    if calc_dth.all:
        inv_sin = -1 / torch.sqrt(1 - cos_theta * cos_theta)  # r_ijik r_jijk r_kjki

        uvect_sq_ij = (1 - uvect_ij * uvect_ij) / Rij
        uvect_sq_ik = (1 - uvect_ik * uvect_ik) / Rik
        uvect_sq_jk = (1 - uvect_jk * uvect_jk) / Rjk

        # center on i
        rij_ik = uvect_sq_ij * uvect_ik
        rik_ij = uvect_sq_ik * uvect_ij
        dth_ijk[0, 1, :] = inv_sin[0] * (uvect_ik - uvect_ij * cos_theta[0]) / Rij  # rij_ik, wrt j
        dth_ijk[0, 2, :] = inv_sin[0] * (uvect_ij - uvect_ik * cos_theta[0]) / Rik  # wrt k
        dth_ijk[0, 0, :] = - dth_ijk[0, 1, :] - dth_ijk[0, 2, :]  # wrt i

        # center on j
        rij_jk = uvect_sq_ij * uvect_jk
        rjk_ij = uvect_sq_jk * uvect_ij
        dth_ijk[1, 0, :] = inv_sin[1] * (uvect_jk + uvect_ij * cos_theta[1]) / Rij  # rij_jk
        dth_ijk[1, 2, :] = -inv_sin[1] * (uvect_ij + uvect_jk * cos_theta[1]) / Rjk  # rjk_ij
        dth_ijk[1, 1, :] = -dth_ijk[1, 0, :] - dth_ijk[1, 2, :]

        # center on k
        rjk_ik = uvect_sq_jk * uvect_ik  # x, y, z
        rik_jk = uvect_sq_ik * uvect_jk 
        dth_ijk[2, 0, :] = -inv_sin[2] * (uvect_jk - uvect_ik * cos_theta[2]) / Rik  # rjk_ik
        dth_ijk[2, 1, :] = -inv_sin[2] * (uvect_ik - uvect_jk * cos_theta[2]) / Rjk  # rik_jk
        dth_ijk[2, 2, :] = -dth_ijk[2, 1, :] - dth_ijk[2, 0, :]
    else:
        dth_ijk = torch.zeros((3, 3, 3), dtype=torch.float64)

    sins = Pi / (2. * r_cut) * sins
    dfc_ij_i = sins[0] * uvect_ij
    dfc_ij_j = -dfc_ij_i
    dfc_ik_i = sins[1] * uvect_ik
    dfc_ik_k = -dfc_ik_i
    dfc_jk_j = sins[2] * uvect_jk
    dfc_jk_k = -dfc_jk_j

    fcuts_12 = fcuts[0] * fcuts[1]
    fcuts_23 = fcuts[1] * fcuts[2]
    fcuts_13 = fcuts[0] * fcuts[2]
    dfc_i = fcuts_23 * dfc_ij_i + fcuts_13 * dfc_ik_i
    dfc_j = fcuts_23 * dfc_ij_j + fcuts_12 * dfc_jk_j
    dfc_k = fcuts_13 * dfc_ik_k + fcuts_12 * dfc_jk_k

    vect_ij = Rij * uvect_ij
    vect_ik = Rik * uvect_ik
    vect_jk = Rjk * uvect_jk
    dexp_i = vect_ij + vect_ik
    dexp_j = -vect_ij + vect_jk
    dexp_k = -vect_ik - vect_jk

    for i in range(3):

        #print('thetas[i]',thetas[i].size())
        #print('theta_ss',theta_ss[i,:].size())

        #prepare EG MODE
        center_mode = element_modes[i]
        other_idx4center = (0, 1, 2)[:i] + (0, 1, 2)[i+1:]
        mode_1 = element_modes[other_idx4center[0]]
        mode_2 = element_modes[other_idx4center[1]]
        arm_1 = torch.tensor([0.5 * (center_mode + mode_1), center_mode - mode_1], dtype=torch.float64)
        arm_2 = torch.tensor([0.5 * (center_mode + mode_2), center_mode - mode_2], dtype=torch.float64)
        mode = arm_1 + arm_2
        #mode /= torch.norm(mode, p = 2 ) # normolization
        #print('mode in G2',arm_1,arm_2,mode)

        thetas_shft[i,:]  = thetas[i] - theta_ss[i,:nFPs] #theta_ss[i, :] 

        #print(' thetas_shft[i,:]',thetas_shft)
        f_thetas_base[i,:]  = 1 + lambdas[i,:nFPs] * torch.cos(thetas_shft[i,:])
        f_thetas_m1[i,:]  = f_thetas_base[i,:]  ** (zetas[i,:nFPs]  - 1)
        f_thetas[i,:]  = f_thetas_m1[i,:]  * f_thetas_base[i,:]
        f_rs[i,:]  = fexpt ** reduced_etas[i,:nFPs]

        #print('SIZE',(f_thetas[i,:]  * f_rs[i,:]  * fct * 2 ** (1 - zetas[i,:nFPs])).size())
        #print('SIZE_mode',mode.size())

        outputs[i,:] = torch.outer( f_thetas[i,:]  * f_rs[i,:]  * fct * 2 ** (1 - zetas[i,:nFPs]) , mode) .reshape(-1)
        #print('outputs[i,:] ',outputs[i,:] )

        for j in range(3):

            dfrs_i_1[j,:]  = 2 * reduced_etas[i,:nFPs]  * f_rs[i,:]  * dexp_i[j]
            dfrs_i_2[j,:]  = 2 * reduced_etas[i,:nFPs]  * f_rs[i,:]  * dexp_j[j]
            dfrs_i_3[j,:]  = 2 * reduced_etas[i,:nFPs]  * f_rs[i,:]  * dexp_k[j]

        if calc_dth:
            dth_coeff = f_thetas_m1[i,:]  * zetas[i,:nFPs]  * (-lambdas[i,:nFPs]  * torch.sin(thetas_shft[i,:] ))

            for j in range(3):
                dth_i_1 = dth_coeff * dth_ijk[i, 0, j]
                dth_i_2 = dth_coeff * dth_ijk[i, 1, j]
                dth_i_3 = dth_coeff * dth_ijk[i, 2, j]

                dg2[i, 0, j] = torch.outer((dth_i_1 * f_rs[i,:] * fct + 
                                f_thetas[i,:]  * (dfrs_i_1[j, :] * fct + 
                                f_rs[i,:]  * dfc_i[j])) * (2**(1-zetas[i,:nFPs] )) ,mode).reshape(-1)
                #print('dg2[i, 0, j]',dg2[i, 0, j])
                dg2[i, 1, j] = torch.outer((dth_i_2 * f_rs[i,:]  * fct + 
                                f_thetas[i,:]  * (dfrs_i_2[j,:] * fct + 
                                f_rs[i,:]  * dfc_j[j])) * (2**(1-zetas[i,:nFPs] )) ,mode).reshape(-1)
                dg2[i, 2, j] = torch.outer((dth_i_3 * f_rs[i,:]  * fct + 
                                f_thetas[i,:]  * (dfrs_i_3[j,:] * fct + 
                                f_rs[i,:]  * dfc_k[j])) * (2**(1-zetas[i,:nFPs] )) ,mode).reshape(-1)
        else:
            for k in range(3):
                dg2[i, 0, k] = torch.outer((dfrs_i_1[k,:] * fct + f_rs[i,:] * dfc_i[k]) * f_thetas[i,:]  * (2**(1-zetas[i,:nFPs] )) ,mode).reshape(-1)
                dg2[i, 1, k] = torch.outer((dfrs_i_2[k,:] * fct + f_rs[i,:] * dfc_j[k]) * f_thetas[i,:]  * (2**(1-zetas[i,:nFPs] )) ,mode).reshape(-1)
                dg2[i, 2, k] = torch.outer((dfrs_i_3[k,:] * fct + f_rs[i,:] * dfc_k[k]) * f_thetas[i,:]  * (2**(1-zetas[i,:nFPs] )) ,mode).reshape(-1)

    #print('dg2',dg2)
    return outputs, dg2

def fdg2(nFPs, theta_ss, etas, zetas, lambdas, r_cut, 
         thetas, f_thetas_1, f_thetas, f_rs, Rs, fcs,
         sins, unitvects, vectsigns):
    # Convert input values to PyTorch tensors

    nFPs = torch.tensor(nFPs, dtype=torch.int)
    theta_ss = torch.tensor(theta_ss, dtype=torch.double)
    etas = torch.tensor(etas, dtype=torch.double)
    zetas = torch.tensor(zetas, dtype=torch.double)
    lambdas = torch.tensor(lambdas, dtype=torch.double)
    r_cut = torch.tensor(r_cut, dtype=torch.double)
    thetas = torch.tensor(thetas, dtype=torch.double)
    f_thetas_1 = torch.tensor(f_thetas_1, dtype=torch.double)
    f_thetas = torch.tensor(f_thetas, dtype=torch.double)
    f_rs = torch.tensor(f_rs, dtype=torch.double)
    Rs = torch.tensor(Rs, dtype=torch.double)
    fcs = torch.tensor(fcs, dtype=torch.double)
    sins = torch.tensor(sins, dtype=torch.double)

    unitvects = torch.zeros(3, 3 , dtype=torch.double)
    vectsigns = torch.zeros(3 , dtype=torch.double)

    # Define some constants
    pi_rc = Pi / (2.0 * r_cut)
    inv_Rc2 = 2.0 / (r_cut ** 2)
    cv = f_thetas * f_rs * (2.0 ** (1.0 - zetas))
    cos_thetas = torch.cos(thetas)

    # Define output arrays
    dfp_i = torch.zeros(3, nFPs, dtype=torch.float64)
    dfp_j = torch.zeros(3, nFPs, dtype=torch.float64)
    dfp_k = torch.zeros(3, nFPs, dtype=torch.float64)

    coeff1 = pi_rc
    coeff2 = inv_Rc2 * etas

    # Calculate derivatives
    # If theta is not equal to 0 or Pi
    if (thetas != Pi) and (thetas != 0):
        # Calculate coefficients

        coeff3 = lambdas * zetas * torch.sin(thetas - theta_ss) / torch.sqrt(1.0 - cos_thetas ** 2)
        tcoeff2 = coeff2 * fcs[0] * fcs[1]
        tcoeff3 = coeff3 * fcs[0] * fcs[1]
        ttcoeff3 = coeff3 * cos_thetas

        # Calculate terms for atom i
        i_term1 = pi_rc * sins[0] * fcs[1]
        i_term2 = tcoeff2 * Rs[0]
        i_term3 = tcoeff3 * (cos_thetas / Rs[0] - 1.0 / Rs[1])
        dfpi_p1 = f_thetas_1 * (i_term1 + i_term2) + i_term3
        i_term1 = pi_rc * sins[1] * fcs[0]
        i_term2 = tcoeff2 * Rs[1]
        i_term3 = tcoeff3 * (cos_thetas / Rs[1] - 1.0 / Rs[0])
        dfpi_p2 = f_thetas_1 * (i_term1 + i_term2) + i_term3

        # Calculate terms for atom j and k
        j_term1 = pi_rc * sins[0]
        k_term1 = pi_rc * sins[1]
        j_term2 = coeff2 * fcs[0] * Rs[0]
        k_term2 = coeff2 * fcs[1] * Rs[1]
        j_term3 = ttcoeff3 * fcs[0] / Rs[0]
        k_term3 = ttcoeff3 * fcs[1] / Rs[1]
        dfpj_p1 = -(j_term3 + f_thetas_1 * (j_term1 + j_term2)) * fcs[2]
        dfpk_p2 = -(k_term3 + f_thetas_1 * (k_term1 + k_term2)) * fcs[2]
        tcoeff3 = coeff3 * fcs[2]
        dfpj_p2 = tcoeff3 * fcs[0] / Rs[0]
        dfpk_p1 = tcoeff3 * fcs[1] / Rs[1]
        coeff1 = pi_rc * sins[2]
        j_term1 = coeff1 * fcs[0]
        k_term1 = coeff1 * fcs[1]
        tcoeff2 = coeff2 * Rs[2] * fcs[2]
        j_term2 = tcoeff2 * fcs[0]
        k_term2 = tcoeff2 * fcs[1]
        dfpj_p3 = f_thetas_1 * (j_term1 + j_term2)
        dfpk_p3 = - f_thetas_1 * (k_term1 + k_term2)

        # Calculate derivatives for three atoms
        for i in range(3):
            dfp_i[i, :] = (dfpi_p1 * unitvects[0, i] * vectsigns[0] + dfpi_p2 * unitvects[1, i] * vectsigns[1]) * cv * fcs[2]
            dfp_j[i, :] = (dfpj_p1 * unitvects[0, i] * vectsigns[0] + dfpj_p2 * unitvects[1, i] * vectsigns[1] + dfpj_p3 * unitvects[2, i] * vectsigns[2]) * cv * fcs[1]
            dfp_k[i, :] = (dfpk_p1 * unitvects[0, i] * vectsigns[0] + dfpk_p2 * unitvects[1, i] * vectsigns[1] + dfpk_p3 * unitvects[2, i] * vectsigns[2]) * cv * fcs[0]

    # Otherwise, if theta is equal to 0 or Pi
    else:
        cv = f_thetas_1 * cv

        i_term1 = pi_rc * sins[0] * fcs[1]
        tcoeff2 = inv_Rc2 * etas * fcs[0] * fcs[1]
        i_term2 = tcoeff2 * Rs[0]
        dfpi_p1 = i_term1 + i_term2

        i_term1 = pi_rc * sins[1] * fcs[0]
        i_term2 = tcoeff2 * Rs[1]
        dfpi_p2 = i_term1 + i_term2

        j_term1 = pi_rc * sins[0]
        k_term1 = pi_rc * sins[1]
        j_term2 = inv_Rc2 * etas * fcs[0] * Rs[0]
        k_term2 = inv_Rc2 * etas * fcs[1] * Rs[1]
        dfpj_p1 = -(j_term1 + j_term2) * fcs[2]
        dfpk_p2 = -(k_term1 + k_term2) * fcs[2]

        coeff1 = pi_rc * sins[2]
        j_term1 = coeff1 * fcs[0]
        k_term1 = coeff1 * fcs[1]
        tcoeff2 = inv_Rc2 * etas * Rs[2] * fcs[2]
        j_term2 = tcoeff2 * fcs[0]
        k_term2 = tcoeff2 * fcs[1]
        dfpj_p3 = j_term1 + j_term2
        dfpk_p3 = - k_term1 - k_term2

        # Calculate derivatives for three atoms
        for i in range(3):
            dfp_i[i, :] = (dfpi_p1 * unitvects[0, i] * vectsigns[0] + dfpi_p2 * unitvects[1, i] * vectsigns[1]) * cv * fcs[2]
            dfp_j[i, :] = (dfpj_p1 * unitvects[0, i] * vectsigns[0] + dfpj_p3 * unitvects[2, i] * vectsigns[2]) * cv * fcs[1]
            dfp_k[i, :] = (dfpk_p2 * unitvects[1, i] * vectsigns[1] + dfpk_p3 * unitvects[2, i] * vectsigns[2]) * cv * fcs[0]

    return dfp_i, dfp_j, dfp_k


def find_loc_char(arr, val, size_arr):

    for i in range(size_arr):
        if arr[i] == val:
            return i

def find_loc_int(arr, val):

    find_loc_int = torch.where(torch.eq(arr, val))
    if len(find_loc_int[0]) == 0:
        return None
    else: 
        return find_loc_int[0][0]
