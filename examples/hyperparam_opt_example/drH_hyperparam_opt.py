#!/usr/bin/env python
import os, subprocess
import shutil
import sys
import numpy as np
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
from pyamff.config import ConfigClass #

#from default_fp import default_fps_main #if we combine with AMP default fps; default_fp is stand alone - no pyamff needed
### would/might be good to get a minimum code of Lei/Naman G(r) based fingerprints scheme
### from /tool/python_gr and its usage in  config.py
###Right now we require the user to give us an fpParas.dat 

### Current Flags Allowed For the Hyperparameter Training
"""
optimize_activation_func:
optimize_scaler:
optimize_force_coefficient:
optimize_model_architecture:
"""

def edit_config(total_path,flag,value):
    # inputs are a file path (config.ini, a flag (e.g "hidden_layers"), and value we want the flag to have (i.e "5 2")
    # Will we request the user to always give us an initial set of hyperparameters?
    with open(total_path, "r") as f: # read config.ini in some /tanh/..._model/config.ini
        lines = f.readlines() # read that file - right now it is exactly same as config in path_initial
    change_line = 0 # keep track of whether we have changed the hyperparam flag line or not
    with open(total_path, "w") as f: #open the file again with write (i.e overwriting now)
        for line in lines: # for each line in the file 
            if line.startswith(flag): #if that line has our flag
                f.write(flag+ " = "+value+"\n") #overwrite the flag with our value
                change_line = 1 # we have updated the config.ini 
            else: # otherwise
                f.write(line) # rewrite the flag content as in original config.ini
        # the code below automatically adds to end of config.ini, which will error
        if change_line == 0: # have to resolve this requirement with yaml defaults somehow
            # Alert: e.g if no activation func given, but optimize_acft on
            #f.write(flag+ " = "+value+"\n")
            sys.exit('please set an initial '+flag+' in config.ini')
        f.close() #close the file
    return 0

# In a directory, need config.ini, fpParas, and traj file to run pyamff

### This function will parse the config.ini for the user specified hyperparameter options
def get_hyperparam_opt_options(filename='config.ini'):
    opt_actf = config.config['optimize_activation_func']
    opt_scaler = config.config['optimize_scaler']
    opt_fcoeff = config.config['optimize_force_coefficient']
    opt_model_arch = config.config['optimize_model_architecture']
    return opt_actf,opt_scaler,opt_fcoeff,opt_model_arch

### condsidering we don't want a matrix, and will change only one hyperparameter at a time;
### we can rely on just 1 list to name directories ...
def one_hyperparam_space_set_up(hp_list,flag='activation_function',replace_space=False,traj_name = 'train.traj',sub_file = None,run_no_queue=False):
    # copy original user config, and only edit 1 line 
    for hp in range(len(hp_list)):
        if replace_space ==True:
            dir_name = flag +'_' + hp_list[hp].replace(" ", "_") # replace spaces in hidden layers with _
        else:
            dir_name = flag +'_' + hp_list[hp]
        if flag =='activation_function' and hp ==0 : # if it's is the first activation func, set the directory name with _user
            # helps with log analysis, this is the default model the user gave us
            dir_name ='user_'+dir_name
        model_dir =(path_initial+"/"+dir_name+"_model")
        #print ("model_dir: ",model_dir)
        if os.path.exists(model_dir)==False: #if the directory does not exist -i.e calc not set up yet
           os.mkdir(model_dir) #make the directory 
        os.chdir(model_dir) # go to the directory
        if sub_file and run_no_queue == False: # assuming SGE queue
            # run the directory via queue
            shutil.copyfile(path_initial+"/"+sub_file ,model_dir+"/"+sub_file) # copy the submission script
            run_command = "qsub "+sub_file
            os.system(run_command) # submit to queue
        elif sub_file and  run_no_queue == True: # will run directly
            # run the directory without a queue on the head node
            shutil.copyfile(path_initial+"/"+sub_file ,model_dir+"/"+sub_file) # copy the submission script
            change_permission = "chmod 755 "+sub_file # make it executable
            os.system(change_permission) # 
            run_command = "./"+sub_file # now run command is ./script
            os.system(run_command) # run it
        else: # setup calc
            # copy the config, fpParas and traj file to the made directory
            shutil.copyfile(path_initial+"/config.ini" ,model_dir+"/config.ini") # copy the original config to unique model directory
            shutil.copyfile(path_initial+"/fpParas.dat" ,model_dir+"/fpParas.dat") # copy the orginal fpParas
            shutil.copyfile(path_initial+"/"+traj_name ,model_dir+"/"+traj_name) # copy the trajectory file 
            # Edit the config.ini with needed hyperparameters    
            edit_config("config.ini",flag,hp_list[hp]) #i.e in the new directory with flag(s) we are training        
    return 0

#Function to set up calculations for all the hyperparameters (lists will be empty if user did not optimize that hyperparam)
def setup_calc(actf_list,fcoeff_list,scaler_list,dModel_dNeurons_list,dModel_dHid_layers_list,traj_name = 'train.traj'):
    #### we want the user's model, and for each list, only change that hyperparam
    one_hyperparam_space_set_up(actf_list,flag='activation_function',traj_name=traj_name)# set's up user's model and dModel/d(actF)
    one_hyperparam_space_set_up(fcoeff_list,flag='force_coefficient',traj_name=traj_name)# dModel/d(Force Coeff)
    one_hyperparam_space_set_up(scaler_list,flag='scaler_type',traj_name=traj_name)#dModel/d(Scaler)
    one_hyperparam_space_set_up(dModel_dNeurons_list,flag='hidden_layers',traj_name=traj_name,replace_space=True) #dModel/d(num_Neurons/layer)
    one_hyperparam_space_set_up(dModel_dHid_layers_list,flag='hidden_layers',traj_name=traj_name,replace_space=True) #dModel/d(num_Hid Layers)_num_neuron const.
    return 0

##### Run all the models based on queue or directly running depending upon -rq or -r given
def run_models(actf_list,fcoeff_list,scaler_list,dModel_dNeurons_list,dModel_dHid_layers_list,traj_name='train.traj',sub_file='run.sh',run_no_queue=False):
    #This function will only be called when the user gives a submission script
    #Don't set up any calcualtions, pass the sub file to func
    if sub_file and run_no_queue == False:
        one_hyperparam_space_set_up(actf_list,flag='activation_function',sub_file=sub_file)
        one_hyperparam_space_set_up(fcoeff_list,flag='force_coefficient',sub_file=sub_file)
        one_hyperparam_space_set_up(scaler_list,flag='scaler_type',sub_file=sub_file)
        one_hyperparam_space_set_up(dModel_dNeurons_list,flag='hidden_layers',replace_space=True,sub_file = sub_file)
        one_hyperparam_space_set_up(dModel_dHid_layers_list,flag='hidden_layers',replace_space=True,sub_file = sub_file)
    else: #sub_file and now run_no_queue = True
        one_hyperparam_space_set_up(actf_list,flag='activation_function',sub_file=sub_file,run_no_queue=run_no_queue)
        one_hyperparam_space_set_up(fcoeff_list,flag='force_coefficient',sub_file=sub_file,run_no_queue=run_no_queue)
        one_hyperparam_space_set_up(scaler_list,flag='scaler_type',sub_file=sub_file,run_no_queue=run_no_queue)
        one_hyperparam_space_set_up(dModel_dNeurons_list,flag='hidden_layers',replace_space=True,sub_file = sub_file,run_no_queue=run_no_queue)
        one_hyperparam_space_set_up(dModel_dHid_layers_list,flag='hidden_layers',replace_space=True,sub_file = sub_file,run_no_queue=run_no_queue)
    return 0
    
# given a path of trained model; 
# return an array of the [filename,Test U RMSE,Test F RMSE, Train U RMSE,Train F RMSE,time(s) for cross validation]
def report_path_result(path):
    ### convert something like: 
    ### True Model U RMSE on Test Set:  5.259957265052852
    ### True Model F RMSE on Test Set:  1.3270509103213997
    ### to ['dummy_path_model',5.2599572,1.327051]
    os.chdir(path) # go the directory
    if os.path.exists('./pyamff.log') ==True:
        lines = os.popen('tail -6 pyamff.log').read()
    else:
        no_log = "please check "+path+ ", it does not have pyamff.log"
        sys.exit(no_log)
    lines = lines.split('\n')
    training_data = lines[0].lstrip()
    training_data = training_data.split()
    Training_U_rmse = training_data[4]
    Training_F_rmse = training_data[5]
    cross_validation_time =  lines[-4].split()[-1]
    Test_U_rmse = lines[-3].split()[-1]
    Test_F_rmse = lines[-2].split()[-1]
    result = [path,float(Test_U_rmse),float(Test_F_rmse),float(Training_U_rmse),float(Training_F_rmse),float(cross_validation_time)] #store as list
    #P.S in write_all_results we convert Ermse,Frmse to str again, but we also need to calculate dModel/dHyperparam
    return result

#Function to calculate rmses,compared to user's model; large negative numbers imply the hyperparameter is important to improving user's model
def dModel_dhp(list_all_path):
    # this should(and is) remain independent of setting up and running models - analysis for no scan mode
    #take a list of all dir,test E rmse, test F rmse, i.e [['filename',energy_rmse,force_rmse],...]
    #and returns formatted 'd(user_Model)/d(Hyperparameter)'
    for l in list_all_path: # for each file in directory
        # each l is ['filename',energy_rmse,force_rmse] 
        if l[0].startswith('user'): #if the file starts with user
            user_list = l
            list_all_path.remove(l) #keep it aside
            break
    #Initialize lists to populate w.r.t hyperparams
    actf_list,actf_difference_E_list,actf_difference_F_list = [],[],[]
    scaler_list,scaler_difference_E_list,scaler_difference_F_list = [],[],[]
    fcoeff_list,fcoeff_difference_E_list,fcoeff_difference_F_list = [],[],[]
    hidden_layer_list,hidden_layer_difference_E_list,hidden_layer_difference_F_list = [],[],[]
    #notice pyamff has one flag 'hidden_layers' for both num. neurons and num hidden layers
    for hp_list in list_all_path: # each list is some non-user hyperparam list
        if hp_list[0].startswith('activation'):
            actf_list.append(hp_list[0]) # to actf_filename_list, append file name
            actf_difference_E_list.append(-user_list[1]+hp_list[1]) # calculate change in E_rmse
            actf_difference_F_list.append(-user_list[2]+hp_list[2]) # calculate change in F_rmse
        if hp_list[0].startswith('scaler'):
            scaler_list.append(hp_list[0]) # to actf_filename_list, append file name
            scaler_difference_E_list.append(-user_list[1]+hp_list[1]) # calculate change in E_rmse
            scaler_difference_F_list.append(-user_list[2]+hp_list[2]) # calculate change in F_rmse
        if hp_list[0].startswith('force'):
            fcoeff_list.append(hp_list[0]) # to actf_filename_list, append file name
            fcoeff_difference_E_list.append(-user_list[1]+hp_list[1]) # calculate change in E_rmse
            fcoeff_difference_F_list.append(-user_list[2]+hp_list[2]) # calculate change in F_rmse
        if hp_list[0].startswith('hidden'):
            hidden_layer_list.append(hp_list[0]) # to actf_filename_list, append file name
            hidden_layer_difference_E_list.append(-user_list[1]+hp_list[1]) # calculate change in E_rmse
            hidden_layer_difference_F_list.append(-user_list[2]+hp_list[2]) # calculate change in F_rmse
    # convert all to individual hyperparam differential lists
    actf_diff_list = [actf_list,actf_difference_E_list,actf_difference_F_list]
    scaler_diff_list = [scaler_list,scaler_difference_E_list,scaler_difference_F_list]
    fcoeff_diff_list = [fcoeff_list,fcoeff_difference_E_list,fcoeff_difference_F_list]
    hidden_layer_list = [hidden_layer_list,hidden_layer_difference_E_list,hidden_layer_difference_F_list]
    return actf_diff_list,scaler_diff_list,fcoeff_diff_list,hidden_layer_list 

def get_plot_lists(list_all_path):
    # ONLY CALL FOR NO-SCAN(i.e normal) MODE  
    # This deserves its own function
    for l in list_all_path:
        # each l is ['filename',energy_rmse,force_rmse]
        if l[0].startswith('user'):
            user_list = l
            list_all_path.remove(l)
            break
    actf_E_list,actf_F_list =[],[] #
    scaler_E_list,scaler_F_list =[],[]#
    fcoeff_E_list,fcoeff_F_list =[],[]#
    hidden_layer_E_list,hidden_layer_F_list =[],[]#
    num_neuron_E_list,num_neuron_F_list =[],[]#
    #initialize the x to plot i.e forward,user or backward
    actf_x,scaler_x,fcoeff_x,hidden_layer_x,num_neuron_x = [],[],[],[],[]
    #get the user's hyperparameters to compute differential direction (backward/forwar)
    num_layer_user = len(config.config['hidden_layers']) # num of hidden layers
    num_neuron_user = config.config['hidden_layers'][0] #num neurons in first hidden layer
    fcoeff_user = config.config['force_coefficient']# force coefficient
    actf_temp_index,scaler_temp_index,opt_arch,opt_fcoeff =0,0,0,0
    # intialize indexes to keep track of how whether we have been in that if statement before
    actf_type =[] # to create annotation for which activation function forward and backward are ...
    # target: have arrays w.r.t hyperparams of format: actf_list = [['backward','user','forward'],[5.090615224426624,5.184304969647711,5.217318794563481],'actf']
    for hp_list in list_all_path: # each list is some non-user hyperparam list
        if hp_list[0].startswith('activation'): # this check ensures that activation function was optimized from the user
                if actf_temp_index == 0 : # if it is the first time we are coming here, want to get the user's model info as well
                    actf_x.append('backward')
                    actf_E_list.append(hp_list[1])
                    actf_F_list.append(hp_list[2])
                    actf_type.append(hp_list[0].split('_')[-2][0]) #gets the first letter of actf
                    # we also need to then add the user
                    actf_x.append('user')
                    actf_E_list.append(user_list[1])
                    actf_F_list.append(user_list[2])
                    actf_type.append(user_list[0].split('_')[-2][0]) #gets the first letter of user's actf
                else: # now we append the forward values
                    actf_x.append('forward')
                    actf_E_list.append(hp_list[1])
                    actf_F_list.append(hp_list[2])
                    actf_type.append(hp_list[0].split('_')[-2][0])
                actf_temp_index +=1
        if hp_list[0].startswith('scaler'):
                if scaler_temp_index == 0 :
                    scaler_x.append('backward')
                    scaler_E_list.append(hp_list[1])
                    scaler_F_list.append(hp_list[2])
                    # we also need to then add the user
                    scaler_x.append('user')
                    scaler_E_list.append(user_list[1])
                    scaler_F_list.append(user_list[2])
                else: # now we append the forward values
                    scaler_x.append('forward')
                    scaler_E_list.append(hp_list[1])
                    scaler_F_list.append(hp_list[2])
                scaler_temp_index +=1
        if hp_list[0].startswith('force'):
                ## Now forward and backward really mean something
                ## start by appending user data
                if opt_fcoeff ==0:
                    fcoeff_x.append('user')
                    fcoeff_E_list.append(user_list[1])
                    fcoeff_F_list.append(user_list[2])
                    opt_fcoeff =1
                if float(hp_list[0].split('_')[-2]) <fcoeff_user: # if backward
                    fcoeff_x.insert(0,'backward') # insert as first entry
                    fcoeff_E_list.insert(0,hp_list[1])
                    fcoeff_F_list.insert(0,hp_list[2])
                else:
                    fcoeff_x.append('forward') # append is fine
                    fcoeff_E_list.append(hp_list[1])
                    fcoeff_F_list.append(hp_list[2])
        if hp_list[0].startswith('hidden'): # the 2D combined into 1
                #start by keeping user data for both 
                if opt_arch == 0:
                    hidden_layer_x.append('user')
                    hidden_layer_E_list.append(user_list[1])
                    hidden_layer_F_list.append(user_list[2])
                    num_neuron_x.append('user')
                    num_neuron_E_list.append(user_list[1])
                    num_neuron_F_list.append(user_list[2])
                    opt_arch = 1
                #now we need to see which dimension (i.e num layers or num neurons)
                #and which direction (i.e forward or backward) this is
                num_layer = len(hp_list[0].split('_'))-3 # calculates the number of layers
                num_neuron = hp_list[0].split('_')[2] # calculates number of neurons in first layer
                if num_layer < num_layer_user: # if layers < user,this means backwards in hidden layer space
                    hidden_layer_x.insert(0,'backward')
                    hidden_layer_E_list.insert(0,hp_list[1])
                    hidden_layer_F_list.insert(0,hp_list[2])
                elif num_layer > num_layer_user: # if layers > user, forward in hidden layer space
                    hidden_layer_x.append('forward')
                    hidden_layer_E_list.append(hp_list[1])
                    hidden_layer_F_list.append(hp_list[2])
                else: # number of hidden layers is the same, it is a neuron directional change
                    if int(num_neuron) < num_neuron_user: # if the number of neurons in first layer is less than user's 
                        num_neuron_x.insert(0,'backward')
                        num_neuron_E_list.insert(0,hp_list[1])
                        num_neuron_F_list.insert(0,hp_list[2])
                    else: #neuron space forward 
                        num_neuron_x.append('forward')
                        num_neuron_E_list.append(hp_list[1])
                        num_neuron_F_list.append(hp_list[2])
    final_actf_plot_data = [actf_x,actf_E_list,actf_F_list,'act F',actf_type]
    final_scaler_plot_data = [scaler_x,scaler_E_list,scaler_F_list,'scaler']
    final_fcoeff_plot_data = [fcoeff_x,fcoeff_E_list,fcoeff_F_list,'f-coeff']
    final_hidden_layer_plot_data = [hidden_layer_x,hidden_layer_E_list,hidden_layer_F_list,'num hid. layers']
    final_num_neuron_plot_data = [num_neuron_x,num_neuron_E_list,num_neuron_F_list,'num neurons']
    return final_actf_plot_data,final_scaler_plot_data,final_fcoeff_plot_data,final_hidden_layer_plot_data,final_num_neuron_plot_data

def make_plot(actf_list,scaler_list,fcoeff_list,num_layers,num_neurons):
    #plot_list =[actf_list,scaler_list,fcoeff_list,num_layers,num_neurons]
    plot_list =[actf_list,scaler_list,fcoeff_list,num_neurons,num_layers] #note num_neuron purposely before num_hid layers
    #plot the Energy RMSEs
    opt_some_hyperparam = False
    fig1,ax1 = plt.subplots()
    for hp_list in plot_list:
        if len(hp_list[0])>0:
            opt_some_hyperparam = True
            plt.plot(hp_list[0],hp_list[1],label=hp_list[3])
            plt.scatter(hp_list[0],hp_list[1])
            if hp_list ==actf_list: #annotate the activation functions s,t,r for sigmoid,tanh,relu
                for actf in range(len(hp_list[0])):
                    plt.annotate(hp_list[4][actf],(hp_list[0][actf],hp_list[1][actf]))
    if opt_some_hyperparam == False:
        print ('user did not optmize any hyperperparam')
        return 0
    plt.legend()    
    plt.title('Hyperparameter Sensitivity on Energy RMSE')
    plt.xlabel('Hyperparameter Space')
    plt.ylabel('Test Energy RMSE')
    fig1.savefig('energy_sensitivity.png')
    #Now the Force RMSEs
    fig2,ax2 = plt.subplots()
    for hp_list in plot_list:
        if len(hp_list[0])>0:
            plt.plot(hp_list[0],hp_list[2],label=hp_list[3])
            plt.scatter(hp_list[0],hp_list[2])
            if hp_list ==actf_list:
                for actf in range(len(hp_list[0])):
                    plt.annotate(hp_list[4][actf],(hp_list[0][actf],hp_list[2][actf]))
    plt.legend()    
    plt.title('Hyperparameter Sensitivity on Force RMSE')
    plt.xlabel('Hyperparameter Space')
    plt.ylabel('Test Force RMSE')
    fig2.savefig('force_sensitivity.png')
    print ('sensitivity plots made')
    return 0

def write_grad(log_file = 'summary_models.txt',head_flag = 'Activation Function',one_grad_hp_list=[]):
    log_file.write('\n\u0394'+ head_flag+' \n')
    log_file.writelines(str(one_grad_hp_list[0]))
    log_file.write('\n\u0394 Test Energy RMSE \n')
    log_file.writelines(str(one_grad_hp_list[1]))
    log_file.write('\n\u0394 Test Force RMSE \n')
    log_file.writelines(str(one_grad_hp_list[2]))
    log_file.write('\n------------------------------')
    return 0

# for each trained model, summarize results into a text file
def write_all_results(main_path,mode='no_scan'):
    summary = open("summary_models.txt", "w") # open the file
    min_E_pair =['model',10**12]
    min_F_pair =['model',10**12]
    list_of_all_data = []
    user_E_rmse = 0.0
    user_F_rmse = 0.0
    min_E_rmse = 10**12
    min_F_rmse = 10**12
    summary.write(" FileName       Test Energy RMSE       Test Force RMSE        Training Energy RMSE       Training Force RMSE        Cross Val. Time on Test(s)\n")
    for file in os.listdir(): # for each file in main_path
        if file.endswith("_model"): # if the file ends in "_model"
            path = os.path.join(main_path,file) #get_path to run directory
            rel_path = os.path.relpath(path,start = main_path)
            for i in range (6): # for each i.e Filename,Test_U_rmse,Test_F_rmse,Train_U_rmse,Train_F_rmse,cv_time
                if i>0: # if not path (i.e energy or force RMSE)
                    summary.write("{:.6f}".format(report_path_result(path)[i])+"       ") # call report_path_result and write the results
                    file_data.append(report_path_result(path)[i])
                    if i == 1:# Test Energy RMSE
                        if report_path_result(path)[i] < min_E_rmse: 
                            min_E_rmse = report_path_result(path)[i]
                            min_E_pair[0]=rel_path # lists are ordered file,E rmse respectively
                            min_E_pair[1]=str(min_E_rmse)
                    if i == 2:# Test Force RMSE
                        if report_path_result(path)[i] < min_F_rmse:
                            min_F_rmse = report_path_result(path)[i]
                            min_F_pair[0]=rel_path # lists are ordered file,F rmse respectively
                            min_F_pair[1]=str(min_F_rmse)
                else: #i =0, filename
                    summary.write(rel_path+"       ")
                    # for the filename, store results (i.e E, F rmse);
                    file_data = []
                    file_data.append(rel_path)
            list_of_all_data.append(file_data[0:3]) # nothing will break if entire list is passed as well, which is faster?
            summary.write("\n")  # after written 1 line, start new line
    plt_actf,plt_scaler,plt_fcoeff,plt_nhl,plt_nn = get_plot_lists(list_of_all_data.copy())
    grad_model_hp = dModel_dhp(list_of_all_data) #calculates d(user_Model)/d(Hyperparameter)
    #Now write the analysis
    summary.write ("\nBest Energy RMSE Model: "+ min_E_pair[0] + ":    "+min_E_pair[1][0:8]+'\n')
    summary.write ("Best Force RMSE Model: "+ min_F_pair[0] + ":    "+min_F_pair[1][0:8] + '\n')
    if mode == 'no_scan':
        summary.write('\n(\u2207Model) w.r.t hyperparameters (Test Set) \n')
        for i in range(len(grad_model_hp)):
            if len(grad_model_hp[i][0])>0: # if the user optimized this hyperparam (otherwise lists are empty)
                if i==0:
                    write_grad(log_file = summary,head_flag = 'Activation Function',one_grad_hp_list=grad_model_hp[i])
                if i==1:
                    write_grad(log_file = summary,head_flag = 'Scaler Type',one_grad_hp_list=grad_model_hp[i])
                if i==2:
                    write_grad(log_file = summary,head_flag = 'Force Coefficient',one_grad_hp_list=grad_model_hp[i])
                if i==3:
                    summary.write("\nuser's model architecture: ")
                    summary.write(str(config.config['hidden_layers']))
                    write_grad(log_file = summary,head_flag = 'Model Architecture',one_grad_hp_list=grad_model_hp[i])
    summary.close() # close the summary file
    if mode == 'no_scan': # we can make the sensitivity plots
        os.chdir(main_path)
        make_plot(plt_actf,plt_scaler,plt_fcoeff,plt_nhl,plt_nn) #line works
    print ("summary_models.txt has been written") #tell user file created
    return 0 #success

def differential_force_coeff(user_fcoef,):
    ##shady numerical d(Models)/d(Force_Coefficient)
    temp = float(user_fcoef) # convert str to float
    if temp<0.0:
        print ("the given force coefficient is negative")
        sys.exit()
    diff_ratio = config.config['differential_ratio']
    backward_fcf = (1.0-diff_ratio)*temp # 20% algorithm (Dr.H) by default
    difference  = temp-backward_fcf      # backward differece
    forward_fcf = temp+difference        # forward to user's given force coeff
    final_list = ["{:.4f}".format(backward_fcf),"{:.4f}".format(forward_fcf)]
    # have to format b/c some times you get things like 0.0999999 not 0.10 
    return final_list

def differential_model_d_num_neurons(user_shape= " "):
    temp = user_shape
    # finite difference like scheme
    # keeping same number of layers, add and remove 20% neurons to each layer
    final_list = [] #initialize output list
    arch_same_num_layer_forward ="" #neuron space 'forward differential' initialization
    arch_same_num_layer_backward ="" #neuron space 'backward differential' initialization
    num_neuron_per_layer = user_shape
    diff_ratio = config.config['differential_ratio'] #user can specify in config.ini
    increase_ratio = 1.0+diff_ratio
    decrease_ratio = 1.0-diff_ratio
    for num_neuron in range (len(num_neuron_per_layer)): # for each number_of_neurons the user gave
        temp_forward = str(round(increase_ratio*num_neuron_per_layer[num_neuron])) # 20% extra, rounding
        temp_backward_num_neurons =round(decrease_ratio*num_neuron_per_layer[num_neuron])
        if temp_backward_num_neurons == 0: #in case the user gives a ratio such that backward neuron = 0; increase to 1
            temp_backward_num_neurons = 1 # basically a sanity check
            print ("note: at least one neuron is always in a layer") # update the user
        #temp_backward = str(round(decrease_ratio*num_neuron_per_layer[num_neuron])) # 20% less, rounding
        temp_backward = str(temp_backward_num_neurons) # 20% less, rounding
        if num_neuron != len(num_neuron_per_layer)-1: #if not last layer, add spaces
            arch_same_num_layer_forward +=temp_forward+" "
            arch_same_num_layer_backward += temp_backward+" "
        else: # at last layer, don't add spaces for fomatting
            arch_same_num_layer_forward +=temp_forward
            arch_same_num_layer_backward += temp_backward
    final_list.append(arch_same_num_layer_forward)
    final_list.append(arch_same_num_layer_backward)
    return final_list

def differential_model_d_num_hidden_layers(user_shape= " "):
    temp = user_shape
    num_neuron_per_layer = user_shape # string when read from config.ini
    # finite difference like scheme
    # additional hidden layer with same num neuron as user's architecture
    final_list=[]
    extra_hidden_layer_with_same_num_neuron = "" # 'layer space' forward differential
    # note the .split and re-combining solves formatting problems
    if len(num_neuron_per_layer) > 1: #If user gave more than 1 hidden layers, want backwards dModel/d(layer) as well
        remove_last_hidden_layer_with_same_num_neuron ="" ##num neurons in each layer stay the same, but last layer is removed
        for num_neuron in range(len(num_neuron_per_layer)):
            if num_neuron != len(num_neuron_per_layer)-1: #if not last layer, add spaces
                extra_hidden_layer_with_same_num_neuron +=str(num_neuron_per_layer[num_neuron])+" "
                remove_last_hidden_layer_with_same_num_neuron +=str(num_neuron_per_layer[num_neuron])+" "
            else: # at last layer, don't add spaces for fomatting for extra, and don't add anything to remove_last_hidden_layer
                extra_hidden_layer_with_same_num_neuron +=str(num_neuron_per_layer[num_neuron])
        remove_last_hidden_layer_with_same_num_neuron = remove_last_hidden_layer_with_same_num_neuron.rstrip() # rstrip over if statemnt in for loop ... 
        extra_hidden_layer_with_same_num_neuron+=" "+str(num_neuron_per_layer[-1]) # additional layer with same neurons as user's shape
        final_list.append(extra_hidden_layer_with_same_num_neuron)
        final_list.append(remove_last_hidden_layer_with_same_num_neuron)
    else : #only forward direction 
        for num_neuron in range(len(num_neuron_per_layer)):
            if num_neuron != len(num_neuron_per_layer)-1: #if not last layer, add spaces
                extra_hidden_layer_with_same_num_neuron +=str(num_neuron_per_layer[num_neuron])+" "
            else: # at last layer, don't add spaces for fomatting
                extra_hidden_layer_with_same_num_neuron +=str(num_neuron_per_layer[num_neuron])
        extra_hidden_layer_with_same_num_neuron+=" "+str(num_neuron_per_layer[-1]) # additional layer with same neurons as user's shape
        final_list.append(extra_hidden_layer_with_same_num_neuron)
    return final_list

#Add function to read /fingerprints once from 
#parent directory to save memory
#def read_fps(fpdir='fingerprints'):
#    if one_fpdir == True:
#        if 'fingerprints' in os.listdir():
#            print ("a start")    

def create_hyperparam_search_list(opt_actf=False,opt_scaler=False,opt_fcoeff=False,opt_model_arch=False):
    # user's model with activation function re-insertion should be enough right?
    if opt_actf == True:
        actf_list = ['relu','sigmoid','tanh'] #old default
        user_actf = config.config['activation_function']
        actf_list.remove(user_actf)
        actf_list.insert(0,user_actf)
        #now actf_list starts with user's activation func
    else:
        actf_list = [config.config['activation_function']]
    if opt_scaler == True:
        scaler_list = ['NoScaler','LinearScaler','STDScaler'] ###
        #scaler_list = ['NoScaler','LinearScaler'] ###Update:fixed Cross Validation Energy and Force RMSE for STDScaler
        user_scaler = config.config['scaler_type']
        scaler_list.remove(user_scaler)
    else:
        scaler_list = []
    if opt_fcoeff == True:
        fcoeff_list = differential_force_coeff(config.config['force_coefficient'])
    else:
        fcoeff_list = []
    #get the search space for model architectures
    if opt_model_arch == True:
        #model_arch_list = ['5','20','10 10'] #old defaults + user's structure
        temp = config.config['hidden_layers']
        dModel_dNeurons_list = differential_model_d_num_neurons(user_shape=temp) #Dr.Henkelman scheme: 20% rule, d_Model_d_num_neurons
        dModel_dHid_layers_list = differential_model_d_num_hidden_layers(user_shape = temp) # Dr.Henkelman scheme d_Model_d_num_hid_layers
    else: #user does not want to optimize nn shape
        dModel_dNeurons_list = [] #
        dModel_dHid_layers_list = [] # 
    #print ("actf_list func: ",actf_list)
    #print ("scaler_list: ",scaler_list)
    #print ("fcoeff_list: ",fcoeff_list)
    #print ("dModel_dNeurons_list: ",dModel_dNeurons_list)
    #print ("dModel_dHid_layers_list: ",dModel_dHid_layers_list)
    return actf_list,scaler_list,fcoeff_list,dModel_dNeurons_list,dModel_dHid_layers_list

def main():
    if config.config['cross_validation'] == False:
        sys.exit('please turn cross validation to True in config.ini') 
    run_only = False # will be used only when -rq is given
    run_only_no_queue = False #True when -r is given
    if len(sys.argv) > 1: # i.e more flags than just "python hyperparam_opt.py"
        if sys.argv[1] == "-s": # this 1 is array index; i.e python hyperparam_opt.py -s has len(sys.argv) = 2
           write_all_results(path_initial)
           return 0
        if sys.argv[1] =="-rq":  # if the user gives a -r 
            #sample usage: "python hyperparam_opt.py -r a.sub"
            print ("Note: we assume an SGE queue. ") #Have to make this for all common clusters
            run_only = True 
            run_file_name = sys.argv[2] # get the submission file name
            if run_file_name not in os.listdir():
                print ("the given run file is not in this directoecty")
                return 1
        if sys.argv[1] =="-r":  # if the user gives a -r 
            #sample usage: "python hyperparam_opt.py -r a.sub"
            run_only_no_queue = True 
            run_file_name = sys.argv[2] # get the submission file name
            if run_file_name not in os.listdir():
                print ("the given run file is not in this directoecty")
                return 1
    traj_name = config.config['trajectory_file']# get the traj filename from config.ini
    optimize_actf,optimize_scaler,optimize_fcoeff,optimize_model_arch = get_hyperparam_opt_options() #read the config.ini and get the user's optimize hyperparam flags
    # create the search space lists
    actf_list,scaler_list,fcoeff_list,dModel_dNeurons_list,dModel_dHid_layers_list = create_hyperparam_search_list(opt_actf=optimize_actf,opt_scaler=optimize_scaler,
                                                                            opt_fcoeff=optimize_fcoeff,opt_model_arch=optimize_model_arch)
    #default_fps_main(trajectory_file= traj_name,fp_level='low') # no math AMP default fps, options are 'low','normal','high'
    # This is the part where we can combine Lei/Naman G(r) based fps; or write another algorithm 
    #one_fpdir = False - if one_fpdir: #call/read /fingerprints - Naman suggestion; might have to edit main.py in pyamff,add flags, and update setup
    if run_only == True: # run pre-set up calculations
        print ("submitting set up models to train")
        run_models(actf_list,fcoeff_list,scaler_list,dModel_dNeurons_list,dModel_dHid_layers_list, sub_file=run_file_name)    
    if run_only_no_queue == True: # run pre-set up calculations
        print ("starting to run set up models to train")
        run_models(actf_list,fcoeff_list,scaler_list,dModel_dNeurons_list,dModel_dHid_layers_list, sub_file=run_file_name,run_no_queue=True)    
    if run_only ==False and run_only_no_queue ==False: #only set up calculations
       print ("setting up the ml models")
       setup_calc(actf_list,fcoeff_list,scaler_list,dModel_dNeurons_list,dModel_dHid_layers_list,traj_name=traj_name)
    print ('done')
    os.chdir(path_initial)  # for sanity check, return python to main dir
    return 0 

if __name__ == "__main__":
    ### Note: path_inital is defined globally - the directory from which we are running 
    ### We also assume the user has config.ini,fpParas,traj files in the directory from which we are running
    path_initial = os.getcwd() 
    config = ConfigClass()
    config.initialize()
    os.remove('pyamff.log') #remove init pyamff.log, not useful for this
    main()
