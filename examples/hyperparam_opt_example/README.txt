Hello,

We need an fpParas.dat, config.ini, and traj file is present in the directory
3 Options: python drH_hyperparam_opt.py : this sets up directories with user specfied hyperparameter optimization flag
given a run script, the user can run the jobs with -r option: i.e python drH_hyperparam_opt.py -r run.sh
if the user is using an SGE queue, they can use -rq option with a .sub file to use the queue
or edit one_hyperparam one_hyperparam_space_set_up func 'qsub' with submission command, e.g sbatch (if slurm queue)
once the models have been trained and tested,
the user can use run python drH_hyperparam_opt.py -s and get a summary and sensitivity plots of the models

Thank you
