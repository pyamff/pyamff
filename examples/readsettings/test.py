#!/usr/bin/env python

from pyamff.config import ConfigClass

config = ConfigClass()
config.initialize()
print("Parameters are stored in a disctionary:")
print("  ",config.config)
print("    To fetch the particular parameter, use 'config.config[<nameOFparameter>]'")
print("      i.e., fetch run_type with config.config['run_type']")
print("          run_type:", config.config['run_type'])
print("  Fetch Fingerprint paras")
print(config.config['fp_paras'].fp_paras)
