This example shows the result of running pyamff with no fpParas.dat file given by default


Important Tags for this example:
1. set gr_calc = True in Fingerprints section of config.ini
2. set gr_cutoff = 5 in Fingerprints section of config.ini
3. ensure fp_use_existing = False 

To run this example:
Step1: make a new directory BENCHMARK
Step2: move batches, fingerprints, gr_data, fpParas.dat, pyamff.log, mlff.pyamff to BENCHMARK directory
Step3: now run pyamff (make sure you only have config.ini and train_620.traj files)
Step4: compare your fingerprint choice in fpParas.dat with fpParas.dat in BENCHMARK directory
Step5: compare your gr in gr_data directory with gr in gr_data directory in BENCHMARK

Note: gr_process_num tag controls the parallelization for this calculation, change it to an approproiate number

