from amp import Amp
from amp.descriptor.gaussian import Gaussian
from amp.model.neuralnetwork import NeuralNetwork
from amp.model import LossFunction
from ase.io import Trajectory
from amp.utilities import hash_images
import os
"""
set up AMP
"""
Gs={'Li': [{'type': 'G2', 'element': 'Li', 'eta': 10.0, 'offset': 0.0},
{'type': 'G4', 'elements': ['Li', 'Li'], 'eta': 0.005, 'gamma': 1.0, 'zeta': 2.0}, {'type': 'G4', 'elements': ['Li', 'Li'], 'eta': 0.005, 'gamma': 1.0, 'zeta': 200.0}]}

descriptor=Gaussian(cutoff=4.5,Gs=Gs)
images=Trajectory('ml_li.traj','r')
images=hash_images(images, ordered=True)
descriptor.calculate_fingerprints(images=images, calculate_derivatives=True)
for index, hash in enumerate(images.keys()):
  fps = descriptor.fingerprints[hash]
  dfps = descriptor.fingerprintprimes[hash]
"""
set up PyAMFF
"""
from pyamff.config import ConfigClass
from pyamff.fingerprints.fingerprints import Fingerprints

config = ConfigClass()
config.initialize()
fp_paras = config.config['fp_paras'].fp_paras
nFPs = {}
for key in fp_paras.keys():
    nFPs[key] = len(fp_paras[key])
if os.path.isfile(config.config['trajectory_file']):
    images = Trajectory(config.config['trajectory_file'], 'r')
else:
    print("Trajectory file %s does not exist" % config.config['trajectory_file'], sys.stderr)
    sys.exit(2)

fpcalc = Fingerprints(uniq_elements=config.config['fp_paras'].uniq_elements, filename=config.config['fp_parameter_file'], nfps=nFPs)

chemsymbols = images[0].get_chemical_symbols()
fps_p, dfps_p = fpcalc.calcFPs(images[0], chemsymbols)


print('fps')
maxfpdiff = 0
for i in range(len(fps_p)):
  diff = fps[i][1] - fps_p[i]
  maxv = max(abs(diff))
  if maxv > maxfpdiff:
    maxfpdiff = maxv
  print(diff)
maxdiff = 0
print('dfps')
for key in dfps.keys():
  diff = dfps[key]-dfps_p[key]
  maxv = max(abs(diff))
  if maxv > maxdiff:
    maxdiff = maxv
  print(key, diff)
print('max diff: fps, dfps', maxfpdiff, maxdiff)




#conv = {'energy_rmse':0.0001,'force_rmse':0.01}
#calc.model.lossfunction = LossFunction(convergence=conv,force_coefficient=0.05)
#calc.train(images='ml_li.traj')
#print('test')
#print(calc.descriptor.fingerprints)

