from ase.io.trajectory import Trajectory
import numpy
import sys

#seperates a traj file into traj files with same number of atoms
traj_name ="train.traj"

if sys.argv[1]:
   traj_name = sys.argv[1]

ger = Trajectory(traj_name,mode='r')

j = 0
num_uniques = []

for i in range(len(ger)):
    temp = ger[i]
    #print ("structure: ",j,len(temp))
    j+=1
    print ("len(temp): ",len(temp))
    if len(temp) not in num_uniques: #we need to create the initial traj file
       num_uniques.append(len(temp))
       name = str(len(temp))
       t = Trajectory(name,mode = 'w')
       t.write(temp,energy=temp.get_potential_energy(),forces=temp.get_forces())
    else: #if not unique, append to old traj of that number of atoms
       name = str(len(temp))
       t = Trajectory(name,mode = 'a')
       t.write(temp,energy=temp.get_potential_energy(),forces=temp.get_forces())
       
       

print ("num_uniques: ",num_uniques)
print ("Number of Unique Number of Atoms: ",len(num_uniques))
