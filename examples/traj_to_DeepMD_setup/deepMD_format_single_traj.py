from ase.io.trajectory import Trajectory
import numpy
import sys
import os

dir_name = '1'  ###Type the Trajectory Filename Here or Enter as Argument

if sys.argv[1]:
   dir_name = sys.argv[1]


dir = Trajectory(dir_name,mode='r')
#print ("Num image?: ",len(dir))
num_img = len(dir)
num_atoms = int(dir_name)

### 5 Input Files Needed for DeepMD

T_file_name = 'type.raw'
B_file_name = 'box.raw'
U_file_name = 'energy.raw'
F_file_name = 'force.raw'
C_file_name = "coord.raw"

#Create Output Files
Tfile = open(T_file_name, 'w')
Bfile = open(B_file_name, 'w')
Ufile = open(U_file_name, 'w')
Pfile = open(C_file_name, 'w')
Ffile = open(F_file_name, 'w')

t_arr = []
f_arr = []
p_arr = []
b_arr = []
c_arr = []
test_arr =[]
for i in range(len(dir)): #for each image in trajectory file
    temp = dir[i]         #get the atoms object
    Box = temp.get_cell().reshape(-1)       #write lattice parameters
    b_arr.append(Box)
    Ufile.write(str(temp.get_potential_energy()))  #write the energy of the structure
    Ufile.write("\n")                              #add a line
    tf = temp.get_forces().reshape(-1)             #get the force for the structure, and compress in 1D array
    f_arr.append((tf))
    c = temp.get_positions().reshape(-1)
    test_arr.append(c.tolist())
    c_arr.append((c))
    
t_arr = [numpy.zeros(num_atoms).reshape(-1)]

numpy.savetxt(Tfile,tuple(t_arr),fmt ='%i') #TYPE
numpy.savetxt(Bfile,tuple(b_arr)) #BOX
numpy.savetxt(Ffile,tuple(f_arr),fmt='%s',delimiter=' ')  #FORCES
numpy.savetxt(Pfile,tuple(c_arr),fmt='%s')   #COORD

P = open('p', 'w')

for i in test_arr:
    print (len(i))
    #P.write(str(i))
    #P.write("\n")    
for f in f_arr:
    print ("len(f)",len(f))
###make the final output directory 
d_name = dir_name+"_rawfile"
os.mkdir(d_name)
###Move the relevant DeepMD files to that directory
command = "mv coord.raw box.raw type.raw energy.raw force.raw "+ d_name
os.system(command)

print ("Formatted Files are in "+d_name+" directory.")
