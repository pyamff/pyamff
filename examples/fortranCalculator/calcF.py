import numpy as np
from pyamff.ase_calc_fortran import aseCalcF
import torch, sys
from ase.io import read, write, Trajectory

args = sys.argv
atoms = read(args[1], index=":")
#results = Trajectory('images_calc.traj','w')
#bmark = Trajectory('images_calc.traj','r')
calc  = aseCalcF()

i=0
for img in atoms:
    img.calc = calc
    print('image:',i)
    print(img.get_potential_energy())
    print(img.get_forces())
    #img.get_potential_energy()
    #img.get_forces()
    i+=1
    #results.write(img)

