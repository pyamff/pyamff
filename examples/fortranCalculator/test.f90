PROGRAM testfp
       use pyamff
       use fpcalc
       implicit none
       INTEGER :: nAtoms, nelement
       INTEGER :: i, j
       !REAL, DIMENSION(nAtoms*3), INTENT(IN) :: R
       DOUBLE PRECISION, DIMENSION(9) :: box
       DOUBLE PRECISION :: U
       DOUBLE PRECISION, DIMENSION(3,3):: F
       INTEGER, DIMENSION(3) :: atomicNumbers

       DOUBLE PRECISION, DIMENSION(3,3) :: pos_car
       INTEGER, DIMENSION(3) :: symbols
       CHARACTER*3, DIMENSION(3) :: atomicSymbols
       INTEGER, DIMENSION(1) :: uniqueNrs
       CHARACTER*20 :: filename
       CHARACTER*20 :: mlff_file
 
       nAtoms = 3
       nelement = 1
       pos_car(1,:) = (/0.,0.,0./)
       pos_car(2,:) = (/2.,0.,0./)
       pos_car(3,:) = (/0.,2.,0./)
       box = (/25.,0.,0., 0., 25., 0., 0., 0., 25./)
       atomicNumbers = (/32,32,32/)
       atomicSymbols = (/'Ge','Ge','Ge'/)
       uniqueNrs = (/32/)
       mlff_file = 'mlff.pyamff'
       CALL calc_ase(nAtoms, pos_car, box, atomicNumbers, F, U, nelement, uniqueNrs, mlff_file)
       
END PROGRAM testfp

