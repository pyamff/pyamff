import numpy as np
from pyamff.aseCalcF import aseCalcF
import torch, sys
from ase.io import read

args = sys.argv
atoms = read(args[1], index=":")

calc  = aseCalcF()

i=0
for img in atoms:
    img.set_calculator(calc)
    print('image:',i)
    print(img.get_potential_energy())
    print(img.get_forces())
    i+=1


