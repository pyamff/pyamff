PROGRAM testAu8
  USE training
  IMPLICIT NONE
  CHARACTER*8 :: opt_type
  INTEGER :: i
  INTEGER, PARAMETER :: max_epoch=10
  INTEGER, PARAMETER :: nAtoms=8, nelement=1
  INTEGER, DIMENSION(nAtoms) :: atomicNumbers
  INTEGER, DIMENSION(nelement) :: uniqueNrs
  DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_car
  REAL, DIMENSION(9) :: box
  DOUBLE PRECISION :: force_coeff

  ! Set atomic numbers and unique element numbers
  atomicNumbers=(/79, 79, 79, 79, 79, 79, 79, 79/)
  uniqueNrs=(/79/)

  ! Set positions in cartesian 
  pos_car(1,1:3)=(/7.04265,  6.3964,   7.4517/)
  pos_car(2,1:3)=(/9.58385,  7.1238,   6.7005/) 
  pos_car(3,1:3)=(/8.48345,  7.4005,   4.2486/)
  pos_car(4,1:3)=(/7.18605,  8.8166,   6.1653/)
  pos_car(5,1:3)=(/6.64055,  7.2958,  10.7514/)
  pos_car(6,1:3)=(/9.26035,  6.0553,   9.2278/)
  pos_car(7,1:3)=(/8.37835,  8.9447,   8.7524/)
  pos_car(8,1:3)=(/5.41615,  8.4411,   8.4142/)

  ! Set box in 1d array
  box(1:9)=(/15., 0., 0., 0., 15., 0., 0., 0., 15./)

  ! Set optimizer type
  opt_type='adam'
  ! Set number of images, and total natoms of images 
  nimages=4
  nAtimg=32

  ! Set energy/force training .true./.false.
  energy_training = .TRUE.
  force_training = .TRUE.

  ! Initiate training: read mlff.pyamff, allocate arrays for NN and backward propagations
  CALL train_init(nAtoms, nelement, atomicNumbers, uniqueNrs)
  ! Set target energy/force values after train_init
  targetE(1)=1.9609610000
  targetE(2)=-0.6536090000
  targetE(3)=-0.6536730000
  targetE(4)=-0.6536790000
  
  targetF(1:3,1)=(/ 0.8799140000,  1.5469880000,  0.8036050000/)
  targetF(1:3,2)=(/-1.6183470000,  0.2690820000,  0.4712980000/)
  targetF(1:3,3)=(/-0.1497420000,  0.3694370000,  1.7673110000/)
  targetF(1:3,4)=(/0.7095770000, -1.3030910000,  0.9290810000/)
  targetF(1:3,5)=(/ 0.3905060000,  0.6688260000, -2.2920590000/)
  targetF(1:3,6)=(/-1.2745240000,  1.4202270000, -1.2543460000/)
  targetF(1:3,7)=(/-1.2145130000, -2.0276030000, -0.7726570000/)
  targetF(1:3,8)=(/2.2771290000, -0.9438650000,  0.3477670000/)
  targetF(1:3,9)=(/3.5260000000e-03, -6.7750000000e-03, -8.3100000000e-04/)
  targetF(1:3,10)=(/7.3810000000e-03, -3.8360000000e-03, -4.1370000000e-03/)
  targetF(1:3,11)=(/-6.4360000000e-03,  4.7410000000e-03,  3.6120000000e-03/)
  targetF(1:3,12)=(/4.3010000000e-03,  1.0660000000e-03,  3.8240000000e-03/)
  targetF(1:3,13)=(/-5.5230000000e-03,  4.7000000000e-05, -9.4600000000e-04/)
  targetF(1:3,14)=(/1.1520000000e-03,  2.3000000000e-05,  2.1080000000e-03/)
  targetF(1:3,15)=(/-2.6970000000e-03,  6.1650000000e-03, -5.6110000000e-03/)
  targetF(1:3,16)=(/-1.7040000000e-03, -1.4310000000e-03,  1.9810000000e-03/)
  targetF(1:3,17)=(/-2.2100000000e-04, -3.3900000000e-04,  9.4000000000e-04/)
  targetF(1:3,18)=(/8.8900000000e-04,  3.2400000000e-04,  1.8870000000e-03/)
  targetF(1:3,19)=(/-8.9000000000e-05,  3.6600000000e-04, -1.3460000000e-03/)
  targetF(1:3,20)=(/-1.6700000000e-03, -2.2650000000e-03, -1.3800000000e-04/)
  targetF(1:3,21)=(/1.3770000000e-03,  4.0300000000e-04,  1.4340000000e-03/)
  targetF(1:3,22)=(/-6.5600000000e-04,  1.3880000000e-03, -1.1690000000e-03/)
  targetF(1:3,23)=(/-4.9000000000e-05, -1.1880000000e-03,  4.0100000000e-04/)
  targetF(1:3,24)=(/4.1900000000e-04,  1.3110000000e-03, -2.0100000000e-03/)
  targetF(1:3,25)=(/-1.5000000000e-05,  0.0000000000e+00,  1.3000000000e-05/)
  targetF(1:3,26)=(/-2.3000000000e-05,  7.2000000000e-05,  5.5000000000e-05/)
  targetF(1:3,27)=(/-2.7000000000e-05, -4.8000000000e-05,  5.0000000000e-06/)
  targetF(1:3,28)=(/-8.0000000000e-06, -4.5000000000e-05, -2.6000000000e-05/)
  targetF(1:3,29)=(/9.0000000000e-06, -5.0000000000e-06,  3.1000000000e-05/)
  targetF(1:3,30)=(/1.0000000000e-05,  2.0000000000e-05, -5.7000000000e-05/)
  targetF(1:3,31)=(/1.6000000000e-05, -5.0000000000e-05,  4.4000000000e-05/)
  targetF(1:3,32)=(/3.7000000000e-05,  5.5000000000e-05, -6.5000000000e-05/)
  ! Set force_coeff (optional)
  force_coeff=0.02

  ! Execution of training over images. Trainer is executed only when i=nimages
  ! From i=1, nimages-1, execute neighborlist, fingerprints, and forward propagation 
  DO i=1, nimages
    img_idx=i
    !Temp
    IF (i==2) THEN
      pos_car(1,1:3)=(/7.0029075,  6.359306,   7.43393/)
      pos_car(2,1:3)=(/9.4939805,  7.399557,   6.857945/)
      pos_car(3,1:3)=(/7.8635915,  6.665707,   4.917912/)
      pos_car(4,1:3)=(/7.2440915,  8.846094,   6.273013/)
      pos_car(5,1:3)=(/6.6156925,  6.812709,  10.082088/)
      pos_car(6,1:3)=(/9.0422615,  6.153906,   9.202339/) 
      pos_car(7,1:3)=(/8.1301035,  8.660731,   8.843164/)
      pos_car(8,1:3)=(/5.5060195,  8.445355,   8.291188/) 
    ELSE IF (i==3) THEN
      pos_car(1,1:3)=(/5.0931635, 7.106821,  6.3905525/)
      pos_car(2,1:3)=(/9.9068365, 9.302827,  6.8749175/)
      pos_car(3,1:3)=(/7.3448365, 5.697173,  6.1856395/)
      pos_car(4,1:3)=(/7.4726905, 8.401315,  6.2267645/)
      pos_car(5,1:3)=(/6.6790245, 6.804956,  8.5455465/)
      pos_car(6,1:3)=(/9.2561715, 6.873125,  7.6748375/)
      pos_car(7,1:3)=(/8.1283345, 9.093233,  8.8143605/)
      pos_car(8,1:3)=(/5.5682755, 9.189734,  7.9827525/)
    ELSE IF (i==4) THEN 
      pos_car(1,1:3)=(/5.09308,   7.1061725, 6.3912045/)
      pos_car(2,1:3)=(/9.90692,   9.3033355, 6.8758185/)
      pos_car(3,1:3)=(/7.344653,  5.6966645, 6.1852655/)
      pos_car(4,1:3)=(/7.473008,  8.4004925, 6.2263425/)
      pos_car(5,1:3)=(/6.679608,  6.8048995, 8.5455555/)
      pos_car(6,1:3)=(/9.256467,  6.8729995, 7.6737835/)
      pos_car(7,1:3)=(/8.128506,  9.0925625, 8.8147345/)
      pos_car(8,1:3)=(/5.569078,  9.1902205, 7.9814515/)
    END IF
    CALL trainExec(nAtoms,pos_car,box,atomicNumbers,nelement,uniqueNrs,opt_type,max_epoch,force_coeff)
  END DO

  ! Deallocate all allocated arrays
  CALL traincleanup


END PROGRAM
