
import os,sys
import numpy as np
from amp import Amp
from amp.descriptor.gaussian import Gaussian, make_symmetry_functions
from amp.descriptor.cutoffs import Cosine,Polynomial
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.pyamff import PyAMFF
from amp.utilities import make_filename, hash_images, check_images, Logger
from pyamff.pyamffCalc import loadModel
import torch


#Define NN parameters
weights = {}
bias = {}
params = []

Gs = {
      "Au": [
             {"type":"G2", "element":"Au", "eta":160, "Rs":0.},
             {"type":"G2", "element":"Au", "eta":120., "Rs":0.5},
             {"type":"G2", "element":"Au", "eta":120., "Rs":1.0},
             {"type":"G2", "element":"Au", "eta":40., "Rs":2.0},
             {"type":"G2", "element":"Au", "eta":800., "Rs":3.2},
             {"type":"G2", "element":"Au", "eta":400., "Rs":3.5},
             {"type":"G2", "element":"Au", "eta":400., "Rs":4.0},
             {"type":"G2", "element":"Au", "eta":400., "Rs":5.0},
             {"type":"G2", "element":"Au", "eta":400., "Rs":6.0},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":-1.0, "zeta":100.0, "theta_s":-1.0},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":-1.0, "zeta":100.0, "theta_s":-1.5},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":-1.0, "zeta":100.0, "theta_s":-1.75},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":-1.0, "zeta":100.0, "theta_s":-2.0},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":-1.0, "zeta":300.0, "theta_s":-2.15},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":-1.0, "zeta":300.0, "theta_s":-2.25},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":1.0, "zeta":160.0, "theta_s":0.0},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":1.0, "zeta":320.0, "theta_s":0.25},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":1.0, "zeta":640.0, "theta_s":0.35},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":1.0, "zeta":160.0, "theta_s":0.5},
             {"type":"G4", "elements":["Au", "Au"],"eta":0.005, "gamma":1.0, "zeta":160.0, "theta_s":0.75},
           # {"type":"G4", "elements":["H", "H"],"eta":0.005, "gamma":0.01, "zeta":1.0},
           # {"type":"G4", "elements":["H", "H"],"eta":0.005, "gamma":2.00, "zeta":1.0},
           # {"type":"G4", "elements":["H", "H"],"eta":0.005, "gamma":4.00, "zeta":1.0},
           # {"type":"G4", "elements":["H", "H"],"eta":0.005, "gamma":8.00, "zeta":1.0},
           # {"type":"G4", "elements":["H", "H"],"eta":0.005, "gamma":20.00, "zeta":1.0},
          #  {"type":"G4", "elements":["H", "Pd"],"eta":0.005, "gamma":40.00, "zeta":1.0},
          #  {"type":"G4", "elements":["H", "Pd"],"eta":0.005, "gamma":80.00, "zeta":1.0},
             ]}
#Define log file
log = Logger(make_filename('test', '-log.txt'))

#Read in images
images = hash_images(images='train.traj', log=log)
check_images(images, forces=True)

parallel = {'envcommand': None, 'cores':1}

#Define fingerprints
descriptor=Gaussian(Gs=Gs,
                    cutoff=Cosine(6.0))
#Calculate fingerpints
descriptor.calculate_fingerprints(
   images=images,
   parallel=parallel,
   log=log,
   calculate_derivatives=True)

params = None
#Define the NN model
model=NeuralNetwork(
          hiddenlayers=(40,),
          nFPs = {'Au': 20},
          forceTraining=True,
          params =params
          )

#Define the pyamff calculator
calc = PyAMFF(model = model,optimizer='LBFGS', 
              debug=None,
              lossConvergence = 4.7195e-6,
              force_coefficient=0.02,
              learningRate=0.01,
              fingerprints=Gs,
             )

#run the calculation
calc.fit(trainingimages=images, descriptor=descriptor, maxEpochs=10000, log=log)
