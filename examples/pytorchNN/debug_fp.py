import os,sys,time
import numpy as np
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.pyamff import PyAMFF
from pyamff.pyamffCalc import loadModel
import torch
from pyamff.neighborlist import NeighborLists
from pyamff.config import ConfigClass
from pyamff.fingerprints.behlerParrinello import BehlerParrinello, represent_BP
from ase.io import Trajectory

#Read and set up setting parameters
config = ConfigClass()
config.initialize()

#Fetch fingerprint parameters in Format: {'H':[G1, G2], 'Pd':[G1, G2]}
fp_paras = config.config['fp_paras'].fp_paras 

#Read in images
images = Trajectory(config.config['trajectory_file'], 'r')

#Convert fingerprint papamter objects and store in a list
starttime = time.time()
#Set up NeighborLists calculator
nl = NeighborLists(cutoff = 6.0)

#Do the calculation
(n_dists, n_list, n_angles, n_unitVects, n_symbols, n_neighborSymbols, n_offsets, n_vects, n_coords) = nl.calculate(images, fortran=True)

#Calculate fingerprints

g1_g2, dg1_dg2 = represent_BP(n_dists, n_list, n_offsets, n_angles, n_unitVects, n_symbols, n_neighborSymbols, n_vects, n_coords, G_paras=fp_paras, fortran=True)
print('Gs: ',g1_g2)

print('timeused:', time.time()-starttime)

#Define NN parameters
weights = {}
bias = {}
params = []

l1_weight = [[-0.06245638, -0.02177071], [ 0.01106360, -0.02641876]]
l1_bias  = [-0.15927899, 0.08015668]
l2_weight = [[-0.01062437, -0.00506942],[0.00031429, -0.12331749]]
l2_bias  = [0.18475361, 0.17465138]
l3_weight = [[-0.10928684, 0.02496535]]
l3_bias  = [0.12033310]

l1_weight1 = [[-0.12125415, -0.09574964], [-0.12175528,-0.01690174]]
l1_bias1   = [-0.00525357, 0.16651967]
l2_weight1 = [[0.14360507, -0.03717202],[0.18517374,-0.01645058]]
l2_bias1   = [0.03095685, -0.14551263]
l3_weight1 = [[-0.12728526, 0.03061981]]
l3_bias1   = [-0.00860174]

params = [l1_weight, l1_bias, l2_weight, l2_bias, l3_weight, l3_bias,
          l1_weight1, l1_bias1, l2_weight1, l2_bias1, l3_weight1, l3_bias1]

#params = None
#Define the NN model
model=NeuralNetwork(
          hiddenlayers=(2, 2),
          nFPs = {'H': 2, 'Pd':2},
          forceTraining=True,
          params =params
          )

#Define the pyamff calculator
calc = PyAMFF(model = model,optimizer='LBFGS', 
              debug=None,
              #weight_decay=0.1,
              fingerprints=None,
             )

#run the calculation
calc.fit(trainingimages=images, fingerprintDB=g1_g2, fingerprintDerDB=dg1_dg2, maxEpochs=5, log=None)
