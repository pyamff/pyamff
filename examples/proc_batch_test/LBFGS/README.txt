To run this example:
the current directory was made to get pyamff.pt file to restart 1b, 2b, 2p from same initial weights and biases. 
Thereofe, before running anything in 1b, 2b, 2p directories, copy pyamff.pt from this directory to 1b, 2b, 2p. 

in every directory, you should be able to run pyamff as:
pyamff > out

This will save output to out file for you to compare. in every directory, there is a RESULT directory. 
your out file should match with out file in respective directory. if it doesn't then something is wrong. did you forget to copy pyamff.pt? 
did you change any setting in config.ini? 
When you compare the out files, make sure every epoch is agrees. It should.


Now, if you are here to test the time.sleep(0.1):
go to training.py file in pyamff
line 355, time.sleep(0.1)
comment that line out and run 2p again. Make sure you remove pyamff.pt from that directory and copy it from LBFGS directory( one directory below)
compare out with out in 2b. It should differ.  You can also check BAD_RESULT in 2p to see if your run in 2p after removing time.sleep(0.1)
 agrees with it.
Happy debugging. I hope you sleep well after this.


