#!/bin/sh
qsub -V -b y -j y -o ll_out -S /bin/bash -N "$1" -wd "$2" -pe mpi24 24 /home/ryanciufo/eon/bin/eonclient | awk '{print $3}'
