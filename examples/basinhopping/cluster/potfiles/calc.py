from pyamff.pyamffCalc import pyamffCalc
from ase.io import read, Trajectory 

f = open('FU', 'w')

images = read("train.traj", index=":")

for atoms in images:
    calc = pyamffCalc('../../../pyamff.pt')
    atoms.set_calculator(calc)

    U = atoms.get_potential_energy()
    F = atoms.get_forces()
#F = f.flatten()
#    print(U)
#    print(F)
    f.write('%f \n' % U)
    for i in range(len(F)):
        f.write("%f %f %f\n" % (F[i][0], F[i][1], F[i][2]))
        #f.write(F + '\n')


f.close()
