import numpy
import ase
from ase.io.trajectory import Trajectory
t = Trajectory('a.traj',mode='w')
p = ase.io.read('POSCAR')
t.write(p,energy=110,forces=numpy.array([[0,0,0],[-1,2,0],[0,0,0]]))
