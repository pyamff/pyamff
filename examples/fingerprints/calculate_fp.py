#!/usr/bin/env python
import os
from pyamff.utilities.logTool import setLogger, writeSysInfo
from pyamff.neighborlist import NeighborLists
from pyamff.utilities.preprocessor import fetchProp
from pyamff.config import ConfigClass
from pyamff.fingerprints.fingerprints import Fingerprints
from ase.io import Trajectory

#Read and set up setting parameters
config = ConfigClass()
config.initialize()
logger = setLogger()
writeSysInfo(logger)

#Fetch fingerprint parameters in Format: {'H':[G1, G2], 'Pd':[G1, G2]}
fp_paras = config.config['fp_paras'].fp_paras 

#Read in images
images = Trajectory(config.config['trajectory_file'], 'r')


#Convert fingerprint papamter objects and store in a list
nFPs = {}
for key in fp_paras.keys():
   nFPs[key] = len(fp_paras[key])


#Preprocess and check the properties and images
trainingimages, properties, slope, intercept = fetchProp(images,forceTraining=True)
srcData = list(trainingimages.keys())


# Do the calculation
fpcalc = Fingerprints(uniq_elements=config.config['fp_paras'].uniq_elements, filename = config.config['fp_parameter_file'], nfps = nFPs)

for struct in trainingimages.keys():
    logger.info('  Calculating FPs for image %d',struct)
    chemsymbols = trainingimages[struct].get_chemical_symbols()
    fingerprints, fingerprintprimes = fpcalc.calcFPs(trainingimages[struct], chemsymbols)
    logger.info(fingerprints)
    
#fprange, magnitudeScale, interceptScale = fpcalc.loop_images(nFPs, config.config['fp_batch_num'], trainingimages, properties, normalize = True, logger=logger, fpDir = None, useexisting=config.config['fp_use_existing']) 



