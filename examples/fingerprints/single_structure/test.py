#!/usr/bin/env python
import os
from pyamff.neighborlist import NeighborLists
from pyamff.config import ConfigClass
from pyamff.fingerprints.behlerParrinello import BehlerParrinello, represent_BP
from ase.io import Trajectory

#Read and set up setting parameters
config = ConfigClass()
config.initialize()

#Fetch fingerprint parameters in Format: {'H':[G1, G2], 'Pd':[G1, G2]}
fp_paras = config.config['fp_paras'].fp_paras 

#Read in images
images = Trajectory(config.config['trajectory_file'], 'r')

#Convert fingerprint papamter objects and store in a list
g_paras = []
for key in fp_paras.keys():
   g_paras.extend(fp_paras[key])

#Set up NeighborLists calculator
nl = NeighborLists(cutoff = 6.0)

#Do the calculation
nl.calculate(images,fortran=True)

#Calculate fingerprints
g1_g2, dg1_dg2 = represent_BP(nl, G_paras=fp_paras, fortran=True)
print(g1_g2)
#print(dg1_dg2)

