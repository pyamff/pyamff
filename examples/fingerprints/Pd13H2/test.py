#!/usr/bin/env python
import os
from pyamff.neighborlist import NeighborLists
from pyamff.config import ConfigClass
from pyamff.fingerprints.behlerParrinello import BehlerParrinello, represent_BP
from ase.io import Trajectory

#Read and set up setting parameters
config = ConfigClass()
config.initialize()

#Fetch fingerprint parameters in Format: {'H':[G1, G2], 'Pd':[G1, G2]}
fp_paras = config.config['fp_paras'].fp_paras 
print('FPPARAS', fp_paras)
#print(fp_paras)
#Read in images
images = Trajectory(config.config['trajectory_file'], 'r')

#Convert fingerprint papamter objects and store in a list
g_paras = []
for key in fp_paras.keys():
   g_paras.extend(fp_paras[key])
#   CA = {'centerA', key}
#   g_paras.append(CA)
   print('g_paras', g_paras)

#for g in g_paras:
#   print('g', g.__dict__)
#Set up NeighborLists calculator
nl = NeighborLists(cutoff = 6.0)

#Do the calculation
#(n_dists, n_list, n_angles, n_unitVects, n_symbols, n_neighborSymbols, n_offsets, n_vects, n_coords) = nl.calculate(images,fortran=True)
nl.calculate(images,fortran=True)
print (nl)
#(n_dists, n_list, n_angles, n_unitVects, n_symbols, n_neighborSymbols, n_offsets, n_vects, n_coords) = nl.calculate(images)
#print('symbols', n_symbols)
#print('neighbor symbols', n_neighborSymbols)
#print('n_data_pairs', n_data_pairs)
#print('nl indices', n_list)
#print('n_dists', n_dists)
#print('n_unitVects', n_unitVects)
#print('g_paras', g_paras)
#Calculate fingerprints

#g1_g2, dg1_dg2 = represent_BP(n_dists, n_list, n_offsets, n_angles, n_unitVects, n_symbols, n_neighborSymbols, n_vects, n_coords, [1,1], fp_paras=g_paras, G_paras=fp_paras)

#use Python Finger print module
#g1_g2, dg1_dg2 = represent_BP(n_dists, n_list, n_offsets, n_angles, n_unitVects, n_symbols, n_neighborSymbols, n_vects, n_coords, G_paras=fp_paras)

#use fortran Finger print module
#g1_g2, dg1_dg2 = represent_BP(n_dists, n_list, n_offsets, n_angles, n_unitVects, n_symbols, n_neighborSymbols, n_vects, n_coords, G_paras=fp_paras, fortran=True)
g1_g2, dg1_dg2 = represent_BP(nl, G_paras=fp_paras, fortran=True)

#print('Gs: ',g1_g2)
#print(dg1_dg2)
#print(n_list)

