MODULE poscar_reader
    IMPLICIT none
    PUBLIC
    INTEGER :: MAX_LINE_LEN = 100

    CONTAINS
    SUBROUTINE READ_POSCAR(filename) ! need to specify output
        CHARACTER(*), INTENT(IN) :: filename

        INTEGER :: io, pos, i, tele
        INTEGER :: num_ele = 0
        INTEGER :: num_atoms = 0
        INTEGER :: file_unit = 33
        REAL :: lattice_constant = 1.0
        REAL, DIMENSION(3,3) :: cell = 0.0

        LOGICAL :: exists, use_select = .FALSE.
        CHARACTER(len=MAX_LINE_LEN) :: line,line_cpy
        REAL, DIMENSION(:,:), ALLOCATABLE :: positions
        INTEGER, DIMENSION(:), ALLOCATABLE :: num_per_ele
        INTEGER, DIMENSION(:), ALLOCATABLE :: symbols ! size of positions, element type
        CHARACTER(len=2), DIMENSION(:), ALLOCATABLE :: type_ele

        OPEN(newunit=file_unit,file=filename,action="read",status="old")
        INQUIRE(unit=file_unit,exist=exists) 
        CLOSE(file_unit)
        IF(.NOT. exists) THEN
            WRITE(*,*) "'" // filename // "' does not exist"
            ERROR STOP
        END IF
        ! NOTE: We are assuming no blank lines and no duplicates
        ! NOTE: Should technically check each read statment for error
        OPEN(newunit=file_unit,file=filename,action="read",status="old",iostat=io)
        IF (io .NE. 0) THEN
            WRITE(*,*) "error opening file"
            ERROR STOP
        END IF
        ! first line is either blank or a comment so skip it
        READ(file_unit,*) 
        ! second line is the lattice constant
        READ(file_unit,*) lattice_constant

        ! third-fifth lines are the lattice vectors
        READ(file_unit,*) cell(1,:)
        READ(file_unit,*) cell(2,:)
        READ(file_unit,*) cell(3,:)
        cell = cell*lattice_constant
        ! sixth line is the element symbols 
        READ(file_unit,'(A)') line
        line = ADJUSTL(line)
        line_cpy = line
        num_ele = 0
        pos = 0
        DO WHILE (LEN(TRIM(line(pos+1:))) .NE. 0)
            num_ele = num_ele + 1
            pos = INDEX(line," ")
            line = line(pos+1:)
        END DO
        ALLOCATE(type_ele(num_ele))
        ALLOCATE(num_per_ele(num_ele))
        pos = INDEX(line_cpy," ")
        DO i=1,num_ele-1
            type_ele(i) = line_cpy(:pos-1)
            line_cpy = line_cpy(pos+1:)
            pos = INDEX(line_cpy," ")
        END DO
        type_ele(num_ele) = TRIM(line_cpy)

        ! seventh line is number of each element
        READ(file_unit,'(A)') line
        line = ADJUSTL(line) 
        DO i=1,num_ele
            READ(line,*) num_per_ele(i)
        END DO
        num_atoms = SUM(num_per_ele)
        ALLOCATE(positions(num_atoms,3))
        ALLOCATE(symbols(num_atoms))
        pos = 1
        DO i=1,num_ele
            tele = num_per_ele(i)
            symbols(pos:pos+tele-1) = i
            pos = pos+tele
        END DO
        ! eighth line is either "DIRECT","CARTESIAN" or "SELECTIVE DYNAMICS"
        ! if eighth line is "SELECTIVE DYNAMICS" then ninth line is "DIRECT" or "CARTESIAN"
        READ(file_unit,'(A)') line
        line = TRIM(ADJUSTL(line))
        ! TODO: Check for all cases by having a lower case function
        IF (line .EQ. "Selective Dynamics" .OR. line .EQ. "Selective dynamics") THEN
            use_select = .TRUE.
            READ(file_unit,'(A)') line
            line = TRIM(ADJUSTL(line))
            IF (.NOT. (line .EQ. "Direct" .OR. line .EQ. "Cartesian")) THEN
                WRITE(*,*) "error while reading line no. 9 in '" // filename // "'"
                ERROR STOP
            END IF
        ELSE IF (.NOT. (line .EQ. "Direct" .OR. line .EQ. "Cartesian")) THEN
            WRITE(*,*) "error while reading line no. 8 in '" // filename // "'"
            ERROR STOP
        END IF
        ! remaining lines are : coord_x coord_y coord_z sd_x sd_y sd_z
        i=1
        DO
            READ(file_unit,'(A)',iostat=io) line
            line = ADJUSTL(line)
            IF (io .EQ. 0) THEN
                IF(use_select) THEN
                    ! get the position of third space
                    pos = INDEX(ADJUSTL(line), " ")
                    pos = pos + INDEX(ADJUSTL(line(pos+1:))," ")
                    pos = pos + INDEX(ADJUSTL(line(pos+1:))," ")
                    READ(line(:pos),*) positions(i,:)
                ELSE
                    READ(line,*) positions(i,:)
                END IF
            ELSE IF (io .GT. 0) THEN ! error
                WRITE(*,*) "error while reading file"; ERROR STOP
            ELSE IF (io .LT. 0) THEN ! EOF
                EXIT
            END IF
            i=i+1
        END DO
        CLOSE(file_unit)
    END SUBROUTINE
END MODULE