#!/usr/bin/env python
"""
This code is used to calculate strain of nanoparticles.
Method: stretch partilces along certain directions by dx and fix the boundary atoms, do optimization
strain_cal_cluster [inputfile]
An example of inputfile (any line starts with '#' will be ignored):
    configs_file = 3w-pos-1.xyz
    central_index = 1 2 3
    target_index = 5 6
    minimum_distance = 1.0
    gr_number = 100
    outputfile = dist_gr.dat
    step_size = 0.02
"""
import sys
import os
import errno
import ase
from ase import Atoms
from ase.io import read, write
from ase.io.vasp import write_vasp
from ase.utils.geometry import sort
from ase.constraints import constrained_indices, FixAtoms
import numpy as np
import subprocess
from collections import Counter 

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def make_dir(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
           raise

def readinputs(filename):
    f=open(filename, 'r')
    parameters = {}
    lines=f.readlines()
    for line in lines:
      if line.startswith('#'):
         continue
      fields = line.split('=')
      parameters[fields[0].strip()]=fields[1].replace("\n","").strip()
    return parameters

def read_atoms(filename, state_number = None, mode = None):
    f = open(filename, 'r')
    atoms=[]
    cycle = -1
    while True:
        elements = []
        positions = []
        line = f.readline()
        if not line:
           break
        if cycle == -1:
           atom_numb = int(line.split()[0])
        line = f.readline()
        for i in range (atom_numb):
            line = f.readline()
            fields = line.split()
            elements.append(fields[0])
            positions.append( [ float(fields[j+1]) for j in range(3) ] )
        elements = np.array(elements)
        positions = np.array(positions)
        atoms.append(Atoms(elements, positions=positions))
        cycle += 1
    f.close()
    return atoms


def toIndex(symbols, uniq_elements, nAtoms):
    elementDict = dict(zip(uniq_elements,range(1, len(uniq_elements)+1)))
    nsymbols = np.zeros(nAtoms,dtype=np.dtype('i4'))
    for i in range(nAtoms):
        try:
            nsymbols[i] = elementDict[symbols[i]]
        except:
            sys.stderr.write('Element %s has no fingerprints defined'%(symbols[i]))
            sys.exit(2)
    return nsymbols


def main():
    try:
       from tools.grCalculator import fmodules
       FMODULES = True
       print("Fortran PairTriplet loaded")
    except:
       FMODULES = False
       print("Fortran PairTriplet not loaded")

    arg = sys.argv
    try:
       configs = read(arg[1], index=":")
    except:
       configs = read_atoms(arg[1])
    max_neighs = 8
   #initialized based on chemical symbols
    dist_gr = {} 
    angle_gr = {}
    numb = {}
    numb_angle = {}
    output = {}
    output_angle = {}
    symbol_index={}
    chem_symbols = Counter(list(configs[0].symbols))
    #Create gr dictionary based on combiation of elements
    for s_a in chem_symbols.keys():
       for s_b in chem_symbols:
           key = [s_a,s_b]
           key = ''.join(sorted(key))
           dist_gr[key]=[]
           for s_c in chem_symbols:
               sides = sorted([s_a, s_c])
               key=''.join([sides[0], s_b, sides[1]])
               if key in angle_gr:
                  continue
               angle_gr[key]=[]
            
    dist_min = 1.5
    gr_numb = 400
    dr = 0.02
    angle_min = 0.0
    gr_angle_numb = 360
    da = 1.0
    dist_record = open('dist.dat','w')
    max_dist = dist_min + (gr_numb-1) * dr
    max_angle = angle_min + (gr_angle_numb -1 ) * da
    for key in dist_gr:
       output[key] = open(key+'.dat', 'w')
       numb[key]=0
       for i in range(gr_numb):
           dist_gr[key].append(0.0)
    for key in angle_gr:
       output_angle[key] = open('angle_'+key+'.dat','w')  
       numb_angle[key] = 0
       for i in range(gr_angle_numb):
           angle_gr[key].append(0.0)
    print(numb)
    nimage = 0
    t_dist = 0
    n_bond = 0
    Rc = 4.

    short_PdPd = 0
    short_HPd = 0
    uniq_elements = chem_symbols.keys()
    forceEngine = 1
    max_rcut = 4.
    for config in configs:
        nAtoms = len(config)
        nimage += 1
        print(nimage)
        nelement = len(uniq_elements)
        symbols = toIndex(config.get_chemical_symbols(), uniq_elements, nAtoms)
        pos_car = config.get_positions()
        cell = config.cell.array
        neighs = np.zeros([nAtoms, max_neighs], order='F')
        num_neigh = np.zeros(nAtoms, dtype=np.dtype('i4'))
 
        pairs = np.zeros(nAtoms * max_neighs, order = 'F')
        pair_indices = np.zeros([2, nAtoms * max_neighs], order='F')
        angles = np.zeros([3, nAtoms * max_neighs * max_neighs], order='F')
        angles_indices = np.zeros([3, nAtoms * max_neighs * max_neighs], order='F')

        npairs, pairs, pair_indices, ntriplets, angles, angles_indices = fmodules.ptcalc.findpt(pos_car, cell, symbols, nelement,forceEngine, max_rcut)
        print(pairs[0:npairs])
        print(ntriplets)
        print(angles[:, 0:ntriplets])
        for j in range(npairs):
          #print(pairs[j])
          #print(pair_indices[0][j])
          #print(pair_indices[1][j])
          key = [config[pair_indices[0][j]-1].symbol,config[pair_indices[1][j]-1].symbol]
          key = ''.join(sorted(key))
          if pairs[j] > max_dist:
             continue
          dist_gr[key][int((pairs[j] - dist_min)/dr)] += 1
          numb[key]=numb[key]+1
                  
        for j in range(ntriplets):
           # fetch the pair index for each pair in triplet
           pairIndex_1 = angles_indices[0][j] - 1
           pairIndex_2 = angles_indices[1][j] - 1
           pairIndex_3 = angles_indices[2][j] - 1

           # fetch the center index for each angle in triplet
           center_1 = pair_indices[0][pairIndex_1] - 1
           center_2 = pair_indices[1][pairIndex_1] - 1
           center_3 = pair_indices[1][pairIndex_2] - 1

           sides = sorted([config[center_2].symbol,config[center_3].symbol])
           key_1=''.join([sides[0], config[center_1].symbol, sides[1]])

           sides = sorted([config[center_1].symbol,config[center_3].symbol])
           key_2=''.join([sides[0], config[center_2].symbol, sides[1]])

           sides = sorted([config[center_1].symbol,config[center_2].symbol])
           key_3=''.join([sides[0], config[center_3].symbol, sides[1]])

           #if angle <30. and key=='PdPdPd':
           #   print(key, nimage, i, j, l)
           if angles[0][j] <= max_angle and angles[0][j] >= angle_min :
              angle_gr[key_1][int((angles[0][j] - angle_min)/da)]  += 1
              numb_angle[key_1] += 1
           if angles[1][j] <= max_angle and angles[1][j] >= angle_min :
              angle_gr[key_2][int((angles[1][j] - angle_min)/da)]  += 1
              numb_angle[key_2] += 1
           if angles[2][j] <= max_angle and angles[2][j] >= angle_min :
              angle_gr[key_3][int((angles[2][j] - angle_min)/da)]  += 1
              numb_angle[key_3] += 1
           
    for key in dist_gr:
       for i in range(gr_numb):
           try:
             dist_gr[key][i] = float(dist_gr[key][i])/numb[key]
           except:
             continue
           r = dist_min + float(i)*dr - dr*0.5
           output[key].write('%15.6f  %15.6f\n' % (r, dist_gr[key][i]))
                #   short_PdPd+=1



     #print nimage, t_dist/n_bond
    
    for key in angle_gr:
       for i in range(gr_angle_numb):
           try:
              angle_gr[key][i] = float(angle_gr[key][i])/numb_angle[key]
           except:
              continue
           r = angle_min + float(i)*da - da*0.5
           output_angle[key].write('%15.6f  %15.6f\n' % (r, angle_gr[key][i]))
if __name__ == '__main__':
    main()
    
