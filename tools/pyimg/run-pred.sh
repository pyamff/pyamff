#!/bin/bash
#BSUB -J filename
#BSUB -q ser
#BSUB -n 1
#BSUB -e %J.err
#BSUB -o %J.out
#BSUB -R "span[ptile=40]"

hostfile=`echo $LSB_DJOB_HOSTFILE`
NP=`cat $hostfile | wc -l`
cd $LS_SUBCWD
#-------------intelmpi+ifort------------------------------------------
source /share/intel/2018u4/compilers_and_libraries/linux/bin/compilervars.sh -arch intel64 -platform linux
source /share/intel/2018u4/impi/2018.4.274/intel64/bin/mpivars.sh
#---------------------------------------------------------------------


python -u pred.py >> pred.log
 
                                                                                                                  
echo `date` >> $HOME/finish.log
echo `pwd` >>  $HOME/finish.log
