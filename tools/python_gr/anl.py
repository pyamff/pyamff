# g(r) tp find angles in cluster trajectory: python script.py traj_file ele1 ele2 ele3 cutoff. Here ele 1 is the center atom
import itertools
import sys, time, ase, math
from ase.io import Trajectory
from ase import geometry
from ase.geometry import analysis
from ase import neighborlist
import matplotlib.pyplot as plt

def ANL(rank, size, traj, el, c1, start, end, logger, rdfDir, backend):
    
    # first we calculate RDF
    cutoff = c1  # cutoff fo g(r)
    bin_size = 0.1
    hist_bin_size = 0.05
    hist_n_bins = int(cutoff/hist_bin_size)
    n_bins = int(cutoff/bin_size)
    min_b = 0.1
    e_list = el
    for j in itertools.product(e_list, repeat=2):

        t_bin = [0 for k in range(0, n_bins)]
        m = 0
        all_distance = []
        a = j[0]  # element 1
        b = j[1]  # element 2
        f1 = Trajectory(traj, "r")
        for struct in f1[start:end]:
            s1 = time.time()
            #if m%10 == 0:
            #print('Completed: %d of total %d in the %d to %d \n' %(m, len(f1), start, end))
            m = m + 1
            cf1 = {(a, b): cutoff}
            nl = ase.neighborlist.neighbor_list('d', struct, cf1)
            for d in nl:
                all_distance.append(d)
                for k in range(0, n_bins):
                    if min_b + k * bin_size <= d < min_b + (k + 1) * bin_size:
                        t_bin[k] += 1 # it goes over all neighbors twice already
                        break
            #print("time for one structure: ", time.time() - s1)

        bin1 = [min_b + k*bin_size for k in range(0, n_bins)]

        bin_list = [p*hist_bin_size for p in range(0, hist_n_bins)]
        #plt.hist(all_distance, bins=bin_list, label="two cutoff")
        #plt.legend()
        #plt.show()
        f1.close()

        tn_bin = [0 for i in range(0, n_bins)]
        filename = rdfDir + '/TEST_' + str(a) + '_' + str(b) + '_' + str(start) + '_' + str(end) + '.dat'
        f1 = open(filename, "w")
        for k in range(0, n_bins):
            r = min_b + k*bin_size
            tn_bin[k] = t_bin[k] / (4 * 3.14 * r * r * bin_size)
            f1.write("%4.2f  %4.2f\n" %(r, t_bin[k]))
        f1.close()

    # Now we calculate ADF
    cutoff = c1 # cutoff for g(r)
    bin_size = 5 # we hard code this
    n_bins = int(180/bin_size) # again we hard code this
    min_b = 1 #we hard code this
    e_list = el
    for j in itertools.product(e_list, repeat=3):

        t_bin = [0 for _ in range(0, n_bins)]
        m = 0
        a = j[0]  # element 1
        b = j[1]  # element 2
        c = j[2]  # element 3
        f1 = Trajectory(traj, "r")
        for struct in f1[start:end]:
            s1 = time.time() 
            print('Completed: %d of total %d in the %d to %d \n' %(m, len(f1), start, end))
            print("rank", rank, flush=True)
            m = m + 1
            # c1 = {(a, b): cutoff, (a, c): cutoff, (b, c): cutoff}
            c2 = [cutoff/2 for _ in range(len(struct))]
            # nl = ase.neighborlist.neighbor_list('d', struct, c1)
            nl = ase.neighborlist.NeighborList(c2, skin=0)
            nl.update(struct)
            ac = ase.geometry.analysis.Analysis(struct, nl)
            if any(ac.get_angles(a,b,c)): 
                temp = ac.get_values(ac.get_angles(a, b, c), 0)[0]
                for curr_angle in temp:
                    for k in range(0, n_bins):
                        if min_b + k * bin_size <= curr_angle < min_b + (k + 1) * bin_size:
                            t_bin[k] += 1
                            break
            #print("time for on structure: ", time.time() - s1)
        
        f1.close()
        bin_list = [p*bin_size for p in range(0, n_bins)]
        #plt.hist(all_angles, bins=bin_list, label="three cutoff")
        #plt.legend()
    
        #write the normalize data to the file 
        tn_bin = [0 for i in range(0, n_bins)]
        filename = rdfDir + '/test_' + str(a) + '_' + str(b) + '_' + str(c) + '_' + str(start) + '_' + str(end) + '.dat'
        f1 = open(filename, "w")
        for k in range(0, n_bins):
            angle = min_b + k*bin_size
            angle = math.radians(angle)
            tn_bin[k] = t_bin[k] / (4 * 3.14 * cutoff * cutoff * bin_size * (angle/math.pi))
            f1.write("%4.2f  %4.2f\n" %(angle, tn_bin[k]))
        f1.close()
 
