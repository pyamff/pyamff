#/usr/bin/env python3

import os, sys, time, glob, math, ase, re, itertools
from os import listdir
from os.path import isfile, join
from ase.io import Trajectory
import torch.distributed as dist
from torch.multiprocessing import Process
from pyamff.utilities.logTool import setLogger, writeSysInfo
from tools.python_gr.anl import ANL
import matplotlib.pyplot as plt
import numpy
from scipy.interpolate import make_interp_spline


def init_fpProcesses(rank, size, fn, trajectory, e_list, cutoff, start, end, logger, fpDir,backend='gloo'):
    #Initialize the distributed environment.
    os.environ['MASTER_ADDR'] = '127.0.0.1'
#    os.environ['MASTER_ADDR'] = 'env://'

    os.environ['MASTER_PORT'] = '12355'
    dist.init_process_group(backend, rank=rank, world_size=size)
    fn(rank, size, trajectory, e_list, cutoff, start, end, logger, fpDir,backend='gloo')


def GRcalc(traj, coff, pr, logger):

    cwd = os.getcwd()
    logger.info('=======================================================')
    logger.info('Starting a Radial and Angular Distribution Function  job at %s', time.strftime('%X %x %Z'))
    #f = sys.argv[1]
    f = traj
    f1 = Trajectory(f, "r")
    e_list = []
    for struct in f1:
        l = list(struct.get_chemical_symbols())
        for e in l:
            if e not in e_list:
                e_list.append(e)
    nimages = len(f1)
    cutoff = float(coff)
    #cutoff = float(sys.argv[2])
    #srcData = [i for i in range(0, nimages)]
    logger.info('  Total number of traning images: %d', nimages)
    logger.info('=======================================================')
    proc_numb = pr
    processes = []
    b_size = math.ceil(nimages/proc_numb)
    rdfdir = os.getcwd() + '/gr_data'
    if not os.path.exists(rdfdir):
        os.mkdir(rdfdir)
    #batches = partitionData(srcData, proc_numb)
    for rank in range(proc_numb):
        start = b_size * rank
        end = start + b_size
        p = Process(target=init_fpProcesses,
                         args=(rank, proc_numb, ANL, f, e_list, cutoff,
                               start, end, logger, rdfdir))
        p.start()
        processes.append(p)
    for p in processes:
        p.join()
    f1.close()
    # now join files for G1
    for n in itertools.product(e_list, repeat=2):
        ele1 = n[0]
        ele2 = n[1]
        print("element list in rdf", ele1, ele2)
        mypath = os.getcwd() + '/gr_data'
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        cutoff = float(coff)
        #cutoff = float(sys.argv[2])
        min_b = 0.1
        bin_size = 0.1
        n_bins = int(cutoff/bin_size)
        t_bin, r = [], []
        # define search to search for filename wherer rdf was written by each proc
        search = 'TEST_' + str(ele1) + '_' + str(ele2)
        for fn in onlyfiles:
            if search in fn:
                f1 = open(mypath + '/' + fn, "r")
                lines = f1.readlines()
                if not r:
                    for line in lines:
                        temp = re.split(' |, |\n', line)
                        r.append(float(temp[0]))
                        t_bin.append(float(temp[2]))
                else:
                    j = 0
                    for line in lines:
                        temp = re.split(' |, |\n', line)
                        t_bin[j] += float(temp[2])
                        j += 1
            f1.close()

        tn_bin = [0 for i in range(0, n_bins)]
        bin1 = [min_b + k*bin_size for k in range(0, n_bins)]
        # make data beautiful and write final data for storage
        logger.info('Writing cumulative data')
        filename = mypath + '/RDF_' + str(ele1) + '_' + str(ele2) + '_' + str(cutoff) + '.dat'
        f1 = open(filename, "w")
        for k in range(0, n_bins):
            r = min_b + k*bin_size
            tn_bin[k] = t_bin[k] / (4 * 3.14 * r * r * bin_size)
            f1.write("%15.6f  %15.6f\n" %(r, tn_bin[k]))
        f1.close()
        X_Y_Spline = make_interp_spline(bin1, tn_bin)
        # find the max peak and write to fpParas.dat from tn_bin
        # we will have to store the data somewhere before writing it finally because
        # many different g(r) will report values
        points = []
        pos = []
        for i in range(1, len(tn_bin)-1):
            if tn_bin[i-1] < tn_bin[i] and tn_bin[i] > tn_bin[i+1]:
                points.append(tn_bin[i])
                pos.append(min_b + i*bin_size)
        # write this to file and read in for all files
        filename = mypath + '/fp_' + str(ele1) + '_' + str(ele2) + '.dat'
        f1 = open(filename, "w")
        logger.info('Writing chosen fp data for this calculation to be used at the end')
        for k in range(0, len(pos)):
            f1.write("%s %s %s %4.2f %4.2f %4.2f\n" %('G1', str(ele1), str(ele2), 20, pos[k], cutoff))
        f1.close()

        X_ = numpy.linspace(bin1[0], bin1[-1], 100)
        Y_ = X_Y_Spline(X_)

        # plt.plot(bin1, t_bin, label="un-normalized Gr")
        plt.plot(bin1, tn_bin, label="normalized Gr")
        plt.plot(X_, Y_, label="smooth")
        plt.legend()
        fname = mypath + '/RDF_' + str(ele1) + '_' + str(ele2) + '_' + str(cutoff) + '.png'
        plt.savefig(fname)
        plt.clf()
        # plt.show()
        # clean up files for now
        for fn in onlyfiles:
            if search in fn:
                os.remove(mypath + '/' + fn)
        logger.info('Closing the rdf job and deleted temporary dat files')

    # now join the files for data for G2
    for n in itertools.product(e_list, repeat=3):
        ele1 = n[0]
        ele2 = n[1]
        ele3 = n[2]
        mypath = os.getcwd() + '/gr_data'
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        bin_size = 5 # we hard code this
        n_bins = int(180/bin_size) # again we hard code this
        min_b = 1 #we hard code this
        t_bin, angle = [], []
        # define search to search for filename wherer rdf was written by each proc
        search = 'test_' + str(ele1) + '_' + str(ele2) + '_' + str(ele3)
        for fn in onlyfiles:
            if search in fn:
                f1 = open(mypath + '/' + fn, "r")
                lines = f1.readlines()
                if not angle:
                    for line in lines:
                        temp = re.split(' |, |\n', line)
                        angle.append(float(temp[0]))
                        t_bin.append(float(temp[2]))
                else:
                    j = 0
                    for line in lines:
                        temp = re.split(' |, |\n', line)
                        t_bin[j] += float(temp[2])
                        j += 1
            f1.close()
        
        tn_bin = [0 for i in range(0, n_bins)]
        bin1 = [min_b + k*bin_size for k in range(0, n_bins)]
        # make data beautiful and write final data for storage
        logger.info('Writing cumulative data')
        filename = mypath + '/ADF_' + str(ele1) + '_' + str(ele2) + '_' + str(ele3) + '_' + str(cutoff) + '.dat'
        f1 = open(filename, "w")
        for k in range(0, n_bins):
            angle = min_b + k*bin_size
            angle = math.radians(angle)
            tn_bin[k] = t_bin[k]
            f1.write("%4.2f  %4.2f\n" %(angle, tn_bin[k]))
        f1.close()
        X_Y_Spline = make_interp_spline(bin1, tn_bin)
        # find the max peak and write to fpParas.dat from tn_bin
        # we will have to store the data somewhere before writing it finally because
        # many different g(r) will report values
        points = []
        pos = []
        for i in range(1, len(tn_bin)-1):
            if tn_bin[i-1] < tn_bin[i] and tn_bin[i] > tn_bin[i+1]:
                points.append(tn_bin[i])
                angle = min_b + i*bin_size
                pos.append(math.radians(angle))
        # write this to file and read in for all files
        filename = mypath + '/fp_' + str(ele1) + '_' + str(ele2) + '_' + str(ele3) + '.dat'
        f1 = open(filename, "w")
        logger.info('Writing chosen fp data for this calculation to be used at the end')
        for k in range(0, len(pos)):
            f1.write("%s %s %s %s %4.2f %4.2f %4.2f %4.2f %4.2f\n" %('G2', str(ele1), str(ele2), str(ele3), 0.005, 100, 1.0, pos[k], cutoff))
        f1.close()
        
        X_ = numpy.linspace(bin1[0], bin1[-1], 100)
        Y_ = X_Y_Spline(X_)
        
        # plt.plot(bin1, t_bin, label="un-normalized Gr")
        plt.plot(bin1, tn_bin, label="normalized Gr")
        plt.plot(X_, Y_, label="smooth")
        plt.legend()
        fname = mypath + '/RDF_' + str(ele1) + '_' + str(ele2) + '_' + str(ele3) + '_' + str(cutoff) + '.png'
        plt.savefig(fname)
        plt.clf()
        # plt.show()
        # clean up files for now
        # Search = 'TEST_' + str(ele1) + '_' + str(ele2)
        for fn in onlyfiles:
            if search in fn:
                os.remove(mypath + '/' + fn)
        logger.info('Closing the rdf job and deleted temporary dat files')
    # combine all the dat files in fpParas.dat
    linesG1 = 0
    linesG2 = 0
    f2 = open("fpParas.dat", "a")
    f2.write("#\n")
    f2.write("BP\n")
    f2.write("#\n")
    el = ''
    be = ''
    for e in e_list:
        el += str(e)
        el += ' '
        be += '0.0 ' #by default I am setting coeh energy to be zero here. for default fps it shouldn't matter?
    f2.write("%s\n" % el)
    f2.write("%s\n" % be)
    f2.write("#\n")
    # calculate length of G1 and G2 here before writing them out
    for v in itertools.product(e_list, repeat=2):
        name = mypath + '/fp_' + str(v[0]) + '_' + str(v[1]) + '.dat'
        f1 = open(name, "r")
        l = f1.readlines()
        for l1 in l:
            linesG1 += 1
        f1.close()
    for v in itertools.product(e_list, repeat=3):
        name = mypath + '/fp_' + str(v[0]) + '_' + str(v[1]) + '_' + str(v[2]) + '.dat'
        f1 = open(name, "r")
        l = f1.readlines()
        for l1 in l:
            linesG2 += 1
        f1.close()
    print(linesG1, linesG2)
    f2.write("%d %d\n" %(linesG1, linesG2))
    f2.write("#\n")
    # time to write the lines out
    for v in itertools.product(e_list, repeat=2):
        name = mypath + '/fp_' + str(v[0]) + '_' + str(v[1]) + '.dat'
        f1 = open(name, "r")
        l = f1.readlines()
        for l1 in l:
            f2.write(l1)
        f1.close()
    f2.write("#\n")
    for v in itertools.product(e_list, repeat=3):
        name = mypath + '/fp_' + str(v[0]) + '_' + str(v[1]) + '_' + str(v[2]) + '.dat'
        f1 = open(name, "r")
        l = f1.readlines()
        for l1 in l:
            f2.write(l1)
        f1.close()
    f2.close()

