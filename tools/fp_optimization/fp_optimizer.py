"""
Contributor: Lei Li ...
Optimize fingerprints with a given template
Example input file:
    fp_type      = G2
    fp_template  = 1500 1.5 (eta0, Rs0, eta1, Rs1)
    Rc           = 6.0
    optimizer    = basinhopping
    T            = 0.1
    stepsize     = 1.0 
    maxsteps     = 1
    maxoptsteps  = 100
    pdf_filename = gr.dat
    Rmin         = 1.0 
    Rmax         = 8.0
"""
import numpy as np
import sys, os, copy
from distutils.util import strtobool
from amp.descriptor.cutoffs import Cosine,dict2cutoff

default_paras = dict(
     run_type='minimize', #minimize, singlepoint, checksmooth
     fp_type= 'G2',
     fp_template=[[1500, 1.5]],
     initial_mode='template', #random, template or user_defined
     initial_paras = [[1,  0.5, 300, 0.005]],
     Rc=6.0, 
     optimizer='basinhopping',
     jac=False,
     T = 0.1, 
     stepsize = 1.0, 
     maxsteps = 0 ,
     maxoptsteps = 100,
     pdf_filename='gr.dat', 
     Rmin=1.0, 
     Rmax=8.0,
     offset=0.01, #xval offset in gr
     to_normalize_theta_ss = [0, 1], #indices of FPs to be normalized in template
     ref_normalized_fps = 4,
     randomize_initial = False,
     paras_range = [0.0, 4.0, 0.1, 0.0, 500.0, 50.0], #[min, max, interval]
     checksmooth=False,
     dx=0.1,
     para_index=1,
     )

class MyBounds(object):
     def __init__(self, xmax=[2000.0, 8.0], xmin=[0.,0.0] ):
         self.xmax = np.array(xmax)
         self.xmin = np.array(xmin)
     def __call__(self, **kwargs):
         x = kwargs["x_new"]
         tmax = bool(np.all(x <= self.xmax))
         tmin = bool(np.all(x >= self.xmin))
         return tmax and tmin

def readinputs(filename):
    f=open(filename, 'r')
    parameters = {}
    lines=f.readlines()
    for line in lines:
      if line.startswith('#'):
         continue
      fields = line.split('=')
      parameters[fields[0].strip()]=fields[1].replace("\n","").strip()
    return parameters

def type_convertion(para, value):
    if type(para) is int:
       return int(value)
    if type(para) is float:
       return float(value)
    if type(para) is str:
       return value
    if type(para) is list:
       return value.split()
    if type(para) is bool:
       return strtobool(value)


def toMatrix(x, fp_type = 'G2'):
    if fp_type == 'G2' and len(x)%2 == 0:
       numb_fps = int(len(x) /2)
       return x.reshape(numb_fps,2)
    if fp_type == 'G4' and len(x)%2 == 0:
       numb_fps = int(len(x) /2)
       return x.reshape(numb_fps,2)

def printMatrix(a, paras_range):
   print "#Matrix["+("%d" %a.shape[0])+"]["+("%d" %a.shape[1])+"]"
   print "#paras_range:",paras_range
   rows = a.shape[0]
   cols = a.shape[1]
   for i in range(0,rows):
      for j in range(0,cols):
         print("%12.10f" %a[i,j]),
      print
   print   

def printResults(x):
    for vs in x:
      result = '  '
      for v in vs:
        result += "{:12.6f}".format(v)
      print(result)


def cal_cutoff(Rc, r):
    if r <= Rc:
      return 0.5*(1.0+np.cos(np.pi * r / Rc))
    if r> Rc:
      return 0

class fp_optimizer(object):
    """
    fp_template: [[eta0, Rs0],[eta1, Rs1]]
    """
    def __init__(self, fp_template,fp_type = "G2", initial_paras = None, Rc=6.0, optimizer='basinhopping', jac=False,
                 T=0.1, stepsize=1.0, maxsteps =1, pdf_filename='gr.dat', Rmin=1.0, Rmax=8.0, offset=0.01,
                 to_normalize_theta_ss=None,
                 ref_normalized_fps=None):
        self.optimizer = optimizer
        self.jac = jac
        self.T = T
        self.stepsize = stepsize
        self.maxsteps = maxsteps
        self.Rc = Rc
        self.Rmin = Rmin
        self.Rmax = Rmax
        #self.dr = 0.02
        self.numb_fps = fp_template.shape[0]
        self.numb_paras = fp_template.shape[1]
        self.fp_type = fp_type   

        self.to_normalize_theta_ss = to_normalize_theta_ss
        self.ref_normalized_fps    = ref_normalized_fps
        #Define cutoff function
        cutoff={'name': 'Cosine', 'kwargs': {'Rc': Rc}}
        self.cutoff_fxn = dict2cutoff(cutoff)
        
        self.step = 0

        #read in gr function
        self.xval = []
        pdf = []
        self.cutoff = []
        pdf_file=open(pdf_filename, 'r')
        lines = pdf_file.readlines()
        for line in lines:
           fields = line.split()
           r = float(fields[0])
           if r+offset >= self.Rmin and r+offset <= self.Rmax:
              pdf.append(float(fields[1]))
              self.xval.append(r+offset)   
              args_cutoff_fxn = dict(Rij=r+offset)
              self.cutoff.append(self.cutoff_fxn(**args_cutoff_fxn))

        self.pdf = np.array(pdf)
        self.xval = np.array(self.xval)
        self.cutoff = np.array(self.cutoff)

        # for G4
        if self.fp_type =='G4':
           self.xval = self.xval * np.pi / 180.0
           #self.r = self.Rc/3.0
           self.r = 2.7
           self.R12 = self.r * np.sin(self.xval/2)
           self.cutoff_1 = self.cutoff_fxn(**dict(Rij=self.r))
           self.cutoff_2 = []
           for r in self.R12:
              self.cutoff_2.append(self.cutoff_fxn(**dict(Rij=r)))

        if initial_paras is None:
           self.initial_paras = fp_template.flatten()
        else:
           self.initial_paras = initial_paras
           #self.numb_paras = initial_paras.shape[1]
        #print 'initial_paras', self.initial_paras
        #Calculate Refrence fps
        print self.initial_paras
        self.reffp_paras=fp_template
        self.fp_template = self.calculate_fps(self.reffp_paras, ref=True)
        if self.to_normalize_theta_ss is not None:
           target_max = max(self.fp_template[self.ref_normalized_fps])
           for index in self.to_normalize_theta_ss:
              coeff = max(self.fp_template[index])/target_max
              self.fp_template[index] /= coeff
        #normalize fps over sum of all fps
        """
        fp_sum = 0
        for fp in self.fp_template:
           fp_sum += sum(fp)
        print fp_sum
        for i in range(len(self.fp_template)):
           self.fp_template[i] /= fp_sum
        """

        self.log_fps(self.fp_template, self.reffp_paras, 'ref_fps.dat')
        #log history
        self.log_min = open('bh_min'+'.dat','w')
        self.log_min.write("xValues    fValue     nAccepted\n")
    
    def log_fps(self, fps, paras, filename):
        fp_out = open(filename,'w')
        output = "#     {:12s}".format('xval')
        for para in paras:
           output += "{:20s} ".format('_'.join([str(round(val,2)) for val in para]))
        fp_out.write("{:s} \n".format(output))
        for i in range(len(self.xval)):
           output = "{:8.4f}".format(self.xval[i])
           for fp in fps:
              output += "{:20.8f} ".format(fp[i])
           fp_out.write("{:s}\n".format(output))
        fp_out.close()
        return

    def calculate_fps(self, fp_paras, ref=False):
        g2s = []
        dfs = []
        if not ref:
           if self.jac:
              for fp_para in fp_paras:
                  g2, df = self.calculate_fp(fp_para, ref, derivative=True)
                  g2s.append(g2)
                  dfs.append(df)
                  #print df
              return np.array(g2s), np.array(dfs)
           else:
              for fp_para in fp_paras:
                  g2 = self.calculate_fp(fp_para, ref, derivative=False)
                  g2s.append(g2)
              return np.array(g2s)
        else: 
           for fp_para in fp_paras:
              g2 = self.calculate_fp(fp_para, ref, derivative=False)
              g2s.append(g2/np.sum(g2))
           return np.array(g2s)

    def calculate_fp(self, fp_para, ref=False, derivative=True):
        """
        Calculate fingerprints with given fp_paras:
        If G2: fp_para = [eta, Rs]
        """
        #if fp_para[1] < 0:
        #   fp_para[1] = 0.0
        if self.fp_type=="G2":
          term0 = self.xval-fp_para[1]
          term1 = (term0 ** 2) / (self.Rc **2)
          fp =  np.exp(-fp_para[0] * 1000 * term1) * self.cutoff
          if derivative:
             term2 = 2 * fp_para[0] * term0      # 2*eta*(Rij-Rs)
             term3 = np.sum(term2 * fp * self.pdf)
             term4 = np.sum(fp * self.pdf)
             df_Rs = ((term2 * term4 - np.sum(fp *self.pdf * term2)) * fp * self.pdf) / ((self.Rc * term4)**2)
             df_eta = ( np.sum(term1 * fp * self.pdf) - term1 * term4)
             return fp, np.array([df_eta, df_Rs])
          return fp

        if self.fp_type=="G4":
          #[gammas, theta_s, zeta, eta]
          #gamma = fp_para[0]
          gamma = 1.0
          theta_s = fp_para[1]
          #zeta = fp_para[2]
          #eta = fp_para[3]
          zeta = fp_para[0] * 1000
          #eta = fp_para[2]
          eta = 0.005
          fp = (1. + gamma * np.cos(self.xval - theta_s)) ** zeta
          fp *= np.exp(-eta * (self.r ** 2. + self.r ** 2. + self.R12 ** 2.) / (self.Rc ** 2.))
          fp *= self.cutoff_1 ** 2
          fp *= self.cutoff_2
          fp *= 2**(1-zeta)
          return fp

    #Calculate derivative relative to eta and Rs
    def calculate_prime(self, fp_paras):
       dfs = []
       for fp_para in fp_paras:
          df_eta = - (self.xval - fp_para[1]) ** 2./ (self.Rc **2.) * self.cutoff * \
                   np.exp(-fp_para[0] * ((self.xval-fp_para[1]) ** 2.) / (self.Rc ** 2.))
          
          df_Rs  = 2 * self.cutoff * fp_para[0] * (self.xval - fp_para[1]) * \
                  np.exp(-fp_para[0] * ((self.xval-fp_para[1]) ** 2.) / (self.Rc ** 2.)) / (self.Rc ** 2)
          #if df_Rs < 10e-8:
             
          dfs.append(df_eta)
          dfs.append(df_Rs)
       #print fp_paras[0], dfs
       return np.array(dfs)

    def check_smooth(self,dx, fp_numb, para_index, fp_paras, fp_range):
        diffs = []
        temp = copy.copy(fp_paras)
        #output_rs = open('y_'+str(para_index)+'.dat', 'w')
        #output_eta = open('y_eta.dat', 'w')
        print fp_range
        target = 2.6
        targets=[]
        if len(fp_range) > 3:
           npara_1 = int((fp_range[1]-fp_range[0])/fp_range[2])
           npara_2 = int((fp_range[4]-fp_range[3])/fp_range[5])
           print npara_1, npara_2
           for i in range(npara_1):
              fp_paras[fp_numb][1] = temp[fp_numb][1]
              fp_paras[fp_numb][0] += fp_range[2]
              for j in range(npara_2):
                #print self.step,fp_paras
                fp_paras[fp_numb][1] += fp_range[5]
                diff = self.get_loss(fp_paras.flatten())
                diffs.append(diff)
                if j == int(1.75)/fp_range[5] - 1:
                  targets.append(diff)
                #output_rs.write("{:6.4f} {:12.10f}\n".format(fp_paras[fp_numb][para_index], self.get_loss(fp_paras.flatten())))
           index_minv = np.argmin(diffs)
           print "minimum: {:12.2f} {:6.2f} {:12.8f}".format((int(index_minv/npara_2)+1) * fp_range[2]*1000.0, (index_minv%npara_2+1)*fp_range[5], diffs[index_minv])
           minv_1 = np.argmin(targets)
           print "local min: {:12.2f} {:6.2f} {:12.8f}".format((minv_1+1)*fp_range[2] *1000.0,target, targets[minv_1])
           printMatrix(np.array(diffs).reshape(npara_1, npara_2), fp_range)
        else:
           output_rs = open('y_'+str(para_index)+'.dat', 'w')
           npara_1 = int((fp_range[1]-fp_range[0])/fp_range[2])
           for i in range(npara_1):
              fp_paras[fp_numb][para_index] += fp_range[2]
              output_rs.write("{:6.4f} {:12.10f}\n".format(fp_paras[fp_numb][para_index], self.get_loss(fp_paras.flatten())))
        #fp_paras=copy.copy(temp)
        #for i in range(100):
        #   fp_paras[fp_numb] = fp_paras[fp_numb]+[dx, 0.0]
        #   output_eta.write("{:6.4f} {:12.10f}\n".format(fp_paras[fp_numb][0], self.get_loss(fp_paras.flatten())))
        
    def get_loss(self,parametervector):

        if any(parametervector<0.0):
           return  1000000.0, None
        if self.jac:
           fps, df_matrix = self.calculate_fps(parametervector.reshape(self.numb_fps,self.numb_paras))
        else:
           fps = self.calculate_fps(parametervector.reshape(self.numb_fps,self.numb_paras))
        diff = 0
        dfs = []
        for i in range(self.numb_fps):
           fp_pdf = fps[i] * self.pdf
           #normalize, deal with corner situation where the fp is truncated at xval=0
           #max_xval = np.argmax(fp_pdf)
           #if fp_pdf[0] >fp_pdf[max_xval]/100.0:
              #print 'trucated FP:', i, fp_pdf[0]
           #   fp_pdf /= (np.sum(fp_pdf)+np.sum(fp_pdf[2*max_xval:]))
           #else:
              #print 'sum',np.sum(fp_pdf)
           summ = np.sum(fp_pdf)
           #temp = copy.copy(fp_pdf)
           fp_pdf /= summ

           #log(fp_gr, self.gr, filename='fp.dat')
           dtao = fp_pdf - self.fp_template[i]
           #diff += np.sum(dtao**2)
           diff += np.sum(np.absolute(dtao))

           #Compute x/|x|
           if self.jac:
              #signs=[]
              for d in dtao:
                if d >= 1e-20:
                  signs.append(1.0)
                else:
                  signs.append(-1.0)
              df_matrix[i][0] *= np.array(signs)
              df_matrix[i][1] *= np.array(signs)
              #df_matrix *= 2*dtao*1000
              dfs.append(np.sum(df_matrix[i], axis=1))
           #self.log_fps([fp_pdf], parametervector.reshape(self.numb_fps,self.numb_paras), 'check.dat')
           #print "{:6d} {:14.6f} {:14.6f} {:14.6f} ".format(step, diff, min(parametervector), 
           #      max(parametervector))
           #      -np.dot(df_deta, gr), -np.dot(df_dRs, gr))
           #print parametervector
        """
        fp_pdf = []
        fp_pdf_sum = 0
        for i in range(self.numb_fps):
           fp_pdf.append(fps[i] * self.pdf)
           fp_pdf_sum += sum(fp_pdf[i])
        for i in range(self.numb_fps):
           fp_pdf[i] /= fp_pdf_sum
           diff += np.sum(np.absolute(fp_pdf[i] - self.fp_template[i]))
        """
        self.step += 1
        #print self.step, parametervector, np.sum(temp)
        if np.isnan(diff):
           #print 'NAN'
           #diff = 10000000.0
           diff = 2.0
        #print 'df eta Rs', dfs
        if self.jac:
           return diff, np.array(dfs).flatten()
        return diff

    def printX(self, x):
        vals = " "
        for val in x:
           vals += "{:12.6f} ".format(val)
        self.log_min.write("%s\n" % (vals))

    def print_fun(self, x, f, accepted=1):
        vals = " "
        for val in x:
           vals += "{:12.6f} ".format(val)
        vals+="{:12.6f} ".format(f) 
        vals+="{:6d}".format(accepted) 
        self.log_min.write("%s\n" % (vals))

    """
    BasinHopping steps
    def take_step(self,):
        y_o = self.get_loss()
        for step in range(steps):
           xval_n = self.alter_xval(xval_o)
           y_n = self.get_loss(xval_n)
           if y_n < self.ymin:
              self.ymin = y_n
              self.xval_min = xval_n
           if y_n < y_o:
              accept = True
           else:
              accept = np.exp((y_n - y_o)/self.T) > np.random.uniform()
           if accept:
              self.acceptnumb += 1.
              self.recentaccept += 1.
              xval_o = xval_n
              y_o = y_n
           else:
              self.rejectnum += 1.0

           if self.min_y != None:
               if self.ymin < self.min_y:
                   break

    def adjust_xval(self,):
        disp = np.zeros(np.shape(atoms.get_positions()))
        while np.alltrue(disp == np.zeros(np.shape(atoms.get_positions()))):
           if self.distribution == 'uniform':
               disp = np.random.uniform(-self.dr, self.dr, (len(atoms), 3))
           elif self.distribution == 'gaussian':
               disp = np.random.normal(0,self.dr,size=(len(atoms), 3))

    def adjust_step(self, step):
        self.ratio = float(self.acceptnumb) / float(step+2)
        ratio = None
        if self.adjust_step_size is not None:
           if step % self.adjust_step_size == 0:
              if self.adjust_method == 'global':
                 ratio = self.ratio
              if self.adjust_mothod == 'local':
                 ratio = float(self.recentaccept)/float(self.adjust_step_size)
                 self.recentaccept = 0.
              if ratio is not None:
                 if self.dr < self.dr_min:
                    self.dr =self.dr_reset
                    return

                 if ratio > self.target_ratio:
                    self.dr = self.dr * (1+self.adjust_fraction)
                 elif ratio < self.target_ratio:
                    self.dr = self.dr * (1-self.adjust_fraction)
    """
    def sd(self, steps):
        x0 = self.initial_paras.flatten()
        f0, d0 = self.get_loss(x0)
        print x0, '  ',d0,' ', f0, ' ', f0
        climbing = False
        for step in range(steps):
           stepsize = d0 * self.stepsize
           #stepsize = [0.001, 0.01]
           #for i in range(len(stepsize)):
           #   if np.absolute(stepsize[i]) > 0.2:
           #      stepsize[i] = np.sign(stepsize[i]) * 0.1
              #if np.absolute(stepsize[i]) < 0.001:
              #   stepsize[i] = np.sign(stepsize[i]) * 0.005
           if climbing:
              stepsize /= 3.0
           #print stepsize
           xn = x0 - stepsize
           d_tao =  np.sum(stepsize * d0)
           if xn[1] < 0.0:
              xn[1] = 0.0
           fn, dn = self.get_loss(xn)
              
           if fn < f0:
              f0 = fn
              x0 = xn
              d0 = dn
              climbing = False
           else:
              climbing = True
           print xn, '  ',dn,' ', fn, ' ', d_tao, ' ',f0
        return xn, f0

    def run(self,steps):
        #Initialize parameters
        #Rs   = [0., 0.5, 1.0,  1.5,  0.0, 0., 0.5]

        x0 = self.initial_paras
       
        if self.optimizer == 'BFGS':
           from scipy.optimize import minimize as optimizer
           optimizer_kwargs = {
                               'method' : 'BFGS',
                               'options': {'gtol': 1e-15, },
                               'disp': True
                              }
           #optimizer_kwargs = {'method':'BFGS', 'gtol': 1e-15, }
        elif self.optimizer == 'basinhopping':
           from scipy.optimize import basinhopping as optimizer
           minimizer_kwargs = {"method": "L-BFGS-B",
                               'options': {
                                           'maxiter': self.maxsteps,
                                           #'maxfun':1
                                           }
                              }
           #mybounds = MyBounds()
           results = optimizer(self.get_loss, x0, 
                               #minimizer_kwargs=minimizer_kwargs,
                               niter=steps,
                               T = self.T,
                               stepsize = self.stepsize,
                               #niter_success=1000,
                               disp=True,
                               callback=self.print_fun,
            #                   accept_test=mybounds
                               )
           opted_paras = toMatrix(results['x'], fp_type=self.fp_type)
           fps = self.calculate_fps(opted_paras, ref=False)
           fps_pdf = []
           for fp in fps:
             temp = fp * self.pdf
             fps_pdf.append(temp/np.sum(temp))

           self.log_fps(fps, opted_paras, filename='optedFPs.dat')
           self.log_fps(fps_pdf, opted_paras, filename='fps_pdf.dat')

           printResults(opted_paras)
           sys.exit()
        elif self.optimizer == 'L-BFGS-B':
           from scipy.optimize import minimize as optimizer
           optimizer_kwargs = {
                               'method': 'L-BFGS-B',
                               'options': {'ftol': 1e-10,
                                           'gtol': 1e-18,
                                           'maxfun': 1000000,
                                           'maxiter':1500000,
                                           'disp': True}
                              }
           import scipy
           from distutils.version import StrictVersion
           if StrictVersion(scipy.__version__) >= StrictVersion('0.17.0'):
               optimizer_kwargs['options']['maxls'] = 2000
        elif self.optimizer == 'CG':
           from scipy.optimize import minimize as optimizer
           optimizer_kwargs = {
                               'method': 'CG',
                               'options': {
                                           'gtol': 1e-18,
                                           'eps': 0.1,
                                           'maxiter':1500000,
                                           'disp': True}
                              }
        elif self.optimizer == 'SD':
            x, f = self.sd(steps)
            print "{:12.2f} {:6.2f} {:8.6f}".format(x[0]*1000, x[1], f)
            sys.exit()
          # import scipy
          # from distutils.version import StrictVersion
          # if StrictVersion(scipy.__version__) >= StrictVersion('0.17.0'):
          #     optimizer_kwargs['options']['maxls'] = 2000
        result=optimizer(self.get_loss, x0, jac=self.jac, 
                         callback=self.printX,hess='2-point',
                         **optimizer_kwargs)
        print result.x
      
      

def print_expectation(g2s, thetas):
    output = ""
    for k in g2s:
       output += "{:12.8f}".format(np.dot(g2s[k], thetas))
    print output

def main():
    #Read input parameters if 'config.ini' exists
    if os.path.exists('config.ini'):
       paras=readinputs('config.ini')
       for para in default_paras:
          try:
            paras[para] = type_convertion(default_paras[para], paras[para])
          except:
            paras[para] = default_paras[para]
       fp_template = np.array([ float(value) for value in paras['fp_template']])
       if len(paras['fp_template'])%2 == 0:
          numb_fps = int(len(paras['fp_template']) /2)
          fp_template=fp_template.reshape(numb_fps,2)
       if paras['to_normalize_theta_ss'] is None:
          to_normalize_theta_ss = paras['to_normalize_theta_ss']
       else:
          to_normalize_theta_ss = [int(field) for field in paras['to_normalize_theta_ss']]
       #print 'before', to_normalize_theta_ss, paras['to_normalize_theta_ss']
       print  paras['randomize_initial']
       if paras['initial_mode']=='random':
          paras_range = [float(field) for field in paras['paras_range']]
          initial_paras = np.stack((np.random.uniform(low=paras_range[0], high=paras_range[1], size=numb_fps),
                                    np.random.uniform(low=paras_range[3], high=paras_range[4], size=numb_fps)),
                                    axis=-1).flatten()
       elif paras['initial_mode']=='template':
          initial_paras = None
       elif paras['initial_mode']=='user_defined':
          initial_paras = np.array([ float(value) for value in paras['initial_paras']])
          if len(paras['initial_paras'])%2 == 0:
             numb_fps = int(len(paras['initial_paras']) /2)
             initial_paras = initial_paras.reshape(numb_fps,2)
    else:
       print("Please provide a config.ini file")

    opt = fp_optimizer(fp_template  = fp_template, 
                       fp_type      = paras['fp_type'],
                       initial_paras= initial_paras,
                       optimizer    = paras['optimizer'],
                       jac          = paras['jac'],
                       T            = paras['T'], 
                       stepsize     = paras['stepsize'], 
                       maxsteps     = paras['maxsteps'],
                       Rc           = paras['Rc'], 
                       pdf_filename = paras['pdf_filename'],
                       Rmin         = paras['Rmin'], 
                       Rmax         = paras['Rmax'],
                       offset       = paras['offset'],
                       to_normalize_theta_ss = to_normalize_theta_ss,
                       ref_normalized_fps = paras['ref_normalized_fps']
                       )

    #test derivative:
    if paras['run_type']=='singlepoint':
       opt.run(1)
       sys.exit()

    if paras['run_type']=='checksmooth':
       paras_range = [float(field) for field in paras['paras_range']]
       opt.check_smooth(dx=paras['dx'], fp_numb=0, para_index=paras['para_index'],
                        fp_paras=initial_paras,
                        fp_range=paras_range)
    #fp = opt.calculate_fps(fp_paras, ref=False)[0]
    #xval = opt.xval
    #g_2 = open('g2.dat','w')
    #for i in range(len(fp)):
    #   g_2.write("{:12.8f} {:12.8f}\n".format(xval[i], fp[i]))
    #g_2.close()
    if paras['run_type']=='minimize':
       opt.run(paras['maxoptsteps'])
if __name__ == '__main__':
    main()
