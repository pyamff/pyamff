#!/usr/bin/env python3
"""
File Converter for LAMMPS

This scripts converts VASP's POSCARs, EON's con files, and ASE's trajectory 
files into data files suitable for LAMMPS.

Author: Eboni Williams with help from Zahra Bajalan
Version: June 2, 2022
"""

import sys
import ase
import time
import math
import shutil
import random
import logging
import argparse
import numpy as np
from ase import Atoms
from ase.constraints import FixAtoms
from ase.build.tools import sort
from ase.io.vasp import read_vasp
from ase.io.trajectory import Trajectory
from ase.geometry import cellpar_to_cell

# get command line arguments
parser = argparse.ArgumentParser(description="Convert a poscar or trajectory \
                                            file to LAMMPS data file format",
                                prog="to_lammps")
parser.add_argument('input',type=str,help='POSCAR/traj/con file to convert \
                                            (DEFAULT: POSCAR)',
                                    default='POSCAR',nargs='?')
parser.add_argument('output',type=str,help='Name of file to output to \
                                            (DEFAULT: data_file)',
                                    default='data_file',nargs='?')
file_types = parser.add_mutually_exclusive_group()
file_types.add_argument("-t","--traj",help="whether input is a trajectory file \
                                            (DEFAULT: False)",
                                    action="store_true") 
file_types.add_argument("-p","--pos",help="whether input is a poscar file \
                                            (DEFAULT: True)",
                                    action="store_true") 
file_types.add_argument("-c","--con",help="whether input is a .con file \
                                            (DEFAULT: False)",
                                    action="store_true") 
parser.add_argument('-i','--img',help='Image number of trajectory file to use,\
                                        numbering starts at 1 (i.e. 1=first,\
                                        2=second),0 indicates pick at random,\
                                        -1 indicates the last image \
                                        (DEFAULT: -1 )',
                                type=int,
                                default=-1)
modes = parser.add_mutually_exclusive_group()
modes.add_argument("--debug",help="whether to show debugging information or not\
                                    (DEFAULT: False)",
                            action="store_true") 
modes.add_argument("--silent",help="Only prints errors (DEFAULT: False)", 
                            action="store_true")
parser.add_argument("--log",help="whether to generate log file: \
                                    [output file]_[time].log \
                                    (DEFAULT: False)",
                            action="store_true")
# new argument to show traceback of error messages
parser.add_argument("--trace",help="whether to show the stack trace for error \
                                    message (DEFAULT: False)",
                            action="store_true")
settings = parser.parse_args()

# The user may have passed a path and not a single file name
out_split = settings.output.rfind('/')
outf = settings.output[out_split+1:]
# Note if out_split = -1, then outf returns full string
if out_split != -1:
    backup = f"{settings.output[:out_split]}/_backup_{outf}"
else:
    backup = f"_backup_{outf}"
# inpath_split = settings.input.rfind('/')

# Let the default file type be POSCAR
if( not settings.traj and not settings.con): settings.pos = True 

# Set up the logging level
if settings.debug:
    lvl = logging.DEBUG
elif settings.silent:
    lvl = logging.ERROR
else:
    lvl = logging.INFO

logger = logging.getLogger(__name__)
logger.setLevel(lvl) # default level
console_handler = logging.StreamHandler()
console_handler.setLevel(lvl)
console_handler.setFormatter(logging.Formatter('%(message)s'))
logger.addHandler(console_handler)
 # only set up file logger if user specifies
if settings.log:
    time_str = time.strftime("%m-%d-%Y_%H-%M-%S",time.localtime())
    file_handler = logging.FileHandler(f"{settings.output}_{time_str}.log")
    file_handler.setLevel(logging.DEBUG) # this was originally lvl
    file_handler.setFormatter(logging.Formatter(
                            fmt='%(asctime)s.%(msecs)03d-%(levelname)s-%(message)s',
                            datefmt='%H:%M:%S'))
    logger.addHandler(file_handler)
    logger.debug(f"Log file will be {settings.output}_{time_str}.log")


def is_not_empty(s):
    """
    Check if string is empty

    Parameter s: the string to check
    """
    return s.replace('\n','').replace(' ','') != ''


def eval_con(filein):
    """
    Gets the relevant information from EON's con file and returns it

    Parameter filein: The file to evaluate
    Precondition: filein exists
    """
    # Open the file
    try:
        f = open(filein,'r')
        lines = f.readlines()
        f.close()
    except UnicodeDecodeError: # trying to pass trajectory as CON
        raise IOError("STOP!! Invalid CON file.") from None
    # Issue error if empty
    if len(lines) == 0:
        raise IOError("CON file is empty!")
    # Remove trailing new lines
    while lines[-1] == '\n':
        lines = lines[:-1]
    orig = [line.replace('\n','') for line in lines if is_not_empty(line)]
    # Issue error about blank lines
    if len(orig) < len(lines):
        raise IOError("CON file cannot have any blank lines")
    
    # Make sure they didn't pass a poscar and get atom information
    try:
        num_types = int(lines[6])
    except ValueError:
        raise IOError("STOP!! Invalid CON file") from None
    atoms_per_type = [int(num) for num in lines[7].split()]
    # Get lattice
    box = [float(num) for num in lines[2].split()]
    angles = [float(num) for num in lines[3].split()]
    cellp = [val for val in box + angles]
    # Get the coords and selective dynamics
    coords = []
    fixed = []
    skip = 0
    descript = ""
    for i in range(num_types):
        skip += 1
        symbol = lines[8+skip].split()[0]
        atms = atoms_per_type[i]
        adds = str(atms) if atms > 1 else ""
        descript += symbol + adds
        # Next line should be "Coordinates of Component n"
        skip += 1
        for j in range(atoms_per_type[i]):
                skip += 1
                line = lines[8+skip].split()
                coords.append([float(x) for x in line[:3]])
                fixed.append(bool(int(line[3])))
    # Create an atoms object
    atoms = Atoms(descript,
                positions = coords,
                cell = cellpar_to_cell(cellp),
                constraint=FixAtoms(mask=fixed))
    return descript,box[0],atoms_per_type,atoms


def eval_poscar(filein):
    """
    Gets the relevant information from VASP POSCAR file and returns it

    Parameter filein: The file to evaluate
    Precondition: filein exists
    """
    # Open the file
    try:
        f = open(filein,'r')
        lines = f.readlines()
        f.close()
    except UnicodeDecodeError: # trying to pass trajectory as POSCAR
        raise IOError("STOP!! Invalid POSCAR.") from None
    if len(lines) == 0:
        raise IOError("POSCAR file is empty!")
    # Remove trailing new lines
    while lines[-1] == '\n':
        lines = lines[:-1]
    orig = [line.replace('\n','') for line in lines if is_not_empty(line)]
    # Issue error about blank lines
    if len(orig) < len(lines):
        raise IOError("POSCAR cannot have any empty lines")
    # Start pulling relevant data from the POSCAR
    desc = orig[0]
    try:
        lattice = float(orig[1])
    except ValueError: # trying to pass a CON file as POSCAR
        raise IOError("STOP!! Invalid POSCAR.") from None
    a = read_vasp(filein)
    data = sort(a)
    num_atom = eval_trajectory(data)[-1]
    return desc,lattice,num_atom,data


def eval_trajectory(image):
    """
    Gets the relevant information from ASE's trajectory file and returns it

    Parameter filein: The file to evaluate
    Precondition: filein exists
    """
    types = list(set(image.get_chemical_symbols()))
    num_atom = [0]*len(types)
    for atom in image:
        num_atom[types.index(atom.symbol)] += 1
    return image.get_chemical_formula(),1.0,num_atom

def print_structure(structure):
    """
    Prints the information about the structure returned in the dictionary
    from the read_file function

    Parameter structure: dictionary returned from read_file
    """
    # Header
    logger.debug('Printing structure . . .')
    logger.debug('#'*35)
    logger.debug(f"Structure: {structure['description']}")
    logger.debug("-"*35)
    logger.debug(f"{structure['atoms']['total']} atoms total")
    # Print atom information
    for i in range(structure['atoms']['num_types']):
        logger.debug("\t{} {} with mass {}".format(
            structure['atoms']['num_per_type'][i],
            structure['atoms']['types'][i],
            structure['atoms']['mass_per_type'][i]))
    # Print lattice information
    logger.debug("Lattice vectors:")
    for i in range(3):
        vec = ' '.join('{:.3f}'.format(val) for val 
                                in structure['lattice']['vectors'][i])
        logger.debug(f"\t[{vec}]")
    # Print Selective dynamics information
    logger.debug(f"Selective Dynamics: \
        {'ON' if structure['selective']['on'] else 'OFF'}")
    logger.debug(f"Coordinates in Lattice") #remove this later
    basis_adj = list()
    for atm in structure['coordinates']['cartesian']:
        line = ""
        for val in atm:
            line += "{:.2f}".format(val/15.0) + " "
        logger.debug(line)
    logger.debug("#"*35)


def read_file(filein,ftype="POS"):
    """
    Reads VASP's POSCARs, EON's con files, and ASE's trajectory files
    and returns a dictionary containing all the information in a dictionary

    Parameter filein: the file to read
    
    Parameter fype: The type of file
    Precondition: must be one of the following: ["CON","POS","TRAJ"]
    """
    # Call the relevant function for handling each file type
    if ftype == "CON":
        logger.debug("Pulling data from .con file")
        desc,lattice,num_atoms,data = eval_con(filein)
    elif ftype == "POS":
        logger.debug("Pulling data from POSCAR")
        desc,lattice,num_atoms,data = eval_poscar(filein)
    elif ftype == "TRAJ":
        logger.debug("Pulling data from .traj file")
        try:
            traj = Trajectory(filein)
        except ase.io.ulm.InvalidULMFileError:#either passing POSCAR or empty
            raise IOError("Invalid trajectory file") from None
        if(settings.img < -1 or settings.img > len(traj)):
            raise IndexError(f"The image number given ({settings.img}) is "+
                                "not accessible for the give trajectory file")
        if settings.img == 0:
            ind = random.randint(0,len(traj))
        elif settings.img != -1:
            ind = settings.img - 1
        else:
            ind = -1
        logger.debug("Using image #{}".format(len(traj) if ind==-1 else ind))
        udata = traj[ind]
        data = sort(udata)
        desc,lattice,num_atoms = eval_trajectory(data)
    else:
        raise TypeError("Invalid:",ftype)
    # Pull all the other needed data
    type_atoms = list(set(data.get_chemical_symbols()))
    mass_atoms = list(set(data.get_masses()))
    total_atoms = sum(num_atoms)
    basis = data.get_cell()[:]
    [a,b,c,alpha,beta,gamma] = data.get_cell_lengths_and_angles()
    # if lengths are not real or all equal to 0
    if (np.nan in basis or np.inf in basis or 
        not a or not b or not c or
        not alpha or not beta or not gamma):
        raise RuntimeError("Lattice vectors must be finite and not coincident." +
                             " At least one lattice length or angle is zero.")
    # Get selective dynamics if exist
    selectiveFlag = True if data.constraints else False
    selective = [" "]*total_atoms
    logger.debug(f"selective: {selectiveFlag}")
    for con in data.constraints:
        params = con.todict()
        if params['name'] == 'FixScaled':
            kwargs = params['kwargs']
            mask = ["T" if not b else "F" for b in kwargs['mask']]
            selective[kwargs['a']] = " ".join(mask)
        elif params['name'] == 'FixAtoms':
            for index in params['kwargs']['indices']:
                selective[index] = "F F F"
    # Get coordinates in direct and cartesian
    c_coord = data.get_positions()
    d_coord = data.get_scaled_positions()
    # Return the structure dictionary
    structure = {
        "description" : desc,
        "atoms": {
            "total" : total_atoms,
            "num_types": len(num_atoms), 
            "types" : type_atoms,
            "num_per_type" : num_atoms, 
            "mass_per_type": mass_atoms
            },
        # Note: these lattice vectors have been scaled by the lattice constant
        "lattice" : {"vectors" : basis, "constant" : lattice}, 
        "selective" : { "on": selectiveFlag,"per_atom": selective},
        "coordinates" : { "direct": d_coord, "cartesian": c_coord}
    }
    print_structure(structure)
    return structure,data


def get_box(struct):
    """
    Return the parameters needed to define the simulation box in lammps

    Parameters struct: The ase Atoms object to pull from
    """
    # Get lattice constants
    [a,b,c,alpha,beta,gamma] = struct.get_cell_lengths_and_angles()
    # Get traditional new basis
    new_basis = [[0]*3 for i in range(3)]
    p = math.cos(math.radians(gamma))
    q = math.cos(math.radians(beta))
    r = math.sin(math.radians(gamma))
    s = math.cos(math.radians(alpha))
    new_basis = [ 
                    [a,b*p,c*q], 
                    [0,b*r,c*(s-p*q)/r], 
                    [0,0,c*math.sqrt(1-q**2-((s-p*q)/r)**2)]
                ]
     # Set near-zero elements to zero
    for i in range(3):
        for j in range(3):
            if(abs(new_basis[i][j]) < 1e-10): new_basis[i][j] = 0

    # Get lammps new basis
    [lx,xy,xz] = new_basis[0]
    [ly,yz] = new_basis[1][1:]
    lz = new_basis[2][2]
    logger.debug("LAMMPS BOX VALUES:")
    logger.debug("\tlx ={:.3f}\txy={:3f}".format(lx,xy))
    logger.debug("\tly ={:.3f}\txz={:3f}".format(ly,xz))
    logger.debug("\tlz ={:.3f}\tyz={:3f}".format(lz,yz))
    return {"lx":lx,"ly":ly,"lz":lz, "xy":xy,"yz":yz,"xz":xz}


def get_type(num_per_type,index):
    """
    Get id of the atom's type based off its position (i.e. index) in coordiantes

    Parameter num_per_type: array with the number of elements per type
    Precondition: nonempty

    Parameter index: Atom's index in coordinates
    """
    count = 0
    if len(num_per_type)==0:
        raise RuntimeError("The type array is empty. Can't determine type.")
    elif len(num_per_type) == 1:
        return 1
    for i in range(len(num_per_type)):
        prev_count = count
        count += num_per_type[i]
        if index in list(range(prev_count,count)):
            return i
    return None
    

def write_data(structure):
    """
    Create the lammps datafile based off the structure dictionary

    Parameter structure: dictionary returned from read_file
    """
    f = open(settings.output,'w')
    print(structure['description']+"\n",file=f)
    print(structure['atoms']['total'],"atoms",file=f)
    print(structure['atoms']['num_types'],"atom types\n",file=f)
    print("0.00 {:.2f} xlo xhi".format(structure['box']['lx']),file=f)
    print("0.00 {:.2f} ylo yhi".format(structure['box']['ly']),file=f)
    print("0.00 {:.2f} zlo zhi".format(structure['box']['lz'],"zlo zhi"),file=f)
    if((structure['box']['xy'] != 0) and 
        (structure['box']['xz'] != 0) and (structure['box']['yz'] != 0)):
        print("{:.3f} {:.3f} {:.3f} xy xz yz\n".format(structure['box']['xy'],
                                                        structure['box']['xz'],
                                                        structure['box']['yz']),
                                                        file=f)
    print("\nMasses\n",file=f)
    for i in range(structure['atoms']['num_types']):
        print("{} {:.3f}".format(i+1,structure['atoms']['mass_per_type'][i]),
                                file=f)
    print("\nAtoms\n",file=f)
    
    if not structure['selective']['on']:
        k = 0
        for i in range(structure['atoms']['num_types']):
            for j in range(structure['atoms']['num_per_type'][i]):
                coord = structure['coordinates']['cartesian'][k]
                print("{} {} {:.2f} {:.2f} {:.2f}".format(k+1,i+1,coord[0],
                                                            coord[1],coord[2]),
                                                            file=f)
                k+=1
    else:
        fixed = {
            "none": { "ids": list(), "fv": "NULL NULL NULL"},
            "x":{ "ids": list(), "fv": "0.0 NULL NULL"},
            "y":{ "ids": list(), "fv": "NULL 0.0 NULL"},
            "z":{ "ids": list(), "fv": "NULL NULL 0.0"},
            "xy":{ "ids": list(), "fv": "0.0 0.0 NULL"},
            "xz":{ "ids": list(), "fv": "0.0 NULL 0.0"},
            "yz":{ "ids": list(), "fv": "NULL 0.0 0.0"},
            "all":{ "ids": list(), "fv": "0.0 0.0 0.0"},
        }
        for i in range(structure['atoms']['total']):
            dyn = structure['selective']['per_atom'][i]
            if dyn == 'T T T':
                fixed['none']["ids"].append(i)
            elif dyn == 'F T T':
                fixed['x']["ids"].append(i)
            elif dyn == 'T F T':
                fixed['y']["ids"].append(i)
            elif dyn == 'T T F':
                fixed['z']["ids"].append(i)
            elif dyn == 'F F T':
                fixed['xy']["ids"].append(i)
            elif dyn == 'F T F':
                fixed['xz']["ids"].append(i)
            elif dyn == 'T F F':
                fixed['yz']["ids"].append(i)
            elif dyn == 'F F F':
                fixed['all']["ids"].append(i)
        j = 0
        # Let the user know how to keep selective dynamics
        gscript = ""
        for cat in fixed:
            isNone = cat == "none"
            if  fixed[cat]["ids"]:
                if not isNone: gscript += "\ngroup {}_FIXED ".format(cat.upper())
                k = j
                for index in fixed[cat]["ids"]:
                    coord = structure['coordinates']['cartesian'][index]
                    typea = get_type(structure['atoms']['num_per_type'],index)+1
                    print("{} {} {:.2f} {:.2f} {:.2f}".format(j+1,typea,coord[0],
                                                            coord[1],coord[2]),
                                                            file=f)
                    j  = j + 1
                
                if (abs(j-k) == 1 or k==0) and not isNone:
                    gscript += "id{} {} \n".format("" if abs(j-k)==1 else " <=",j)
                    gscript += "fix {} {}_FIXED setforce {}\n".format(k,
                                                    cat.upper(),fixed[cat]["fv"])
                    gscript += "velocity {}_FIXED set {}\n".format(cat.upper(),
                                                    fixed[cat]["fv"])
                elif not isNone:
                    gscript += "id <> {} {} \n".format(k,j)
                    gscript += "fix {} {}_FIXED setforce {}\n".format(k,
                                                    cat.upper(),fixed[cat]["fv"])
                    gscript += "velocity {}_FIXED set {}\n".format(cat.upper(),
                                                    fixed[cat]["fv"])
        if gscript: # if None should be empty
            logger.info("\nIn order to utilize selective dynamics," +
                                " please use the following commands ")
            logger.info("(or the equivalent) in your input script "+
                                "immediately after 'read_data' or 'velocity' "+
                                "(whichever is later)")
            logger.info("*"*50)
            logger.info(gscript)
            logger.info("*"*50)
    f.close()


if __name__ == "__main__":
    # Because I want zero traceback; 
    # if you don't, just comment this out or pass --trace flag when running
    if not settings.trace: sys.tracebacklimit = 0
    # Check if input and output files already exist
    try:
        f = open(settings.input)
        f.close()
    except FileNotFoundError:
        raise
    try:
        f = open(settings.output)
        f.close()
    except FileNotFoundError:
        pass
    else:
        logger.warning("The desired output file, '{}'".format(settings.output) +
                ", already exists. The original file will be copied to " +
                "{} so it can be overwritten.".format(backup))
        shutil.copy2(settings.output,backup)
    # Process the file
    if (settings.traj):
        structure, data = read_file(settings.input,"TRAJ")
    elif(settings.con):
        structure,data = read_file(settings.input,"CON")
    elif(settings.pos):
        structure, data = read_file(settings.input)
    else:
        raise TypeError("You should only be passing a .con, .traj, or POSCAR file.")
    # Get the box
    box = get_box(data)
    structure['box'] = box
    # Create the LAMMPS data file
    write_data(structure)
    # IT SHOULD BE NOTED THAT SOME POSCARS INCLUDE VELOCITIES!!!
    # WE DO NOT ADD THESE AS THIS IS A RARE CASE
    # probably unneccessary but reset to default when down
    if not settings.trace: sys.tracebacklimit = 1000
