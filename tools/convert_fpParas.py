####Script to convert fpParas to amp fingerprints
import re
import sys


fp_file_name = sys.argv[1]

elements = []
G1s = []
fp_par = []
with open(fp_file_name, mode ='r') as f:
    for i, lines in enumerate(f):
        if i == 3:
          el = lines.replace(" ", "")
          el = lines.replace("\n", "")
          elements = re.findall('[A-Z][^A-Z]*',el)
        if i >9:
          temp = lines.replace("  ", " ")
          temp = lines.replace("\n", "")
          fp_par.append(temp)
#print ("Final Elements: ",elements)
G1s =[]
G2s =[]
for i in range(len(fp_par)):
    temp = fp_par[i].split()
    if len(temp)==6:
       G1s.append(temp)
    if len(temp) ==9:
       G2s.append(temp)
G2s.pop(0) ##remove the header line of G2 from fpParas.dat       

#print ("G2s: ",G2s)
#G2s:  [['G2', 'Ge', 'Ge', 'Ge', '0.005', '400', '1.00', '0.2', '5.7'], ...]
#G1s:  [['G1', 'Ge', 'Ge', '1200.00', '1.80', '5.7'],...]


amp_fgs={}
for i in G1s:  #for each array in G1s
    if i[1] in amp_fgs:  ###in central element is key of dictionary
       amp_fgs[i[1]].append({"type":"G2", "element":i[2], "eta":float(i[3]),"offset": float(i[4])})
    else:
       amp_fgs[i[1]]=[]
       amp_fgs[i[1]].append({"type":"G2", "element":i[2], "eta":float(i[3]),"offset": float(i[4])})

for i in G2s:  #for each array in G1s
    if i[1] in amp_fgs:  ###in central element is key of dictionary
       amp_fgs[i[1]].append({"type":"G4", "elements":[i[2],i[3]], "eta":float(i[4]),"gamma": float(i[6]),"zeta": float(i[5])})
    else:
       amp_fgs[i[1]]=[]
       amp_fgs[i[1]].append({"type":"G4", "elements":[i[2],i[3]], "eta":float(i[4]),"gamma": float(i[6]),"zeta": float(i[5])})

print ("final amp_fgs: ",amp_fgs)


