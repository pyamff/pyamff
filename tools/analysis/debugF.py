import numpy as np
from pyamff.aseCalcF import aseCalcF #pure fortran calculator
import torch, sys
from ase.io import read

args = sys.argv
images = read(args[1], index=":")

calc = aseCalcF()

es = open('es.dat','w')
fs = open('fs.dat','w')
rmses = open('rmse.dat','w')
e_j = 0
f_j = 0
f_rmse = 0
e_rmse = 0
for atoms in images:
  dft_e = atoms.get_potential_energy()
  dft_fs = atoms.get_forces()
  atoms.set_calculator(calc)
  ml_e = atoms.get_potential_energy()
  ml_fs = atoms.get_forces()
  e_rmse = e_rmse + ((dft_e - ml_e)/len(atoms))**2
  e_j = e_j + 1
  es.write("{:12.6f} {:12.6f} \n".format(dft_e, ml_e))
  for dft_f, ml_f in zip(dft_fs, ml_fs):
     f_rmse_internal = 0
     for i in range(3):
       f_rmse_internal = f_rmse_internal + ((dft_f[i]-ml_f[i])**2)
       fs.write("{:12.6f} {:12.6f} \n".format(dft_f[i], ml_f[i]))
     f_rmse_internal = f_rmse_internal/(len(atoms))
     f_rmse = f_rmse + f_rmse_internal
rmses.write('{:>30s} {:>8d}\n'.format('number of images', len(images)))
rmses.write('{:>30s} {:12.6f}\n'.format('energy rmse per atom', np.sqrt(e_rmse)))
rmses.write('{:>30s} {:12.6f}\n'.format('energy rmse per atom and image', np.sqrt(e_rmse/(len(images)))))
rmses.write('{:>30s} {:12.6f}\n'.format('force rmse per atom and image', np.sqrt(f_rmse/(3*len(images)))))
                                                                         
